import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IEmployeesAssets, NewEmployeesAssets } from '../employees-assets.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEmployeesAssets for edit and NewEmployeesAssetsFormGroupInput for create.
 */
type EmployeesAssetsFormGroupInput = IEmployeesAssets | PartialWithRequiredKeyOf<NewEmployeesAssets>;

type EmployeesAssetsFormDefaults = Pick<NewEmployeesAssets, 'id'>;

type EmployeesAssetsFormGroupContent = {
  id: FormControl<IEmployeesAssets['id'] | NewEmployeesAssets['id']>;
  name: FormControl<IEmployeesAssets['name']>;
  assignedBy: FormControl<IEmployeesAssets['assignedBy']>;
  recieversProtocol: FormControl<IEmployeesAssets['recieversProtocol']>;
  employee: FormControl<IEmployeesAssets['employee']>;
};

export type EmployeesAssetsFormGroup = FormGroup<EmployeesAssetsFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EmployeesAssetsFormService {
  createEmployeesAssetsFormGroup(employeesAssets: EmployeesAssetsFormGroupInput = { id: null }): EmployeesAssetsFormGroup {
    const employeesAssetsRawValue = {
      ...this.getFormDefaults(),
      ...employeesAssets,
    };
    return new FormGroup<EmployeesAssetsFormGroupContent>({
      id: new FormControl(
        { value: employeesAssetsRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(employeesAssetsRawValue.name, {
        validators: [Validators.required],
      }),
      assignedBy: new FormControl(employeesAssetsRawValue.assignedBy),
      recieversProtocol: new FormControl(employeesAssetsRawValue.recieversProtocol),
      employee: new FormControl(employeesAssetsRawValue.employee),
    });
  }

  getEmployeesAssets(form: EmployeesAssetsFormGroup): IEmployeesAssets | NewEmployeesAssets {
    return form.getRawValue() as IEmployeesAssets | NewEmployeesAssets;
  }

  resetForm(form: EmployeesAssetsFormGroup, employeesAssets: EmployeesAssetsFormGroupInput): void {
    const employeesAssetsRawValue = { ...this.getFormDefaults(), ...employeesAssets };
    form.reset(
      {
        ...employeesAssetsRawValue,
        id: { value: employeesAssetsRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): EmployeesAssetsFormDefaults {
    return {
      id: null,
    };
  }
}
