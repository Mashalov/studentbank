import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IEmployee } from 'app/entities/employee/employee.model';
import { EmployeeService } from 'app/entities/employee/service/employee.service';
import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { IEmployeesAssets } from '../employees-assets.model';
import { EmployeesAssetsService } from '../service/employees-assets.service';
import { EmployeesAssetsFormService } from './employees-assets-form.service';

import { EmployeesAssetsUpdateComponent } from './employees-assets-update.component';

describe('EmployeesAssets Management Update Component', () => {
  let comp: EmployeesAssetsUpdateComponent;
  let fixture: ComponentFixture<EmployeesAssetsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let employeesAssetsFormService: EmployeesAssetsFormService;
  let employeesAssetsService: EmployeesAssetsService;
  let employeeService: EmployeeService;
  let documentService: DocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [EmployeesAssetsUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EmployeesAssetsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmployeesAssetsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    employeesAssetsFormService = TestBed.inject(EmployeesAssetsFormService);
    employeesAssetsService = TestBed.inject(EmployeesAssetsService);
    employeeService = TestBed.inject(EmployeeService);
    documentService = TestBed.inject(DocumentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Employee query and add missing value', () => {
      const employeesAssets: IEmployeesAssets = { id: 456 };
      const assignedBy: IEmployee = { id: 9232 };
      employeesAssets.assignedBy = assignedBy;
      const employee: IEmployee = { id: 3667 };
      employeesAssets.employee = employee;

      const employeeCollection: IEmployee[] = [{ id: 27444 }];
      jest.spyOn(employeeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeeCollection })));
      const additionalEmployees = [assignedBy, employee];
      const expectedCollection: IEmployee[] = [...additionalEmployees, ...employeeCollection];
      jest.spyOn(employeeService, 'addEmployeeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employeesAssets });
      comp.ngOnInit();

      expect(employeeService.query).toHaveBeenCalled();
      expect(employeeService.addEmployeeToCollectionIfMissing).toHaveBeenCalledWith(
        employeeCollection,
        ...additionalEmployees.map(expect.objectContaining),
      );
      expect(comp.employeesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Document query and add missing value', () => {
      const employeesAssets: IEmployeesAssets = { id: 456 };
      const recieversProtocol: IDocument = { id: 8565 };
      employeesAssets.recieversProtocol = recieversProtocol;

      const documentCollection: IDocument[] = [{ id: 27436 }];
      jest.spyOn(documentService, 'query').mockReturnValue(of(new HttpResponse({ body: documentCollection })));
      const additionalDocuments = [recieversProtocol];
      const expectedCollection: IDocument[] = [...additionalDocuments, ...documentCollection];
      jest.spyOn(documentService, 'addDocumentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employeesAssets });
      comp.ngOnInit();

      expect(documentService.query).toHaveBeenCalled();
      expect(documentService.addDocumentToCollectionIfMissing).toHaveBeenCalledWith(
        documentCollection,
        ...additionalDocuments.map(expect.objectContaining),
      );
      expect(comp.documentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const employeesAssets: IEmployeesAssets = { id: 456 };
      const assignedBy: IEmployee = { id: 12560 };
      employeesAssets.assignedBy = assignedBy;
      const employee: IEmployee = { id: 8313 };
      employeesAssets.employee = employee;
      const recieversProtocol: IDocument = { id: 20497 };
      employeesAssets.recieversProtocol = recieversProtocol;

      activatedRoute.data = of({ employeesAssets });
      comp.ngOnInit();

      expect(comp.employeesSharedCollection).toContain(assignedBy);
      expect(comp.employeesSharedCollection).toContain(employee);
      expect(comp.documentsSharedCollection).toContain(recieversProtocol);
      expect(comp.employeesAssets).toEqual(employeesAssets);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployeesAssets>>();
      const employeesAssets = { id: 123 };
      jest.spyOn(employeesAssetsFormService, 'getEmployeesAssets').mockReturnValue(employeesAssets);
      jest.spyOn(employeesAssetsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employeesAssets });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employeesAssets }));
      saveSubject.complete();

      // THEN
      expect(employeesAssetsFormService.getEmployeesAssets).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(employeesAssetsService.update).toHaveBeenCalledWith(expect.objectContaining(employeesAssets));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployeesAssets>>();
      const employeesAssets = { id: 123 };
      jest.spyOn(employeesAssetsFormService, 'getEmployeesAssets').mockReturnValue({ id: null });
      jest.spyOn(employeesAssetsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employeesAssets: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employeesAssets }));
      saveSubject.complete();

      // THEN
      expect(employeesAssetsFormService.getEmployeesAssets).toHaveBeenCalled();
      expect(employeesAssetsService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployeesAssets>>();
      const employeesAssets = { id: 123 };
      jest.spyOn(employeesAssetsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employeesAssets });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(employeesAssetsService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEmployee', () => {
      it('Should forward to employeeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeeService, 'compareEmployee');
        comp.compareEmployee(entity, entity2);
        expect(employeeService.compareEmployee).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareDocument', () => {
      it('Should forward to documentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(documentService, 'compareDocument');
        comp.compareDocument(entity, entity2);
        expect(documentService.compareDocument).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
