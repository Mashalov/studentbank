import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IEmployee } from 'app/entities/employee/employee.model';
import { EmployeeService } from 'app/entities/employee/service/employee.service';
import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { EmployeesAssetsService } from '../service/employees-assets.service';
import { IEmployeesAssets } from '../employees-assets.model';
import { EmployeesAssetsFormService, EmployeesAssetsFormGroup } from './employees-assets-form.service';

@Component({
  standalone: true,
  selector: 'jhi-employees-assets-update',
  templateUrl: './employees-assets-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class EmployeesAssetsUpdateComponent implements OnInit {
  isSaving = false;
  employeesAssets: IEmployeesAssets | null = null;

  employeesSharedCollection: IEmployee[] = [];
  documentsSharedCollection: IDocument[] = [];

  protected employeesAssetsService = inject(EmployeesAssetsService);
  protected employeesAssetsFormService = inject(EmployeesAssetsFormService);
  protected employeeService = inject(EmployeeService);
  protected documentService = inject(DocumentService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: EmployeesAssetsFormGroup = this.employeesAssetsFormService.createEmployeesAssetsFormGroup();

  compareEmployee = (o1: IEmployee | null, o2: IEmployee | null): boolean => this.employeeService.compareEmployee(o1, o2);

  compareDocument = (o1: IDocument | null, o2: IDocument | null): boolean => this.documentService.compareDocument(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employeesAssets }) => {
      this.employeesAssets = employeesAssets;
      if (employeesAssets) {
        this.updateForm(employeesAssets);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const employeesAssets = this.employeesAssetsFormService.getEmployeesAssets(this.editForm);
    if (employeesAssets.id !== null) {
      this.subscribeToSaveResponse(this.employeesAssetsService.update(employeesAssets));
    } else {
      this.subscribeToSaveResponse(this.employeesAssetsService.create(employeesAssets));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployeesAssets>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(employeesAssets: IEmployeesAssets): void {
    this.employeesAssets = employeesAssets;
    this.employeesAssetsFormService.resetForm(this.editForm, employeesAssets);

    this.employeesSharedCollection = this.employeeService.addEmployeeToCollectionIfMissing<IEmployee>(
      this.employeesSharedCollection,
      employeesAssets.assignedBy,
      employeesAssets.employee,
    );
    this.documentsSharedCollection = this.documentService.addDocumentToCollectionIfMissing<IDocument>(
      this.documentsSharedCollection,
      employeesAssets.recieversProtocol,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.employeeService
      .query()
      .pipe(map((res: HttpResponse<IEmployee[]>) => res.body ?? []))
      .pipe(
        map((employees: IEmployee[]) =>
          this.employeeService.addEmployeeToCollectionIfMissing<IEmployee>(
            employees,
            this.employeesAssets?.assignedBy,
            this.employeesAssets?.employee,
          ),
        ),
      )
      .subscribe((employees: IEmployee[]) => (this.employeesSharedCollection = employees));

    this.documentService
      .query()
      .pipe(map((res: HttpResponse<IDocument[]>) => res.body ?? []))
      .pipe(
        map((documents: IDocument[]) =>
          this.documentService.addDocumentToCollectionIfMissing<IDocument>(documents, this.employeesAssets?.recieversProtocol),
        ),
      )
      .subscribe((documents: IDocument[]) => (this.documentsSharedCollection = documents));
  }
}
