import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../employees-assets.test-samples';

import { EmployeesAssetsFormService } from './employees-assets-form.service';

describe('EmployeesAssets Form Service', () => {
  let service: EmployeesAssetsFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeesAssetsFormService);
  });

  describe('Service methods', () => {
    describe('createEmployeesAssetsFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createEmployeesAssetsFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assignedBy: expect.any(Object),
            recieversProtocol: expect.any(Object),
            employee: expect.any(Object),
          }),
        );
      });

      it('passing IEmployeesAssets should create a new form with FormGroup', () => {
        const formGroup = service.createEmployeesAssetsFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            assignedBy: expect.any(Object),
            recieversProtocol: expect.any(Object),
            employee: expect.any(Object),
          }),
        );
      });
    });

    describe('getEmployeesAssets', () => {
      it('should return NewEmployeesAssets for default EmployeesAssets initial value', () => {
        const formGroup = service.createEmployeesAssetsFormGroup(sampleWithNewData);

        const employeesAssets = service.getEmployeesAssets(formGroup) as any;

        expect(employeesAssets).toMatchObject(sampleWithNewData);
      });

      it('should return NewEmployeesAssets for empty EmployeesAssets initial value', () => {
        const formGroup = service.createEmployeesAssetsFormGroup();

        const employeesAssets = service.getEmployeesAssets(formGroup) as any;

        expect(employeesAssets).toMatchObject({});
      });

      it('should return IEmployeesAssets', () => {
        const formGroup = service.createEmployeesAssetsFormGroup(sampleWithRequiredData);

        const employeesAssets = service.getEmployeesAssets(formGroup) as any;

        expect(employeesAssets).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IEmployeesAssets should not enable id FormControl', () => {
        const formGroup = service.createEmployeesAssetsFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewEmployeesAssets should disable id FormControl', () => {
        const formGroup = service.createEmployeesAssetsFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
