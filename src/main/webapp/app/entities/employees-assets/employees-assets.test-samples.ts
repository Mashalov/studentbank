import { IEmployeesAssets, NewEmployeesAssets } from './employees-assets.model';

export const sampleWithRequiredData: IEmployeesAssets = {
  id: 2368,
  name: 'filth certainly jovially',
};

export const sampleWithPartialData: IEmployeesAssets = {
  id: 21215,
  name: 'loyal enthusiastically',
};

export const sampleWithFullData: IEmployeesAssets = {
  id: 14351,
  name: 'closely',
};

export const sampleWithNewData: NewEmployeesAssets = {
  name: 'indent alfalfa gah',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
