import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { EmployeesAssetsComponent } from './list/employees-assets.component';
import { EmployeesAssetsDetailComponent } from './detail/employees-assets-detail.component';
import { EmployeesAssetsUpdateComponent } from './update/employees-assets-update.component';
import EmployeesAssetsResolve from './route/employees-assets-routing-resolve.service';

const employeesAssetsRoute: Routes = [
  {
    path: '',
    component: EmployeesAssetsComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmployeesAssetsDetailComponent,
    resolve: {
      employeesAssets: EmployeesAssetsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmployeesAssetsUpdateComponent,
    resolve: {
      employeesAssets: EmployeesAssetsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmployeesAssetsUpdateComponent,
    resolve: {
      employeesAssets: EmployeesAssetsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default employeesAssetsRoute;
