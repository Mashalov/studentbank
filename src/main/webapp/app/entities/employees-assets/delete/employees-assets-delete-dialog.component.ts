import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IEmployeesAssets } from '../employees-assets.model';
import { EmployeesAssetsService } from '../service/employees-assets.service';

@Component({
  standalone: true,
  templateUrl: './employees-assets-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class EmployeesAssetsDeleteDialogComponent {
  employeesAssets?: IEmployeesAssets;

  protected employeesAssetsService = inject(EmployeesAssetsService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.employeesAssetsService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
