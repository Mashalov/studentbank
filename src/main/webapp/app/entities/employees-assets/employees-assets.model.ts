import { IEmployee } from 'app/entities/employee/employee.model';
import { IDocument } from 'app/entities/document/document.model';

export interface IEmployeesAssets {
  id: number;
  name?: string | null;
  assignedBy?: Pick<IEmployee, 'id'> | null;
  recieversProtocol?: Pick<IDocument, 'id'> | null;
  employee?: Pick<IEmployee, 'id'> | null;
}

export type NewEmployeesAssets = Omit<IEmployeesAssets, 'id'> & { id: null };
