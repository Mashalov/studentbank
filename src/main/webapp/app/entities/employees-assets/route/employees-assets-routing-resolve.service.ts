import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEmployeesAssets } from '../employees-assets.model';
import { EmployeesAssetsService } from '../service/employees-assets.service';

const employeesAssetsResolve = (route: ActivatedRouteSnapshot): Observable<null | IEmployeesAssets> => {
  const id = route.params['id'];
  if (id) {
    return inject(EmployeesAssetsService)
      .find(id)
      .pipe(
        mergeMap((employeesAssets: HttpResponse<IEmployeesAssets>) => {
          if (employeesAssets.body) {
            return of(employeesAssets.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default employeesAssetsResolve;
