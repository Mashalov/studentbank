import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IEmployeesAssets } from '../employees-assets.model';

@Component({
  standalone: true,
  selector: 'jhi-employees-assets-detail',
  templateUrl: './employees-assets-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class EmployeesAssetsDetailComponent {
  employeesAssets = input<IEmployeesAssets | null>(null);

  previousState(): void {
    window.history.back();
  }
}
