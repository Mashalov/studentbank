import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness } from '@angular/router/testing';
import { of } from 'rxjs';

import { EmployeesAssetsDetailComponent } from './employees-assets-detail.component';

describe('EmployeesAssets Management Detail Component', () => {
  let comp: EmployeesAssetsDetailComponent;
  let fixture: ComponentFixture<EmployeesAssetsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployeesAssetsDetailComponent],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: EmployeesAssetsDetailComponent,
              resolve: { employeesAssets: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(EmployeesAssetsDetailComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesAssetsDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load employeesAssets on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', EmployeesAssetsDetailComponent);

      // THEN
      expect(instance.employeesAssets()).toEqual(expect.objectContaining({ id: 123 }));
    });
  });

  describe('PreviousState', () => {
    it('Should navigate to previous state', () => {
      jest.spyOn(window.history, 'back');
      comp.previousState();
      expect(window.history.back).toHaveBeenCalled();
    });
  });
});
