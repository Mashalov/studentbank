import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEmployeesAssets, NewEmployeesAssets } from '../employees-assets.model';

export type PartialUpdateEmployeesAssets = Partial<IEmployeesAssets> & Pick<IEmployeesAssets, 'id'>;

export type EntityResponseType = HttpResponse<IEmployeesAssets>;
export type EntityArrayResponseType = HttpResponse<IEmployeesAssets[]>;

@Injectable({ providedIn: 'root' })
export class EmployeesAssetsService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/employees-assets');

  create(employeesAssets: NewEmployeesAssets): Observable<EntityResponseType> {
    return this.http.post<IEmployeesAssets>(this.resourceUrl, employeesAssets, { observe: 'response' });
  }

  update(employeesAssets: IEmployeesAssets): Observable<EntityResponseType> {
    return this.http.put<IEmployeesAssets>(`${this.resourceUrl}/${this.getEmployeesAssetsIdentifier(employeesAssets)}`, employeesAssets, {
      observe: 'response',
    });
  }

  partialUpdate(employeesAssets: PartialUpdateEmployeesAssets): Observable<EntityResponseType> {
    return this.http.patch<IEmployeesAssets>(`${this.resourceUrl}/${this.getEmployeesAssetsIdentifier(employeesAssets)}`, employeesAssets, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEmployeesAssets>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployeesAssets[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getEmployeesAssetsIdentifier(employeesAssets: Pick<IEmployeesAssets, 'id'>): number {
    return employeesAssets.id;
  }

  compareEmployeesAssets(o1: Pick<IEmployeesAssets, 'id'> | null, o2: Pick<IEmployeesAssets, 'id'> | null): boolean {
    return o1 && o2 ? this.getEmployeesAssetsIdentifier(o1) === this.getEmployeesAssetsIdentifier(o2) : o1 === o2;
  }

  addEmployeesAssetsToCollectionIfMissing<Type extends Pick<IEmployeesAssets, 'id'>>(
    employeesAssetsCollection: Type[],
    ...employeesAssetsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const employeesAssets: Type[] = employeesAssetsToCheck.filter(isPresent);
    if (employeesAssets.length > 0) {
      const employeesAssetsCollectionIdentifiers = employeesAssetsCollection.map(employeesAssetsItem =>
        this.getEmployeesAssetsIdentifier(employeesAssetsItem),
      );
      const employeesAssetsToAdd = employeesAssets.filter(employeesAssetsItem => {
        const employeesAssetsIdentifier = this.getEmployeesAssetsIdentifier(employeesAssetsItem);
        if (employeesAssetsCollectionIdentifiers.includes(employeesAssetsIdentifier)) {
          return false;
        }
        employeesAssetsCollectionIdentifiers.push(employeesAssetsIdentifier);
        return true;
      });
      return [...employeesAssetsToAdd, ...employeesAssetsCollection];
    }
    return employeesAssetsCollection;
  }
}
