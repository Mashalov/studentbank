import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IEmployeesAssets } from '../employees-assets.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../employees-assets.test-samples';

import { EmployeesAssetsService } from './employees-assets.service';

const requireRestSample: IEmployeesAssets = {
  ...sampleWithRequiredData,
};

describe('EmployeesAssets Service', () => {
  let service: EmployeesAssetsService;
  let httpMock: HttpTestingController;
  let expectedResult: IEmployeesAssets | IEmployeesAssets[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(EmployeesAssetsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a EmployeesAssets', () => {
      const employeesAssets = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(employeesAssets).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a EmployeesAssets', () => {
      const employeesAssets = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(employeesAssets).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a EmployeesAssets', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of EmployeesAssets', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a EmployeesAssets', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addEmployeesAssetsToCollectionIfMissing', () => {
      it('should add a EmployeesAssets to an empty array', () => {
        const employeesAssets: IEmployeesAssets = sampleWithRequiredData;
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing([], employeesAssets);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(employeesAssets);
      });

      it('should not add a EmployeesAssets to an array that contains it', () => {
        const employeesAssets: IEmployeesAssets = sampleWithRequiredData;
        const employeesAssetsCollection: IEmployeesAssets[] = [
          {
            ...employeesAssets,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing(employeesAssetsCollection, employeesAssets);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a EmployeesAssets to an array that doesn't contain it", () => {
        const employeesAssets: IEmployeesAssets = sampleWithRequiredData;
        const employeesAssetsCollection: IEmployeesAssets[] = [sampleWithPartialData];
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing(employeesAssetsCollection, employeesAssets);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(employeesAssets);
      });

      it('should add only unique EmployeesAssets to an array', () => {
        const employeesAssetsArray: IEmployeesAssets[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const employeesAssetsCollection: IEmployeesAssets[] = [sampleWithRequiredData];
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing(employeesAssetsCollection, ...employeesAssetsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const employeesAssets: IEmployeesAssets = sampleWithRequiredData;
        const employeesAssets2: IEmployeesAssets = sampleWithPartialData;
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing([], employeesAssets, employeesAssets2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(employeesAssets);
        expect(expectedResult).toContain(employeesAssets2);
      });

      it('should accept null and undefined values', () => {
        const employeesAssets: IEmployeesAssets = sampleWithRequiredData;
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing([], null, employeesAssets, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(employeesAssets);
      });

      it('should return initial array if no EmployeesAssets is added', () => {
        const employeesAssetsCollection: IEmployeesAssets[] = [sampleWithRequiredData];
        expectedResult = service.addEmployeesAssetsToCollectionIfMissing(employeesAssetsCollection, undefined, null);
        expect(expectedResult).toEqual(employeesAssetsCollection);
      });
    });

    describe('compareEmployeesAssets', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareEmployeesAssets(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareEmployeesAssets(entity1, entity2);
        const compareResult2 = service.compareEmployeesAssets(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareEmployeesAssets(entity1, entity2);
        const compareResult2 = service.compareEmployeesAssets(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareEmployeesAssets(entity1, entity2);
        const compareResult2 = service.compareEmployeesAssets(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
