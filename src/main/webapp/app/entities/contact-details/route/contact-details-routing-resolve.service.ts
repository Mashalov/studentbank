import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IContactDetails } from '../contact-details.model';
import { ContactDetailsService } from '../service/contact-details.service';

const contactDetailsResolve = (route: ActivatedRouteSnapshot): Observable<null | IContactDetails> => {
  const id = route.params['id'];
  if (id) {
    return inject(ContactDetailsService)
      .find(id)
      .pipe(
        mergeMap((contactDetails: HttpResponse<IContactDetails>) => {
          if (contactDetails.body) {
            return of(contactDetails.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default contactDetailsResolve;
