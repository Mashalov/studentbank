import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IContactDetails } from '../contact-details.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../contact-details.test-samples';

import { ContactDetailsService } from './contact-details.service';

const requireRestSample: IContactDetails = {
  ...sampleWithRequiredData,
};

describe('ContactDetails Service', () => {
  let service: ContactDetailsService;
  let httpMock: HttpTestingController;
  let expectedResult: IContactDetails | IContactDetails[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(ContactDetailsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a ContactDetails', () => {
      const contactDetails = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(contactDetails).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ContactDetails', () => {
      const contactDetails = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(contactDetails).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ContactDetails', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ContactDetails', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a ContactDetails', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addContactDetailsToCollectionIfMissing', () => {
      it('should add a ContactDetails to an empty array', () => {
        const contactDetails: IContactDetails = sampleWithRequiredData;
        expectedResult = service.addContactDetailsToCollectionIfMissing([], contactDetails);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(contactDetails);
      });

      it('should not add a ContactDetails to an array that contains it', () => {
        const contactDetails: IContactDetails = sampleWithRequiredData;
        const contactDetailsCollection: IContactDetails[] = [
          {
            ...contactDetails,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addContactDetailsToCollectionIfMissing(contactDetailsCollection, contactDetails);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ContactDetails to an array that doesn't contain it", () => {
        const contactDetails: IContactDetails = sampleWithRequiredData;
        const contactDetailsCollection: IContactDetails[] = [sampleWithPartialData];
        expectedResult = service.addContactDetailsToCollectionIfMissing(contactDetailsCollection, contactDetails);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(contactDetails);
      });

      it('should add only unique ContactDetails to an array', () => {
        const contactDetailsArray: IContactDetails[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const contactDetailsCollection: IContactDetails[] = [sampleWithRequiredData];
        expectedResult = service.addContactDetailsToCollectionIfMissing(contactDetailsCollection, ...contactDetailsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const contactDetails: IContactDetails = sampleWithRequiredData;
        const contactDetails2: IContactDetails = sampleWithPartialData;
        expectedResult = service.addContactDetailsToCollectionIfMissing([], contactDetails, contactDetails2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(contactDetails);
        expect(expectedResult).toContain(contactDetails2);
      });

      it('should accept null and undefined values', () => {
        const contactDetails: IContactDetails = sampleWithRequiredData;
        expectedResult = service.addContactDetailsToCollectionIfMissing([], null, contactDetails, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(contactDetails);
      });

      it('should return initial array if no ContactDetails is added', () => {
        const contactDetailsCollection: IContactDetails[] = [sampleWithRequiredData];
        expectedResult = service.addContactDetailsToCollectionIfMissing(contactDetailsCollection, undefined, null);
        expect(expectedResult).toEqual(contactDetailsCollection);
      });
    });

    describe('compareContactDetails', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareContactDetails(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareContactDetails(entity1, entity2);
        const compareResult2 = service.compareContactDetails(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareContactDetails(entity1, entity2);
        const compareResult2 = service.compareContactDetails(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareContactDetails(entity1, entity2);
        const compareResult2 = service.compareContactDetails(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
