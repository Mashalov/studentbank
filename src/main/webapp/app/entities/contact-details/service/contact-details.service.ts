import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IContactDetails, NewContactDetails } from '../contact-details.model';

export type PartialUpdateContactDetails = Partial<IContactDetails> & Pick<IContactDetails, 'id'>;

export type EntityResponseType = HttpResponse<IContactDetails>;
export type EntityArrayResponseType = HttpResponse<IContactDetails[]>;

@Injectable({ providedIn: 'root' })
export class ContactDetailsService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/contact-details');

  create(contactDetails: NewContactDetails): Observable<EntityResponseType> {
    return this.http.post<IContactDetails>(this.resourceUrl, contactDetails, { observe: 'response' });
  }

  update(contactDetails: IContactDetails): Observable<EntityResponseType> {
    return this.http.put<IContactDetails>(`${this.resourceUrl}/${this.getContactDetailsIdentifier(contactDetails)}`, contactDetails, {
      observe: 'response',
    });
  }

  partialUpdate(contactDetails: PartialUpdateContactDetails): Observable<EntityResponseType> {
    return this.http.patch<IContactDetails>(`${this.resourceUrl}/${this.getContactDetailsIdentifier(contactDetails)}`, contactDetails, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IContactDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IContactDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getContactDetailsIdentifier(contactDetails: Pick<IContactDetails, 'id'>): number {
    return contactDetails.id;
  }

  compareContactDetails(o1: Pick<IContactDetails, 'id'> | null, o2: Pick<IContactDetails, 'id'> | null): boolean {
    return o1 && o2 ? this.getContactDetailsIdentifier(o1) === this.getContactDetailsIdentifier(o2) : o1 === o2;
  }

  addContactDetailsToCollectionIfMissing<Type extends Pick<IContactDetails, 'id'>>(
    contactDetailsCollection: Type[],
    ...contactDetailsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const contactDetails: Type[] = contactDetailsToCheck.filter(isPresent);
    if (contactDetails.length > 0) {
      const contactDetailsCollectionIdentifiers = contactDetailsCollection.map(contactDetailsItem =>
        this.getContactDetailsIdentifier(contactDetailsItem),
      );
      const contactDetailsToAdd = contactDetails.filter(contactDetailsItem => {
        const contactDetailsIdentifier = this.getContactDetailsIdentifier(contactDetailsItem);
        if (contactDetailsCollectionIdentifiers.includes(contactDetailsIdentifier)) {
          return false;
        }
        contactDetailsCollectionIdentifiers.push(contactDetailsIdentifier);
        return true;
      });
      return [...contactDetailsToAdd, ...contactDetailsCollection];
    }
    return contactDetailsCollection;
  }
}
