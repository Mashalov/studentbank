import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IContactDetails } from '../contact-details.model';
import { ContactDetailsService } from '../service/contact-details.service';

@Component({
  standalone: true,
  templateUrl: './contact-details-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class ContactDetailsDeleteDialogComponent {
  contactDetails?: IContactDetails;

  protected contactDetailsService = inject(ContactDetailsService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.contactDetailsService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
