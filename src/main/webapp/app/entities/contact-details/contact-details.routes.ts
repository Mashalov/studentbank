import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { ContactDetailsComponent } from './list/contact-details.component';
import { ContactDetailsDetailComponent } from './detail/contact-details-detail.component';
import { ContactDetailsUpdateComponent } from './update/contact-details-update.component';
import ContactDetailsResolve from './route/contact-details-routing-resolve.service';

const contactDetailsRoute: Routes = [
  {
    path: '',
    component: ContactDetailsComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ContactDetailsDetailComponent,
    resolve: {
      contactDetails: ContactDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ContactDetailsUpdateComponent,
    resolve: {
      contactDetails: ContactDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ContactDetailsUpdateComponent,
    resolve: {
      contactDetails: ContactDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default contactDetailsRoute;
