import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IOffice } from 'app/entities/office/office.model';
import { OfficeService } from 'app/entities/office/service/office.service';
import { IContactDetails } from '../contact-details.model';
import { ContactDetailsService } from '../service/contact-details.service';
import { ContactDetailsFormService, ContactDetailsFormGroup } from './contact-details-form.service';

@Component({
  standalone: true,
  selector: 'jhi-contact-details-update',
  templateUrl: './contact-details-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class ContactDetailsUpdateComponent implements OnInit {
  isSaving = false;
  contactDetails: IContactDetails | null = null;

  officesSharedCollection: IOffice[] = [];

  protected contactDetailsService = inject(ContactDetailsService);
  protected contactDetailsFormService = inject(ContactDetailsFormService);
  protected officeService = inject(OfficeService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: ContactDetailsFormGroup = this.contactDetailsFormService.createContactDetailsFormGroup();

  compareOffice = (o1: IOffice | null, o2: IOffice | null): boolean => this.officeService.compareOffice(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contactDetails }) => {
      this.contactDetails = contactDetails;
      if (contactDetails) {
        this.updateForm(contactDetails);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const contactDetails = this.contactDetailsFormService.getContactDetails(this.editForm);
    if (contactDetails.id !== null) {
      this.subscribeToSaveResponse(this.contactDetailsService.update(contactDetails));
    } else {
      this.subscribeToSaveResponse(this.contactDetailsService.create(contactDetails));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContactDetails>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(contactDetails: IContactDetails): void {
    this.contactDetails = contactDetails;
    this.contactDetailsFormService.resetForm(this.editForm, contactDetails);

    this.officesSharedCollection = this.officeService.addOfficeToCollectionIfMissing<IOffice>(
      this.officesSharedCollection,
      contactDetails.office,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.officeService
      .query()
      .pipe(map((res: HttpResponse<IOffice[]>) => res.body ?? []))
      .pipe(map((offices: IOffice[]) => this.officeService.addOfficeToCollectionIfMissing<IOffice>(offices, this.contactDetails?.office)))
      .subscribe((offices: IOffice[]) => (this.officesSharedCollection = offices));
  }
}
