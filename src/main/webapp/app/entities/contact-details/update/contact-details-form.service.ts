import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IContactDetails, NewContactDetails } from '../contact-details.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IContactDetails for edit and NewContactDetailsFormGroupInput for create.
 */
type ContactDetailsFormGroupInput = IContactDetails | PartialWithRequiredKeyOf<NewContactDetails>;

type ContactDetailsFormDefaults = Pick<NewContactDetails, 'id'>;

type ContactDetailsFormGroupContent = {
  id: FormControl<IContactDetails['id'] | NewContactDetails['id']>;
  countryCode: FormControl<IContactDetails['countryCode']>;
  phoneNumber: FormControl<IContactDetails['phoneNumber']>;
  email: FormControl<IContactDetails['email']>;
  firstName: FormControl<IContactDetails['firstName']>;
  lastName: FormControl<IContactDetails['lastName']>;
  middleName: FormControl<IContactDetails['middleName']>;
  name: FormControl<IContactDetails['name']>;
  office: FormControl<IContactDetails['office']>;
};

export type ContactDetailsFormGroup = FormGroup<ContactDetailsFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ContactDetailsFormService {
  createContactDetailsFormGroup(contactDetails: ContactDetailsFormGroupInput = { id: null }): ContactDetailsFormGroup {
    const contactDetailsRawValue = {
      ...this.getFormDefaults(),
      ...contactDetails,
    };
    return new FormGroup<ContactDetailsFormGroupContent>({
      id: new FormControl(
        { value: contactDetailsRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      countryCode: new FormControl(contactDetailsRawValue.countryCode, {
        validators: [Validators.required],
      }),
      phoneNumber: new FormControl(contactDetailsRawValue.phoneNumber, {
        validators: [Validators.required],
      }),
      email: new FormControl(contactDetailsRawValue.email, {
        validators: [Validators.required],
      }),
      firstName: new FormControl(contactDetailsRawValue.firstName, {
        validators: [Validators.required],
      }),
      lastName: new FormControl(contactDetailsRawValue.lastName, {
        validators: [Validators.required],
      }),
      middleName: new FormControl(contactDetailsRawValue.middleName),
      name: new FormControl(contactDetailsRawValue.name),
      office: new FormControl(contactDetailsRawValue.office),
    });
  }

  getContactDetails(form: ContactDetailsFormGroup): IContactDetails | NewContactDetails {
    return form.getRawValue() as IContactDetails | NewContactDetails;
  }

  resetForm(form: ContactDetailsFormGroup, contactDetails: ContactDetailsFormGroupInput): void {
    const contactDetailsRawValue = { ...this.getFormDefaults(), ...contactDetails };
    form.reset(
      {
        ...contactDetailsRawValue,
        id: { value: contactDetailsRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): ContactDetailsFormDefaults {
    return {
      id: null,
    };
  }
}
