import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../contact-details.test-samples';

import { ContactDetailsFormService } from './contact-details-form.service';

describe('ContactDetails Form Service', () => {
  let service: ContactDetailsFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactDetailsFormService);
  });

  describe('Service methods', () => {
    describe('createContactDetailsFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createContactDetailsFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            countryCode: expect.any(Object),
            phoneNumber: expect.any(Object),
            email: expect.any(Object),
            firstName: expect.any(Object),
            lastName: expect.any(Object),
            middleName: expect.any(Object),
            name: expect.any(Object),
            office: expect.any(Object),
          }),
        );
      });

      it('passing IContactDetails should create a new form with FormGroup', () => {
        const formGroup = service.createContactDetailsFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            countryCode: expect.any(Object),
            phoneNumber: expect.any(Object),
            email: expect.any(Object),
            firstName: expect.any(Object),
            lastName: expect.any(Object),
            middleName: expect.any(Object),
            name: expect.any(Object),
            office: expect.any(Object),
          }),
        );
      });
    });

    describe('getContactDetails', () => {
      it('should return NewContactDetails for default ContactDetails initial value', () => {
        const formGroup = service.createContactDetailsFormGroup(sampleWithNewData);

        const contactDetails = service.getContactDetails(formGroup) as any;

        expect(contactDetails).toMatchObject(sampleWithNewData);
      });

      it('should return NewContactDetails for empty ContactDetails initial value', () => {
        const formGroup = service.createContactDetailsFormGroup();

        const contactDetails = service.getContactDetails(formGroup) as any;

        expect(contactDetails).toMatchObject({});
      });

      it('should return IContactDetails', () => {
        const formGroup = service.createContactDetailsFormGroup(sampleWithRequiredData);

        const contactDetails = service.getContactDetails(formGroup) as any;

        expect(contactDetails).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IContactDetails should not enable id FormControl', () => {
        const formGroup = service.createContactDetailsFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewContactDetails should disable id FormControl', () => {
        const formGroup = service.createContactDetailsFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
