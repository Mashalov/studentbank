import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IOffice } from 'app/entities/office/office.model';
import { OfficeService } from 'app/entities/office/service/office.service';
import { ContactDetailsService } from '../service/contact-details.service';
import { IContactDetails } from '../contact-details.model';
import { ContactDetailsFormService } from './contact-details-form.service';

import { ContactDetailsUpdateComponent } from './contact-details-update.component';

describe('ContactDetails Management Update Component', () => {
  let comp: ContactDetailsUpdateComponent;
  let fixture: ComponentFixture<ContactDetailsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let contactDetailsFormService: ContactDetailsFormService;
  let contactDetailsService: ContactDetailsService;
  let officeService: OfficeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ContactDetailsUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ContactDetailsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ContactDetailsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    contactDetailsFormService = TestBed.inject(ContactDetailsFormService);
    contactDetailsService = TestBed.inject(ContactDetailsService);
    officeService = TestBed.inject(OfficeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Office query and add missing value', () => {
      const contactDetails: IContactDetails = { id: 456 };
      const office: IOffice = { id: 26518 };
      contactDetails.office = office;

      const officeCollection: IOffice[] = [{ id: 23255 }];
      jest.spyOn(officeService, 'query').mockReturnValue(of(new HttpResponse({ body: officeCollection })));
      const additionalOffices = [office];
      const expectedCollection: IOffice[] = [...additionalOffices, ...officeCollection];
      jest.spyOn(officeService, 'addOfficeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ contactDetails });
      comp.ngOnInit();

      expect(officeService.query).toHaveBeenCalled();
      expect(officeService.addOfficeToCollectionIfMissing).toHaveBeenCalledWith(
        officeCollection,
        ...additionalOffices.map(expect.objectContaining),
      );
      expect(comp.officesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const contactDetails: IContactDetails = { id: 456 };
      const office: IOffice = { id: 307 };
      contactDetails.office = office;

      activatedRoute.data = of({ contactDetails });
      comp.ngOnInit();

      expect(comp.officesSharedCollection).toContain(office);
      expect(comp.contactDetails).toEqual(contactDetails);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IContactDetails>>();
      const contactDetails = { id: 123 };
      jest.spyOn(contactDetailsFormService, 'getContactDetails').mockReturnValue(contactDetails);
      jest.spyOn(contactDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contactDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: contactDetails }));
      saveSubject.complete();

      // THEN
      expect(contactDetailsFormService.getContactDetails).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(contactDetailsService.update).toHaveBeenCalledWith(expect.objectContaining(contactDetails));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IContactDetails>>();
      const contactDetails = { id: 123 };
      jest.spyOn(contactDetailsFormService, 'getContactDetails').mockReturnValue({ id: null });
      jest.spyOn(contactDetailsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contactDetails: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: contactDetails }));
      saveSubject.complete();

      // THEN
      expect(contactDetailsFormService.getContactDetails).toHaveBeenCalled();
      expect(contactDetailsService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IContactDetails>>();
      const contactDetails = { id: 123 };
      jest.spyOn(contactDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ contactDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(contactDetailsService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareOffice', () => {
      it('Should forward to officeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(officeService, 'compareOffice');
        comp.compareOffice(entity, entity2);
        expect(officeService.compareOffice).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
