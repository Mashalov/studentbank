import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IContactDetails } from '../contact-details.model';

@Component({
  standalone: true,
  selector: 'jhi-contact-details-detail',
  templateUrl: './contact-details-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class ContactDetailsDetailComponent {
  contactDetails = input<IContactDetails | null>(null);

  previousState(): void {
    window.history.back();
  }
}
