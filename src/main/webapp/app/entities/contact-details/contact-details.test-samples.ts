import { IContactDetails, NewContactDetails } from './contact-details.model';

export const sampleWithRequiredData: IContactDetails = {
  id: 2781,
  countryCode: 1927,
  phoneNumber: 16925,
  email: 'Fae_Balistreri90@yahoo.com',
  firstName: 'Macie',
  lastName: 'Dietrich',
};

export const sampleWithPartialData: IContactDetails = {
  id: 17513,
  countryCode: 18218,
  phoneNumber: 14479,
  email: 'Elissa39@hotmail.com',
  firstName: 'Aylin',
  lastName: 'Hickle',
};

export const sampleWithFullData: IContactDetails = {
  id: 19913,
  countryCode: 6986,
  phoneNumber: 27815,
  email: 'Rusty_Kuphal@hotmail.com',
  firstName: 'Connie',
  lastName: 'Sawayn',
  middleName: 'who unless fairly',
  name: 'immediately',
};

export const sampleWithNewData: NewContactDetails = {
  countryCode: 32208,
  phoneNumber: 13623,
  email: 'Maureen99@hotmail.com',
  firstName: 'Natalia',
  lastName: 'Cole',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
