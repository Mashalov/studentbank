import { IOffice } from 'app/entities/office/office.model';

export interface IContactDetails {
  id: number;
  countryCode?: number | null;
  phoneNumber?: number | null;
  email?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  middleName?: string | null;
  name?: string | null;
  office?: Pick<IOffice, 'id'> | null;
}

export type NewContactDetails = Omit<IContactDetails, 'id'> & { id: null };
