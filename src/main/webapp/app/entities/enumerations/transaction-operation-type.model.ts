export enum TransactionOperationType {
  TRANSFER = 'TRANSFER',

  CREDIT = 'CREDIT',

  DEPOSIT = 'DEPOSIT',
}
