export enum CustomerType {
  LEGAL_PERSON = 'LEGAL_PERSON',

  NATURAL_PERSON = 'NATURAL_PERSON',

  NOT_SET = 'NOT_SET',
}
