export enum DocumentType {
  PDF = 'PDF',

  DOCX = 'DOCX',
}
