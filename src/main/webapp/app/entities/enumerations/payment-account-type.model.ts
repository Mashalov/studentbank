export enum PaymentAccountType {
  SAVING_ACCOUNT = 'SAVING_ACCOUNT',

  CREDIT_ACCOUNT = 'CREDIT_ACCOUNT',

  OVERDRAFT = 'OVERDRAFT',

  DEPOSIT = 'DEPOSIT',
}
