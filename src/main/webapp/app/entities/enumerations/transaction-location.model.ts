export enum TransactionLocation {
  ATM = 'ATM',

  OFFICE = 'OFFICE',

  ONLINE = 'ONLINE',
}
