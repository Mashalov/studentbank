import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { JobPositionComponent } from './list/job-position.component';
import { JobPositionDetailComponent } from './detail/job-position-detail.component';
import { JobPositionUpdateComponent } from './update/job-position-update.component';
import JobPositionResolve from './route/job-position-routing-resolve.service';

const jobPositionRoute: Routes = [
  {
    path: '',
    component: JobPositionComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JobPositionDetailComponent,
    resolve: {
      jobPosition: JobPositionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JobPositionUpdateComponent,
    resolve: {
      jobPosition: JobPositionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JobPositionUpdateComponent,
    resolve: {
      jobPosition: JobPositionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default jobPositionRoute;
