import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IJobPosition } from '../job-position.model';

@Component({
  standalone: true,
  selector: 'jhi-job-position-detail',
  templateUrl: './job-position-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class JobPositionDetailComponent {
  jobPosition = input<IJobPosition | null>(null);

  previousState(): void {
    window.history.back();
  }
}
