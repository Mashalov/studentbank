import { IJobPosition, NewJobPosition } from './job-position.model';

export const sampleWithRequiredData: IJobPosition = {
  id: 9524,
  name: 'live out instance',
  company: 'store residence',
};

export const sampleWithPartialData: IJobPosition = {
  id: 1934,
  name: 'alongside rapidly',
  company: 'er southeast',
};

export const sampleWithFullData: IJobPosition = {
  id: 2474,
  name: 'anxiously to',
  company: 'gah',
};

export const sampleWithNewData: NewJobPosition = {
  name: 'peek sally yuck',
  company: 'whimsical an',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
