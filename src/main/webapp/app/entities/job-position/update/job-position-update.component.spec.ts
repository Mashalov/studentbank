import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { JobPositionService } from '../service/job-position.service';
import { IJobPosition } from '../job-position.model';
import { JobPositionFormService } from './job-position-form.service';

import { JobPositionUpdateComponent } from './job-position-update.component';

describe('JobPosition Management Update Component', () => {
  let comp: JobPositionUpdateComponent;
  let fixture: ComponentFixture<JobPositionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let jobPositionFormService: JobPositionFormService;
  let jobPositionService: JobPositionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [JobPositionUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(JobPositionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(JobPositionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    jobPositionFormService = TestBed.inject(JobPositionFormService);
    jobPositionService = TestBed.inject(JobPositionService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const jobPosition: IJobPosition = { id: 456 };

      activatedRoute.data = of({ jobPosition });
      comp.ngOnInit();

      expect(comp.jobPosition).toEqual(jobPosition);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IJobPosition>>();
      const jobPosition = { id: 123 };
      jest.spyOn(jobPositionFormService, 'getJobPosition').mockReturnValue(jobPosition);
      jest.spyOn(jobPositionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ jobPosition });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: jobPosition }));
      saveSubject.complete();

      // THEN
      expect(jobPositionFormService.getJobPosition).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(jobPositionService.update).toHaveBeenCalledWith(expect.objectContaining(jobPosition));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IJobPosition>>();
      const jobPosition = { id: 123 };
      jest.spyOn(jobPositionFormService, 'getJobPosition').mockReturnValue({ id: null });
      jest.spyOn(jobPositionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ jobPosition: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: jobPosition }));
      saveSubject.complete();

      // THEN
      expect(jobPositionFormService.getJobPosition).toHaveBeenCalled();
      expect(jobPositionService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IJobPosition>>();
      const jobPosition = { id: 123 };
      jest.spyOn(jobPositionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ jobPosition });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(jobPositionService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
