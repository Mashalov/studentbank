import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IJobPosition } from '../job-position.model';
import { JobPositionService } from '../service/job-position.service';
import { JobPositionFormService, JobPositionFormGroup } from './job-position-form.service';

@Component({
  standalone: true,
  selector: 'jhi-job-position-update',
  templateUrl: './job-position-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class JobPositionUpdateComponent implements OnInit {
  isSaving = false;
  jobPosition: IJobPosition | null = null;

  protected jobPositionService = inject(JobPositionService);
  protected jobPositionFormService = inject(JobPositionFormService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: JobPositionFormGroup = this.jobPositionFormService.createJobPositionFormGroup();

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jobPosition }) => {
      this.jobPosition = jobPosition;
      if (jobPosition) {
        this.updateForm(jobPosition);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jobPosition = this.jobPositionFormService.getJobPosition(this.editForm);
    if (jobPosition.id !== null) {
      this.subscribeToSaveResponse(this.jobPositionService.update(jobPosition));
    } else {
      this.subscribeToSaveResponse(this.jobPositionService.create(jobPosition));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJobPosition>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(jobPosition: IJobPosition): void {
    this.jobPosition = jobPosition;
    this.jobPositionFormService.resetForm(this.editForm, jobPosition);
  }
}
