import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../job-position.test-samples';

import { JobPositionFormService } from './job-position-form.service';

describe('JobPosition Form Service', () => {
  let service: JobPositionFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobPositionFormService);
  });

  describe('Service methods', () => {
    describe('createJobPositionFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createJobPositionFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            company: expect.any(Object),
          }),
        );
      });

      it('passing IJobPosition should create a new form with FormGroup', () => {
        const formGroup = service.createJobPositionFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            company: expect.any(Object),
          }),
        );
      });
    });

    describe('getJobPosition', () => {
      it('should return NewJobPosition for default JobPosition initial value', () => {
        const formGroup = service.createJobPositionFormGroup(sampleWithNewData);

        const jobPosition = service.getJobPosition(formGroup) as any;

        expect(jobPosition).toMatchObject(sampleWithNewData);
      });

      it('should return NewJobPosition for empty JobPosition initial value', () => {
        const formGroup = service.createJobPositionFormGroup();

        const jobPosition = service.getJobPosition(formGroup) as any;

        expect(jobPosition).toMatchObject({});
      });

      it('should return IJobPosition', () => {
        const formGroup = service.createJobPositionFormGroup(sampleWithRequiredData);

        const jobPosition = service.getJobPosition(formGroup) as any;

        expect(jobPosition).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IJobPosition should not enable id FormControl', () => {
        const formGroup = service.createJobPositionFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewJobPosition should disable id FormControl', () => {
        const formGroup = service.createJobPositionFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
