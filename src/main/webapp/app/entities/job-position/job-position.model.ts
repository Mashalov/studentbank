export interface IJobPosition {
  id: number;
  name?: string | null;
  company?: string | null;
}

export type NewJobPosition = Omit<IJobPosition, 'id'> & { id: null };
