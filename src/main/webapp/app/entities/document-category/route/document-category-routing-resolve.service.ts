import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDocumentCategory } from '../document-category.model';
import { DocumentCategoryService } from '../service/document-category.service';

const documentCategoryResolve = (route: ActivatedRouteSnapshot): Observable<null | IDocumentCategory> => {
  const id = route.params['id'];
  if (id) {
    return inject(DocumentCategoryService)
      .find(id)
      .pipe(
        mergeMap((documentCategory: HttpResponse<IDocumentCategory>) => {
          if (documentCategory.body) {
            return of(documentCategory.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default documentCategoryResolve;
