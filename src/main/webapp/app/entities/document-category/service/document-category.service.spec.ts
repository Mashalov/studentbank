import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IDocumentCategory } from '../document-category.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../document-category.test-samples';

import { DocumentCategoryService } from './document-category.service';

const requireRestSample: IDocumentCategory = {
  ...sampleWithRequiredData,
};

describe('DocumentCategory Service', () => {
  let service: DocumentCategoryService;
  let httpMock: HttpTestingController;
  let expectedResult: IDocumentCategory | IDocumentCategory[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(DocumentCategoryService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a DocumentCategory', () => {
      const documentCategory = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(documentCategory).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a DocumentCategory', () => {
      const documentCategory = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(documentCategory).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a DocumentCategory', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of DocumentCategory', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a DocumentCategory', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDocumentCategoryToCollectionIfMissing', () => {
      it('should add a DocumentCategory to an empty array', () => {
        const documentCategory: IDocumentCategory = sampleWithRequiredData;
        expectedResult = service.addDocumentCategoryToCollectionIfMissing([], documentCategory);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(documentCategory);
      });

      it('should not add a DocumentCategory to an array that contains it', () => {
        const documentCategory: IDocumentCategory = sampleWithRequiredData;
        const documentCategoryCollection: IDocumentCategory[] = [
          {
            ...documentCategory,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDocumentCategoryToCollectionIfMissing(documentCategoryCollection, documentCategory);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a DocumentCategory to an array that doesn't contain it", () => {
        const documentCategory: IDocumentCategory = sampleWithRequiredData;
        const documentCategoryCollection: IDocumentCategory[] = [sampleWithPartialData];
        expectedResult = service.addDocumentCategoryToCollectionIfMissing(documentCategoryCollection, documentCategory);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(documentCategory);
      });

      it('should add only unique DocumentCategory to an array', () => {
        const documentCategoryArray: IDocumentCategory[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const documentCategoryCollection: IDocumentCategory[] = [sampleWithRequiredData];
        expectedResult = service.addDocumentCategoryToCollectionIfMissing(documentCategoryCollection, ...documentCategoryArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const documentCategory: IDocumentCategory = sampleWithRequiredData;
        const documentCategory2: IDocumentCategory = sampleWithPartialData;
        expectedResult = service.addDocumentCategoryToCollectionIfMissing([], documentCategory, documentCategory2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(documentCategory);
        expect(expectedResult).toContain(documentCategory2);
      });

      it('should accept null and undefined values', () => {
        const documentCategory: IDocumentCategory = sampleWithRequiredData;
        expectedResult = service.addDocumentCategoryToCollectionIfMissing([], null, documentCategory, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(documentCategory);
      });

      it('should return initial array if no DocumentCategory is added', () => {
        const documentCategoryCollection: IDocumentCategory[] = [sampleWithRequiredData];
        expectedResult = service.addDocumentCategoryToCollectionIfMissing(documentCategoryCollection, undefined, null);
        expect(expectedResult).toEqual(documentCategoryCollection);
      });
    });

    describe('compareDocumentCategory', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDocumentCategory(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDocumentCategory(entity1, entity2);
        const compareResult2 = service.compareDocumentCategory(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDocumentCategory(entity1, entity2);
        const compareResult2 = service.compareDocumentCategory(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDocumentCategory(entity1, entity2);
        const compareResult2 = service.compareDocumentCategory(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
