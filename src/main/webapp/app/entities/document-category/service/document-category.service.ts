import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDocumentCategory, NewDocumentCategory } from '../document-category.model';

export type PartialUpdateDocumentCategory = Partial<IDocumentCategory> & Pick<IDocumentCategory, 'id'>;

export type EntityResponseType = HttpResponse<IDocumentCategory>;
export type EntityArrayResponseType = HttpResponse<IDocumentCategory[]>;

@Injectable({ providedIn: 'root' })
export class DocumentCategoryService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/document-categories');

  create(documentCategory: NewDocumentCategory): Observable<EntityResponseType> {
    return this.http.post<IDocumentCategory>(this.resourceUrl, documentCategory, { observe: 'response' });
  }

  update(documentCategory: IDocumentCategory): Observable<EntityResponseType> {
    return this.http.put<IDocumentCategory>(
      `${this.resourceUrl}/${this.getDocumentCategoryIdentifier(documentCategory)}`,
      documentCategory,
      { observe: 'response' },
    );
  }

  partialUpdate(documentCategory: PartialUpdateDocumentCategory): Observable<EntityResponseType> {
    return this.http.patch<IDocumentCategory>(
      `${this.resourceUrl}/${this.getDocumentCategoryIdentifier(documentCategory)}`,
      documentCategory,
      { observe: 'response' },
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDocumentCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDocumentCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDocumentCategoryIdentifier(documentCategory: Pick<IDocumentCategory, 'id'>): number {
    return documentCategory.id;
  }

  compareDocumentCategory(o1: Pick<IDocumentCategory, 'id'> | null, o2: Pick<IDocumentCategory, 'id'> | null): boolean {
    return o1 && o2 ? this.getDocumentCategoryIdentifier(o1) === this.getDocumentCategoryIdentifier(o2) : o1 === o2;
  }

  addDocumentCategoryToCollectionIfMissing<Type extends Pick<IDocumentCategory, 'id'>>(
    documentCategoryCollection: Type[],
    ...documentCategoriesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const documentCategories: Type[] = documentCategoriesToCheck.filter(isPresent);
    if (documentCategories.length > 0) {
      const documentCategoryCollectionIdentifiers = documentCategoryCollection.map(documentCategoryItem =>
        this.getDocumentCategoryIdentifier(documentCategoryItem),
      );
      const documentCategoriesToAdd = documentCategories.filter(documentCategoryItem => {
        const documentCategoryIdentifier = this.getDocumentCategoryIdentifier(documentCategoryItem);
        if (documentCategoryCollectionIdentifiers.includes(documentCategoryIdentifier)) {
          return false;
        }
        documentCategoryCollectionIdentifiers.push(documentCategoryIdentifier);
        return true;
      });
      return [...documentCategoriesToAdd, ...documentCategoryCollection];
    }
    return documentCategoryCollection;
  }
}
