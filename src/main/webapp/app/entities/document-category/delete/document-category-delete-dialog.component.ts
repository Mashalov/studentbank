import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IDocumentCategory } from '../document-category.model';
import { DocumentCategoryService } from '../service/document-category.service';

@Component({
  standalone: true,
  templateUrl: './document-category-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class DocumentCategoryDeleteDialogComponent {
  documentCategory?: IDocumentCategory;

  protected documentCategoryService = inject(DocumentCategoryService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.documentCategoryService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
