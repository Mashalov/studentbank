import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { DocumentCategoryComponent } from './list/document-category.component';
import { DocumentCategoryDetailComponent } from './detail/document-category-detail.component';
import { DocumentCategoryUpdateComponent } from './update/document-category-update.component';
import DocumentCategoryResolve from './route/document-category-routing-resolve.service';

const documentCategoryRoute: Routes = [
  {
    path: '',
    component: DocumentCategoryComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DocumentCategoryDetailComponent,
    resolve: {
      documentCategory: DocumentCategoryResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DocumentCategoryUpdateComponent,
    resolve: {
      documentCategory: DocumentCategoryResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DocumentCategoryUpdateComponent,
    resolve: {
      documentCategory: DocumentCategoryResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default documentCategoryRoute;
