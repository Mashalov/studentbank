import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../document-category.test-samples';

import { DocumentCategoryFormService } from './document-category-form.service';

describe('DocumentCategory Form Service', () => {
  let service: DocumentCategoryFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentCategoryFormService);
  });

  describe('Service methods', () => {
    describe('createDocumentCategoryFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDocumentCategoryFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            documentCategory: expect.any(Object),
          }),
        );
      });

      it('passing IDocumentCategory should create a new form with FormGroup', () => {
        const formGroup = service.createDocumentCategoryFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            documentCategory: expect.any(Object),
          }),
        );
      });
    });

    describe('getDocumentCategory', () => {
      it('should return NewDocumentCategory for default DocumentCategory initial value', () => {
        const formGroup = service.createDocumentCategoryFormGroup(sampleWithNewData);

        const documentCategory = service.getDocumentCategory(formGroup) as any;

        expect(documentCategory).toMatchObject(sampleWithNewData);
      });

      it('should return NewDocumentCategory for empty DocumentCategory initial value', () => {
        const formGroup = service.createDocumentCategoryFormGroup();

        const documentCategory = service.getDocumentCategory(formGroup) as any;

        expect(documentCategory).toMatchObject({});
      });

      it('should return IDocumentCategory', () => {
        const formGroup = service.createDocumentCategoryFormGroup(sampleWithRequiredData);

        const documentCategory = service.getDocumentCategory(formGroup) as any;

        expect(documentCategory).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDocumentCategory should not enable id FormControl', () => {
        const formGroup = service.createDocumentCategoryFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDocumentCategory should disable id FormControl', () => {
        const formGroup = service.createDocumentCategoryFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
