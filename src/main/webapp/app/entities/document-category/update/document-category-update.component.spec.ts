import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { DocumentCategoryService } from '../service/document-category.service';
import { IDocumentCategory } from '../document-category.model';
import { DocumentCategoryFormService } from './document-category-form.service';

import { DocumentCategoryUpdateComponent } from './document-category-update.component';

describe('DocumentCategory Management Update Component', () => {
  let comp: DocumentCategoryUpdateComponent;
  let fixture: ComponentFixture<DocumentCategoryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let documentCategoryFormService: DocumentCategoryFormService;
  let documentCategoryService: DocumentCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DocumentCategoryUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DocumentCategoryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DocumentCategoryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    documentCategoryFormService = TestBed.inject(DocumentCategoryFormService);
    documentCategoryService = TestBed.inject(DocumentCategoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call DocumentCategory query and add missing value', () => {
      const documentCategory: IDocumentCategory = { id: 456 };
      const documentCategory: IDocumentCategory = { id: 5767 };
      documentCategory.documentCategory = documentCategory;

      const documentCategoryCollection: IDocumentCategory[] = [{ id: 22431 }];
      jest.spyOn(documentCategoryService, 'query').mockReturnValue(of(new HttpResponse({ body: documentCategoryCollection })));
      const additionalDocumentCategories = [documentCategory];
      const expectedCollection: IDocumentCategory[] = [...additionalDocumentCategories, ...documentCategoryCollection];
      jest.spyOn(documentCategoryService, 'addDocumentCategoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ documentCategory });
      comp.ngOnInit();

      expect(documentCategoryService.query).toHaveBeenCalled();
      expect(documentCategoryService.addDocumentCategoryToCollectionIfMissing).toHaveBeenCalledWith(
        documentCategoryCollection,
        ...additionalDocumentCategories.map(expect.objectContaining),
      );
      expect(comp.documentCategoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const documentCategory: IDocumentCategory = { id: 456 };
      const documentCategory: IDocumentCategory = { id: 29140 };
      documentCategory.documentCategory = documentCategory;

      activatedRoute.data = of({ documentCategory });
      comp.ngOnInit();

      expect(comp.documentCategoriesSharedCollection).toContain(documentCategory);
      expect(comp.documentCategory).toEqual(documentCategory);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDocumentCategory>>();
      const documentCategory = { id: 123 };
      jest.spyOn(documentCategoryFormService, 'getDocumentCategory').mockReturnValue(documentCategory);
      jest.spyOn(documentCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ documentCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: documentCategory }));
      saveSubject.complete();

      // THEN
      expect(documentCategoryFormService.getDocumentCategory).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(documentCategoryService.update).toHaveBeenCalledWith(expect.objectContaining(documentCategory));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDocumentCategory>>();
      const documentCategory = { id: 123 };
      jest.spyOn(documentCategoryFormService, 'getDocumentCategory').mockReturnValue({ id: null });
      jest.spyOn(documentCategoryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ documentCategory: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: documentCategory }));
      saveSubject.complete();

      // THEN
      expect(documentCategoryFormService.getDocumentCategory).toHaveBeenCalled();
      expect(documentCategoryService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDocumentCategory>>();
      const documentCategory = { id: 123 };
      jest.spyOn(documentCategoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ documentCategory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(documentCategoryService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareDocumentCategory', () => {
      it('Should forward to documentCategoryService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(documentCategoryService, 'compareDocumentCategory');
        comp.compareDocumentCategory(entity, entity2);
        expect(documentCategoryService.compareDocumentCategory).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
