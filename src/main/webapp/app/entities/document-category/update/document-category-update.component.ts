import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IDocumentCategory } from '../document-category.model';
import { DocumentCategoryService } from '../service/document-category.service';
import { DocumentCategoryFormService, DocumentCategoryFormGroup } from './document-category-form.service';

@Component({
  standalone: true,
  selector: 'jhi-document-category-update',
  templateUrl: './document-category-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class DocumentCategoryUpdateComponent implements OnInit {
  isSaving = false;
  documentCategory: IDocumentCategory | null = null;

  documentCategoriesSharedCollection: IDocumentCategory[] = [];

  protected documentCategoryService = inject(DocumentCategoryService);
  protected documentCategoryFormService = inject(DocumentCategoryFormService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: DocumentCategoryFormGroup = this.documentCategoryFormService.createDocumentCategoryFormGroup();

  compareDocumentCategory = (o1: IDocumentCategory | null, o2: IDocumentCategory | null): boolean =>
    this.documentCategoryService.compareDocumentCategory(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ documentCategory }) => {
      this.documentCategory = documentCategory;
      if (documentCategory) {
        this.updateForm(documentCategory);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const documentCategory = this.documentCategoryFormService.getDocumentCategory(this.editForm);
    if (documentCategory.id !== null) {
      this.subscribeToSaveResponse(this.documentCategoryService.update(documentCategory));
    } else {
      this.subscribeToSaveResponse(this.documentCategoryService.create(documentCategory));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDocumentCategory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(documentCategory: IDocumentCategory): void {
    this.documentCategory = documentCategory;
    this.documentCategoryFormService.resetForm(this.editForm, documentCategory);

    this.documentCategoriesSharedCollection = this.documentCategoryService.addDocumentCategoryToCollectionIfMissing<IDocumentCategory>(
      this.documentCategoriesSharedCollection,
      documentCategory.documentCategory,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.documentCategoryService
      .query()
      .pipe(map((res: HttpResponse<IDocumentCategory[]>) => res.body ?? []))
      .pipe(
        map((documentCategories: IDocumentCategory[]) =>
          this.documentCategoryService.addDocumentCategoryToCollectionIfMissing<IDocumentCategory>(
            documentCategories,
            this.documentCategory?.documentCategory,
          ),
        ),
      )
      .subscribe((documentCategories: IDocumentCategory[]) => (this.documentCategoriesSharedCollection = documentCategories));
  }
}
