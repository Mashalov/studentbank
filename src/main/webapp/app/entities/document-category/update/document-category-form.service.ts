import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDocumentCategory, NewDocumentCategory } from '../document-category.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDocumentCategory for edit and NewDocumentCategoryFormGroupInput for create.
 */
type DocumentCategoryFormGroupInput = IDocumentCategory | PartialWithRequiredKeyOf<NewDocumentCategory>;

type DocumentCategoryFormDefaults = Pick<NewDocumentCategory, 'id'>;

type DocumentCategoryFormGroupContent = {
  id: FormControl<IDocumentCategory['id'] | NewDocumentCategory['id']>;
  name: FormControl<IDocumentCategory['name']>;
  documentCategory: FormControl<IDocumentCategory['documentCategory']>;
};

export type DocumentCategoryFormGroup = FormGroup<DocumentCategoryFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DocumentCategoryFormService {
  createDocumentCategoryFormGroup(documentCategory: DocumentCategoryFormGroupInput = { id: null }): DocumentCategoryFormGroup {
    const documentCategoryRawValue = {
      ...this.getFormDefaults(),
      ...documentCategory,
    };
    return new FormGroup<DocumentCategoryFormGroupContent>({
      id: new FormControl(
        { value: documentCategoryRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(documentCategoryRawValue.name, {
        validators: [Validators.required],
      }),
      documentCategory: new FormControl(documentCategoryRawValue.documentCategory),
    });
  }

  getDocumentCategory(form: DocumentCategoryFormGroup): IDocumentCategory | NewDocumentCategory {
    return form.getRawValue() as IDocumentCategory | NewDocumentCategory;
  }

  resetForm(form: DocumentCategoryFormGroup, documentCategory: DocumentCategoryFormGroupInput): void {
    const documentCategoryRawValue = { ...this.getFormDefaults(), ...documentCategory };
    form.reset(
      {
        ...documentCategoryRawValue,
        id: { value: documentCategoryRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): DocumentCategoryFormDefaults {
    return {
      id: null,
    };
  }
}
