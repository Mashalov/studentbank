export interface IDocumentCategory {
  id: number;
  name?: string | null;
  documentCategory?: Pick<IDocumentCategory, 'id'> | null;
}

export type NewDocumentCategory = Omit<IDocumentCategory, 'id'> & { id: null };
