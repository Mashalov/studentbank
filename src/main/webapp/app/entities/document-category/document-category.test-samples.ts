import { IDocumentCategory, NewDocumentCategory } from './document-category.model';

export const sampleWithRequiredData: IDocumentCategory = {
  id: 4968,
  name: 'eke conscientise',
};

export const sampleWithPartialData: IDocumentCategory = {
  id: 20293,
  name: 'communist the athwart',
};

export const sampleWithFullData: IDocumentCategory = {
  id: 6607,
  name: 'whose but',
};

export const sampleWithNewData: NewDocumentCategory = {
  name: 'until',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
