import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IDocumentCategory } from '../document-category.model';

@Component({
  standalone: true,
  selector: 'jhi-document-category-detail',
  templateUrl: './document-category-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class DocumentCategoryDetailComponent {
  documentCategory = input<IDocumentCategory | null>(null);

  previousState(): void {
    window.history.back();
  }
}
