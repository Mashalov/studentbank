import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IExchange, NewExchange } from '../exchange.model';

export type PartialUpdateExchange = Partial<IExchange> & Pick<IExchange, 'id'>;

export type EntityResponseType = HttpResponse<IExchange>;
export type EntityArrayResponseType = HttpResponse<IExchange[]>;

@Injectable({ providedIn: 'root' })
export class ExchangeService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/exchanges');

  create(exchange: NewExchange): Observable<EntityResponseType> {
    return this.http.post<IExchange>(this.resourceUrl, exchange, { observe: 'response' });
  }

  update(exchange: IExchange): Observable<EntityResponseType> {
    return this.http.put<IExchange>(`${this.resourceUrl}/${this.getExchangeIdentifier(exchange)}`, exchange, { observe: 'response' });
  }

  partialUpdate(exchange: PartialUpdateExchange): Observable<EntityResponseType> {
    return this.http.patch<IExchange>(`${this.resourceUrl}/${this.getExchangeIdentifier(exchange)}`, exchange, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExchange>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExchange[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getExchangeIdentifier(exchange: Pick<IExchange, 'id'>): number {
    return exchange.id;
  }

  compareExchange(o1: Pick<IExchange, 'id'> | null, o2: Pick<IExchange, 'id'> | null): boolean {
    return o1 && o2 ? this.getExchangeIdentifier(o1) === this.getExchangeIdentifier(o2) : o1 === o2;
  }

  addExchangeToCollectionIfMissing<Type extends Pick<IExchange, 'id'>>(
    exchangeCollection: Type[],
    ...exchangesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const exchanges: Type[] = exchangesToCheck.filter(isPresent);
    if (exchanges.length > 0) {
      const exchangeCollectionIdentifiers = exchangeCollection.map(exchangeItem => this.getExchangeIdentifier(exchangeItem));
      const exchangesToAdd = exchanges.filter(exchangeItem => {
        const exchangeIdentifier = this.getExchangeIdentifier(exchangeItem);
        if (exchangeCollectionIdentifiers.includes(exchangeIdentifier)) {
          return false;
        }
        exchangeCollectionIdentifiers.push(exchangeIdentifier);
        return true;
      });
      return [...exchangesToAdd, ...exchangeCollection];
    }
    return exchangeCollection;
  }
}
