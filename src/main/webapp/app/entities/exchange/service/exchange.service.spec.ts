import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IExchange } from '../exchange.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../exchange.test-samples';

import { ExchangeService } from './exchange.service';

const requireRestSample: IExchange = {
  ...sampleWithRequiredData,
};

describe('Exchange Service', () => {
  let service: ExchangeService;
  let httpMock: HttpTestingController;
  let expectedResult: IExchange | IExchange[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(ExchangeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Exchange', () => {
      const exchange = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(exchange).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Exchange', () => {
      const exchange = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(exchange).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Exchange', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Exchange', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Exchange', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addExchangeToCollectionIfMissing', () => {
      it('should add a Exchange to an empty array', () => {
        const exchange: IExchange = sampleWithRequiredData;
        expectedResult = service.addExchangeToCollectionIfMissing([], exchange);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(exchange);
      });

      it('should not add a Exchange to an array that contains it', () => {
        const exchange: IExchange = sampleWithRequiredData;
        const exchangeCollection: IExchange[] = [
          {
            ...exchange,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addExchangeToCollectionIfMissing(exchangeCollection, exchange);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Exchange to an array that doesn't contain it", () => {
        const exchange: IExchange = sampleWithRequiredData;
        const exchangeCollection: IExchange[] = [sampleWithPartialData];
        expectedResult = service.addExchangeToCollectionIfMissing(exchangeCollection, exchange);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(exchange);
      });

      it('should add only unique Exchange to an array', () => {
        const exchangeArray: IExchange[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const exchangeCollection: IExchange[] = [sampleWithRequiredData];
        expectedResult = service.addExchangeToCollectionIfMissing(exchangeCollection, ...exchangeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const exchange: IExchange = sampleWithRequiredData;
        const exchange2: IExchange = sampleWithPartialData;
        expectedResult = service.addExchangeToCollectionIfMissing([], exchange, exchange2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(exchange);
        expect(expectedResult).toContain(exchange2);
      });

      it('should accept null and undefined values', () => {
        const exchange: IExchange = sampleWithRequiredData;
        expectedResult = service.addExchangeToCollectionIfMissing([], null, exchange, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(exchange);
      });

      it('should return initial array if no Exchange is added', () => {
        const exchangeCollection: IExchange[] = [sampleWithRequiredData];
        expectedResult = service.addExchangeToCollectionIfMissing(exchangeCollection, undefined, null);
        expect(expectedResult).toEqual(exchangeCollection);
      });
    });

    describe('compareExchange', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareExchange(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareExchange(entity1, entity2);
        const compareResult2 = service.compareExchange(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareExchange(entity1, entity2);
        const compareResult2 = service.compareExchange(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareExchange(entity1, entity2);
        const compareResult2 = service.compareExchange(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
