import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IExchange } from '../exchange.model';

@Component({
  standalone: true,
  selector: 'jhi-exchange-detail',
  templateUrl: './exchange-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class ExchangeDetailComponent {
  exchange = input<IExchange | null>(null);

  previousState(): void {
    window.history.back();
  }
}
