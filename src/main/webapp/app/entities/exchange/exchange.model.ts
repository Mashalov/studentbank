import { IEmployee } from 'app/entities/employee/employee.model';
import { IOffice } from 'app/entities/office/office.model';
import { IDocument } from 'app/entities/document/document.model';
import { Currency } from 'app/entities/enumerations/currency.model';

export interface IExchange {
  id: number;
  amount?: number | null;
  currencyIn?: keyof typeof Currency | null;
  currencyOut?: keyof typeof Currency | null;
  exchangeRate?: number | null;
  employee?: Pick<IEmployee, 'id'> | null;
  office?: Pick<IOffice, 'id'> | null;
  document?: Pick<IDocument, 'id'> | null;
}

export type NewExchange = Omit<IExchange, 'id'> & { id: null };
