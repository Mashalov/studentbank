import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../exchange.test-samples';

import { ExchangeFormService } from './exchange-form.service';

describe('Exchange Form Service', () => {
  let service: ExchangeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExchangeFormService);
  });

  describe('Service methods', () => {
    describe('createExchangeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createExchangeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            amount: expect.any(Object),
            currencyIn: expect.any(Object),
            currencyOut: expect.any(Object),
            exchangeRate: expect.any(Object),
            employee: expect.any(Object),
            office: expect.any(Object),
            document: expect.any(Object),
          }),
        );
      });

      it('passing IExchange should create a new form with FormGroup', () => {
        const formGroup = service.createExchangeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            amount: expect.any(Object),
            currencyIn: expect.any(Object),
            currencyOut: expect.any(Object),
            exchangeRate: expect.any(Object),
            employee: expect.any(Object),
            office: expect.any(Object),
            document: expect.any(Object),
          }),
        );
      });
    });

    describe('getExchange', () => {
      it('should return NewExchange for default Exchange initial value', () => {
        const formGroup = service.createExchangeFormGroup(sampleWithNewData);

        const exchange = service.getExchange(formGroup) as any;

        expect(exchange).toMatchObject(sampleWithNewData);
      });

      it('should return NewExchange for empty Exchange initial value', () => {
        const formGroup = service.createExchangeFormGroup();

        const exchange = service.getExchange(formGroup) as any;

        expect(exchange).toMatchObject({});
      });

      it('should return IExchange', () => {
        const formGroup = service.createExchangeFormGroup(sampleWithRequiredData);

        const exchange = service.getExchange(formGroup) as any;

        expect(exchange).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IExchange should not enable id FormControl', () => {
        const formGroup = service.createExchangeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewExchange should disable id FormControl', () => {
        const formGroup = service.createExchangeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
