import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IExchange, NewExchange } from '../exchange.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IExchange for edit and NewExchangeFormGroupInput for create.
 */
type ExchangeFormGroupInput = IExchange | PartialWithRequiredKeyOf<NewExchange>;

type ExchangeFormDefaults = Pick<NewExchange, 'id'>;

type ExchangeFormGroupContent = {
  id: FormControl<IExchange['id'] | NewExchange['id']>;
  amount: FormControl<IExchange['amount']>;
  currencyIn: FormControl<IExchange['currencyIn']>;
  currencyOut: FormControl<IExchange['currencyOut']>;
  exchangeRate: FormControl<IExchange['exchangeRate']>;
  employee: FormControl<IExchange['employee']>;
  office: FormControl<IExchange['office']>;
  document: FormControl<IExchange['document']>;
};

export type ExchangeFormGroup = FormGroup<ExchangeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ExchangeFormService {
  createExchangeFormGroup(exchange: ExchangeFormGroupInput = { id: null }): ExchangeFormGroup {
    const exchangeRawValue = {
      ...this.getFormDefaults(),
      ...exchange,
    };
    return new FormGroup<ExchangeFormGroupContent>({
      id: new FormControl(
        { value: exchangeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      amount: new FormControl(exchangeRawValue.amount, {
        validators: [Validators.required],
      }),
      currencyIn: new FormControl(exchangeRawValue.currencyIn, {
        validators: [Validators.required],
      }),
      currencyOut: new FormControl(exchangeRawValue.currencyOut, {
        validators: [Validators.required],
      }),
      exchangeRate: new FormControl(exchangeRawValue.exchangeRate),
      employee: new FormControl(exchangeRawValue.employee),
      office: new FormControl(exchangeRawValue.office),
      document: new FormControl(exchangeRawValue.document),
    });
  }

  getExchange(form: ExchangeFormGroup): IExchange | NewExchange {
    return form.getRawValue() as IExchange | NewExchange;
  }

  resetForm(form: ExchangeFormGroup, exchange: ExchangeFormGroupInput): void {
    const exchangeRawValue = { ...this.getFormDefaults(), ...exchange };
    form.reset(
      {
        ...exchangeRawValue,
        id: { value: exchangeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): ExchangeFormDefaults {
    return {
      id: null,
    };
  }
}
