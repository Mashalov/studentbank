import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IEmployee } from 'app/entities/employee/employee.model';
import { EmployeeService } from 'app/entities/employee/service/employee.service';
import { IOffice } from 'app/entities/office/office.model';
import { OfficeService } from 'app/entities/office/service/office.service';
import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { IExchange } from '../exchange.model';
import { ExchangeService } from '../service/exchange.service';
import { ExchangeFormService } from './exchange-form.service';

import { ExchangeUpdateComponent } from './exchange-update.component';

describe('Exchange Management Update Component', () => {
  let comp: ExchangeUpdateComponent;
  let fixture: ComponentFixture<ExchangeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let exchangeFormService: ExchangeFormService;
  let exchangeService: ExchangeService;
  let employeeService: EmployeeService;
  let officeService: OfficeService;
  let documentService: DocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ExchangeUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ExchangeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ExchangeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    exchangeFormService = TestBed.inject(ExchangeFormService);
    exchangeService = TestBed.inject(ExchangeService);
    employeeService = TestBed.inject(EmployeeService);
    officeService = TestBed.inject(OfficeService);
    documentService = TestBed.inject(DocumentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Employee query and add missing value', () => {
      const exchange: IExchange = { id: 456 };
      const employee: IEmployee = { id: 32252 };
      exchange.employee = employee;

      const employeeCollection: IEmployee[] = [{ id: 29886 }];
      jest.spyOn(employeeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeeCollection })));
      const additionalEmployees = [employee];
      const expectedCollection: IEmployee[] = [...additionalEmployees, ...employeeCollection];
      jest.spyOn(employeeService, 'addEmployeeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      expect(employeeService.query).toHaveBeenCalled();
      expect(employeeService.addEmployeeToCollectionIfMissing).toHaveBeenCalledWith(
        employeeCollection,
        ...additionalEmployees.map(expect.objectContaining),
      );
      expect(comp.employeesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Office query and add missing value', () => {
      const exchange: IExchange = { id: 456 };
      const office: IOffice = { id: 13381 };
      exchange.office = office;

      const officeCollection: IOffice[] = [{ id: 26883 }];
      jest.spyOn(officeService, 'query').mockReturnValue(of(new HttpResponse({ body: officeCollection })));
      const additionalOffices = [office];
      const expectedCollection: IOffice[] = [...additionalOffices, ...officeCollection];
      jest.spyOn(officeService, 'addOfficeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      expect(officeService.query).toHaveBeenCalled();
      expect(officeService.addOfficeToCollectionIfMissing).toHaveBeenCalledWith(
        officeCollection,
        ...additionalOffices.map(expect.objectContaining),
      );
      expect(comp.officesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Document query and add missing value', () => {
      const exchange: IExchange = { id: 456 };
      const document: IDocument = { id: 10941 };
      exchange.document = document;

      const documentCollection: IDocument[] = [{ id: 18610 }];
      jest.spyOn(documentService, 'query').mockReturnValue(of(new HttpResponse({ body: documentCollection })));
      const additionalDocuments = [document];
      const expectedCollection: IDocument[] = [...additionalDocuments, ...documentCollection];
      jest.spyOn(documentService, 'addDocumentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      expect(documentService.query).toHaveBeenCalled();
      expect(documentService.addDocumentToCollectionIfMissing).toHaveBeenCalledWith(
        documentCollection,
        ...additionalDocuments.map(expect.objectContaining),
      );
      expect(comp.documentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const exchange: IExchange = { id: 456 };
      const employee: IEmployee = { id: 31949 };
      exchange.employee = employee;
      const office: IOffice = { id: 27996 };
      exchange.office = office;
      const document: IDocument = { id: 26292 };
      exchange.document = document;

      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      expect(comp.employeesSharedCollection).toContain(employee);
      expect(comp.officesSharedCollection).toContain(office);
      expect(comp.documentsSharedCollection).toContain(document);
      expect(comp.exchange).toEqual(exchange);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExchange>>();
      const exchange = { id: 123 };
      jest.spyOn(exchangeFormService, 'getExchange').mockReturnValue(exchange);
      jest.spyOn(exchangeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: exchange }));
      saveSubject.complete();

      // THEN
      expect(exchangeFormService.getExchange).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(exchangeService.update).toHaveBeenCalledWith(expect.objectContaining(exchange));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExchange>>();
      const exchange = { id: 123 };
      jest.spyOn(exchangeFormService, 'getExchange').mockReturnValue({ id: null });
      jest.spyOn(exchangeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ exchange: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: exchange }));
      saveSubject.complete();

      // THEN
      expect(exchangeFormService.getExchange).toHaveBeenCalled();
      expect(exchangeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IExchange>>();
      const exchange = { id: 123 };
      jest.spyOn(exchangeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ exchange });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(exchangeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEmployee', () => {
      it('Should forward to employeeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeeService, 'compareEmployee');
        comp.compareEmployee(entity, entity2);
        expect(employeeService.compareEmployee).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareOffice', () => {
      it('Should forward to officeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(officeService, 'compareOffice');
        comp.compareOffice(entity, entity2);
        expect(officeService.compareOffice).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareDocument', () => {
      it('Should forward to documentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(documentService, 'compareDocument');
        comp.compareDocument(entity, entity2);
        expect(documentService.compareDocument).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
