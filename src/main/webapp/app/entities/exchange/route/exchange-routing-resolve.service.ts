import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IExchange } from '../exchange.model';
import { ExchangeService } from '../service/exchange.service';

const exchangeResolve = (route: ActivatedRouteSnapshot): Observable<null | IExchange> => {
  const id = route.params['id'];
  if (id) {
    return inject(ExchangeService)
      .find(id)
      .pipe(
        mergeMap((exchange: HttpResponse<IExchange>) => {
          if (exchange.body) {
            return of(exchange.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default exchangeResolve;
