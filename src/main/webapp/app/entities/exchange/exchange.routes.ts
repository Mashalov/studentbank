import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { ExchangeComponent } from './list/exchange.component';
import { ExchangeDetailComponent } from './detail/exchange-detail.component';
import { ExchangeUpdateComponent } from './update/exchange-update.component';
import ExchangeResolve from './route/exchange-routing-resolve.service';

const exchangeRoute: Routes = [
  {
    path: '',
    component: ExchangeComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ExchangeDetailComponent,
    resolve: {
      exchange: ExchangeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ExchangeUpdateComponent,
    resolve: {
      exchange: ExchangeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ExchangeUpdateComponent,
    resolve: {
      exchange: ExchangeResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default exchangeRoute;
