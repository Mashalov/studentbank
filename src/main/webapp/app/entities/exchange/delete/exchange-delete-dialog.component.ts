import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IExchange } from '../exchange.model';
import { ExchangeService } from '../service/exchange.service';

@Component({
  standalone: true,
  templateUrl: './exchange-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class ExchangeDeleteDialogComponent {
  exchange?: IExchange;

  protected exchangeService = inject(ExchangeService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.exchangeService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
