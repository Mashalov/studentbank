import { IExchange, NewExchange } from './exchange.model';

export const sampleWithRequiredData: IExchange = {
  id: 13521,
  amount: 26398.12,
  currencyIn: 'EUR',
  currencyOut: 'EUR',
};

export const sampleWithPartialData: IExchange = {
  id: 2321,
  amount: 7016.53,
  currencyIn: 'BGN',
  currencyOut: 'USD',
  exchangeRate: 13295,
};

export const sampleWithFullData: IExchange = {
  id: 24896,
  amount: 6189.79,
  currencyIn: 'BGN',
  currencyOut: 'BGN',
  exchangeRate: 8791,
};

export const sampleWithNewData: NewExchange = {
  amount: 15024.89,
  currencyIn: 'BGN',
  currencyOut: 'BGN',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
