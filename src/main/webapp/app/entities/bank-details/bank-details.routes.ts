import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BankDetailsComponent } from './list/bank-details.component';
import { BankDetailsDetailComponent } from './detail/bank-details-detail.component';
import { BankDetailsUpdateComponent } from './update/bank-details-update.component';
import BankDetailsResolve from './route/bank-details-routing-resolve.service';

const bankDetailsRoute: Routes = [
  {
    path: '',
    component: BankDetailsComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BankDetailsDetailComponent,
    resolve: {
      bankDetails: BankDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BankDetailsUpdateComponent,
    resolve: {
      bankDetails: BankDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BankDetailsUpdateComponent,
    resolve: {
      bankDetails: BankDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default bankDetailsRoute;
