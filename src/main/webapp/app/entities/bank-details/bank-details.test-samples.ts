import dayjs from 'dayjs/esm';

import { IBankDetails, NewBankDetails } from './bank-details.model';

export const sampleWithRequiredData: IBankDetails = {
  id: 10388,
  name: 'beatify considering',
};

export const sampleWithPartialData: IBankDetails = {
  id: 3539,
  name: 'scimitar creamy ringed',
  ceo: 'independent oof want',
};

export const sampleWithFullData: IBankDetails = {
  id: 28776,
  name: 'avert credential mmm',
  foundationYear: dayjs('2024-07-06T12:21'),
  ceo: 'fooey',
  mainWebsite: 'spelling drat',
};

export const sampleWithNewData: NewBankDetails = {
  name: 'gambling',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
