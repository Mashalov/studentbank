import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IBankDetails } from '../bank-details.model';

@Component({
  standalone: true,
  selector: 'jhi-bank-details-detail',
  templateUrl: './bank-details-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class BankDetailsDetailComponent {
  bankDetails = input<IBankDetails | null>(null);

  previousState(): void {
    window.history.back();
  }
}
