import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IBankDetails, NewBankDetails } from '../bank-details.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBankDetails for edit and NewBankDetailsFormGroupInput for create.
 */
type BankDetailsFormGroupInput = IBankDetails | PartialWithRequiredKeyOf<NewBankDetails>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IBankDetails | NewBankDetails> = Omit<T, 'foundationYear'> & {
  foundationYear?: string | null;
};

type BankDetailsFormRawValue = FormValueOf<IBankDetails>;

type NewBankDetailsFormRawValue = FormValueOf<NewBankDetails>;

type BankDetailsFormDefaults = Pick<NewBankDetails, 'id' | 'foundationYear'>;

type BankDetailsFormGroupContent = {
  id: FormControl<BankDetailsFormRawValue['id'] | NewBankDetails['id']>;
  name: FormControl<BankDetailsFormRawValue['name']>;
  foundationYear: FormControl<BankDetailsFormRawValue['foundationYear']>;
  ceo: FormControl<BankDetailsFormRawValue['ceo']>;
  mainWebsite: FormControl<BankDetailsFormRawValue['mainWebsite']>;
};

export type BankDetailsFormGroup = FormGroup<BankDetailsFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BankDetailsFormService {
  createBankDetailsFormGroup(bankDetails: BankDetailsFormGroupInput = { id: null }): BankDetailsFormGroup {
    const bankDetailsRawValue = this.convertBankDetailsToBankDetailsRawValue({
      ...this.getFormDefaults(),
      ...bankDetails,
    });
    return new FormGroup<BankDetailsFormGroupContent>({
      id: new FormControl(
        { value: bankDetailsRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(bankDetailsRawValue.name, {
        validators: [Validators.required],
      }),
      foundationYear: new FormControl(bankDetailsRawValue.foundationYear),
      ceo: new FormControl(bankDetailsRawValue.ceo),
      mainWebsite: new FormControl(bankDetailsRawValue.mainWebsite),
    });
  }

  getBankDetails(form: BankDetailsFormGroup): IBankDetails | NewBankDetails {
    return this.convertBankDetailsRawValueToBankDetails(form.getRawValue() as BankDetailsFormRawValue | NewBankDetailsFormRawValue);
  }

  resetForm(form: BankDetailsFormGroup, bankDetails: BankDetailsFormGroupInput): void {
    const bankDetailsRawValue = this.convertBankDetailsToBankDetailsRawValue({ ...this.getFormDefaults(), ...bankDetails });
    form.reset(
      {
        ...bankDetailsRawValue,
        id: { value: bankDetailsRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): BankDetailsFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      foundationYear: currentTime,
    };
  }

  private convertBankDetailsRawValueToBankDetails(
    rawBankDetails: BankDetailsFormRawValue | NewBankDetailsFormRawValue,
  ): IBankDetails | NewBankDetails {
    return {
      ...rawBankDetails,
      foundationYear: dayjs(rawBankDetails.foundationYear, DATE_TIME_FORMAT),
    };
  }

  private convertBankDetailsToBankDetailsRawValue(
    bankDetails: IBankDetails | (Partial<NewBankDetails> & BankDetailsFormDefaults),
  ): BankDetailsFormRawValue | PartialWithRequiredKeyOf<NewBankDetailsFormRawValue> {
    return {
      ...bankDetails,
      foundationYear: bankDetails.foundationYear ? bankDetails.foundationYear.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
