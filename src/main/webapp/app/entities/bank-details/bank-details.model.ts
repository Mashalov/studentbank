import dayjs from 'dayjs/esm';

export interface IBankDetails {
  id: number;
  name?: string | null;
  foundationYear?: dayjs.Dayjs | null;
  ceo?: string | null;
  mainWebsite?: string | null;
}

export type NewBankDetails = Omit<IBankDetails, 'id'> & { id: null };
