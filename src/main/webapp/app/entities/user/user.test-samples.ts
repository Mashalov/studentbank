import { IUser } from './user.model';

export const sampleWithRequiredData: IUser = {
  id: 9364,
  login: '!G@qn\\ODq\\An\\8sP4hc',
};

export const sampleWithPartialData: IUser = {
  id: 27210,
  login: 'GkCFT@xsU2T',
};

export const sampleWithFullData: IUser = {
  id: 3437,
  login: 'Av0|X@AW\\B8\\H6l\\WlpQvp\\WV\\+x',
};
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
