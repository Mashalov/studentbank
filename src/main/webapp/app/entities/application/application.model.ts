import { ICustomer } from 'app/entities/customer/customer.model';

export interface IApplication {
  id: number;
  customer?: Pick<ICustomer, 'id'> | null;
}

export type NewApplication = Omit<IApplication, 'id'> & { id: null };
