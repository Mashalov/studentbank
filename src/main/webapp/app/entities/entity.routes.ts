import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'authority',
    data: { pageTitle: 'studentBankJhipsterApp.adminAuthority.home.title' },
    loadChildren: () => import('./admin/authority/authority.routes'),
  },
  {
    path: 'address',
    data: { pageTitle: 'studentBankJhipsterApp.address.home.title' },
    loadChildren: () => import('./address/address.routes'),
  },
  {
    path: 'application',
    data: { pageTitle: 'studentBankJhipsterApp.application.home.title' },
    loadChildren: () => import('./application/application.routes'),
  },
  {
    path: 'document',
    data: { pageTitle: 'studentBankJhipsterApp.document.home.title' },
    loadChildren: () => import('./document/document.routes'),
  },
  {
    path: 'atm',
    data: { pageTitle: 'studentBankJhipsterApp.atm.home.title' },
    loadChildren: () => import('./atm/atm.routes'),
  },
  {
    path: 'bank-details',
    data: { pageTitle: 'studentBankJhipsterApp.bankDetails.home.title' },
    loadChildren: () => import('./bank-details/bank-details.routes'),
  },
  {
    path: 'card-details',
    data: { pageTitle: 'studentBankJhipsterApp.cardDetails.home.title' },
    loadChildren: () => import('./card-details/card-details.routes'),
  },
  {
    path: 'contact-details',
    data: { pageTitle: 'studentBankJhipsterApp.contactDetails.home.title' },
    loadChildren: () => import('./contact-details/contact-details.routes'),
  },
  {
    path: 'customer',
    data: { pageTitle: 'studentBankJhipsterApp.customer.home.title' },
    loadChildren: () => import('./customer/customer.routes'),
  },
  {
    path: 'document-category',
    data: { pageTitle: 'studentBankJhipsterApp.documentCategory.home.title' },
    loadChildren: () => import('./document-category/document-category.routes'),
  },
  {
    path: 'employee',
    data: { pageTitle: 'studentBankJhipsterApp.employee.home.title' },
    loadChildren: () => import('./employee/employee.routes'),
  },
  {
    path: 'employees-assets',
    data: { pageTitle: 'studentBankJhipsterApp.employeesAssets.home.title' },
    loadChildren: () => import('./employees-assets/employees-assets.routes'),
  },
  {
    path: 'exchange',
    data: { pageTitle: 'studentBankJhipsterApp.exchange.home.title' },
    loadChildren: () => import('./exchange/exchange.routes'),
  },
  {
    path: 'office',
    data: { pageTitle: 'studentBankJhipsterApp.office.home.title' },
    loadChildren: () => import('./office/office.routes'),
  },
  {
    path: 'job-position',
    data: { pageTitle: 'studentBankJhipsterApp.jobPosition.home.title' },
    loadChildren: () => import('./job-position/job-position.routes'),
  },
  {
    path: 'payment-account',
    data: { pageTitle: 'studentBankJhipsterApp.paymentAccount.home.title' },
    loadChildren: () => import('./payment-account/payment-account.routes'),
  },
  {
    path: 'account-transaction',
    data: { pageTitle: 'studentBankJhipsterApp.accountTransaction.home.title' },
    loadChildren: () => import('./account-transaction/account-transaction.routes'),
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

export default routes;
