import { IOffice, NewOffice } from './office.model';

export const sampleWithRequiredData: IOffice = {
  id: 2634,
  name: 'yippee',
  openningTime: 'onto concerning',
  closingTime: 'inside silently than',
};

export const sampleWithPartialData: IOffice = {
  id: 15198,
  name: 'rigidly meh',
  isMain: false,
  openningTime: 'rhapsodize uh-huh meanwhile',
  closingTime: 'nor jubilantly hm',
};

export const sampleWithFullData: IOffice = {
  id: 29610,
  name: 'spy',
  isMain: true,
  openningTime: 'analogy lest indeed',
  closingTime: 'monthly',
};

export const sampleWithNewData: NewOffice = {
  name: 'enroll',
  openningTime: 'erode',
  closingTime: 'versus er geez',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
