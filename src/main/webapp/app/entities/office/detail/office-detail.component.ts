import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IOffice } from '../office.model';

@Component({
  standalone: true,
  selector: 'jhi-office-detail',
  templateUrl: './office-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class OfficeDetailComponent {
  office = input<IOffice | null>(null);

  previousState(): void {
    window.history.back();
  }
}
