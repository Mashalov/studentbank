import { IAddress } from 'app/entities/address/address.model';
import { IBankDetails } from 'app/entities/bank-details/bank-details.model';

export interface IOffice {
  id: number;
  name?: string | null;
  isMain?: boolean | null;
  openningTime?: string | null;
  closingTime?: string | null;
  address?: Pick<IAddress, 'id'> | null;
  bankDetails?: Pick<IBankDetails, 'id'> | null;
}

export type NewOffice = Omit<IOffice, 'id'> & { id: null };
