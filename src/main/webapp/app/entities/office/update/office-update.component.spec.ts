import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IAddress } from 'app/entities/address/address.model';
import { AddressService } from 'app/entities/address/service/address.service';
import { IBankDetails } from 'app/entities/bank-details/bank-details.model';
import { BankDetailsService } from 'app/entities/bank-details/service/bank-details.service';
import { IOffice } from '../office.model';
import { OfficeService } from '../service/office.service';
import { OfficeFormService } from './office-form.service';

import { OfficeUpdateComponent } from './office-update.component';

describe('Office Management Update Component', () => {
  let comp: OfficeUpdateComponent;
  let fixture: ComponentFixture<OfficeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let officeFormService: OfficeFormService;
  let officeService: OfficeService;
  let addressService: AddressService;
  let bankDetailsService: BankDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OfficeUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OfficeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OfficeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    officeFormService = TestBed.inject(OfficeFormService);
    officeService = TestBed.inject(OfficeService);
    addressService = TestBed.inject(AddressService);
    bankDetailsService = TestBed.inject(BankDetailsService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Address query and add missing value', () => {
      const office: IOffice = { id: 456 };
      const address: IAddress = { id: 5151 };
      office.address = address;

      const addressCollection: IAddress[] = [{ id: 14658 }];
      jest.spyOn(addressService, 'query').mockReturnValue(of(new HttpResponse({ body: addressCollection })));
      const additionalAddresses = [address];
      const expectedCollection: IAddress[] = [...additionalAddresses, ...addressCollection];
      jest.spyOn(addressService, 'addAddressToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ office });
      comp.ngOnInit();

      expect(addressService.query).toHaveBeenCalled();
      expect(addressService.addAddressToCollectionIfMissing).toHaveBeenCalledWith(
        addressCollection,
        ...additionalAddresses.map(expect.objectContaining),
      );
      expect(comp.addressesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call BankDetails query and add missing value', () => {
      const office: IOffice = { id: 456 };
      const bankDetails: IBankDetails = { id: 16992 };
      office.bankDetails = bankDetails;

      const bankDetailsCollection: IBankDetails[] = [{ id: 14253 }];
      jest.spyOn(bankDetailsService, 'query').mockReturnValue(of(new HttpResponse({ body: bankDetailsCollection })));
      const additionalBankDetails = [bankDetails];
      const expectedCollection: IBankDetails[] = [...additionalBankDetails, ...bankDetailsCollection];
      jest.spyOn(bankDetailsService, 'addBankDetailsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ office });
      comp.ngOnInit();

      expect(bankDetailsService.query).toHaveBeenCalled();
      expect(bankDetailsService.addBankDetailsToCollectionIfMissing).toHaveBeenCalledWith(
        bankDetailsCollection,
        ...additionalBankDetails.map(expect.objectContaining),
      );
      expect(comp.bankDetailsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const office: IOffice = { id: 456 };
      const address: IAddress = { id: 6949 };
      office.address = address;
      const bankDetails: IBankDetails = { id: 10233 };
      office.bankDetails = bankDetails;

      activatedRoute.data = of({ office });
      comp.ngOnInit();

      expect(comp.addressesSharedCollection).toContain(address);
      expect(comp.bankDetailsSharedCollection).toContain(bankDetails);
      expect(comp.office).toEqual(office);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOffice>>();
      const office = { id: 123 };
      jest.spyOn(officeFormService, 'getOffice').mockReturnValue(office);
      jest.spyOn(officeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ office });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: office }));
      saveSubject.complete();

      // THEN
      expect(officeFormService.getOffice).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(officeService.update).toHaveBeenCalledWith(expect.objectContaining(office));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOffice>>();
      const office = { id: 123 };
      jest.spyOn(officeFormService, 'getOffice').mockReturnValue({ id: null });
      jest.spyOn(officeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ office: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: office }));
      saveSubject.complete();

      // THEN
      expect(officeFormService.getOffice).toHaveBeenCalled();
      expect(officeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOffice>>();
      const office = { id: 123 };
      jest.spyOn(officeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ office });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(officeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAddress', () => {
      it('Should forward to addressService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(addressService, 'compareAddress');
        comp.compareAddress(entity, entity2);
        expect(addressService.compareAddress).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBankDetails', () => {
      it('Should forward to bankDetailsService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(bankDetailsService, 'compareBankDetails');
        comp.compareBankDetails(entity, entity2);
        expect(bankDetailsService.compareBankDetails).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
