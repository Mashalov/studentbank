import { ICustomer } from 'app/entities/customer/customer.model';
import { Country } from 'app/entities/enumerations/country.model';

export interface IAddress {
  id: number;
  country?: keyof typeof Country | null;
  region?: string | null;
  city?: string | null;
  street?: string | null;
  zipCode?: number | null;
  customer?: Pick<ICustomer, 'id'> | null;
}

export type NewAddress = Omit<IAddress, 'id'> & { id: null };
