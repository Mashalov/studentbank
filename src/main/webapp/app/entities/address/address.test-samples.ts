import { IAddress, NewAddress } from './address.model';

export const sampleWithRequiredData: IAddress = {
  id: 21853,
  region: 'whoever under',
  city: 'Garetttown',
  street: 'Mill Road',
  zipCode: 8441,
};

export const sampleWithPartialData: IAddress = {
  id: 2646,
  country: 'Bulgaria',
  region: 'instead',
  city: 'Livermore',
  street: 'W Front Street',
  zipCode: 2982,
};

export const sampleWithFullData: IAddress = {
  id: 20839,
  country: 'Germany',
  region: 'yum',
  city: 'Ornberg',
  street: 'Station Street',
  zipCode: 12760,
};

export const sampleWithNewData: NewAddress = {
  region: 'ha blah',
  city: 'Hickleberg',
  street: 'Toy Mews',
  zipCode: 14647,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
