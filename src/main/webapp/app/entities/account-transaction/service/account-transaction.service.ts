import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAccountTransaction, NewAccountTransaction } from '../account-transaction.model';

export type PartialUpdateAccountTransaction = Partial<IAccountTransaction> & Pick<IAccountTransaction, 'id'>;

type RestOf<T extends IAccountTransaction | NewAccountTransaction> = Omit<T, 'sendTime' | 'receiveTime'> & {
  sendTime?: string | null;
  receiveTime?: string | null;
};

export type RestAccountTransaction = RestOf<IAccountTransaction>;

export type NewRestAccountTransaction = RestOf<NewAccountTransaction>;

export type PartialUpdateRestAccountTransaction = RestOf<PartialUpdateAccountTransaction>;

export type EntityResponseType = HttpResponse<IAccountTransaction>;
export type EntityArrayResponseType = HttpResponse<IAccountTransaction[]>;

@Injectable({ providedIn: 'root' })
export class AccountTransactionService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/account-transactions');

  create(accountTransaction: NewAccountTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(accountTransaction);
    return this.http
      .post<RestAccountTransaction>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(accountTransaction: IAccountTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(accountTransaction);
    return this.http
      .put<RestAccountTransaction>(`${this.resourceUrl}/${this.getAccountTransactionIdentifier(accountTransaction)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(accountTransaction: PartialUpdateAccountTransaction): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(accountTransaction);
    return this.http
      .patch<RestAccountTransaction>(`${this.resourceUrl}/${this.getAccountTransactionIdentifier(accountTransaction)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAccountTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAccountTransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAccountTransactionIdentifier(accountTransaction: Pick<IAccountTransaction, 'id'>): number {
    return accountTransaction.id;
  }

  compareAccountTransaction(o1: Pick<IAccountTransaction, 'id'> | null, o2: Pick<IAccountTransaction, 'id'> | null): boolean {
    return o1 && o2 ? this.getAccountTransactionIdentifier(o1) === this.getAccountTransactionIdentifier(o2) : o1 === o2;
  }

  addAccountTransactionToCollectionIfMissing<Type extends Pick<IAccountTransaction, 'id'>>(
    accountTransactionCollection: Type[],
    ...accountTransactionsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const accountTransactions: Type[] = accountTransactionsToCheck.filter(isPresent);
    if (accountTransactions.length > 0) {
      const accountTransactionCollectionIdentifiers = accountTransactionCollection.map(accountTransactionItem =>
        this.getAccountTransactionIdentifier(accountTransactionItem),
      );
      const accountTransactionsToAdd = accountTransactions.filter(accountTransactionItem => {
        const accountTransactionIdentifier = this.getAccountTransactionIdentifier(accountTransactionItem);
        if (accountTransactionCollectionIdentifiers.includes(accountTransactionIdentifier)) {
          return false;
        }
        accountTransactionCollectionIdentifiers.push(accountTransactionIdentifier);
        return true;
      });
      return [...accountTransactionsToAdd, ...accountTransactionCollection];
    }
    return accountTransactionCollection;
  }

  protected convertDateFromClient<T extends IAccountTransaction | NewAccountTransaction | PartialUpdateAccountTransaction>(
    accountTransaction: T,
  ): RestOf<T> {
    return {
      ...accountTransaction,
      sendTime: accountTransaction.sendTime?.toJSON() ?? null,
      receiveTime: accountTransaction.receiveTime?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restAccountTransaction: RestAccountTransaction): IAccountTransaction {
    return {
      ...restAccountTransaction,
      sendTime: restAccountTransaction.sendTime ? dayjs(restAccountTransaction.sendTime) : undefined,
      receiveTime: restAccountTransaction.receiveTime ? dayjs(restAccountTransaction.receiveTime) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAccountTransaction>): HttpResponse<IAccountTransaction> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAccountTransaction[]>): HttpResponse<IAccountTransaction[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
