import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAccountTransaction } from '../account-transaction.model';
import { AccountTransactionService } from '../service/account-transaction.service';

const accountTransactionResolve = (route: ActivatedRouteSnapshot): Observable<null | IAccountTransaction> => {
  const id = route.params['id'];
  if (id) {
    return inject(AccountTransactionService)
      .find(id)
      .pipe(
        mergeMap((accountTransaction: HttpResponse<IAccountTransaction>) => {
          if (accountTransaction.body) {
            return of(accountTransaction.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default accountTransactionResolve;
