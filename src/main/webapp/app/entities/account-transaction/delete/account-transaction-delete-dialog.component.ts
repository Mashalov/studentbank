import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IAccountTransaction } from '../account-transaction.model';
import { AccountTransactionService } from '../service/account-transaction.service';

@Component({
  standalone: true,
  templateUrl: './account-transaction-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class AccountTransactionDeleteDialogComponent {
  accountTransaction?: IAccountTransaction;

  protected accountTransactionService = inject(AccountTransactionService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.accountTransactionService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
