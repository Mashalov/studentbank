import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { AccountTransactionComponent } from './list/account-transaction.component';
import { AccountTransactionDetailComponent } from './detail/account-transaction-detail.component';
import { AccountTransactionUpdateComponent } from './update/account-transaction-update.component';
import AccountTransactionResolve from './route/account-transaction-routing-resolve.service';

const accountTransactionRoute: Routes = [
  {
    path: '',
    component: AccountTransactionComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AccountTransactionDetailComponent,
    resolve: {
      accountTransaction: AccountTransactionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AccountTransactionUpdateComponent,
    resolve: {
      accountTransaction: AccountTransactionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AccountTransactionUpdateComponent,
    resolve: {
      accountTransaction: AccountTransactionResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default accountTransactionRoute;
