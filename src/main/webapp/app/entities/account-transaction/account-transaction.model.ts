import dayjs from 'dayjs/esm';
import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { TransactionOperationType } from 'app/entities/enumerations/transaction-operation-type.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { TransactionLocation } from 'app/entities/enumerations/transaction-location.model';

export interface IAccountTransaction {
  id: number;
  externalId?: string | null;
  transactionOperationType?: keyof typeof TransactionOperationType | null;
  amount?: number | null;
  currency?: keyof typeof Currency | null;
  location?: keyof typeof TransactionLocation | null;
  sendTime?: dayjs.Dayjs | null;
  receiveTime?: dayjs.Dayjs | null;
  sender?: Pick<IPaymentAccount, 'id'> | null;
  receiver?: Pick<IPaymentAccount, 'id'> | null;
}

export type NewAccountTransaction = Omit<IAccountTransaction, 'id'> & { id: null };
