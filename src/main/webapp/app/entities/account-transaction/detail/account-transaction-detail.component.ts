import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IAccountTransaction } from '../account-transaction.model';

@Component({
  standalone: true,
  selector: 'jhi-account-transaction-detail',
  templateUrl: './account-transaction-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class AccountTransactionDetailComponent {
  accountTransaction = input<IAccountTransaction | null>(null);

  previousState(): void {
    window.history.back();
  }
}
