import dayjs from 'dayjs/esm';

import { IAccountTransaction, NewAccountTransaction } from './account-transaction.model';

export const sampleWithRequiredData: IAccountTransaction = {
  id: 2045,
  transactionOperationType: 'TRANSFER',
  amount: 31413.14,
  currency: 'EUR',
  location: 'ATM',
};

export const sampleWithPartialData: IAccountTransaction = {
  id: 8324,
  externalId: 'honeybee snake but',
  transactionOperationType: 'CREDIT',
  amount: 30013.8,
  currency: 'EUR',
  location: 'OFFICE',
  sendTime: dayjs('2024-07-06T14:36'),
};

export const sampleWithFullData: IAccountTransaction = {
  id: 28366,
  externalId: 'wrongly under',
  transactionOperationType: 'DEPOSIT',
  amount: 21796.27,
  currency: 'EUR',
  location: 'ATM',
  sendTime: dayjs('2024-07-05T20:53'),
  receiveTime: dayjs('2024-07-06T09:55'),
};

export const sampleWithNewData: NewAccountTransaction = {
  transactionOperationType: 'DEPOSIT',
  amount: 10890.21,
  currency: 'BGN',
  location: 'ATM',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
