import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { PaymentAccountService } from 'app/entities/payment-account/service/payment-account.service';
import { TransactionOperationType } from 'app/entities/enumerations/transaction-operation-type.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { TransactionLocation } from 'app/entities/enumerations/transaction-location.model';
import { AccountTransactionService } from '../service/account-transaction.service';
import { IAccountTransaction } from '../account-transaction.model';
import { AccountTransactionFormService, AccountTransactionFormGroup } from './account-transaction-form.service';

@Component({
  standalone: true,
  selector: 'jhi-account-transaction-update',
  templateUrl: './account-transaction-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class AccountTransactionUpdateComponent implements OnInit {
  isSaving = false;
  accountTransaction: IAccountTransaction | null = null;
  transactionOperationTypeValues = Object.keys(TransactionOperationType);
  currencyValues = Object.keys(Currency);
  transactionLocationValues = Object.keys(TransactionLocation);

  paymentAccountsSharedCollection: IPaymentAccount[] = [];

  protected accountTransactionService = inject(AccountTransactionService);
  protected accountTransactionFormService = inject(AccountTransactionFormService);
  protected paymentAccountService = inject(PaymentAccountService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: AccountTransactionFormGroup = this.accountTransactionFormService.createAccountTransactionFormGroup();

  comparePaymentAccount = (o1: IPaymentAccount | null, o2: IPaymentAccount | null): boolean =>
    this.paymentAccountService.comparePaymentAccount(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accountTransaction }) => {
      this.accountTransaction = accountTransaction;
      if (accountTransaction) {
        this.updateForm(accountTransaction);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accountTransaction = this.accountTransactionFormService.getAccountTransaction(this.editForm);
    if (accountTransaction.id !== null) {
      this.subscribeToSaveResponse(this.accountTransactionService.update(accountTransaction));
    } else {
      this.subscribeToSaveResponse(this.accountTransactionService.create(accountTransaction));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccountTransaction>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(accountTransaction: IAccountTransaction): void {
    this.accountTransaction = accountTransaction;
    this.accountTransactionFormService.resetForm(this.editForm, accountTransaction);

    this.paymentAccountsSharedCollection = this.paymentAccountService.addPaymentAccountToCollectionIfMissing<IPaymentAccount>(
      this.paymentAccountsSharedCollection,
      accountTransaction.sender,
      accountTransaction.receiver,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.paymentAccountService
      .query()
      .pipe(map((res: HttpResponse<IPaymentAccount[]>) => res.body ?? []))
      .pipe(
        map((paymentAccounts: IPaymentAccount[]) =>
          this.paymentAccountService.addPaymentAccountToCollectionIfMissing<IPaymentAccount>(
            paymentAccounts,
            this.accountTransaction?.sender,
            this.accountTransaction?.receiver,
          ),
        ),
      )
      .subscribe((paymentAccounts: IPaymentAccount[]) => (this.paymentAccountsSharedCollection = paymentAccounts));
  }
}
