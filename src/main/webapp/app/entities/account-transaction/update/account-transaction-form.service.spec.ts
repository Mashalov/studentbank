import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../account-transaction.test-samples';

import { AccountTransactionFormService } from './account-transaction-form.service';

describe('AccountTransaction Form Service', () => {
  let service: AccountTransactionFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountTransactionFormService);
  });

  describe('Service methods', () => {
    describe('createAccountTransactionFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAccountTransactionFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            externalId: expect.any(Object),
            transactionOperationType: expect.any(Object),
            amount: expect.any(Object),
            currency: expect.any(Object),
            location: expect.any(Object),
            sendTime: expect.any(Object),
            receiveTime: expect.any(Object),
            sender: expect.any(Object),
            receiver: expect.any(Object),
          }),
        );
      });

      it('passing IAccountTransaction should create a new form with FormGroup', () => {
        const formGroup = service.createAccountTransactionFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            externalId: expect.any(Object),
            transactionOperationType: expect.any(Object),
            amount: expect.any(Object),
            currency: expect.any(Object),
            location: expect.any(Object),
            sendTime: expect.any(Object),
            receiveTime: expect.any(Object),
            sender: expect.any(Object),
            receiver: expect.any(Object),
          }),
        );
      });
    });

    describe('getAccountTransaction', () => {
      it('should return NewAccountTransaction for default AccountTransaction initial value', () => {
        const formGroup = service.createAccountTransactionFormGroup(sampleWithNewData);

        const accountTransaction = service.getAccountTransaction(formGroup) as any;

        expect(accountTransaction).toMatchObject(sampleWithNewData);
      });

      it('should return NewAccountTransaction for empty AccountTransaction initial value', () => {
        const formGroup = service.createAccountTransactionFormGroup();

        const accountTransaction = service.getAccountTransaction(formGroup) as any;

        expect(accountTransaction).toMatchObject({});
      });

      it('should return IAccountTransaction', () => {
        const formGroup = service.createAccountTransactionFormGroup(sampleWithRequiredData);

        const accountTransaction = service.getAccountTransaction(formGroup) as any;

        expect(accountTransaction).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAccountTransaction should not enable id FormControl', () => {
        const formGroup = service.createAccountTransactionFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAccountTransaction should disable id FormControl', () => {
        const formGroup = service.createAccountTransactionFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
