import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { PaymentAccountService } from 'app/entities/payment-account/service/payment-account.service';
import { AccountTransactionService } from '../service/account-transaction.service';
import { IAccountTransaction } from '../account-transaction.model';
import { AccountTransactionFormService } from './account-transaction-form.service';

import { AccountTransactionUpdateComponent } from './account-transaction-update.component';

describe('AccountTransaction Management Update Component', () => {
  let comp: AccountTransactionUpdateComponent;
  let fixture: ComponentFixture<AccountTransactionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let accountTransactionFormService: AccountTransactionFormService;
  let accountTransactionService: AccountTransactionService;
  let paymentAccountService: PaymentAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AccountTransactionUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AccountTransactionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AccountTransactionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    accountTransactionFormService = TestBed.inject(AccountTransactionFormService);
    accountTransactionService = TestBed.inject(AccountTransactionService);
    paymentAccountService = TestBed.inject(PaymentAccountService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call PaymentAccount query and add missing value', () => {
      const accountTransaction: IAccountTransaction = { id: 456 };
      const sender: IPaymentAccount = { id: 3122 };
      accountTransaction.sender = sender;
      const receiver: IPaymentAccount = { id: 13286 };
      accountTransaction.receiver = receiver;

      const paymentAccountCollection: IPaymentAccount[] = [{ id: 10124 }];
      jest.spyOn(paymentAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: paymentAccountCollection })));
      const additionalPaymentAccounts = [sender, receiver];
      const expectedCollection: IPaymentAccount[] = [...additionalPaymentAccounts, ...paymentAccountCollection];
      jest.spyOn(paymentAccountService, 'addPaymentAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ accountTransaction });
      comp.ngOnInit();

      expect(paymentAccountService.query).toHaveBeenCalled();
      expect(paymentAccountService.addPaymentAccountToCollectionIfMissing).toHaveBeenCalledWith(
        paymentAccountCollection,
        ...additionalPaymentAccounts.map(expect.objectContaining),
      );
      expect(comp.paymentAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const accountTransaction: IAccountTransaction = { id: 456 };
      const sender: IPaymentAccount = { id: 32324 };
      accountTransaction.sender = sender;
      const receiver: IPaymentAccount = { id: 527 };
      accountTransaction.receiver = receiver;

      activatedRoute.data = of({ accountTransaction });
      comp.ngOnInit();

      expect(comp.paymentAccountsSharedCollection).toContain(sender);
      expect(comp.paymentAccountsSharedCollection).toContain(receiver);
      expect(comp.accountTransaction).toEqual(accountTransaction);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountTransaction>>();
      const accountTransaction = { id: 123 };
      jest.spyOn(accountTransactionFormService, 'getAccountTransaction').mockReturnValue(accountTransaction);
      jest.spyOn(accountTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountTransaction }));
      saveSubject.complete();

      // THEN
      expect(accountTransactionFormService.getAccountTransaction).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(accountTransactionService.update).toHaveBeenCalledWith(expect.objectContaining(accountTransaction));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountTransaction>>();
      const accountTransaction = { id: 123 };
      jest.spyOn(accountTransactionFormService, 'getAccountTransaction').mockReturnValue({ id: null });
      jest.spyOn(accountTransactionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountTransaction: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountTransaction }));
      saveSubject.complete();

      // THEN
      expect(accountTransactionFormService.getAccountTransaction).toHaveBeenCalled();
      expect(accountTransactionService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountTransaction>>();
      const accountTransaction = { id: 123 };
      jest.spyOn(accountTransactionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountTransaction });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(accountTransactionService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePaymentAccount', () => {
      it('Should forward to paymentAccountService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(paymentAccountService, 'comparePaymentAccount');
        comp.comparePaymentAccount(entity, entity2);
        expect(paymentAccountService.comparePaymentAccount).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
