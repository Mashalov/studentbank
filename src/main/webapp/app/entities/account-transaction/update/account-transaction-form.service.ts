import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAccountTransaction, NewAccountTransaction } from '../account-transaction.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAccountTransaction for edit and NewAccountTransactionFormGroupInput for create.
 */
type AccountTransactionFormGroupInput = IAccountTransaction | PartialWithRequiredKeyOf<NewAccountTransaction>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IAccountTransaction | NewAccountTransaction> = Omit<T, 'sendTime' | 'receiveTime'> & {
  sendTime?: string | null;
  receiveTime?: string | null;
};

type AccountTransactionFormRawValue = FormValueOf<IAccountTransaction>;

type NewAccountTransactionFormRawValue = FormValueOf<NewAccountTransaction>;

type AccountTransactionFormDefaults = Pick<NewAccountTransaction, 'id' | 'sendTime' | 'receiveTime'>;

type AccountTransactionFormGroupContent = {
  id: FormControl<AccountTransactionFormRawValue['id'] | NewAccountTransaction['id']>;
  externalId: FormControl<AccountTransactionFormRawValue['externalId']>;
  transactionOperationType: FormControl<AccountTransactionFormRawValue['transactionOperationType']>;
  amount: FormControl<AccountTransactionFormRawValue['amount']>;
  currency: FormControl<AccountTransactionFormRawValue['currency']>;
  location: FormControl<AccountTransactionFormRawValue['location']>;
  sendTime: FormControl<AccountTransactionFormRawValue['sendTime']>;
  receiveTime: FormControl<AccountTransactionFormRawValue['receiveTime']>;
  sender: FormControl<AccountTransactionFormRawValue['sender']>;
  receiver: FormControl<AccountTransactionFormRawValue['receiver']>;
};

export type AccountTransactionFormGroup = FormGroup<AccountTransactionFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AccountTransactionFormService {
  createAccountTransactionFormGroup(accountTransaction: AccountTransactionFormGroupInput = { id: null }): AccountTransactionFormGroup {
    const accountTransactionRawValue = this.convertAccountTransactionToAccountTransactionRawValue({
      ...this.getFormDefaults(),
      ...accountTransaction,
    });
    return new FormGroup<AccountTransactionFormGroupContent>({
      id: new FormControl(
        { value: accountTransactionRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      externalId: new FormControl(accountTransactionRawValue.externalId),
      transactionOperationType: new FormControl(accountTransactionRawValue.transactionOperationType, {
        validators: [Validators.required],
      }),
      amount: new FormControl(accountTransactionRawValue.amount, {
        validators: [Validators.required],
      }),
      currency: new FormControl(accountTransactionRawValue.currency, {
        validators: [Validators.required],
      }),
      location: new FormControl(accountTransactionRawValue.location, {
        validators: [Validators.required],
      }),
      sendTime: new FormControl(accountTransactionRawValue.sendTime),
      receiveTime: new FormControl(accountTransactionRawValue.receiveTime),
      sender: new FormControl(accountTransactionRawValue.sender),
      receiver: new FormControl(accountTransactionRawValue.receiver),
    });
  }

  getAccountTransaction(form: AccountTransactionFormGroup): IAccountTransaction | NewAccountTransaction {
    return this.convertAccountTransactionRawValueToAccountTransaction(
      form.getRawValue() as AccountTransactionFormRawValue | NewAccountTransactionFormRawValue,
    );
  }

  resetForm(form: AccountTransactionFormGroup, accountTransaction: AccountTransactionFormGroupInput): void {
    const accountTransactionRawValue = this.convertAccountTransactionToAccountTransactionRawValue({
      ...this.getFormDefaults(),
      ...accountTransaction,
    });
    form.reset(
      {
        ...accountTransactionRawValue,
        id: { value: accountTransactionRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): AccountTransactionFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      sendTime: currentTime,
      receiveTime: currentTime,
    };
  }

  private convertAccountTransactionRawValueToAccountTransaction(
    rawAccountTransaction: AccountTransactionFormRawValue | NewAccountTransactionFormRawValue,
  ): IAccountTransaction | NewAccountTransaction {
    return {
      ...rawAccountTransaction,
      sendTime: dayjs(rawAccountTransaction.sendTime, DATE_TIME_FORMAT),
      receiveTime: dayjs(rawAccountTransaction.receiveTime, DATE_TIME_FORMAT),
    };
  }

  private convertAccountTransactionToAccountTransactionRawValue(
    accountTransaction: IAccountTransaction | (Partial<NewAccountTransaction> & AccountTransactionFormDefaults),
  ): AccountTransactionFormRawValue | PartialWithRequiredKeyOf<NewAccountTransactionFormRawValue> {
    return {
      ...accountTransaction,
      sendTime: accountTransaction.sendTime ? accountTransaction.sendTime.format(DATE_TIME_FORMAT) : undefined,
      receiveTime: accountTransaction.receiveTime ? accountTransaction.receiveTime.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
