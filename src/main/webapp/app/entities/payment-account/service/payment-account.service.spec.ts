import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IPaymentAccount } from '../payment-account.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../payment-account.test-samples';

import { PaymentAccountService } from './payment-account.service';

const requireRestSample: IPaymentAccount = {
  ...sampleWithRequiredData,
};

describe('PaymentAccount Service', () => {
  let service: PaymentAccountService;
  let httpMock: HttpTestingController;
  let expectedResult: IPaymentAccount | IPaymentAccount[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(PaymentAccountService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a PaymentAccount', () => {
      const paymentAccount = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(paymentAccount).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PaymentAccount', () => {
      const paymentAccount = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(paymentAccount).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PaymentAccount', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PaymentAccount', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a PaymentAccount', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPaymentAccountToCollectionIfMissing', () => {
      it('should add a PaymentAccount to an empty array', () => {
        const paymentAccount: IPaymentAccount = sampleWithRequiredData;
        expectedResult = service.addPaymentAccountToCollectionIfMissing([], paymentAccount);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(paymentAccount);
      });

      it('should not add a PaymentAccount to an array that contains it', () => {
        const paymentAccount: IPaymentAccount = sampleWithRequiredData;
        const paymentAccountCollection: IPaymentAccount[] = [
          {
            ...paymentAccount,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPaymentAccountToCollectionIfMissing(paymentAccountCollection, paymentAccount);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PaymentAccount to an array that doesn't contain it", () => {
        const paymentAccount: IPaymentAccount = sampleWithRequiredData;
        const paymentAccountCollection: IPaymentAccount[] = [sampleWithPartialData];
        expectedResult = service.addPaymentAccountToCollectionIfMissing(paymentAccountCollection, paymentAccount);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(paymentAccount);
      });

      it('should add only unique PaymentAccount to an array', () => {
        const paymentAccountArray: IPaymentAccount[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const paymentAccountCollection: IPaymentAccount[] = [sampleWithRequiredData];
        expectedResult = service.addPaymentAccountToCollectionIfMissing(paymentAccountCollection, ...paymentAccountArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const paymentAccount: IPaymentAccount = sampleWithRequiredData;
        const paymentAccount2: IPaymentAccount = sampleWithPartialData;
        expectedResult = service.addPaymentAccountToCollectionIfMissing([], paymentAccount, paymentAccount2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(paymentAccount);
        expect(expectedResult).toContain(paymentAccount2);
      });

      it('should accept null and undefined values', () => {
        const paymentAccount: IPaymentAccount = sampleWithRequiredData;
        expectedResult = service.addPaymentAccountToCollectionIfMissing([], null, paymentAccount, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(paymentAccount);
      });

      it('should return initial array if no PaymentAccount is added', () => {
        const paymentAccountCollection: IPaymentAccount[] = [sampleWithRequiredData];
        expectedResult = service.addPaymentAccountToCollectionIfMissing(paymentAccountCollection, undefined, null);
        expect(expectedResult).toEqual(paymentAccountCollection);
      });
    });

    describe('comparePaymentAccount', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePaymentAccount(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePaymentAccount(entity1, entity2);
        const compareResult2 = service.comparePaymentAccount(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePaymentAccount(entity1, entity2);
        const compareResult2 = service.comparePaymentAccount(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePaymentAccount(entity1, entity2);
        const compareResult2 = service.comparePaymentAccount(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
