import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPaymentAccount, NewPaymentAccount } from '../payment-account.model';

export type PartialUpdatePaymentAccount = Partial<IPaymentAccount> & Pick<IPaymentAccount, 'id'>;

export type EntityResponseType = HttpResponse<IPaymentAccount>;
export type EntityArrayResponseType = HttpResponse<IPaymentAccount[]>;

@Injectable({ providedIn: 'root' })
export class PaymentAccountService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/payment-accounts');

  create(paymentAccount: NewPaymentAccount): Observable<EntityResponseType> {
    return this.http.post<IPaymentAccount>(this.resourceUrl, paymentAccount, { observe: 'response' });
  }

  update(paymentAccount: IPaymentAccount): Observable<EntityResponseType> {
    return this.http.put<IPaymentAccount>(`${this.resourceUrl}/${this.getPaymentAccountIdentifier(paymentAccount)}`, paymentAccount, {
      observe: 'response',
    });
  }

  partialUpdate(paymentAccount: PartialUpdatePaymentAccount): Observable<EntityResponseType> {
    return this.http.patch<IPaymentAccount>(`${this.resourceUrl}/${this.getPaymentAccountIdentifier(paymentAccount)}`, paymentAccount, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPaymentAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPaymentAccount[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPaymentAccountIdentifier(paymentAccount: Pick<IPaymentAccount, 'id'>): number {
    return paymentAccount.id;
  }

  comparePaymentAccount(o1: Pick<IPaymentAccount, 'id'> | null, o2: Pick<IPaymentAccount, 'id'> | null): boolean {
    return o1 && o2 ? this.getPaymentAccountIdentifier(o1) === this.getPaymentAccountIdentifier(o2) : o1 === o2;
  }

  addPaymentAccountToCollectionIfMissing<Type extends Pick<IPaymentAccount, 'id'>>(
    paymentAccountCollection: Type[],
    ...paymentAccountsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const paymentAccounts: Type[] = paymentAccountsToCheck.filter(isPresent);
    if (paymentAccounts.length > 0) {
      const paymentAccountCollectionIdentifiers = paymentAccountCollection.map(paymentAccountItem =>
        this.getPaymentAccountIdentifier(paymentAccountItem),
      );
      const paymentAccountsToAdd = paymentAccounts.filter(paymentAccountItem => {
        const paymentAccountIdentifier = this.getPaymentAccountIdentifier(paymentAccountItem);
        if (paymentAccountCollectionIdentifiers.includes(paymentAccountIdentifier)) {
          return false;
        }
        paymentAccountCollectionIdentifiers.push(paymentAccountIdentifier);
        return true;
      });
      return [...paymentAccountsToAdd, ...paymentAccountCollection];
    }
    return paymentAccountCollection;
  }
}
