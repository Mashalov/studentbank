import { IPaymentAccount, NewPaymentAccount } from './payment-account.model';

export const sampleWithRequiredData: IPaymentAccount = {
  id: 7266,
  paymentAccountType: 'SAVING_ACCOUNT',
  iban: 'QA76KFEQ9930V3T274Y59190I36D6',
  amount: 15129.11,
  currency: 'BGN',
};

export const sampleWithPartialData: IPaymentAccount = {
  id: 32700,
  paymentAccountType: 'SAVING_ACCOUNT',
  iban: 'SE1668288080812500335002',
  amount: 21065.12,
  currency: 'BGN',
};

export const sampleWithFullData: IPaymentAccount = {
  id: 10941,
  paymentAccountType: 'CREDIT_ACCOUNT',
  iban: 'GI08AOUKM6N228108868S50',
  amount: 18276.87,
  currency: 'EUR',
  active: false,
  deleted: false,
};

export const sampleWithNewData: NewPaymentAccount = {
  paymentAccountType: 'OVERDRAFT',
  iban: 'GI27NWAA3922356M64A8072',
  amount: 31481.31,
  currency: 'BGN',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
