import { IDocument } from 'app/entities/document/document.model';
import { ICustomer } from 'app/entities/customer/customer.model';
import { PaymentAccountType } from 'app/entities/enumerations/payment-account-type.model';
import { Currency } from 'app/entities/enumerations/currency.model';

export interface IPaymentAccount {
  id: number;
  paymentAccountType?: keyof typeof PaymentAccountType | null;
  iban?: string | null;
  amount?: number | null;
  currency?: keyof typeof Currency | null;
  active?: boolean | null;
  deleted?: boolean | null;
  contract?: Pick<IDocument, 'id'> | null;
  customer?: ICustomer | null;
}

export type NewPaymentAccount = Omit<IPaymentAccount, 'id'> & { id: null };
