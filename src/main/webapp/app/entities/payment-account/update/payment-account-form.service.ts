import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPaymentAccount, NewPaymentAccount } from '../payment-account.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPaymentAccount for edit and NewPaymentAccountFormGroupInput for create.
 */
type PaymentAccountFormGroupInput = IPaymentAccount | PartialWithRequiredKeyOf<NewPaymentAccount>;

type PaymentAccountFormDefaults = Pick<NewPaymentAccount, 'id' | 'active' | 'deleted'>;

type PaymentAccountFormGroupContent = {
  id: FormControl<IPaymentAccount['id'] | NewPaymentAccount['id']>;
  paymentAccountType: FormControl<IPaymentAccount['paymentAccountType']>;
  iban: FormControl<IPaymentAccount['iban']>;
  amount: FormControl<IPaymentAccount['amount']>;
  currency: FormControl<IPaymentAccount['currency']>;
  active: FormControl<IPaymentAccount['active']>;
  deleted: FormControl<IPaymentAccount['deleted']>;
  contract: FormControl<IPaymentAccount['contract']>;
  customer: FormControl<IPaymentAccount['customer']>;
};

export type PaymentAccountFormGroup = FormGroup<PaymentAccountFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PaymentAccountFormService {
  createPaymentAccountFormGroup(paymentAccount: PaymentAccountFormGroupInput = { id: null }): PaymentAccountFormGroup {
    const paymentAccountRawValue = {
      ...this.getFormDefaults(),
      ...paymentAccount,
    };
    return new FormGroup<PaymentAccountFormGroupContent>({
      id: new FormControl(
        { value: paymentAccountRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      paymentAccountType: new FormControl(paymentAccountRawValue.paymentAccountType, {
        validators: [Validators.required],
      }),
      iban: new FormControl(paymentAccountRawValue.iban, {
        validators: [Validators.required],
      }),
      amount: new FormControl(paymentAccountRawValue.amount, {
        validators: [Validators.required],
      }),
      currency: new FormControl(paymentAccountRawValue.currency, {
        validators: [Validators.required],
      }),
      active: new FormControl(paymentAccountRawValue.active),
      deleted: new FormControl(paymentAccountRawValue.deleted),
      contract: new FormControl(paymentAccountRawValue.contract),
      customer: new FormControl(paymentAccountRawValue.customer),
    });
  }

  getPaymentAccount(form: PaymentAccountFormGroup): IPaymentAccount | NewPaymentAccount {
    return form.getRawValue() as IPaymentAccount | NewPaymentAccount;
  }

  resetForm(form: PaymentAccountFormGroup, paymentAccount: PaymentAccountFormGroupInput): void {
    const paymentAccountRawValue = { ...this.getFormDefaults(), ...paymentAccount };
    form.reset(
      {
        ...paymentAccountRawValue,
        id: { value: paymentAccountRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): PaymentAccountFormDefaults {
    return {
      id: null,
      active: false,
      deleted: false,
    };
  }
}
