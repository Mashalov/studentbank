import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { PaymentAccountType } from 'app/entities/enumerations/payment-account-type.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { PaymentAccountService } from '../service/payment-account.service';
import { IPaymentAccount } from '../payment-account.model';
import { PaymentAccountFormService, PaymentAccountFormGroup } from './payment-account-form.service';

@Component({
  standalone: true,
  selector: 'jhi-payment-account-update',
  templateUrl: './payment-account-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class PaymentAccountUpdateComponent implements OnInit {
  isSaving = false;
  paymentAccount: IPaymentAccount | null = null;
  paymentAccountTypeValues = Object.keys(PaymentAccountType);
  currencyValues = Object.keys(Currency);

  documentsSharedCollection: IDocument[] = [];
  customersSharedCollection: ICustomer[] = [];

  protected paymentAccountService = inject(PaymentAccountService);
  protected paymentAccountFormService = inject(PaymentAccountFormService);
  protected documentService = inject(DocumentService);
  protected customerService = inject(CustomerService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: PaymentAccountFormGroup = this.paymentAccountFormService.createPaymentAccountFormGroup();

  compareDocument = (o1: IDocument | null, o2: IDocument | null): boolean => this.documentService.compareDocument(o1, o2);

  compareCustomer = (o1: ICustomer | null, o2: ICustomer | null): boolean => this.customerService.compareCustomer(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paymentAccount }) => {
      this.paymentAccount = paymentAccount;
      if (paymentAccount) {
        this.updateForm(paymentAccount);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const paymentAccount = this.paymentAccountFormService.getPaymentAccount(this.editForm);
    if (paymentAccount.id !== null) {
      this.subscribeToSaveResponse(this.paymentAccountService.update(paymentAccount));
    } else {
      this.subscribeToSaveResponse(this.paymentAccountService.create(paymentAccount));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(paymentAccount: IPaymentAccount): void {
    this.paymentAccount = paymentAccount;
    this.paymentAccountFormService.resetForm(this.editForm, paymentAccount);

    this.documentsSharedCollection = this.documentService.addDocumentToCollectionIfMissing<IDocument>(
      this.documentsSharedCollection,
      paymentAccount.contract,
    );
    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing<ICustomer>(
      this.customersSharedCollection,
      paymentAccount.customer,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.documentService
      .query()
      .pipe(map((res: HttpResponse<IDocument[]>) => res.body ?? []))
      .pipe(
        map((documents: IDocument[]) =>
          this.documentService.addDocumentToCollectionIfMissing<IDocument>(documents, this.paymentAccount?.contract),
        ),
      )
      .subscribe((documents: IDocument[]) => (this.documentsSharedCollection = documents));

    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing<ICustomer>(customers, this.paymentAccount?.customer),
        ),
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));
  }
}
