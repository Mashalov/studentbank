import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../payment-account.test-samples';

import { PaymentAccountFormService } from './payment-account-form.service';

describe('PaymentAccount Form Service', () => {
  let service: PaymentAccountFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentAccountFormService);
  });

  describe('Service methods', () => {
    describe('createPaymentAccountFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPaymentAccountFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            paymentAccountType: expect.any(Object),
            iban: expect.any(Object),
            amount: expect.any(Object),
            currency: expect.any(Object),
            active: expect.any(Object),
            deleted: expect.any(Object),
            contract: expect.any(Object),
            customer: expect.any(Object),
          }),
        );
      });

      it('passing IPaymentAccount should create a new form with FormGroup', () => {
        const formGroup = service.createPaymentAccountFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            paymentAccountType: expect.any(Object),
            iban: expect.any(Object),
            amount: expect.any(Object),
            currency: expect.any(Object),
            active: expect.any(Object),
            deleted: expect.any(Object),
            contract: expect.any(Object),
            customer: expect.any(Object),
          }),
        );
      });
    });

    describe('getPaymentAccount', () => {
      it('should return NewPaymentAccount for default PaymentAccount initial value', () => {
        const formGroup = service.createPaymentAccountFormGroup(sampleWithNewData);

        const paymentAccount = service.getPaymentAccount(formGroup) as any;

        expect(paymentAccount).toMatchObject(sampleWithNewData);
      });

      it('should return NewPaymentAccount for empty PaymentAccount initial value', () => {
        const formGroup = service.createPaymentAccountFormGroup();

        const paymentAccount = service.getPaymentAccount(formGroup) as any;

        expect(paymentAccount).toMatchObject({});
      });

      it('should return IPaymentAccount', () => {
        const formGroup = service.createPaymentAccountFormGroup(sampleWithRequiredData);

        const paymentAccount = service.getPaymentAccount(formGroup) as any;

        expect(paymentAccount).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPaymentAccount should not enable id FormControl', () => {
        const formGroup = service.createPaymentAccountFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPaymentAccount should disable id FormControl', () => {
        const formGroup = service.createPaymentAccountFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
