import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { IPaymentAccount } from '../payment-account.model';
import { PaymentAccountService } from '../service/payment-account.service';
import { PaymentAccountFormService } from './payment-account-form.service';

import { PaymentAccountUpdateComponent } from './payment-account-update.component';

describe('PaymentAccount Management Update Component', () => {
  let comp: PaymentAccountUpdateComponent;
  let fixture: ComponentFixture<PaymentAccountUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let paymentAccountFormService: PaymentAccountFormService;
  let paymentAccountService: PaymentAccountService;
  let documentService: DocumentService;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PaymentAccountUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PaymentAccountUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PaymentAccountUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    paymentAccountFormService = TestBed.inject(PaymentAccountFormService);
    paymentAccountService = TestBed.inject(PaymentAccountService);
    documentService = TestBed.inject(DocumentService);
    customerService = TestBed.inject(CustomerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Document query and add missing value', () => {
      const paymentAccount: IPaymentAccount = { id: 456 };
      const contract: IDocument = { id: 18255 };
      paymentAccount.contract = contract;

      const documentCollection: IDocument[] = [{ id: 16776 }];
      jest.spyOn(documentService, 'query').mockReturnValue(of(new HttpResponse({ body: documentCollection })));
      const additionalDocuments = [contract];
      const expectedCollection: IDocument[] = [...additionalDocuments, ...documentCollection];
      jest.spyOn(documentService, 'addDocumentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ paymentAccount });
      comp.ngOnInit();

      expect(documentService.query).toHaveBeenCalled();
      expect(documentService.addDocumentToCollectionIfMissing).toHaveBeenCalledWith(
        documentCollection,
        ...additionalDocuments.map(expect.objectContaining),
      );
      expect(comp.documentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Customer query and add missing value', () => {
      const paymentAccount: IPaymentAccount = { id: 456 };
      const customer: ICustomer = { id: 15303 };
      paymentAccount.customer = customer;

      const customerCollection: ICustomer[] = [{ id: 10288 }];
      jest.spyOn(customerService, 'query').mockReturnValue(of(new HttpResponse({ body: customerCollection })));
      const additionalCustomers = [customer];
      const expectedCollection: ICustomer[] = [...additionalCustomers, ...customerCollection];
      jest.spyOn(customerService, 'addCustomerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ paymentAccount });
      comp.ngOnInit();

      expect(customerService.query).toHaveBeenCalled();
      expect(customerService.addCustomerToCollectionIfMissing).toHaveBeenCalledWith(
        customerCollection,
        ...additionalCustomers.map(expect.objectContaining),
      );
      expect(comp.customersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const paymentAccount: IPaymentAccount = { id: 456 };
      const contract: IDocument = { id: 32287 };
      paymentAccount.contract = contract;
      const customer: ICustomer = { id: 22712 };
      paymentAccount.customer = customer;

      activatedRoute.data = of({ paymentAccount });
      comp.ngOnInit();

      expect(comp.documentsSharedCollection).toContain(contract);
      expect(comp.customersSharedCollection).toContain(customer);
      expect(comp.paymentAccount).toEqual(paymentAccount);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPaymentAccount>>();
      const paymentAccount = { id: 123 };
      jest.spyOn(paymentAccountFormService, 'getPaymentAccount').mockReturnValue(paymentAccount);
      jest.spyOn(paymentAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ paymentAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: paymentAccount }));
      saveSubject.complete();

      // THEN
      expect(paymentAccountFormService.getPaymentAccount).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(paymentAccountService.update).toHaveBeenCalledWith(expect.objectContaining(paymentAccount));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPaymentAccount>>();
      const paymentAccount = { id: 123 };
      jest.spyOn(paymentAccountFormService, 'getPaymentAccount').mockReturnValue({ id: null });
      jest.spyOn(paymentAccountService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ paymentAccount: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: paymentAccount }));
      saveSubject.complete();

      // THEN
      expect(paymentAccountFormService.getPaymentAccount).toHaveBeenCalled();
      expect(paymentAccountService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPaymentAccount>>();
      const paymentAccount = { id: 123 };
      jest.spyOn(paymentAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ paymentAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(paymentAccountService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareDocument', () => {
      it('Should forward to documentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(documentService, 'compareDocument');
        comp.compareDocument(entity, entity2);
        expect(documentService.compareDocument).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareCustomer', () => {
      it('Should forward to customerService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(customerService, 'compareCustomer');
        comp.compareCustomer(entity, entity2);
        expect(customerService.compareCustomer).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
