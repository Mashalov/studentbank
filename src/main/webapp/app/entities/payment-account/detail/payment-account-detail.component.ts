import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { IPaymentAccount } from '../payment-account.model';

@Component({
  standalone: true,
  selector: 'jhi-payment-account-detail',
  templateUrl: './payment-account-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class PaymentAccountDetailComponent {
  paymentAccount = input<IPaymentAccount | null>(null);

  previousState(): void {
    window.history.back();
  }
}
