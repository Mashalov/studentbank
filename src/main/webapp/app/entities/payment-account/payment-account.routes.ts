import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PaymentAccountComponent } from './list/payment-account.component';
import { PaymentAccountDetailComponent } from './detail/payment-account-detail.component';
import { PaymentAccountUpdateComponent } from './update/payment-account-update.component';
import PaymentAccountResolve from './route/payment-account-routing-resolve.service';

const paymentAccountRoute: Routes = [
  {
    path: '',
    component: PaymentAccountComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PaymentAccountDetailComponent,
    resolve: {
      paymentAccount: PaymentAccountResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PaymentAccountUpdateComponent,
    resolve: {
      paymentAccount: PaymentAccountResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PaymentAccountUpdateComponent,
    resolve: {
      paymentAccount: PaymentAccountResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default paymentAccountRoute;
