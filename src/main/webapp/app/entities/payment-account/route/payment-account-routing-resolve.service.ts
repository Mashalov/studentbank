import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPaymentAccount } from '../payment-account.model';
import { PaymentAccountService } from '../service/payment-account.service';

const paymentAccountResolve = (route: ActivatedRouteSnapshot): Observable<null | IPaymentAccount> => {
  const id = route.params['id'];
  if (id) {
    return inject(PaymentAccountService)
      .find(id)
      .pipe(
        mergeMap((paymentAccount: HttpResponse<IPaymentAccount>) => {
          if (paymentAccount.body) {
            return of(paymentAccount.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default paymentAccountResolve;
