import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAtm } from '../atm.model';
import { AtmService } from '../service/atm.service';

const atmResolve = (route: ActivatedRouteSnapshot): Observable<null | IAtm> => {
  const id = route.params['id'];
  if (id) {
    return inject(AtmService)
      .find(id)
      .pipe(
        mergeMap((atm: HttpResponse<IAtm>) => {
          if (atm.body) {
            return of(atm.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default atmResolve;
