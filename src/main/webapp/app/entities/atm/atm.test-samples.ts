import { IAtm, NewAtm } from './atm.model';

export const sampleWithRequiredData: IAtm = {
  id: 13953,
  name: 'implode even cooperative',
};

export const sampleWithPartialData: IAtm = {
  id: 16639,
  name: 'stray unless optimistically',
};

export const sampleWithFullData: IAtm = {
  id: 11263,
  name: 'um taxicab task',
  documentType: 'DOCX',
};

export const sampleWithNewData: NewAtm = {
  name: 'gee',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
