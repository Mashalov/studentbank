import { IAddress } from 'app/entities/address/address.model';
import { DocumentType } from 'app/entities/enumerations/document-type.model';

export interface IAtm {
  id: number;
  name?: string | null;
  documentType?: keyof typeof DocumentType | null;
  address?: Pick<IAddress, 'id'> | null;
}

export type NewAtm = Omit<IAtm, 'id'> & { id: null };
