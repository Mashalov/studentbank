import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness } from '@angular/router/testing';
import { of } from 'rxjs';

import { AtmDetailComponent } from './atm-detail.component';

describe('Atm Management Detail Component', () => {
  let comp: AtmDetailComponent;
  let fixture: ComponentFixture<AtmDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AtmDetailComponent],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: AtmDetailComponent,
              resolve: { atm: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(AtmDetailComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load atm on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', AtmDetailComponent);

      // THEN
      expect(instance.atm()).toEqual(expect.objectContaining({ id: 123 }));
    });
  });

  describe('PreviousState', () => {
    it('Should navigate to previous state', () => {
      jest.spyOn(window.history, 'back');
      comp.previousState();
      expect(window.history.back).toHaveBeenCalled();
    });
  });
});
