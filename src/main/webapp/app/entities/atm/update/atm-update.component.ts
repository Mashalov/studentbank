import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IAddress } from 'app/entities/address/address.model';
import { AddressService } from 'app/entities/address/service/address.service';
import { DocumentType } from 'app/entities/enumerations/document-type.model';
import { AtmService } from '../service/atm.service';
import { IAtm } from '../atm.model';
import { AtmFormService, AtmFormGroup } from './atm-form.service';

@Component({
  standalone: true,
  selector: 'jhi-atm-update',
  templateUrl: './atm-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class AtmUpdateComponent implements OnInit {
  isSaving = false;
  atm: IAtm | null = null;
  documentTypeValues = Object.keys(DocumentType);

  addressesSharedCollection: IAddress[] = [];

  protected atmService = inject(AtmService);
  protected atmFormService = inject(AtmFormService);
  protected addressService = inject(AddressService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: AtmFormGroup = this.atmFormService.createAtmFormGroup();

  compareAddress = (o1: IAddress | null, o2: IAddress | null): boolean => this.addressService.compareAddress(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ atm }) => {
      this.atm = atm;
      if (atm) {
        this.updateForm(atm);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const atm = this.atmFormService.getAtm(this.editForm);
    if (atm.id !== null) {
      this.subscribeToSaveResponse(this.atmService.update(atm));
    } else {
      this.subscribeToSaveResponse(this.atmService.create(atm));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtm>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(atm: IAtm): void {
    this.atm = atm;
    this.atmFormService.resetForm(this.editForm, atm);

    this.addressesSharedCollection = this.addressService.addAddressToCollectionIfMissing<IAddress>(
      this.addressesSharedCollection,
      atm.address,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.addressService
      .query()
      .pipe(map((res: HttpResponse<IAddress[]>) => res.body ?? []))
      .pipe(map((addresses: IAddress[]) => this.addressService.addAddressToCollectionIfMissing<IAddress>(addresses, this.atm?.address)))
      .subscribe((addresses: IAddress[]) => (this.addressesSharedCollection = addresses));
  }
}
