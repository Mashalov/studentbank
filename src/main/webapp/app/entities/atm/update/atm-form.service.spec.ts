import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../atm.test-samples';

import { AtmFormService } from './atm-form.service';

describe('Atm Form Service', () => {
  let service: AtmFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtmFormService);
  });

  describe('Service methods', () => {
    describe('createAtmFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAtmFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            documentType: expect.any(Object),
            address: expect.any(Object),
          }),
        );
      });

      it('passing IAtm should create a new form with FormGroup', () => {
        const formGroup = service.createAtmFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            documentType: expect.any(Object),
            address: expect.any(Object),
          }),
        );
      });
    });

    describe('getAtm', () => {
      it('should return NewAtm for default Atm initial value', () => {
        const formGroup = service.createAtmFormGroup(sampleWithNewData);

        const atm = service.getAtm(formGroup) as any;

        expect(atm).toMatchObject(sampleWithNewData);
      });

      it('should return NewAtm for empty Atm initial value', () => {
        const formGroup = service.createAtmFormGroup();

        const atm = service.getAtm(formGroup) as any;

        expect(atm).toMatchObject({});
      });

      it('should return IAtm', () => {
        const formGroup = service.createAtmFormGroup(sampleWithRequiredData);

        const atm = service.getAtm(formGroup) as any;

        expect(atm).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAtm should not enable id FormControl', () => {
        const formGroup = service.createAtmFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAtm should disable id FormControl', () => {
        const formGroup = service.createAtmFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
