import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IAddress } from 'app/entities/address/address.model';
import { AddressService } from 'app/entities/address/service/address.service';
import { AtmService } from '../service/atm.service';
import { IAtm } from '../atm.model';
import { AtmFormService } from './atm-form.service';

import { AtmUpdateComponent } from './atm-update.component';

describe('Atm Management Update Component', () => {
  let comp: AtmUpdateComponent;
  let fixture: ComponentFixture<AtmUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let atmFormService: AtmFormService;
  let atmService: AtmService;
  let addressService: AddressService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AtmUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AtmUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AtmUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    atmFormService = TestBed.inject(AtmFormService);
    atmService = TestBed.inject(AtmService);
    addressService = TestBed.inject(AddressService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Address query and add missing value', () => {
      const atm: IAtm = { id: 456 };
      const address: IAddress = { id: 8312 };
      atm.address = address;

      const addressCollection: IAddress[] = [{ id: 153 }];
      jest.spyOn(addressService, 'query').mockReturnValue(of(new HttpResponse({ body: addressCollection })));
      const additionalAddresses = [address];
      const expectedCollection: IAddress[] = [...additionalAddresses, ...addressCollection];
      jest.spyOn(addressService, 'addAddressToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ atm });
      comp.ngOnInit();

      expect(addressService.query).toHaveBeenCalled();
      expect(addressService.addAddressToCollectionIfMissing).toHaveBeenCalledWith(
        addressCollection,
        ...additionalAddresses.map(expect.objectContaining),
      );
      expect(comp.addressesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const atm: IAtm = { id: 456 };
      const address: IAddress = { id: 29880 };
      atm.address = address;

      activatedRoute.data = of({ atm });
      comp.ngOnInit();

      expect(comp.addressesSharedCollection).toContain(address);
      expect(comp.atm).toEqual(atm);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtm>>();
      const atm = { id: 123 };
      jest.spyOn(atmFormService, 'getAtm').mockReturnValue(atm);
      jest.spyOn(atmService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atm });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atm }));
      saveSubject.complete();

      // THEN
      expect(atmFormService.getAtm).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(atmService.update).toHaveBeenCalledWith(expect.objectContaining(atm));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtm>>();
      const atm = { id: 123 };
      jest.spyOn(atmFormService, 'getAtm').mockReturnValue({ id: null });
      jest.spyOn(atmService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atm: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: atm }));
      saveSubject.complete();

      // THEN
      expect(atmFormService.getAtm).toHaveBeenCalled();
      expect(atmService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAtm>>();
      const atm = { id: 123 };
      jest.spyOn(atmService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ atm });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(atmService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareAddress', () => {
      it('Should forward to addressService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(addressService, 'compareAddress');
        comp.compareAddress(entity, entity2);
        expect(addressService.compareAddress).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
