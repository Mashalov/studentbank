import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAtm, NewAtm } from '../atm.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAtm for edit and NewAtmFormGroupInput for create.
 */
type AtmFormGroupInput = IAtm | PartialWithRequiredKeyOf<NewAtm>;

type AtmFormDefaults = Pick<NewAtm, 'id'>;

type AtmFormGroupContent = {
  id: FormControl<IAtm['id'] | NewAtm['id']>;
  name: FormControl<IAtm['name']>;
  documentType: FormControl<IAtm['documentType']>;
  address: FormControl<IAtm['address']>;
};

export type AtmFormGroup = FormGroup<AtmFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AtmFormService {
  createAtmFormGroup(atm: AtmFormGroupInput = { id: null }): AtmFormGroup {
    const atmRawValue = {
      ...this.getFormDefaults(),
      ...atm,
    };
    return new FormGroup<AtmFormGroupContent>({
      id: new FormControl(
        { value: atmRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(atmRawValue.name, {
        validators: [Validators.required],
      }),
      documentType: new FormControl(atmRawValue.documentType),
      address: new FormControl(atmRawValue.address),
    });
  }

  getAtm(form: AtmFormGroup): IAtm | NewAtm {
    return form.getRawValue() as IAtm | NewAtm;
  }

  resetForm(form: AtmFormGroup, atm: AtmFormGroupInput): void {
    const atmRawValue = { ...this.getFormDefaults(), ...atm };
    form.reset(
      {
        ...atmRawValue,
        id: { value: atmRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): AtmFormDefaults {
    return {
      id: null,
    };
  }
}
