import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IAtm } from '../atm.model';
import { AtmService } from '../service/atm.service';

@Component({
  standalone: true,
  templateUrl: './atm-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class AtmDeleteDialogComponent {
  atm?: IAtm;

  protected atmService = inject(AtmService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.atmService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
