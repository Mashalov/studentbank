import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { AtmComponent } from './list/atm.component';
import { AtmDetailComponent } from './detail/atm-detail.component';
import { AtmUpdateComponent } from './update/atm-update.component';
import AtmResolve from './route/atm-routing-resolve.service';

const atmRoute: Routes = [
  {
    path: '',
    component: AtmComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AtmDetailComponent,
    resolve: {
      atm: AtmResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AtmUpdateComponent,
    resolve: {
      atm: AtmResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AtmUpdateComponent,
    resolve: {
      atm: AtmResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default atmRoute;
