import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAtm, NewAtm } from '../atm.model';

export type PartialUpdateAtm = Partial<IAtm> & Pick<IAtm, 'id'>;

export type EntityResponseType = HttpResponse<IAtm>;
export type EntityArrayResponseType = HttpResponse<IAtm[]>;

@Injectable({ providedIn: 'root' })
export class AtmService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/atms');

  create(atm: NewAtm): Observable<EntityResponseType> {
    return this.http.post<IAtm>(this.resourceUrl, atm, { observe: 'response' });
  }

  update(atm: IAtm): Observable<EntityResponseType> {
    return this.http.put<IAtm>(`${this.resourceUrl}/${this.getAtmIdentifier(atm)}`, atm, { observe: 'response' });
  }

  partialUpdate(atm: PartialUpdateAtm): Observable<EntityResponseType> {
    return this.http.patch<IAtm>(`${this.resourceUrl}/${this.getAtmIdentifier(atm)}`, atm, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAtm>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAtm[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAtmIdentifier(atm: Pick<IAtm, 'id'>): number {
    return atm.id;
  }

  compareAtm(o1: Pick<IAtm, 'id'> | null, o2: Pick<IAtm, 'id'> | null): boolean {
    return o1 && o2 ? this.getAtmIdentifier(o1) === this.getAtmIdentifier(o2) : o1 === o2;
  }

  addAtmToCollectionIfMissing<Type extends Pick<IAtm, 'id'>>(atmCollection: Type[], ...atmsToCheck: (Type | null | undefined)[]): Type[] {
    const atms: Type[] = atmsToCheck.filter(isPresent);
    if (atms.length > 0) {
      const atmCollectionIdentifiers = atmCollection.map(atmItem => this.getAtmIdentifier(atmItem));
      const atmsToAdd = atms.filter(atmItem => {
        const atmIdentifier = this.getAtmIdentifier(atmItem);
        if (atmCollectionIdentifiers.includes(atmIdentifier)) {
          return false;
        }
        atmCollectionIdentifiers.push(atmIdentifier);
        return true;
      });
      return [...atmsToAdd, ...atmCollection];
    }
    return atmCollection;
  }
}
