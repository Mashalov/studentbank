import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IAtm } from '../atm.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../atm.test-samples';

import { AtmService } from './atm.service';

const requireRestSample: IAtm = {
  ...sampleWithRequiredData,
};

describe('Atm Service', () => {
  let service: AtmService;
  let httpMock: HttpTestingController;
  let expectedResult: IAtm | IAtm[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(AtmService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Atm', () => {
      const atm = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(atm).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Atm', () => {
      const atm = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(atm).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Atm', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Atm', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Atm', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAtmToCollectionIfMissing', () => {
      it('should add a Atm to an empty array', () => {
        const atm: IAtm = sampleWithRequiredData;
        expectedResult = service.addAtmToCollectionIfMissing([], atm);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(atm);
      });

      it('should not add a Atm to an array that contains it', () => {
        const atm: IAtm = sampleWithRequiredData;
        const atmCollection: IAtm[] = [
          {
            ...atm,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAtmToCollectionIfMissing(atmCollection, atm);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Atm to an array that doesn't contain it", () => {
        const atm: IAtm = sampleWithRequiredData;
        const atmCollection: IAtm[] = [sampleWithPartialData];
        expectedResult = service.addAtmToCollectionIfMissing(atmCollection, atm);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(atm);
      });

      it('should add only unique Atm to an array', () => {
        const atmArray: IAtm[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const atmCollection: IAtm[] = [sampleWithRequiredData];
        expectedResult = service.addAtmToCollectionIfMissing(atmCollection, ...atmArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const atm: IAtm = sampleWithRequiredData;
        const atm2: IAtm = sampleWithPartialData;
        expectedResult = service.addAtmToCollectionIfMissing([], atm, atm2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(atm);
        expect(expectedResult).toContain(atm2);
      });

      it('should accept null and undefined values', () => {
        const atm: IAtm = sampleWithRequiredData;
        expectedResult = service.addAtmToCollectionIfMissing([], null, atm, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(atm);
      });

      it('should return initial array if no Atm is added', () => {
        const atmCollection: IAtm[] = [sampleWithRequiredData];
        expectedResult = service.addAtmToCollectionIfMissing(atmCollection, undefined, null);
        expect(expectedResult).toEqual(atmCollection);
      });
    });

    describe('compareAtm', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAtm(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAtm(entity1, entity2);
        const compareResult2 = service.compareAtm(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAtm(entity1, entity2);
        const compareResult2 = service.compareAtm(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAtm(entity1, entity2);
        const compareResult2 = service.compareAtm(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
