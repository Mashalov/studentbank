import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { PaymentAccountService } from 'app/entities/payment-account/service/payment-account.service';
import { CardType } from 'app/entities/enumerations/card-type.model';
import { CardProvider } from 'app/entities/enumerations/card-provider.model';
import { CardDetailsService } from '../service/card-details.service';
import { ICardDetails } from '../card-details.model';
import { CardDetailsFormService, CardDetailsFormGroup } from './card-details-form.service';

@Component({
  standalone: true,
  selector: 'jhi-card-details-update',
  templateUrl: './card-details-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class CardDetailsUpdateComponent implements OnInit {
  isSaving = false;
  cardDetails: ICardDetails | null = null;
  cardTypeValues = Object.keys(CardType);
  cardProviderValues = Object.keys(CardProvider);

  customersSharedCollection: ICustomer[] = [];
  paymentAccountsSharedCollection: IPaymentAccount[] = [];

  protected cardDetailsService = inject(CardDetailsService);
  protected cardDetailsFormService = inject(CardDetailsFormService);
  protected customerService = inject(CustomerService);
  protected paymentAccountService = inject(PaymentAccountService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: CardDetailsFormGroup = this.cardDetailsFormService.createCardDetailsFormGroup();

  compareCustomer = (o1: ICustomer | null, o2: ICustomer | null): boolean => this.customerService.compareCustomer(o1, o2);

  comparePaymentAccount = (o1: IPaymentAccount | null, o2: IPaymentAccount | null): boolean =>
    this.paymentAccountService.comparePaymentAccount(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cardDetails }) => {
      this.cardDetails = cardDetails;
      if (cardDetails) {
        this.updateForm(cardDetails);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cardDetails = this.cardDetailsFormService.getCardDetails(this.editForm);
    if (cardDetails.id !== null) {
      this.subscribeToSaveResponse(this.cardDetailsService.update(cardDetails));
    } else {
      this.subscribeToSaveResponse(this.cardDetailsService.create(cardDetails));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICardDetails>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cardDetails: ICardDetails): void {
    this.cardDetails = cardDetails;
    this.cardDetailsFormService.resetForm(this.editForm, cardDetails);

    this.customersSharedCollection = this.customerService.addCustomerToCollectionIfMissing<ICustomer>(
      this.customersSharedCollection,
      cardDetails.customer,
    );
    this.paymentAccountsSharedCollection = this.paymentAccountService.addPaymentAccountToCollectionIfMissing<IPaymentAccount>(
      this.paymentAccountsSharedCollection,
      cardDetails.paymentAccount,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.customerService
      .query()
      .pipe(map((res: HttpResponse<ICustomer[]>) => res.body ?? []))
      .pipe(
        map((customers: ICustomer[]) =>
          this.customerService.addCustomerToCollectionIfMissing<ICustomer>(customers, this.cardDetails?.customer),
        ),
      )
      .subscribe((customers: ICustomer[]) => (this.customersSharedCollection = customers));

    this.paymentAccountService
      .query()
      .pipe(map((res: HttpResponse<IPaymentAccount[]>) => res.body ?? []))
      .pipe(
        map((paymentAccounts: IPaymentAccount[]) =>
          this.paymentAccountService.addPaymentAccountToCollectionIfMissing<IPaymentAccount>(
            paymentAccounts,
            this.cardDetails?.paymentAccount,
          ),
        ),
      )
      .subscribe((paymentAccounts: IPaymentAccount[]) => (this.paymentAccountsSharedCollection = paymentAccounts));
  }
}
