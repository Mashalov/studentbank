import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ICardDetails, NewCardDetails } from '../card-details.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ICardDetails for edit and NewCardDetailsFormGroupInput for create.
 */
type CardDetailsFormGroupInput = ICardDetails | PartialWithRequiredKeyOf<NewCardDetails>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends ICardDetails | NewCardDetails> = Omit<T, 'issueDate' | 'expiryDate'> & {
  issueDate?: string | null;
  expiryDate?: string | null;
};

type CardDetailsFormRawValue = FormValueOf<ICardDetails>;

type NewCardDetailsFormRawValue = FormValueOf<NewCardDetails>;

type CardDetailsFormDefaults = Pick<NewCardDetails, 'id' | 'issueDate' | 'expiryDate' | 'isActive' | 'isBlocked'>;

type CardDetailsFormGroupContent = {
  id: FormControl<CardDetailsFormRawValue['id'] | NewCardDetails['id']>;
  cardNumber: FormControl<CardDetailsFormRawValue['cardNumber']>;
  cardType: FormControl<CardDetailsFormRawValue['cardType']>;
  cardProvider: FormControl<CardDetailsFormRawValue['cardProvider']>;
  issueDate: FormControl<CardDetailsFormRawValue['issueDate']>;
  expiryDate: FormControl<CardDetailsFormRawValue['expiryDate']>;
  cvv: FormControl<CardDetailsFormRawValue['cvv']>;
  isActive: FormControl<CardDetailsFormRawValue['isActive']>;
  isBlocked: FormControl<CardDetailsFormRawValue['isBlocked']>;
  pin: FormControl<CardDetailsFormRawValue['pin']>;
  customer: FormControl<CardDetailsFormRawValue['customer']>;
  paymentAccount: FormControl<CardDetailsFormRawValue['paymentAccount']>;
};

export type CardDetailsFormGroup = FormGroup<CardDetailsFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class CardDetailsFormService {
  createCardDetailsFormGroup(cardDetails: CardDetailsFormGroupInput = { id: null }): CardDetailsFormGroup {
    const cardDetailsRawValue = this.convertCardDetailsToCardDetailsRawValue({
      ...this.getFormDefaults(),
      ...cardDetails,
    });
    return new FormGroup<CardDetailsFormGroupContent>({
      id: new FormControl(
        { value: cardDetailsRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      cardNumber: new FormControl(cardDetailsRawValue.cardNumber, {
        validators: [Validators.required],
      }),
      cardType: new FormControl(cardDetailsRawValue.cardType, {
        validators: [Validators.required],
      }),
      cardProvider: new FormControl(cardDetailsRawValue.cardProvider, {
        validators: [Validators.required],
      }),
      issueDate: new FormControl(cardDetailsRawValue.issueDate, {
        validators: [Validators.required],
      }),
      expiryDate: new FormControl(cardDetailsRawValue.expiryDate, {
        validators: [Validators.required],
      }),
      cvv: new FormControl(cardDetailsRawValue.cvv, {
        validators: [Validators.required],
      }),
      isActive: new FormControl(cardDetailsRawValue.isActive),
      isBlocked: new FormControl(cardDetailsRawValue.isBlocked),
      pin: new FormControl(cardDetailsRawValue.pin),
      customer: new FormControl(cardDetailsRawValue.customer),
      paymentAccount: new FormControl(cardDetailsRawValue.paymentAccount),
    });
  }

  getCardDetails(form: CardDetailsFormGroup): ICardDetails | NewCardDetails {
    return this.convertCardDetailsRawValueToCardDetails(form.getRawValue() as CardDetailsFormRawValue | NewCardDetailsFormRawValue);
  }

  resetForm(form: CardDetailsFormGroup, cardDetails: CardDetailsFormGroupInput): void {
    const cardDetailsRawValue = this.convertCardDetailsToCardDetailsRawValue({ ...this.getFormDefaults(), ...cardDetails });
    form.reset(
      {
        ...cardDetailsRawValue,
        id: { value: cardDetailsRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): CardDetailsFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      issueDate: currentTime,
      expiryDate: currentTime,
      isActive: false,
      isBlocked: false,
    };
  }

  private convertCardDetailsRawValueToCardDetails(
    rawCardDetails: CardDetailsFormRawValue | NewCardDetailsFormRawValue,
  ): ICardDetails | NewCardDetails {
    return {
      ...rawCardDetails,
      issueDate: dayjs(rawCardDetails.issueDate, DATE_TIME_FORMAT),
      expiryDate: dayjs(rawCardDetails.expiryDate, DATE_TIME_FORMAT),
    };
  }

  private convertCardDetailsToCardDetailsRawValue(
    cardDetails: ICardDetails | (Partial<NewCardDetails> & CardDetailsFormDefaults),
  ): CardDetailsFormRawValue | PartialWithRequiredKeyOf<NewCardDetailsFormRawValue> {
    return {
      ...cardDetails,
      issueDate: cardDetails.issueDate ? cardDetails.issueDate.format(DATE_TIME_FORMAT) : undefined,
      expiryDate: cardDetails.expiryDate ? cardDetails.expiryDate.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
