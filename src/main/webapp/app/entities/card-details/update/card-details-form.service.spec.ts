import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../card-details.test-samples';

import { CardDetailsFormService } from './card-details-form.service';

describe('CardDetails Form Service', () => {
  let service: CardDetailsFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardDetailsFormService);
  });

  describe('Service methods', () => {
    describe('createCardDetailsFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createCardDetailsFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            cardNumber: expect.any(Object),
            cardType: expect.any(Object),
            cardProvider: expect.any(Object),
            issueDate: expect.any(Object),
            expiryDate: expect.any(Object),
            cvv: expect.any(Object),
            isActive: expect.any(Object),
            isBlocked: expect.any(Object),
            pin: expect.any(Object),
            customer: expect.any(Object),
            paymentAccount: expect.any(Object),
          }),
        );
      });

      it('passing ICardDetails should create a new form with FormGroup', () => {
        const formGroup = service.createCardDetailsFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            cardNumber: expect.any(Object),
            cardType: expect.any(Object),
            cardProvider: expect.any(Object),
            issueDate: expect.any(Object),
            expiryDate: expect.any(Object),
            cvv: expect.any(Object),
            isActive: expect.any(Object),
            isBlocked: expect.any(Object),
            pin: expect.any(Object),
            customer: expect.any(Object),
            paymentAccount: expect.any(Object),
          }),
        );
      });
    });

    describe('getCardDetails', () => {
      it('should return NewCardDetails for default CardDetails initial value', () => {
        const formGroup = service.createCardDetailsFormGroup(sampleWithNewData);

        const cardDetails = service.getCardDetails(formGroup) as any;

        expect(cardDetails).toMatchObject(sampleWithNewData);
      });

      it('should return NewCardDetails for empty CardDetails initial value', () => {
        const formGroup = service.createCardDetailsFormGroup();

        const cardDetails = service.getCardDetails(formGroup) as any;

        expect(cardDetails).toMatchObject({});
      });

      it('should return ICardDetails', () => {
        const formGroup = service.createCardDetailsFormGroup(sampleWithRequiredData);

        const cardDetails = service.getCardDetails(formGroup) as any;

        expect(cardDetails).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ICardDetails should not enable id FormControl', () => {
        const formGroup = service.createCardDetailsFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewCardDetails should disable id FormControl', () => {
        const formGroup = service.createCardDetailsFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
