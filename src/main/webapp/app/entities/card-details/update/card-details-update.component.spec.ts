import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { ICustomer } from 'app/entities/customer/customer.model';
import { CustomerService } from 'app/entities/customer/service/customer.service';
import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { PaymentAccountService } from 'app/entities/payment-account/service/payment-account.service';
import { ICardDetails } from '../card-details.model';
import { CardDetailsService } from '../service/card-details.service';
import { CardDetailsFormService } from './card-details-form.service';

import { CardDetailsUpdateComponent } from './card-details-update.component';

describe('CardDetails Management Update Component', () => {
  let comp: CardDetailsUpdateComponent;
  let fixture: ComponentFixture<CardDetailsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let cardDetailsFormService: CardDetailsFormService;
  let cardDetailsService: CardDetailsService;
  let customerService: CustomerService;
  let paymentAccountService: PaymentAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CardDetailsUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CardDetailsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CardDetailsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    cardDetailsFormService = TestBed.inject(CardDetailsFormService);
    cardDetailsService = TestBed.inject(CardDetailsService);
    customerService = TestBed.inject(CustomerService);
    paymentAccountService = TestBed.inject(PaymentAccountService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Customer query and add missing value', () => {
      const cardDetails: ICardDetails = { id: 456 };
      const customer: ICustomer = { id: 32435 };
      cardDetails.customer = customer;

      const customerCollection: ICustomer[] = [{ id: 3072 }];
      jest.spyOn(customerService, 'query').mockReturnValue(of(new HttpResponse({ body: customerCollection })));
      const additionalCustomers = [customer];
      const expectedCollection: ICustomer[] = [...additionalCustomers, ...customerCollection];
      jest.spyOn(customerService, 'addCustomerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ cardDetails });
      comp.ngOnInit();

      expect(customerService.query).toHaveBeenCalled();
      expect(customerService.addCustomerToCollectionIfMissing).toHaveBeenCalledWith(
        customerCollection,
        ...additionalCustomers.map(expect.objectContaining),
      );
      expect(comp.customersSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PaymentAccount query and add missing value', () => {
      const cardDetails: ICardDetails = { id: 456 };
      const paymentAccount: IPaymentAccount = { id: 10603 };
      cardDetails.paymentAccount = paymentAccount;

      const paymentAccountCollection: IPaymentAccount[] = [{ id: 22780 }];
      jest.spyOn(paymentAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: paymentAccountCollection })));
      const additionalPaymentAccounts = [paymentAccount];
      const expectedCollection: IPaymentAccount[] = [...additionalPaymentAccounts, ...paymentAccountCollection];
      jest.spyOn(paymentAccountService, 'addPaymentAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ cardDetails });
      comp.ngOnInit();

      expect(paymentAccountService.query).toHaveBeenCalled();
      expect(paymentAccountService.addPaymentAccountToCollectionIfMissing).toHaveBeenCalledWith(
        paymentAccountCollection,
        ...additionalPaymentAccounts.map(expect.objectContaining),
      );
      expect(comp.paymentAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const cardDetails: ICardDetails = { id: 456 };
      const customer: ICustomer = { id: 10909 };
      cardDetails.customer = customer;
      const paymentAccount: IPaymentAccount = { id: 11792 };
      cardDetails.paymentAccount = paymentAccount;

      activatedRoute.data = of({ cardDetails });
      comp.ngOnInit();

      expect(comp.customersSharedCollection).toContain(customer);
      expect(comp.paymentAccountsSharedCollection).toContain(paymentAccount);
      expect(comp.cardDetails).toEqual(cardDetails);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICardDetails>>();
      const cardDetails = { id: 123 };
      jest.spyOn(cardDetailsFormService, 'getCardDetails').mockReturnValue(cardDetails);
      jest.spyOn(cardDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cardDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cardDetails }));
      saveSubject.complete();

      // THEN
      expect(cardDetailsFormService.getCardDetails).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(cardDetailsService.update).toHaveBeenCalledWith(expect.objectContaining(cardDetails));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICardDetails>>();
      const cardDetails = { id: 123 };
      jest.spyOn(cardDetailsFormService, 'getCardDetails').mockReturnValue({ id: null });
      jest.spyOn(cardDetailsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cardDetails: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: cardDetails }));
      saveSubject.complete();

      // THEN
      expect(cardDetailsFormService.getCardDetails).toHaveBeenCalled();
      expect(cardDetailsService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICardDetails>>();
      const cardDetails = { id: 123 };
      jest.spyOn(cardDetailsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ cardDetails });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(cardDetailsService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCustomer', () => {
      it('Should forward to customerService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(customerService, 'compareCustomer');
        comp.compareCustomer(entity, entity2);
        expect(customerService.compareCustomer).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePaymentAccount', () => {
      it('Should forward to paymentAccountService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(paymentAccountService, 'comparePaymentAccount');
        comp.comparePaymentAccount(entity, entity2);
        expect(paymentAccountService.comparePaymentAccount).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
