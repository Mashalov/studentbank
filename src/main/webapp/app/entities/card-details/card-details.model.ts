import dayjs from 'dayjs/esm';
import { ICustomer } from 'app/entities/customer/customer.model';
import { IPaymentAccount } from 'app/entities/payment-account/payment-account.model';
import { CardType } from 'app/entities/enumerations/card-type.model';
import { CardProvider } from 'app/entities/enumerations/card-provider.model';

export interface ICardDetails {
  id: number;
  cardNumber?: number | null;
  cardType?: keyof typeof CardType | null;
  cardProvider?: keyof typeof CardProvider | null;
  issueDate?: dayjs.Dayjs | null;
  expiryDate?: dayjs.Dayjs | null;
  cvv?: number | null;
  isActive?: boolean | null;
  isBlocked?: boolean | null;
  pin?: number | null;
  customer?: Pick<ICustomer, 'id'> | null;
  paymentAccount?: Pick<IPaymentAccount, 'id'> | null;
}

export type NewCardDetails = Omit<ICardDetails, 'id'> & { id: null };
