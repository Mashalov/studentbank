import dayjs from 'dayjs/esm';

import { ICardDetails, NewCardDetails } from './card-details.model';

export const sampleWithRequiredData: ICardDetails = {
  id: 8793,
  cardNumber: 23048,
  cardType: 'CREDIT',
  cardProvider: 'MAESTRO',
  issueDate: dayjs('2024-07-06T00:51'),
  expiryDate: dayjs('2024-07-06T03:35'),
  cvv: 20094,
};

export const sampleWithPartialData: ICardDetails = {
  id: 22599,
  cardNumber: 21219,
  cardType: 'CREDIT',
  cardProvider: 'MAESTRO',
  issueDate: dayjs('2024-07-06T07:13'),
  expiryDate: dayjs('2024-07-06T14:17'),
  cvv: 27723,
  pin: 5015,
};

export const sampleWithFullData: ICardDetails = {
  id: 7381,
  cardNumber: 29179,
  cardType: 'CREDIT',
  cardProvider: 'MAESTRO',
  issueDate: dayjs('2024-07-05T18:03'),
  expiryDate: dayjs('2024-07-06T00:10'),
  cvv: 18217,
  isActive: false,
  isBlocked: false,
  pin: 2742,
};

export const sampleWithNewData: NewCardDetails = {
  cardNumber: 12303,
  cardType: 'CREDIT',
  cardProvider: 'VISA',
  issueDate: dayjs('2024-07-06T13:42'),
  expiryDate: dayjs('2024-07-05T21:48'),
  cvv: 22578,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
