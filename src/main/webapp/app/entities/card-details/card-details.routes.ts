import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { CardDetailsComponent } from './list/card-details.component';
import { CardDetailsDetailComponent } from './detail/card-details-detail.component';
import { CardDetailsUpdateComponent } from './update/card-details-update.component';
import CardDetailsResolve from './route/card-details-routing-resolve.service';

const cardDetailsRoute: Routes = [
  {
    path: '',
    component: CardDetailsComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CardDetailsDetailComponent,
    resolve: {
      cardDetails: CardDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CardDetailsUpdateComponent,
    resolve: {
      cardDetails: CardDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CardDetailsUpdateComponent,
    resolve: {
      cardDetails: CardDetailsResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default cardDetailsRoute;
