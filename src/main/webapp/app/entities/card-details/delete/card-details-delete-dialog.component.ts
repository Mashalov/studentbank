import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { ICardDetails } from '../card-details.model';
import { CardDetailsService } from '../service/card-details.service';

@Component({
  standalone: true,
  templateUrl: './card-details-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class CardDetailsDeleteDialogComponent {
  cardDetails?: ICardDetails;

  protected cardDetailsService = inject(CardDetailsService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cardDetailsService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
