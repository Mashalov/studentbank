import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICardDetails } from '../card-details.model';
import { CardDetailsService } from '../service/card-details.service';

const cardDetailsResolve = (route: ActivatedRouteSnapshot): Observable<null | ICardDetails> => {
  const id = route.params['id'];
  if (id) {
    return inject(CardDetailsService)
      .find(id)
      .pipe(
        mergeMap((cardDetails: HttpResponse<ICardDetails>) => {
          if (cardDetails.body) {
            return of(cardDetails.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default cardDetailsResolve;
