import { Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe } from 'app/shared/date';
import { ICardDetails } from '../card-details.model';

@Component({
  standalone: true,
  selector: 'jhi-card-details-detail',
  templateUrl: './card-details-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class CardDetailsDetailComponent {
  cardDetails = input<ICardDetails | null>(null);

  previousState(): void {
    window.history.back();
  }
}
