import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICardDetails, NewCardDetails } from '../card-details.model';

export type PartialUpdateCardDetails = Partial<ICardDetails> & Pick<ICardDetails, 'id'>;

type RestOf<T extends ICardDetails | NewCardDetails> = Omit<T, 'issueDate' | 'expiryDate'> & {
  issueDate?: string | null;
  expiryDate?: string | null;
};

export type RestCardDetails = RestOf<ICardDetails>;

export type NewRestCardDetails = RestOf<NewCardDetails>;

export type PartialUpdateRestCardDetails = RestOf<PartialUpdateCardDetails>;

export type EntityResponseType = HttpResponse<ICardDetails>;
export type EntityArrayResponseType = HttpResponse<ICardDetails[]>;

@Injectable({ providedIn: 'root' })
export class CardDetailsService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/card-details');

  create(cardDetails: NewCardDetails): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cardDetails);
    return this.http
      .post<RestCardDetails>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(cardDetails: ICardDetails): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cardDetails);
    return this.http
      .put<RestCardDetails>(`${this.resourceUrl}/${this.getCardDetailsIdentifier(cardDetails)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(cardDetails: PartialUpdateCardDetails): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cardDetails);
    return this.http
      .patch<RestCardDetails>(`${this.resourceUrl}/${this.getCardDetailsIdentifier(cardDetails)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestCardDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestCardDetails[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getCardDetailsIdentifier(cardDetails: Pick<ICardDetails, 'id'>): number {
    return cardDetails.id;
  }

  compareCardDetails(o1: Pick<ICardDetails, 'id'> | null, o2: Pick<ICardDetails, 'id'> | null): boolean {
    return o1 && o2 ? this.getCardDetailsIdentifier(o1) === this.getCardDetailsIdentifier(o2) : o1 === o2;
  }

  addCardDetailsToCollectionIfMissing<Type extends Pick<ICardDetails, 'id'>>(
    cardDetailsCollection: Type[],
    ...cardDetailsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const cardDetails: Type[] = cardDetailsToCheck.filter(isPresent);
    if (cardDetails.length > 0) {
      const cardDetailsCollectionIdentifiers = cardDetailsCollection.map(cardDetailsItem => this.getCardDetailsIdentifier(cardDetailsItem));
      const cardDetailsToAdd = cardDetails.filter(cardDetailsItem => {
        const cardDetailsIdentifier = this.getCardDetailsIdentifier(cardDetailsItem);
        if (cardDetailsCollectionIdentifiers.includes(cardDetailsIdentifier)) {
          return false;
        }
        cardDetailsCollectionIdentifiers.push(cardDetailsIdentifier);
        return true;
      });
      return [...cardDetailsToAdd, ...cardDetailsCollection];
    }
    return cardDetailsCollection;
  }

  protected convertDateFromClient<T extends ICardDetails | NewCardDetails | PartialUpdateCardDetails>(cardDetails: T): RestOf<T> {
    return {
      ...cardDetails,
      issueDate: cardDetails.issueDate?.toJSON() ?? null,
      expiryDate: cardDetails.expiryDate?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restCardDetails: RestCardDetails): ICardDetails {
    return {
      ...restCardDetails,
      issueDate: restCardDetails.issueDate ? dayjs(restCardDetails.issueDate) : undefined,
      expiryDate: restCardDetails.expiryDate ? dayjs(restCardDetails.expiryDate) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestCardDetails>): HttpResponse<ICardDetails> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestCardDetails[]>): HttpResponse<ICardDetails[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
