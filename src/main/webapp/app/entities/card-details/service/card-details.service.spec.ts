import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { ICardDetails } from '../card-details.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../card-details.test-samples';

import { CardDetailsService, RestCardDetails } from './card-details.service';

const requireRestSample: RestCardDetails = {
  ...sampleWithRequiredData,
  issueDate: sampleWithRequiredData.issueDate?.toJSON(),
  expiryDate: sampleWithRequiredData.expiryDate?.toJSON(),
};

describe('CardDetails Service', () => {
  let service: CardDetailsService;
  let httpMock: HttpTestingController;
  let expectedResult: ICardDetails | ICardDetails[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(CardDetailsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a CardDetails', () => {
      const cardDetails = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(cardDetails).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CardDetails', () => {
      const cardDetails = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(cardDetails).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CardDetails', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CardDetails', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a CardDetails', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addCardDetailsToCollectionIfMissing', () => {
      it('should add a CardDetails to an empty array', () => {
        const cardDetails: ICardDetails = sampleWithRequiredData;
        expectedResult = service.addCardDetailsToCollectionIfMissing([], cardDetails);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(cardDetails);
      });

      it('should not add a CardDetails to an array that contains it', () => {
        const cardDetails: ICardDetails = sampleWithRequiredData;
        const cardDetailsCollection: ICardDetails[] = [
          {
            ...cardDetails,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addCardDetailsToCollectionIfMissing(cardDetailsCollection, cardDetails);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CardDetails to an array that doesn't contain it", () => {
        const cardDetails: ICardDetails = sampleWithRequiredData;
        const cardDetailsCollection: ICardDetails[] = [sampleWithPartialData];
        expectedResult = service.addCardDetailsToCollectionIfMissing(cardDetailsCollection, cardDetails);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(cardDetails);
      });

      it('should add only unique CardDetails to an array', () => {
        const cardDetailsArray: ICardDetails[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const cardDetailsCollection: ICardDetails[] = [sampleWithRequiredData];
        expectedResult = service.addCardDetailsToCollectionIfMissing(cardDetailsCollection, ...cardDetailsArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const cardDetails: ICardDetails = sampleWithRequiredData;
        const cardDetails2: ICardDetails = sampleWithPartialData;
        expectedResult = service.addCardDetailsToCollectionIfMissing([], cardDetails, cardDetails2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(cardDetails);
        expect(expectedResult).toContain(cardDetails2);
      });

      it('should accept null and undefined values', () => {
        const cardDetails: ICardDetails = sampleWithRequiredData;
        expectedResult = service.addCardDetailsToCollectionIfMissing([], null, cardDetails, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(cardDetails);
      });

      it('should return initial array if no CardDetails is added', () => {
        const cardDetailsCollection: ICardDetails[] = [sampleWithRequiredData];
        expectedResult = service.addCardDetailsToCollectionIfMissing(cardDetailsCollection, undefined, null);
        expect(expectedResult).toEqual(cardDetailsCollection);
      });
    });

    describe('compareCardDetails', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareCardDetails(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareCardDetails(entity1, entity2);
        const compareResult2 = service.compareCardDetails(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareCardDetails(entity1, entity2);
        const compareResult2 = service.compareCardDetails(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareCardDetails(entity1, entity2);
        const compareResult2 = service.compareCardDetails(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
