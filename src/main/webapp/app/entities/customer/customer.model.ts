import { IContactDetails } from 'app/entities/contact-details/contact-details.model';
import { CustomerType } from 'app/entities/enumerations/customer-type.model';

export interface ICustomer {
  id: number;
  customerType?: keyof typeof CustomerType | null;
  creditScore?: number | null;
  contactDetails?: IContactDetails | null;
}

export type NewCustomer = Omit<ICustomer, 'id'> & { id: null };
