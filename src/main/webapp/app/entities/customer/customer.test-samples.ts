import { ICustomer, NewCustomer } from './customer.model';

export const sampleWithRequiredData: ICustomer = {
  id: 30643,
  customerType: 'NOT_SET',
};

export const sampleWithPartialData: ICustomer = {
  id: 9465,
  customerType: 'NATURAL_PERSON',
};

export const sampleWithFullData: ICustomer = {
  id: 16523,
  customerType: 'NATURAL_PERSON',
  creditScore: 24312,
};

export const sampleWithNewData: NewCustomer = {
  customerType: 'LEGAL_PERSON',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
