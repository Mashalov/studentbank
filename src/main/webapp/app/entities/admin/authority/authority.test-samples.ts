import { IAuthority, NewAuthority } from './authority.model';

export const sampleWithRequiredData: IAuthority = {
  name: '6490420a-3240-431b-abfb-840cc63afe99',
};

export const sampleWithPartialData: IAuthority = {
  name: 'd1d8e26b-e0da-45a4-9f29-458b59720eb4',
};

export const sampleWithFullData: IAuthority = {
  name: 'a4aee3cc-43ff-46a6-b4a9-5fc6f1eed800',
};

export const sampleWithNewData: NewAuthority = {
  name: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
