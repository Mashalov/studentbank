import { IContactDetails } from 'app/entities/contact-details/contact-details.model';
import { IJobPosition } from 'app/entities/job-position/job-position.model';
import { IDocument } from 'app/entities/document/document.model';
import { IAtm } from 'app/entities/atm/atm.model';
import { IOffice } from 'app/entities/office/office.model';

export interface IEmployee {
  id: number;
  isOnHoliday?: boolean | null;
  contactDetails?: Pick<IContactDetails, 'id'> | null;
  jobPosition?: Pick<IJobPosition, 'id'> | null;
  employeeContract?: Pick<IDocument, 'id'> | null;
  manager?: Pick<IEmployee, 'id'> | null;
  atm?: Pick<IAtm, 'id'> | null;
  office?: Pick<IOffice, 'id'> | null;
}

export type NewEmployee = Omit<IEmployee, 'id'> & { id: null };
