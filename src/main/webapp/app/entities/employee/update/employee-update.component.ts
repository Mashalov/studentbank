import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IContactDetails } from 'app/entities/contact-details/contact-details.model';
import { ContactDetailsService } from 'app/entities/contact-details/service/contact-details.service';
import { IJobPosition } from 'app/entities/job-position/job-position.model';
import { JobPositionService } from 'app/entities/job-position/service/job-position.service';
import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { IAtm } from 'app/entities/atm/atm.model';
import { AtmService } from 'app/entities/atm/service/atm.service';
import { IOffice } from 'app/entities/office/office.model';
import { OfficeService } from 'app/entities/office/service/office.service';
import { EmployeeService } from '../service/employee.service';
import { IEmployee } from '../employee.model';
import { EmployeeFormService, EmployeeFormGroup } from './employee-form.service';

@Component({
  standalone: true,
  selector: 'jhi-employee-update',
  templateUrl: './employee-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class EmployeeUpdateComponent implements OnInit {
  isSaving = false;
  employee: IEmployee | null = null;

  contactDetailsSharedCollection: IContactDetails[] = [];
  jobPositionsSharedCollection: IJobPosition[] = [];
  documentsSharedCollection: IDocument[] = [];
  employeesSharedCollection: IEmployee[] = [];
  atmsSharedCollection: IAtm[] = [];
  officesSharedCollection: IOffice[] = [];

  protected employeeService = inject(EmployeeService);
  protected employeeFormService = inject(EmployeeFormService);
  protected contactDetailsService = inject(ContactDetailsService);
  protected jobPositionService = inject(JobPositionService);
  protected documentService = inject(DocumentService);
  protected atmService = inject(AtmService);
  protected officeService = inject(OfficeService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: EmployeeFormGroup = this.employeeFormService.createEmployeeFormGroup();

  compareContactDetails = (o1: IContactDetails | null, o2: IContactDetails | null): boolean =>
    this.contactDetailsService.compareContactDetails(o1, o2);

  compareJobPosition = (o1: IJobPosition | null, o2: IJobPosition | null): boolean => this.jobPositionService.compareJobPosition(o1, o2);

  compareDocument = (o1: IDocument | null, o2: IDocument | null): boolean => this.documentService.compareDocument(o1, o2);

  compareEmployee = (o1: IEmployee | null, o2: IEmployee | null): boolean => this.employeeService.compareEmployee(o1, o2);

  compareAtm = (o1: IAtm | null, o2: IAtm | null): boolean => this.atmService.compareAtm(o1, o2);

  compareOffice = (o1: IOffice | null, o2: IOffice | null): boolean => this.officeService.compareOffice(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employee }) => {
      this.employee = employee;
      if (employee) {
        this.updateForm(employee);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const employee = this.employeeFormService.getEmployee(this.editForm);
    if (employee.id !== null) {
      this.subscribeToSaveResponse(this.employeeService.update(employee));
    } else {
      this.subscribeToSaveResponse(this.employeeService.create(employee));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(employee: IEmployee): void {
    this.employee = employee;
    this.employeeFormService.resetForm(this.editForm, employee);

    this.contactDetailsSharedCollection = this.contactDetailsService.addContactDetailsToCollectionIfMissing<IContactDetails>(
      this.contactDetailsSharedCollection,
      employee.contactDetails,
    );
    this.jobPositionsSharedCollection = this.jobPositionService.addJobPositionToCollectionIfMissing<IJobPosition>(
      this.jobPositionsSharedCollection,
      employee.jobPosition,
    );
    this.documentsSharedCollection = this.documentService.addDocumentToCollectionIfMissing<IDocument>(
      this.documentsSharedCollection,
      employee.employeeContract,
    );
    this.employeesSharedCollection = this.employeeService.addEmployeeToCollectionIfMissing<IEmployee>(
      this.employeesSharedCollection,
      employee.manager,
    );
    this.atmsSharedCollection = this.atmService.addAtmToCollectionIfMissing<IAtm>(this.atmsSharedCollection, employee.atm);
    this.officesSharedCollection = this.officeService.addOfficeToCollectionIfMissing<IOffice>(
      this.officesSharedCollection,
      employee.office,
    );
  }

  protected loadRelationshipsOptions(): void {
    this.contactDetailsService
      .query()
      .pipe(map((res: HttpResponse<IContactDetails[]>) => res.body ?? []))
      .pipe(
        map((contactDetails: IContactDetails[]) =>
          this.contactDetailsService.addContactDetailsToCollectionIfMissing<IContactDetails>(contactDetails, this.employee?.contactDetails),
        ),
      )
      .subscribe((contactDetails: IContactDetails[]) => (this.contactDetailsSharedCollection = contactDetails));

    this.jobPositionService
      .query()
      .pipe(map((res: HttpResponse<IJobPosition[]>) => res.body ?? []))
      .pipe(
        map((jobPositions: IJobPosition[]) =>
          this.jobPositionService.addJobPositionToCollectionIfMissing<IJobPosition>(jobPositions, this.employee?.jobPosition),
        ),
      )
      .subscribe((jobPositions: IJobPosition[]) => (this.jobPositionsSharedCollection = jobPositions));

    this.documentService
      .query()
      .pipe(map((res: HttpResponse<IDocument[]>) => res.body ?? []))
      .pipe(
        map((documents: IDocument[]) =>
          this.documentService.addDocumentToCollectionIfMissing<IDocument>(documents, this.employee?.employeeContract),
        ),
      )
      .subscribe((documents: IDocument[]) => (this.documentsSharedCollection = documents));

    this.employeeService
      .query()
      .pipe(map((res: HttpResponse<IEmployee[]>) => res.body ?? []))
      .pipe(
        map((employees: IEmployee[]) =>
          this.employeeService.addEmployeeToCollectionIfMissing<IEmployee>(employees, this.employee?.manager),
        ),
      )
      .subscribe((employees: IEmployee[]) => (this.employeesSharedCollection = employees));

    this.atmService
      .query()
      .pipe(map((res: HttpResponse<IAtm[]>) => res.body ?? []))
      .pipe(map((atms: IAtm[]) => this.atmService.addAtmToCollectionIfMissing<IAtm>(atms, this.employee?.atm)))
      .subscribe((atms: IAtm[]) => (this.atmsSharedCollection = atms));

    this.officeService
      .query()
      .pipe(map((res: HttpResponse<IOffice[]>) => res.body ?? []))
      .pipe(map((offices: IOffice[]) => this.officeService.addOfficeToCollectionIfMissing<IOffice>(offices, this.employee?.office)))
      .subscribe((offices: IOffice[]) => (this.officesSharedCollection = offices));
  }
}
