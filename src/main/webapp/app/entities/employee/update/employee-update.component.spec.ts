import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IContactDetails } from 'app/entities/contact-details/contact-details.model';
import { ContactDetailsService } from 'app/entities/contact-details/service/contact-details.service';
import { IJobPosition } from 'app/entities/job-position/job-position.model';
import { JobPositionService } from 'app/entities/job-position/service/job-position.service';
import { IDocument } from 'app/entities/document/document.model';
import { DocumentService } from 'app/entities/document/service/document.service';
import { IAtm } from 'app/entities/atm/atm.model';
import { AtmService } from 'app/entities/atm/service/atm.service';
import { IOffice } from 'app/entities/office/office.model';
import { OfficeService } from 'app/entities/office/service/office.service';
import { IEmployee } from '../employee.model';
import { EmployeeService } from '../service/employee.service';
import { EmployeeFormService } from './employee-form.service';

import { EmployeeUpdateComponent } from './employee-update.component';

describe('Employee Management Update Component', () => {
  let comp: EmployeeUpdateComponent;
  let fixture: ComponentFixture<EmployeeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let employeeFormService: EmployeeFormService;
  let employeeService: EmployeeService;
  let contactDetailsService: ContactDetailsService;
  let jobPositionService: JobPositionService;
  let documentService: DocumentService;
  let atmService: AtmService;
  let officeService: OfficeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [EmployeeUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EmployeeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmployeeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    employeeFormService = TestBed.inject(EmployeeFormService);
    employeeService = TestBed.inject(EmployeeService);
    contactDetailsService = TestBed.inject(ContactDetailsService);
    jobPositionService = TestBed.inject(JobPositionService);
    documentService = TestBed.inject(DocumentService);
    atmService = TestBed.inject(AtmService);
    officeService = TestBed.inject(OfficeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ContactDetails query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const contactDetails: IContactDetails = { id: 21831 };
      employee.contactDetails = contactDetails;

      const contactDetailsCollection: IContactDetails[] = [{ id: 459 }];
      jest.spyOn(contactDetailsService, 'query').mockReturnValue(of(new HttpResponse({ body: contactDetailsCollection })));
      const additionalContactDetails = [contactDetails];
      const expectedCollection: IContactDetails[] = [...additionalContactDetails, ...contactDetailsCollection];
      jest.spyOn(contactDetailsService, 'addContactDetailsToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(contactDetailsService.query).toHaveBeenCalled();
      expect(contactDetailsService.addContactDetailsToCollectionIfMissing).toHaveBeenCalledWith(
        contactDetailsCollection,
        ...additionalContactDetails.map(expect.objectContaining),
      );
      expect(comp.contactDetailsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call JobPosition query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const jobPosition: IJobPosition = { id: 2817 };
      employee.jobPosition = jobPosition;

      const jobPositionCollection: IJobPosition[] = [{ id: 19288 }];
      jest.spyOn(jobPositionService, 'query').mockReturnValue(of(new HttpResponse({ body: jobPositionCollection })));
      const additionalJobPositions = [jobPosition];
      const expectedCollection: IJobPosition[] = [...additionalJobPositions, ...jobPositionCollection];
      jest.spyOn(jobPositionService, 'addJobPositionToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(jobPositionService.query).toHaveBeenCalled();
      expect(jobPositionService.addJobPositionToCollectionIfMissing).toHaveBeenCalledWith(
        jobPositionCollection,
        ...additionalJobPositions.map(expect.objectContaining),
      );
      expect(comp.jobPositionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Document query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const employeeContract: IDocument = { id: 17380 };
      employee.employeeContract = employeeContract;

      const documentCollection: IDocument[] = [{ id: 20457 }];
      jest.spyOn(documentService, 'query').mockReturnValue(of(new HttpResponse({ body: documentCollection })));
      const additionalDocuments = [employeeContract];
      const expectedCollection: IDocument[] = [...additionalDocuments, ...documentCollection];
      jest.spyOn(documentService, 'addDocumentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(documentService.query).toHaveBeenCalled();
      expect(documentService.addDocumentToCollectionIfMissing).toHaveBeenCalledWith(
        documentCollection,
        ...additionalDocuments.map(expect.objectContaining),
      );
      expect(comp.documentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employee query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const manager: IEmployee = { id: 32453 };
      employee.manager = manager;

      const employeeCollection: IEmployee[] = [{ id: 2644 }];
      jest.spyOn(employeeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeeCollection })));
      const additionalEmployees = [manager];
      const expectedCollection: IEmployee[] = [...additionalEmployees, ...employeeCollection];
      jest.spyOn(employeeService, 'addEmployeeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(employeeService.query).toHaveBeenCalled();
      expect(employeeService.addEmployeeToCollectionIfMissing).toHaveBeenCalledWith(
        employeeCollection,
        ...additionalEmployees.map(expect.objectContaining),
      );
      expect(comp.employeesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Atm query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const atm: IAtm = { id: 21804 };
      employee.atm = atm;

      const atmCollection: IAtm[] = [{ id: 25236 }];
      jest.spyOn(atmService, 'query').mockReturnValue(of(new HttpResponse({ body: atmCollection })));
      const additionalAtms = [atm];
      const expectedCollection: IAtm[] = [...additionalAtms, ...atmCollection];
      jest.spyOn(atmService, 'addAtmToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(atmService.query).toHaveBeenCalled();
      expect(atmService.addAtmToCollectionIfMissing).toHaveBeenCalledWith(atmCollection, ...additionalAtms.map(expect.objectContaining));
      expect(comp.atmsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Office query and add missing value', () => {
      const employee: IEmployee = { id: 456 };
      const office: IOffice = { id: 29605 };
      employee.office = office;

      const officeCollection: IOffice[] = [{ id: 12414 }];
      jest.spyOn(officeService, 'query').mockReturnValue(of(new HttpResponse({ body: officeCollection })));
      const additionalOffices = [office];
      const expectedCollection: IOffice[] = [...additionalOffices, ...officeCollection];
      jest.spyOn(officeService, 'addOfficeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(officeService.query).toHaveBeenCalled();
      expect(officeService.addOfficeToCollectionIfMissing).toHaveBeenCalledWith(
        officeCollection,
        ...additionalOffices.map(expect.objectContaining),
      );
      expect(comp.officesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const employee: IEmployee = { id: 456 };
      const contactDetails: IContactDetails = { id: 28460 };
      employee.contactDetails = contactDetails;
      const jobPosition: IJobPosition = { id: 16753 };
      employee.jobPosition = jobPosition;
      const employeeContract: IDocument = { id: 15692 };
      employee.employeeContract = employeeContract;
      const manager: IEmployee = { id: 24054 };
      employee.manager = manager;
      const atm: IAtm = { id: 21943 };
      employee.atm = atm;
      const office: IOffice = { id: 24587 };
      employee.office = office;

      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      expect(comp.contactDetailsSharedCollection).toContain(contactDetails);
      expect(comp.jobPositionsSharedCollection).toContain(jobPosition);
      expect(comp.documentsSharedCollection).toContain(employeeContract);
      expect(comp.employeesSharedCollection).toContain(manager);
      expect(comp.atmsSharedCollection).toContain(atm);
      expect(comp.officesSharedCollection).toContain(office);
      expect(comp.employee).toEqual(employee);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployee>>();
      const employee = { id: 123 };
      jest.spyOn(employeeFormService, 'getEmployee').mockReturnValue(employee);
      jest.spyOn(employeeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employee }));
      saveSubject.complete();

      // THEN
      expect(employeeFormService.getEmployee).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(employeeService.update).toHaveBeenCalledWith(expect.objectContaining(employee));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployee>>();
      const employee = { id: 123 };
      jest.spyOn(employeeFormService, 'getEmployee').mockReturnValue({ id: null });
      jest.spyOn(employeeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employee: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employee }));
      saveSubject.complete();

      // THEN
      expect(employeeFormService.getEmployee).toHaveBeenCalled();
      expect(employeeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmployee>>();
      const employee = { id: 123 };
      jest.spyOn(employeeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employee });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(employeeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareContactDetails', () => {
      it('Should forward to contactDetailsService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(contactDetailsService, 'compareContactDetails');
        comp.compareContactDetails(entity, entity2);
        expect(contactDetailsService.compareContactDetails).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareJobPosition', () => {
      it('Should forward to jobPositionService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(jobPositionService, 'compareJobPosition');
        comp.compareJobPosition(entity, entity2);
        expect(jobPositionService.compareJobPosition).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareDocument', () => {
      it('Should forward to documentService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(documentService, 'compareDocument');
        comp.compareDocument(entity, entity2);
        expect(documentService.compareDocument).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmployee', () => {
      it('Should forward to employeeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeeService, 'compareEmployee');
        comp.compareEmployee(entity, entity2);
        expect(employeeService.compareEmployee).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareAtm', () => {
      it('Should forward to atmService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(atmService, 'compareAtm');
        comp.compareAtm(entity, entity2);
        expect(atmService.compareAtm).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareOffice', () => {
      it('Should forward to officeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(officeService, 'compareOffice');
        comp.compareOffice(entity, entity2);
        expect(officeService.compareOffice).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
