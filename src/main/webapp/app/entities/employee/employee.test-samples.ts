import { IEmployee, NewEmployee } from './employee.model';

export const sampleWithRequiredData: IEmployee = {
  id: 15399,
};

export const sampleWithPartialData: IEmployee = {
  id: 15078,
  isOnHoliday: true,
};

export const sampleWithFullData: IEmployee = {
  id: 21702,
  isOnHoliday: false,
};

export const sampleWithNewData: NewEmployee = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
