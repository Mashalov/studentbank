import { IDocumentCategory } from 'app/entities/document-category/document-category.model';
import { IApplication } from 'app/entities/application/application.model';
import { ICustomer } from 'app/entities/customer/customer.model';
import { DocumentType } from 'app/entities/enumerations/document-type.model';

export interface IDocument {
  id: number;
  name?: string | null;
  documentExtension?: string | null;
  documentData?: string | null;
  documentDataContentType?: string | null;
  documentType?: keyof typeof DocumentType | null;
  isOrinalDocument?: boolean | null;
  documentCompression?: string | null;
  documentCategory?: Pick<IDocumentCategory, 'id'> | null;
  application?: Pick<IApplication, 'id'> | null;
  customer?: Pick<ICustomer, 'id'> | null;
}

export type NewDocument = Omit<IDocument, 'id'> & { id: null };
