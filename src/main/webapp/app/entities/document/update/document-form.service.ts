import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDocument, NewDocument } from '../document.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDocument for edit and NewDocumentFormGroupInput for create.
 */
type DocumentFormGroupInput = IDocument | PartialWithRequiredKeyOf<NewDocument>;

type DocumentFormDefaults = Pick<NewDocument, 'id' | 'isOrinalDocument'>;

type DocumentFormGroupContent = {
  id: FormControl<IDocument['id'] | NewDocument['id']>;
  name: FormControl<IDocument['name']>;
  documentExtension: FormControl<IDocument['documentExtension']>;
  documentData: FormControl<IDocument['documentData']>;
  documentDataContentType: FormControl<IDocument['documentDataContentType']>;
  documentType: FormControl<IDocument['documentType']>;
  isOrinalDocument: FormControl<IDocument['isOrinalDocument']>;
  documentCompression: FormControl<IDocument['documentCompression']>;
  documentCategory: FormControl<IDocument['documentCategory']>;
  application: FormControl<IDocument['application']>;
  customer: FormControl<IDocument['customer']>;
};

export type DocumentFormGroup = FormGroup<DocumentFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DocumentFormService {
  createDocumentFormGroup(document: DocumentFormGroupInput = { id: null }): DocumentFormGroup {
    const documentRawValue = {
      ...this.getFormDefaults(),
      ...document,
    };
    return new FormGroup<DocumentFormGroupContent>({
      id: new FormControl(
        { value: documentRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(documentRawValue.name, {
        validators: [Validators.required],
      }),
      documentExtension: new FormControl(documentRawValue.documentExtension, {
        validators: [Validators.required],
      }),
      documentData: new FormControl(documentRawValue.documentData, {
        validators: [Validators.required],
      }),
      documentDataContentType: new FormControl(documentRawValue.documentDataContentType),
      documentType: new FormControl(documentRawValue.documentType, {
        validators: [Validators.required],
      }),
      isOrinalDocument: new FormControl(documentRawValue.isOrinalDocument),
      documentCompression: new FormControl(documentRawValue.documentCompression),
      documentCategory: new FormControl(documentRawValue.documentCategory),
      application: new FormControl(documentRawValue.application),
      customer: new FormControl(documentRawValue.customer),
    });
  }

  getDocument(form: DocumentFormGroup): IDocument | NewDocument {
    return form.getRawValue() as IDocument | NewDocument;
  }

  resetForm(form: DocumentFormGroup, document: DocumentFormGroupInput): void {
    const documentRawValue = { ...this.getFormDefaults(), ...document };
    form.reset(
      {
        ...documentRawValue,
        id: { value: documentRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): DocumentFormDefaults {
    return {
      id: null,
      isOrinalDocument: false,
    };
  }
}
