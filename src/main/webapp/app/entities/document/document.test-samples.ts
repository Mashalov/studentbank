import { IDocument, NewDocument } from './document.model';

export const sampleWithRequiredData: IDocument = {
  id: 23055,
  name: 'pint keeper messy',
  documentExtension: 'untrue talk',
  documentData: '../fake-data/blob/hipster.png',
  documentDataContentType: 'unknown',
  documentType: 'DOCX',
};

export const sampleWithPartialData: IDocument = {
  id: 6116,
  name: 'duh and supposing',
  documentExtension: 'forenenst estrogen',
  documentData: '../fake-data/blob/hipster.png',
  documentDataContentType: 'unknown',
  documentType: 'DOCX',
  isOrinalDocument: true,
};

export const sampleWithFullData: IDocument = {
  id: 24973,
  name: 'yahoo',
  documentExtension: 'hospital spiteful meh',
  documentData: '../fake-data/blob/hipster.png',
  documentDataContentType: 'unknown',
  documentType: 'PDF',
  isOrinalDocument: true,
  documentCompression: 'among forenenst',
};

export const sampleWithNewData: NewDocument = {
  name: 'ouch',
  documentExtension: 'and anodize anywhere',
  documentData: '../fake-data/blob/hipster.png',
  documentDataContentType: 'unknown',
  documentType: 'PDF',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
