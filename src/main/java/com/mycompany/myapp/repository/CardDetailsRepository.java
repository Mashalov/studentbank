package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CardDetails;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CardDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CardDetailsRepository extends JpaRepository<CardDetails, Long> {}
