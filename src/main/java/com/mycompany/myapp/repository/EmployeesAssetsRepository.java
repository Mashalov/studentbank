package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.EmployeesAssets;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the EmployeesAssets entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmployeesAssetsRepository extends JpaRepository<EmployeesAssets, Long> {}
