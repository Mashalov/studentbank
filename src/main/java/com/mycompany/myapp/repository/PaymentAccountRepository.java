package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.PaymentAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PaymentAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentAccountRepository extends JpaRepository<PaymentAccount, Long> {}
