package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.JobPosition;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the JobPosition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JobPositionRepository extends JpaRepository<JobPosition, Long> {}
