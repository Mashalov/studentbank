package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Atm;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Atm entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtmRepository extends JpaRepository<Atm, Long> {}
