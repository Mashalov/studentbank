package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.DocumentCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DocumentCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocumentCategoryRepository extends JpaRepository<DocumentCategory, Long> {}
