package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.EmployeesAssets;
import com.mycompany.myapp.repository.EmployeesAssetsRepository;
import com.mycompany.myapp.service.dto.EmployeesAssetsDTO;
import com.mycompany.myapp.service.mapper.EmployeesAssetsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.EmployeesAssets}.
 */
@Service
@Transactional
public class EmployeesAssetsService {

    private static final Logger log = LoggerFactory.getLogger(EmployeesAssetsService.class);

    private final EmployeesAssetsRepository employeesAssetsRepository;

    private final EmployeesAssetsMapper employeesAssetsMapper;

    public EmployeesAssetsService(EmployeesAssetsRepository employeesAssetsRepository, EmployeesAssetsMapper employeesAssetsMapper) {
        this.employeesAssetsRepository = employeesAssetsRepository;
        this.employeesAssetsMapper = employeesAssetsMapper;
    }

    /**
     * Save a employeesAssets.
     *
     * @param employeesAssetsDTO the entity to save.
     * @return the persisted entity.
     */
    public EmployeesAssetsDTO save(EmployeesAssetsDTO employeesAssetsDTO) {
        log.debug("Request to save EmployeesAssets : {}", employeesAssetsDTO);
        EmployeesAssets employeesAssets = employeesAssetsMapper.toEntity(employeesAssetsDTO);
        employeesAssets = employeesAssetsRepository.save(employeesAssets);
        return employeesAssetsMapper.toDto(employeesAssets);
    }

    /**
     * Update a employeesAssets.
     *
     * @param employeesAssetsDTO the entity to save.
     * @return the persisted entity.
     */
    public EmployeesAssetsDTO update(EmployeesAssetsDTO employeesAssetsDTO) {
        log.debug("Request to update EmployeesAssets : {}", employeesAssetsDTO);
        EmployeesAssets employeesAssets = employeesAssetsMapper.toEntity(employeesAssetsDTO);
        employeesAssets = employeesAssetsRepository.save(employeesAssets);
        return employeesAssetsMapper.toDto(employeesAssets);
    }

    /**
     * Partially update a employeesAssets.
     *
     * @param employeesAssetsDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EmployeesAssetsDTO> partialUpdate(EmployeesAssetsDTO employeesAssetsDTO) {
        log.debug("Request to partially update EmployeesAssets : {}", employeesAssetsDTO);

        return employeesAssetsRepository
            .findById(employeesAssetsDTO.getId())
            .map(existingEmployeesAssets -> {
                employeesAssetsMapper.partialUpdate(existingEmployeesAssets, employeesAssetsDTO);

                return existingEmployeesAssets;
            })
            .map(employeesAssetsRepository::save)
            .map(employeesAssetsMapper::toDto);
    }

    /**
     * Get all the employeesAssets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EmployeesAssetsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EmployeesAssets");
        return employeesAssetsRepository.findAll(pageable).map(employeesAssetsMapper::toDto);
    }

    /**
     * Get one employeesAssets by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EmployeesAssetsDTO> findOne(Long id) {
        log.debug("Request to get EmployeesAssets : {}", id);
        return employeesAssetsRepository.findById(id).map(employeesAssetsMapper::toDto);
    }

    /**
     * Delete the employeesAssets by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EmployeesAssets : {}", id);
        employeesAssetsRepository.deleteById(id);
    }
}
