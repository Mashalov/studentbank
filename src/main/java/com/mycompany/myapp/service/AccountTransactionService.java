package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.AccountTransaction;
import com.mycompany.myapp.repository.AccountTransactionRepository;
import com.mycompany.myapp.service.dto.AccountTransactionDTO;
import com.mycompany.myapp.service.mapper.AccountTransactionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.AccountTransaction}.
 */
@Service
@Transactional
public class AccountTransactionService {

    private static final Logger log = LoggerFactory.getLogger(AccountTransactionService.class);

    private final AccountTransactionRepository accountTransactionRepository;

    private final AccountTransactionMapper accountTransactionMapper;

    public AccountTransactionService(
        AccountTransactionRepository accountTransactionRepository,
        AccountTransactionMapper accountTransactionMapper
    ) {
        this.accountTransactionRepository = accountTransactionRepository;
        this.accountTransactionMapper = accountTransactionMapper;
    }

    /**
     * Save a accountTransaction.
     *
     * @param accountTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    public AccountTransactionDTO save(AccountTransactionDTO accountTransactionDTO) {
        log.debug("Request to save AccountTransaction : {}", accountTransactionDTO);
        AccountTransaction accountTransaction = accountTransactionMapper.toEntity(accountTransactionDTO);
        accountTransaction = accountTransactionRepository.save(accountTransaction);
        return accountTransactionMapper.toDto(accountTransaction);
    }

    /**
     * Update a accountTransaction.
     *
     * @param accountTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    public AccountTransactionDTO update(AccountTransactionDTO accountTransactionDTO) {
        log.debug("Request to update AccountTransaction : {}", accountTransactionDTO);
        AccountTransaction accountTransaction = accountTransactionMapper.toEntity(accountTransactionDTO);
        accountTransaction = accountTransactionRepository.save(accountTransaction);
        return accountTransactionMapper.toDto(accountTransaction);
    }

    /**
     * Partially update a accountTransaction.
     *
     * @param accountTransactionDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AccountTransactionDTO> partialUpdate(AccountTransactionDTO accountTransactionDTO) {
        log.debug("Request to partially update AccountTransaction : {}", accountTransactionDTO);

        return accountTransactionRepository
            .findById(accountTransactionDTO.getId())
            .map(existingAccountTransaction -> {
                accountTransactionMapper.partialUpdate(existingAccountTransaction, accountTransactionDTO);

                return existingAccountTransaction;
            })
            .map(accountTransactionRepository::save)
            .map(accountTransactionMapper::toDto);
    }

    /**
     * Get all the accountTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AccountTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccountTransactions");
        return accountTransactionRepository.findAll(pageable).map(accountTransactionMapper::toDto);
    }

    /**
     * Get one accountTransaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AccountTransactionDTO> findOne(Long id) {
        log.debug("Request to get AccountTransaction : {}", id);
        return accountTransactionRepository.findById(id).map(accountTransactionMapper::toDto);
    }

    /**
     * Delete the accountTransaction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AccountTransaction : {}", id);
        accountTransactionRepository.deleteById(id);
    }
}
