package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Atm;
import com.mycompany.myapp.repository.AtmRepository;
import com.mycompany.myapp.service.dto.AtmDTO;
import com.mycompany.myapp.service.mapper.AtmMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.Atm}.
 */
@Service
@Transactional
public class AtmService {

    private static final Logger log = LoggerFactory.getLogger(AtmService.class);

    private final AtmRepository atmRepository;

    private final AtmMapper atmMapper;

    public AtmService(AtmRepository atmRepository, AtmMapper atmMapper) {
        this.atmRepository = atmRepository;
        this.atmMapper = atmMapper;
    }

    /**
     * Save a atm.
     *
     * @param atmDTO the entity to save.
     * @return the persisted entity.
     */
    public AtmDTO save(AtmDTO atmDTO) {
        log.debug("Request to save Atm : {}", atmDTO);
        Atm atm = atmMapper.toEntity(atmDTO);
        atm = atmRepository.save(atm);
        return atmMapper.toDto(atm);
    }

    /**
     * Update a atm.
     *
     * @param atmDTO the entity to save.
     * @return the persisted entity.
     */
    public AtmDTO update(AtmDTO atmDTO) {
        log.debug("Request to update Atm : {}", atmDTO);
        Atm atm = atmMapper.toEntity(atmDTO);
        atm = atmRepository.save(atm);
        return atmMapper.toDto(atm);
    }

    /**
     * Partially update a atm.
     *
     * @param atmDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AtmDTO> partialUpdate(AtmDTO atmDTO) {
        log.debug("Request to partially update Atm : {}", atmDTO);

        return atmRepository
            .findById(atmDTO.getId())
            .map(existingAtm -> {
                atmMapper.partialUpdate(existingAtm, atmDTO);

                return existingAtm;
            })
            .map(atmRepository::save)
            .map(atmMapper::toDto);
    }

    /**
     * Get all the atms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AtmDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Atms");
        return atmRepository.findAll(pageable).map(atmMapper::toDto);
    }

    /**
     * Get one atm by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AtmDTO> findOne(Long id) {
        log.debug("Request to get Atm : {}", id);
        return atmRepository.findById(id).map(atmMapper::toDto);
    }

    /**
     * Delete the atm by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Atm : {}", id);
        atmRepository.deleteById(id);
    }
}
