package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.DocumentCategory;
import com.mycompany.myapp.repository.DocumentCategoryRepository;
import com.mycompany.myapp.service.dto.DocumentCategoryDTO;
import com.mycompany.myapp.service.mapper.DocumentCategoryMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.DocumentCategory}.
 */
@Service
@Transactional
public class DocumentCategoryService {

    private static final Logger log = LoggerFactory.getLogger(DocumentCategoryService.class);

    private final DocumentCategoryRepository documentCategoryRepository;

    private final DocumentCategoryMapper documentCategoryMapper;

    public DocumentCategoryService(DocumentCategoryRepository documentCategoryRepository, DocumentCategoryMapper documentCategoryMapper) {
        this.documentCategoryRepository = documentCategoryRepository;
        this.documentCategoryMapper = documentCategoryMapper;
    }

    /**
     * Save a documentCategory.
     *
     * @param documentCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    public DocumentCategoryDTO save(DocumentCategoryDTO documentCategoryDTO) {
        log.debug("Request to save DocumentCategory : {}", documentCategoryDTO);
        DocumentCategory documentCategory = documentCategoryMapper.toEntity(documentCategoryDTO);
        documentCategory = documentCategoryRepository.save(documentCategory);
        return documentCategoryMapper.toDto(documentCategory);
    }

    /**
     * Update a documentCategory.
     *
     * @param documentCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    public DocumentCategoryDTO update(DocumentCategoryDTO documentCategoryDTO) {
        log.debug("Request to update DocumentCategory : {}", documentCategoryDTO);
        DocumentCategory documentCategory = documentCategoryMapper.toEntity(documentCategoryDTO);
        documentCategory = documentCategoryRepository.save(documentCategory);
        return documentCategoryMapper.toDto(documentCategory);
    }

    /**
     * Partially update a documentCategory.
     *
     * @param documentCategoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DocumentCategoryDTO> partialUpdate(DocumentCategoryDTO documentCategoryDTO) {
        log.debug("Request to partially update DocumentCategory : {}", documentCategoryDTO);

        return documentCategoryRepository
            .findById(documentCategoryDTO.getId())
            .map(existingDocumentCategory -> {
                documentCategoryMapper.partialUpdate(existingDocumentCategory, documentCategoryDTO);

                return existingDocumentCategory;
            })
            .map(documentCategoryRepository::save)
            .map(documentCategoryMapper::toDto);
    }

    /**
     * Get all the documentCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DocumentCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DocumentCategories");
        return documentCategoryRepository.findAll(pageable).map(documentCategoryMapper::toDto);
    }

    /**
     * Get one documentCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DocumentCategoryDTO> findOne(Long id) {
        log.debug("Request to get DocumentCategory : {}", id);
        return documentCategoryRepository.findById(id).map(documentCategoryMapper::toDto);
    }

    /**
     * Delete the documentCategory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DocumentCategory : {}", id);
        documentCategoryRepository.deleteById(id);
    }
}
