package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.PaymentAccount;
import com.mycompany.myapp.repository.PaymentAccountRepository;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import com.mycompany.myapp.service.mapper.PaymentAccountMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.PaymentAccount}.
 */
@Service
@Transactional
public class PaymentAccountService {

    private static final Logger log = LoggerFactory.getLogger(PaymentAccountService.class);

    private final PaymentAccountRepository paymentAccountRepository;

    private final PaymentAccountMapper paymentAccountMapper;

    public PaymentAccountService(PaymentAccountRepository paymentAccountRepository, PaymentAccountMapper paymentAccountMapper) {
        this.paymentAccountRepository = paymentAccountRepository;
        this.paymentAccountMapper = paymentAccountMapper;
    }

    /**
     * Save a paymentAccount.
     *
     * @param paymentAccountDTO the entity to save.
     * @return the persisted entity.
     */
    public PaymentAccountDTO save(PaymentAccountDTO paymentAccountDTO) {
        log.debug("Request to save PaymentAccount : {}", paymentAccountDTO);
        PaymentAccount paymentAccount = paymentAccountMapper.toEntity(paymentAccountDTO);
        paymentAccount = paymentAccountRepository.save(paymentAccount);
        return paymentAccountMapper.toDto(paymentAccount);
    }

    /**
     * Update a paymentAccount.
     *
     * @param paymentAccountDTO the entity to save.
     * @return the persisted entity.
     */
    public PaymentAccountDTO update(PaymentAccountDTO paymentAccountDTO) {
        log.debug("Request to update PaymentAccount : {}", paymentAccountDTO);
        PaymentAccount paymentAccount = paymentAccountMapper.toEntity(paymentAccountDTO);
        paymentAccount = paymentAccountRepository.save(paymentAccount);
        return paymentAccountMapper.toDto(paymentAccount);
    }

    /**
     * Partially update a paymentAccount.
     *
     * @param paymentAccountDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PaymentAccountDTO> partialUpdate(PaymentAccountDTO paymentAccountDTO) {
        log.debug("Request to partially update PaymentAccount : {}", paymentAccountDTO);

        return paymentAccountRepository
            .findById(paymentAccountDTO.getId())
            .map(existingPaymentAccount -> {
                paymentAccountMapper.partialUpdate(existingPaymentAccount, paymentAccountDTO);

                return existingPaymentAccount;
            })
            .map(paymentAccountRepository::save)
            .map(paymentAccountMapper::toDto);
    }

    /**
     * Get all the paymentAccounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PaymentAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentAccounts");
        return paymentAccountRepository.findAll(pageable).map(paymentAccountMapper::toDto);
    }

    /**
     * Get one paymentAccount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentAccountDTO> findOne(Long id) {
        log.debug("Request to get PaymentAccount : {}", id);
        return paymentAccountRepository.findById(id).map(paymentAccountMapper::toDto);
    }

    /**
     * Delete the paymentAccount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentAccount : {}", id);
        paymentAccountRepository.deleteById(id);
    }
}
