package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.JobPosition;
import com.mycompany.myapp.repository.JobPositionRepository;
import com.mycompany.myapp.service.dto.JobPositionDTO;
import com.mycompany.myapp.service.mapper.JobPositionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.JobPosition}.
 */
@Service
@Transactional
public class JobPositionService {

    private static final Logger log = LoggerFactory.getLogger(JobPositionService.class);

    private final JobPositionRepository jobPositionRepository;

    private final JobPositionMapper jobPositionMapper;

    public JobPositionService(JobPositionRepository jobPositionRepository, JobPositionMapper jobPositionMapper) {
        this.jobPositionRepository = jobPositionRepository;
        this.jobPositionMapper = jobPositionMapper;
    }

    /**
     * Save a jobPosition.
     *
     * @param jobPositionDTO the entity to save.
     * @return the persisted entity.
     */
    public JobPositionDTO save(JobPositionDTO jobPositionDTO) {
        log.debug("Request to save JobPosition : {}", jobPositionDTO);
        JobPosition jobPosition = jobPositionMapper.toEntity(jobPositionDTO);
        jobPosition = jobPositionRepository.save(jobPosition);
        return jobPositionMapper.toDto(jobPosition);
    }

    /**
     * Update a jobPosition.
     *
     * @param jobPositionDTO the entity to save.
     * @return the persisted entity.
     */
    public JobPositionDTO update(JobPositionDTO jobPositionDTO) {
        log.debug("Request to update JobPosition : {}", jobPositionDTO);
        JobPosition jobPosition = jobPositionMapper.toEntity(jobPositionDTO);
        jobPosition = jobPositionRepository.save(jobPosition);
        return jobPositionMapper.toDto(jobPosition);
    }

    /**
     * Partially update a jobPosition.
     *
     * @param jobPositionDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<JobPositionDTO> partialUpdate(JobPositionDTO jobPositionDTO) {
        log.debug("Request to partially update JobPosition : {}", jobPositionDTO);

        return jobPositionRepository
            .findById(jobPositionDTO.getId())
            .map(existingJobPosition -> {
                jobPositionMapper.partialUpdate(existingJobPosition, jobPositionDTO);

                return existingJobPosition;
            })
            .map(jobPositionRepository::save)
            .map(jobPositionMapper::toDto);
    }

    /**
     * Get all the jobPositions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<JobPositionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all JobPositions");
        return jobPositionRepository.findAll(pageable).map(jobPositionMapper::toDto);
    }

    /**
     * Get one jobPosition by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<JobPositionDTO> findOne(Long id) {
        log.debug("Request to get JobPosition : {}", id);
        return jobPositionRepository.findById(id).map(jobPositionMapper::toDto);
    }

    /**
     * Delete the jobPosition by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete JobPosition : {}", id);
        jobPositionRepository.deleteById(id);
    }
}
