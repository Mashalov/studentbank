package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.domain.Document;
import com.mycompany.myapp.domain.PaymentAccount;
import com.mycompany.myapp.service.dto.CustomerDTO;
import com.mycompany.myapp.service.dto.DocumentDTO;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PaymentAccount} and its DTO {@link PaymentAccountDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaymentAccountMapper extends EntityMapper<PaymentAccountDTO, PaymentAccount> {
    @Mapping(target = "contract", source = "contract", qualifiedByName = "documentId")
    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
    PaymentAccountDTO toDto(PaymentAccount s);

    @Named("documentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentDTO toDtoDocumentId(Document document);

    @Named("customerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CustomerDTO toDtoCustomerId(Customer customer);
}
