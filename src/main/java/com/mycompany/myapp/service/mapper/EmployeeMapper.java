package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Atm;
import com.mycompany.myapp.domain.ContactDetails;
import com.mycompany.myapp.domain.Document;
import com.mycompany.myapp.domain.Employee;
import com.mycompany.myapp.domain.JobPosition;
import com.mycompany.myapp.domain.Office;
import com.mycompany.myapp.service.dto.AtmDTO;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.service.dto.DocumentDTO;
import com.mycompany.myapp.service.dto.EmployeeDTO;
import com.mycompany.myapp.service.dto.JobPositionDTO;
import com.mycompany.myapp.service.dto.OfficeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Employee} and its DTO {@link EmployeeDTO}.
 */
@Mapper(componentModel = "spring")
public interface EmployeeMapper extends EntityMapper<EmployeeDTO, Employee> {
    @Mapping(target = "contactDetails", source = "contactDetails", qualifiedByName = "contactDetailsId")
    @Mapping(target = "jobPosition", source = "jobPosition", qualifiedByName = "jobPositionId")
    @Mapping(target = "employeeContract", source = "employeeContract", qualifiedByName = "documentId")
    @Mapping(target = "manager", source = "manager", qualifiedByName = "employeeId")
    @Mapping(target = "atm", source = "atm", qualifiedByName = "atmId")
    @Mapping(target = "office", source = "office", qualifiedByName = "officeId")
    EmployeeDTO toDto(Employee s);

    @Named("contactDetailsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ContactDetailsDTO toDtoContactDetailsId(ContactDetails contactDetails);

    @Named("jobPositionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    JobPositionDTO toDtoJobPositionId(JobPosition jobPosition);

    @Named("documentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentDTO toDtoDocumentId(Document document);

    @Named("employeeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeeDTO toDtoEmployeeId(Employee employee);

    @Named("atmId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AtmDTO toDtoAtmId(Atm atm);

    @Named("officeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OfficeDTO toDtoOfficeId(Office office);
}
