package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.domain.BankDetails;
import com.mycompany.myapp.domain.Office;
import com.mycompany.myapp.service.dto.AddressDTO;
import com.mycompany.myapp.service.dto.BankDetailsDTO;
import com.mycompany.myapp.service.dto.OfficeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Office} and its DTO {@link OfficeDTO}.
 */
@Mapper(componentModel = "spring")
public interface OfficeMapper extends EntityMapper<OfficeDTO, Office> {
    @Mapping(target = "address", source = "address", qualifiedByName = "addressId")
    @Mapping(target = "bankDetails", source = "bankDetails", qualifiedByName = "bankDetailsId")
    OfficeDTO toDto(Office s);

    @Named("addressId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AddressDTO toDtoAddressId(Address address);

    @Named("bankDetailsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    BankDetailsDTO toDtoBankDetailsId(BankDetails bankDetails);
}
