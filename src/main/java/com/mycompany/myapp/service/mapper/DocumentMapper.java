package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Application;
import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.domain.Document;
import com.mycompany.myapp.domain.DocumentCategory;
import com.mycompany.myapp.service.dto.ApplicationDTO;
import com.mycompany.myapp.service.dto.CustomerDTO;
import com.mycompany.myapp.service.dto.DocumentCategoryDTO;
import com.mycompany.myapp.service.dto.DocumentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Document} and its DTO {@link DocumentDTO}.
 */
@Mapper(componentModel = "spring")
public interface DocumentMapper extends EntityMapper<DocumentDTO, Document> {
    @Mapping(target = "documentCategory", source = "documentCategory", qualifiedByName = "documentCategoryId")
    @Mapping(target = "application", source = "application", qualifiedByName = "applicationId")
    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
    DocumentDTO toDto(Document s);

    @Named("documentCategoryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentCategoryDTO toDtoDocumentCategoryId(DocumentCategory documentCategory);

    @Named("applicationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ApplicationDTO toDtoApplicationId(Application application);

    @Named("customerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CustomerDTO toDtoCustomerId(Customer customer);
}
