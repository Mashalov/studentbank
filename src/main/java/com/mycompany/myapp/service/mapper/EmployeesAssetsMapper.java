package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Document;
import com.mycompany.myapp.domain.Employee;
import com.mycompany.myapp.domain.EmployeesAssets;
import com.mycompany.myapp.service.dto.DocumentDTO;
import com.mycompany.myapp.service.dto.EmployeeDTO;
import com.mycompany.myapp.service.dto.EmployeesAssetsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EmployeesAssets} and its DTO {@link EmployeesAssetsDTO}.
 */
@Mapper(componentModel = "spring")
public interface EmployeesAssetsMapper extends EntityMapper<EmployeesAssetsDTO, EmployeesAssets> {
    @Mapping(target = "assignedBy", source = "assignedBy", qualifiedByName = "employeeId")
    @Mapping(target = "recieversProtocol", source = "recieversProtocol", qualifiedByName = "documentId")
    @Mapping(target = "employee", source = "employee", qualifiedByName = "employeeId")
    EmployeesAssetsDTO toDto(EmployeesAssets s);

    @Named("employeeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeeDTO toDtoEmployeeId(Employee employee);

    @Named("documentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentDTO toDtoDocumentId(Document document);
}
