package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.ContactDetails;
import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.service.dto.CustomerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Customer} and its DTO {@link CustomerDTO}.
 */
@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {
    @Mapping(target = "contactDetails", source = "contactDetails", qualifiedByName = "contactDetailsId")
    CustomerDTO toDto(Customer s);

    @Named("contactDetailsId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ContactDetailsDTO toDtoContactDetailsId(ContactDetails contactDetails);
}
