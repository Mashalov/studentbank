package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.ContactDetails;
import com.mycompany.myapp.domain.Office;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.service.dto.OfficeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactDetails} and its DTO {@link ContactDetailsDTO}.
 */
@Mapper(componentModel = "spring")
public interface ContactDetailsMapper extends EntityMapper<ContactDetailsDTO, ContactDetails> {
    @Mapping(target = "office", source = "office", qualifiedByName = "officeId")
    ContactDetailsDTO toDto(ContactDetails s);

    @Named("officeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OfficeDTO toDtoOfficeId(Office office);
}
