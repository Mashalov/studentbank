package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.JobPosition;
import com.mycompany.myapp.service.dto.JobPositionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link JobPosition} and its DTO {@link JobPositionDTO}.
 */
@Mapper(componentModel = "spring")
public interface JobPositionMapper extends EntityMapper<JobPositionDTO, JobPosition> {}
