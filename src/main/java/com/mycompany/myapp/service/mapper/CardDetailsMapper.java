package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.CardDetails;
import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.domain.PaymentAccount;
import com.mycompany.myapp.service.dto.CardDetailsDTO;
import com.mycompany.myapp.service.dto.CustomerDTO;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CardDetails} and its DTO {@link CardDetailsDTO}.
 */
@Mapper(componentModel = "spring")
public interface CardDetailsMapper extends EntityMapper<CardDetailsDTO, CardDetails> {
    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
    @Mapping(target = "paymentAccount", source = "paymentAccount", qualifiedByName = "paymentAccountId")
    CardDetailsDTO toDto(CardDetails s);

    @Named("customerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CustomerDTO toDtoCustomerId(Customer customer);

    @Named("paymentAccountId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PaymentAccountDTO toDtoPaymentAccountId(PaymentAccount paymentAccount);
}
