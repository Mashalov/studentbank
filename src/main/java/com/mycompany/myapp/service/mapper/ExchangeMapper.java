package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Document;
import com.mycompany.myapp.domain.Employee;
import com.mycompany.myapp.domain.Exchange;
import com.mycompany.myapp.domain.Office;
import com.mycompany.myapp.service.dto.DocumentDTO;
import com.mycompany.myapp.service.dto.EmployeeDTO;
import com.mycompany.myapp.service.dto.ExchangeDTO;
import com.mycompany.myapp.service.dto.OfficeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Exchange} and its DTO {@link ExchangeDTO}.
 */
@Mapper(componentModel = "spring")
public interface ExchangeMapper extends EntityMapper<ExchangeDTO, Exchange> {
    @Mapping(target = "employee", source = "employee", qualifiedByName = "employeeId")
    @Mapping(target = "office", source = "office", qualifiedByName = "officeId")
    @Mapping(target = "document", source = "document", qualifiedByName = "documentId")
    ExchangeDTO toDto(Exchange s);

    @Named("employeeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeeDTO toDtoEmployeeId(Employee employee);

    @Named("officeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    OfficeDTO toDtoOfficeId(Office office);

    @Named("documentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentDTO toDtoDocumentId(Document document);
}
