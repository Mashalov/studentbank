package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.AccountTransaction;
import com.mycompany.myapp.domain.PaymentAccount;
import com.mycompany.myapp.service.dto.AccountTransactionDTO;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AccountTransaction} and its DTO {@link AccountTransactionDTO}.
 */
@Mapper(componentModel = "spring")
public interface AccountTransactionMapper extends EntityMapper<AccountTransactionDTO, AccountTransaction> {
    @Mapping(target = "sender", source = "sender", qualifiedByName = "paymentAccountId")
    @Mapping(target = "receiver", source = "receiver", qualifiedByName = "paymentAccountId")
    AccountTransactionDTO toDto(AccountTransaction s);

    @Named("paymentAccountId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PaymentAccountDTO toDtoPaymentAccountId(PaymentAccount paymentAccount);
}
