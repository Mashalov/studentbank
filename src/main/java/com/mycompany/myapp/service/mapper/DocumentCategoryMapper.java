package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.DocumentCategory;
import com.mycompany.myapp.service.dto.DocumentCategoryDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DocumentCategory} and its DTO {@link DocumentCategoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface DocumentCategoryMapper extends EntityMapper<DocumentCategoryDTO, DocumentCategory> {
    @Mapping(target = "documentCategory", source = "documentCategory", qualifiedByName = "documentCategoryId")
    DocumentCategoryDTO toDto(DocumentCategory s);

    @Named("documentCategoryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DocumentCategoryDTO toDtoDocumentCategoryId(DocumentCategory documentCategory);
}
