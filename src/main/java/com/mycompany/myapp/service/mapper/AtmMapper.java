package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Address;
import com.mycompany.myapp.domain.Atm;
import com.mycompany.myapp.service.dto.AddressDTO;
import com.mycompany.myapp.service.dto.AtmDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Atm} and its DTO {@link AtmDTO}.
 */
@Mapper(componentModel = "spring")
public interface AtmMapper extends EntityMapper<AtmDTO, Atm> {
    @Mapping(target = "address", source = "address", qualifiedByName = "addressId")
    AtmDTO toDto(Atm s);

    @Named("addressId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AddressDTO toDtoAddressId(Address address);
}
