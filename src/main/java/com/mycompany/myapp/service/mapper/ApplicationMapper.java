package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Application;
import com.mycompany.myapp.domain.Customer;
import com.mycompany.myapp.service.dto.ApplicationDTO;
import com.mycompany.myapp.service.dto.CustomerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Application} and its DTO {@link ApplicationDTO}.
 */
@Mapper(componentModel = "spring")
public interface ApplicationMapper extends EntityMapper<ApplicationDTO, Application> {
    @Mapping(target = "customer", source = "customer", qualifiedByName = "customerId")
    ApplicationDTO toDto(Application s);

    @Named("customerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CustomerDTO toDtoCustomerId(Customer customer);
}
