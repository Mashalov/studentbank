package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.CardDetails;
import com.mycompany.myapp.repository.CardDetailsRepository;
import com.mycompany.myapp.service.dto.CardDetailsDTO;
import com.mycompany.myapp.service.mapper.CardDetailsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.CardDetails}.
 */
@Service
@Transactional
public class CardDetailsService {

    private static final Logger log = LoggerFactory.getLogger(CardDetailsService.class);

    private final CardDetailsRepository cardDetailsRepository;

    private final CardDetailsMapper cardDetailsMapper;

    public CardDetailsService(CardDetailsRepository cardDetailsRepository, CardDetailsMapper cardDetailsMapper) {
        this.cardDetailsRepository = cardDetailsRepository;
        this.cardDetailsMapper = cardDetailsMapper;
    }

    /**
     * Save a cardDetails.
     *
     * @param cardDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    public CardDetailsDTO save(CardDetailsDTO cardDetailsDTO) {
        log.debug("Request to save CardDetails : {}", cardDetailsDTO);
        CardDetails cardDetails = cardDetailsMapper.toEntity(cardDetailsDTO);
        cardDetails = cardDetailsRepository.save(cardDetails);
        return cardDetailsMapper.toDto(cardDetails);
    }

    /**
     * Update a cardDetails.
     *
     * @param cardDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    public CardDetailsDTO update(CardDetailsDTO cardDetailsDTO) {
        log.debug("Request to update CardDetails : {}", cardDetailsDTO);
        CardDetails cardDetails = cardDetailsMapper.toEntity(cardDetailsDTO);
        cardDetails = cardDetailsRepository.save(cardDetails);
        return cardDetailsMapper.toDto(cardDetails);
    }

    /**
     * Partially update a cardDetails.
     *
     * @param cardDetailsDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CardDetailsDTO> partialUpdate(CardDetailsDTO cardDetailsDTO) {
        log.debug("Request to partially update CardDetails : {}", cardDetailsDTO);

        return cardDetailsRepository
            .findById(cardDetailsDTO.getId())
            .map(existingCardDetails -> {
                cardDetailsMapper.partialUpdate(existingCardDetails, cardDetailsDTO);

                return existingCardDetails;
            })
            .map(cardDetailsRepository::save)
            .map(cardDetailsMapper::toDto);
    }

    /**
     * Get all the cardDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CardDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CardDetails");
        return cardDetailsRepository.findAll(pageable).map(cardDetailsMapper::toDto);
    }

    /**
     * Get one cardDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CardDetailsDTO> findOne(Long id) {
        log.debug("Request to get CardDetails : {}", id);
        return cardDetailsRepository.findById(id).map(cardDetailsMapper::toDto);
    }

    /**
     * Delete the cardDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CardDetails : {}", id);
        cardDetailsRepository.deleteById(id);
    }
}
