package com.mycompany.myapp.service.dto;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.EmployeesAssets} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EmployeesAssetsDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private EmployeeDTO assignedBy;

    private DocumentDTO recieversProtocol;

    private EmployeeDTO employee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeDTO getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(EmployeeDTO assignedBy) {
        this.assignedBy = assignedBy;
    }

    public DocumentDTO getRecieversProtocol() {
        return recieversProtocol;
    }

    public void setRecieversProtocol(DocumentDTO recieversProtocol) {
        this.recieversProtocol = recieversProtocol;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeesAssetsDTO)) {
            return false;
        }

        EmployeesAssetsDTO employeesAssetsDTO = (EmployeesAssetsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, employeesAssetsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeesAssetsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", assignedBy=" + getAssignedBy() +
            ", recieversProtocol=" + getRecieversProtocol() +
            ", employee=" + getEmployee() +
            "}";
    }
}
