package com.mycompany.myapp.service.dto;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.DocumentCategory} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DocumentCategoryDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private DocumentCategoryDTO documentCategory;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentCategoryDTO getDocumentCategory() {
        return documentCategory;
    }

    public void setDocumentCategory(DocumentCategoryDTO documentCategory) {
        this.documentCategory = documentCategory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentCategoryDTO)) {
            return false;
        }

        DocumentCategoryDTO documentCategoryDTO = (DocumentCategoryDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, documentCategoryDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DocumentCategoryDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", documentCategory=" + getDocumentCategory() +
            "}";
    }
}
