package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.Currency;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Exchange} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ExchangeDTO implements Serializable {

    private Long id;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private Currency currencyIn;

    @NotNull
    private Currency currencyOut;

    private Long exchangeRate;

    private EmployeeDTO employee;

    private OfficeDTO office;

    private DocumentDTO document;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrencyIn() {
        return currencyIn;
    }

    public void setCurrencyIn(Currency currencyIn) {
        this.currencyIn = currencyIn;
    }

    public Currency getCurrencyOut() {
        return currencyOut;
    }

    public void setCurrencyOut(Currency currencyOut) {
        this.currencyOut = currencyOut;
    }

    public Long getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Long exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public OfficeDTO getOffice() {
        return office;
    }

    public void setOffice(OfficeDTO office) {
        this.office = office;
    }

    public DocumentDTO getDocument() {
        return document;
    }

    public void setDocument(DocumentDTO document) {
        this.document = document;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExchangeDTO)) {
            return false;
        }

        ExchangeDTO exchangeDTO = (ExchangeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, exchangeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExchangeDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", currencyIn='" + getCurrencyIn() + "'" +
            ", currencyOut='" + getCurrencyOut() + "'" +
            ", exchangeRate=" + getExchangeRate() +
            ", employee=" + getEmployee() +
            ", office=" + getOffice() +
            ", document=" + getDocument() +
            "}";
    }
}
