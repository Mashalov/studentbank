package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.DocumentType;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Atm} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AtmDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private DocumentType documentType;

    private AddressDTO address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AtmDTO)) {
            return false;
        }

        AtmDTO atmDTO = (AtmDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, atmDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AtmDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", documentType='" + getDocumentType() + "'" +
            ", address=" + getAddress() +
            "}";
    }
}
