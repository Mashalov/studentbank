package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Employee} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EmployeeDTO implements Serializable {

    private Long id;

    private Boolean isOnHoliday;

    private ContactDetailsDTO contactDetails;

    private JobPositionDTO jobPosition;

    private DocumentDTO employeeContract;

    private EmployeeDTO manager;

    private AtmDTO atm;

    private OfficeDTO office;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsOnHoliday() {
        return isOnHoliday;
    }

    public void setIsOnHoliday(Boolean isOnHoliday) {
        this.isOnHoliday = isOnHoliday;
    }

    public ContactDetailsDTO getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetailsDTO contactDetails) {
        this.contactDetails = contactDetails;
    }

    public JobPositionDTO getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(JobPositionDTO jobPosition) {
        this.jobPosition = jobPosition;
    }

    public DocumentDTO getEmployeeContract() {
        return employeeContract;
    }

    public void setEmployeeContract(DocumentDTO employeeContract) {
        this.employeeContract = employeeContract;
    }

    public EmployeeDTO getManager() {
        return manager;
    }

    public void setManager(EmployeeDTO manager) {
        this.manager = manager;
    }

    public AtmDTO getAtm() {
        return atm;
    }

    public void setAtm(AtmDTO atm) {
        this.atm = atm;
    }

    public OfficeDTO getOffice() {
        return office;
    }

    public void setOffice(OfficeDTO office) {
        this.office = office;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeeDTO)) {
            return false;
        }

        EmployeeDTO employeeDTO = (EmployeeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, employeeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeeDTO{" +
            "id=" + getId() +
            ", isOnHoliday='" + getIsOnHoliday() + "'" +
            ", contactDetails=" + getContactDetails() +
            ", jobPosition=" + getJobPosition() +
            ", employeeContract=" + getEmployeeContract() +
            ", manager=" + getManager() +
            ", atm=" + getAtm() +
            ", office=" + getOffice() +
            "}";
    }
}
