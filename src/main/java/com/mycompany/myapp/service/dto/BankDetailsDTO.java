package com.mycompany.myapp.service.dto;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.BankDetails} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BankDetailsDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime foundationYear;

    private String ceo;

    private String mainWebsite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(ZonedDateTime foundationYear) {
        this.foundationYear = foundationYear;
    }

    public String getCeo() {
        return ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getMainWebsite() {
        return mainWebsite;
    }

    public void setMainWebsite(String mainWebsite) {
        this.mainWebsite = mainWebsite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankDetailsDTO)) {
            return false;
        }

        BankDetailsDTO bankDetailsDTO = (BankDetailsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, bankDetailsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankDetailsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", foundationYear='" + getFoundationYear() + "'" +
            ", ceo='" + getCeo() + "'" +
            ", mainWebsite='" + getMainWebsite() + "'" +
            "}";
    }
}
