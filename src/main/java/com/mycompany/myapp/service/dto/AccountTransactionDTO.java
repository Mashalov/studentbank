package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.TransactionLocation;
import com.mycompany.myapp.domain.enumeration.TransactionOperationType;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.AccountTransaction} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountTransactionDTO implements Serializable {

    private Long id;

    private String externalId;

    @NotNull
    private TransactionOperationType transactionOperationType;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private Currency currency;

    @NotNull
    private TransactionLocation location;

    private ZonedDateTime sendTime;

    private ZonedDateTime receiveTime;

    private PaymentAccountDTO sender;

    private PaymentAccountDTO receiver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public TransactionOperationType getTransactionOperationType() {
        return transactionOperationType;
    }

    public void setTransactionOperationType(TransactionOperationType transactionOperationType) {
        this.transactionOperationType = transactionOperationType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public TransactionLocation getLocation() {
        return location;
    }

    public void setLocation(TransactionLocation location) {
        this.location = location;
    }

    public ZonedDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(ZonedDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public ZonedDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(ZonedDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public PaymentAccountDTO getSender() {
        return sender;
    }

    public void setSender(PaymentAccountDTO sender) {
        this.sender = sender;
    }

    public PaymentAccountDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PaymentAccountDTO receiver) {
        this.receiver = receiver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountTransactionDTO)) {
            return false;
        }

        AccountTransactionDTO accountTransactionDTO = (AccountTransactionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, accountTransactionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountTransactionDTO{" +
            "id=" + getId() +
            ", externalId='" + getExternalId() + "'" +
            ", transactionOperationType='" + getTransactionOperationType() + "'" +
            ", amount=" + getAmount() +
            ", currency='" + getCurrency() + "'" +
            ", location='" + getLocation() + "'" +
            ", sendTime='" + getSendTime() + "'" +
            ", receiveTime='" + getReceiveTime() + "'" +
            ", sender=" + getSender() +
            ", receiver=" + getReceiver() +
            "}";
    }
}
