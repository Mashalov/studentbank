package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.DocumentType;
import jakarta.persistence.Lob;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Document} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DocumentDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String documentExtension;

    @Lob
    private byte[] documentData;

    private String documentDataContentType;

    @NotNull
    private DocumentType documentType;

    private Boolean isOrinalDocument;

    private String documentCompression;

    private DocumentCategoryDTO documentCategory;

    private ApplicationDTO application;

    private CustomerDTO customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentExtension() {
        return documentExtension;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }

    public byte[] getDocumentData() {
        return documentData;
    }

    public void setDocumentData(byte[] documentData) {
        this.documentData = documentData;
    }

    public String getDocumentDataContentType() {
        return documentDataContentType;
    }

    public void setDocumentDataContentType(String documentDataContentType) {
        this.documentDataContentType = documentDataContentType;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public Boolean getIsOrinalDocument() {
        return isOrinalDocument;
    }

    public void setIsOrinalDocument(Boolean isOrinalDocument) {
        this.isOrinalDocument = isOrinalDocument;
    }

    public String getDocumentCompression() {
        return documentCompression;
    }

    public void setDocumentCompression(String documentCompression) {
        this.documentCompression = documentCompression;
    }

    public DocumentCategoryDTO getDocumentCategory() {
        return documentCategory;
    }

    public void setDocumentCategory(DocumentCategoryDTO documentCategory) {
        this.documentCategory = documentCategory;
    }

    public ApplicationDTO getApplication() {
        return application;
    }

    public void setApplication(ApplicationDTO application) {
        this.application = application;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentDTO)) {
            return false;
        }

        DocumentDTO documentDTO = (DocumentDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, documentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DocumentDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", documentExtension='" + getDocumentExtension() + "'" +
            ", documentData='" + getDocumentData() + "'" +
            ", documentType='" + getDocumentType() + "'" +
            ", isOrinalDocument='" + getIsOrinalDocument() + "'" +
            ", documentCompression='" + getDocumentCompression() + "'" +
            ", documentCategory=" + getDocumentCategory() +
            ", application=" + getApplication() +
            ", customer=" + getCustomer() +
            "}";
    }
}
