package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.PaymentAccountType;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.PaymentAccount} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PaymentAccountDTO implements Serializable {

    private Long id;

    @NotNull
    private PaymentAccountType paymentAccountType;

    @NotNull
    private String iban;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private Currency currency;

    private Boolean active;

    private Boolean deleted;

    private DocumentDTO contract;

    private CustomerDTO customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentAccountType getPaymentAccountType() {
        return paymentAccountType;
    }

    public void setPaymentAccountType(PaymentAccountType paymentAccountType) {
        this.paymentAccountType = paymentAccountType;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public DocumentDTO getContract() {
        return contract;
    }

    public void setContract(DocumentDTO contract) {
        this.contract = contract;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentAccountDTO)) {
            return false;
        }

        PaymentAccountDTO paymentAccountDTO = (PaymentAccountDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, paymentAccountDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentAccountDTO{" +
            "id=" + getId() +
            ", paymentAccountType='" + getPaymentAccountType() + "'" +
            ", iban='" + getIban() + "'" +
            ", amount=" + getAmount() +
            ", currency='" + getCurrency() + "'" +
            ", active='" + getActive() + "'" +
            ", deleted='" + getDeleted() + "'" +
            ", contract=" + getContract() +
            ", customer=" + getCustomer() +
            "}";
    }
}
