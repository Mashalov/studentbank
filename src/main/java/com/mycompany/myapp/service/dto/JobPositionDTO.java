package com.mycompany.myapp.service.dto;

import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.JobPosition} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class JobPositionDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String company;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JobPositionDTO)) {
            return false;
        }

        JobPositionDTO jobPositionDTO = (JobPositionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, jobPositionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JobPositionDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", company='" + getCompany() + "'" +
            "}";
    }
}
