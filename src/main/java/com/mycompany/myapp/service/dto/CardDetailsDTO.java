package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.enumeration.CardProvider;
import com.mycompany.myapp.domain.enumeration.CardType;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.CardDetails} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CardDetailsDTO implements Serializable {

    private Long id;

    @NotNull
    private Long cardNumber;

    @NotNull
    private CardType cardType;

    @NotNull
    private CardProvider cardProvider;

    @NotNull
    private ZonedDateTime issueDate;

    @NotNull
    private ZonedDateTime expiryDate;

    @NotNull
    private Long cvv;

    private Boolean isActive;

    private Boolean isBlocked;

    private Long pin;

    private CustomerDTO customer;

    private PaymentAccountDTO paymentAccount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public CardProvider getCardProvider() {
        return cardProvider;
    }

    public void setCardProvider(CardProvider cardProvider) {
        this.cardProvider = cardProvider;
    }

    public ZonedDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(ZonedDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public ZonedDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(ZonedDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getCvv() {
        return cvv;
    }

    public void setCvv(Long cvv) {
        this.cvv = cvv;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Long getPin() {
        return pin;
    }

    public void setPin(Long pin) {
        this.pin = pin;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public PaymentAccountDTO getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(PaymentAccountDTO paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CardDetailsDTO)) {
            return false;
        }

        CardDetailsDTO cardDetailsDTO = (CardDetailsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cardDetailsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CardDetailsDTO{" +
            "id=" + getId() +
            ", cardNumber=" + getCardNumber() +
            ", cardType='" + getCardType() + "'" +
            ", cardProvider='" + getCardProvider() + "'" +
            ", issueDate='" + getIssueDate() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", cvv=" + getCvv() +
            ", isActive='" + getIsActive() + "'" +
            ", isBlocked='" + getIsBlocked() + "'" +
            ", pin=" + getPin() +
            ", customer=" + getCustomer() +
            ", paymentAccount=" + getPaymentAccount() +
            "}";
    }
}
