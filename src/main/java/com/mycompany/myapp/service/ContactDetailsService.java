package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.ContactDetails;
import com.mycompany.myapp.repository.ContactDetailsRepository;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.service.mapper.ContactDetailsMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.mycompany.myapp.domain.ContactDetails}.
 */
@Service
@Transactional
public class ContactDetailsService {

    private static final Logger log = LoggerFactory.getLogger(ContactDetailsService.class);

    private final ContactDetailsRepository contactDetailsRepository;

    private final ContactDetailsMapper contactDetailsMapper;

    public ContactDetailsService(ContactDetailsRepository contactDetailsRepository, ContactDetailsMapper contactDetailsMapper) {
        this.contactDetailsRepository = contactDetailsRepository;
        this.contactDetailsMapper = contactDetailsMapper;
    }

    /**
     * Save a contactDetails.
     *
     * @param contactDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactDetailsDTO save(ContactDetailsDTO contactDetailsDTO) {
        log.debug("Request to save ContactDetails : {}", contactDetailsDTO);
        ContactDetails contactDetails = contactDetailsMapper.toEntity(contactDetailsDTO);
        contactDetails = contactDetailsRepository.save(contactDetails);
        return contactDetailsMapper.toDto(contactDetails);
    }

    /**
     * Update a contactDetails.
     *
     * @param contactDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactDetailsDTO update(ContactDetailsDTO contactDetailsDTO) {
        log.debug("Request to update ContactDetails : {}", contactDetailsDTO);
        ContactDetails contactDetails = contactDetailsMapper.toEntity(contactDetailsDTO);
        contactDetails = contactDetailsRepository.save(contactDetails);
        return contactDetailsMapper.toDto(contactDetails);
    }

    /**
     * Partially update a contactDetails.
     *
     * @param contactDetailsDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ContactDetailsDTO> partialUpdate(ContactDetailsDTO contactDetailsDTO) {
        log.debug("Request to partially update ContactDetails : {}", contactDetailsDTO);

        return contactDetailsRepository
            .findById(contactDetailsDTO.getId())
            .map(existingContactDetails -> {
                contactDetailsMapper.partialUpdate(existingContactDetails, contactDetailsDTO);

                return existingContactDetails;
            })
            .map(contactDetailsRepository::save)
            .map(contactDetailsMapper::toDto);
    }

    /**
     * Get all the contactDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactDetails");
        return contactDetailsRepository.findAll(pageable).map(contactDetailsMapper::toDto);
    }

    /**
     * Get one contactDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactDetailsDTO> findOne(Long id) {
        log.debug("Request to get ContactDetails : {}", id);
        return contactDetailsRepository.findById(id).map(contactDetailsMapper::toDto);
    }

    /**
     * Delete the contactDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactDetails : {}", id);
        contactDetailsRepository.deleteById(id);
    }
}
