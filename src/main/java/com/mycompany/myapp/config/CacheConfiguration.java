package com.mycompany.myapp.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(
                Object.class,
                Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries())
            )
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build()
        );
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.mycompany.myapp.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.mycompany.myapp.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.mycompany.myapp.domain.Authority.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Address.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Application.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Application.class.getName() + ".documents");
            createCache(cm, com.mycompany.myapp.domain.Document.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Atm.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Atm.class.getName() + ".supportPersons");
            createCache(cm, com.mycompany.myapp.domain.BankDetails.class.getName());
            createCache(cm, com.mycompany.myapp.domain.BankDetails.class.getName() + ".offices");
            createCache(cm, com.mycompany.myapp.domain.CardDetails.class.getName());
            createCache(cm, com.mycompany.myapp.domain.ContactDetails.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Customer.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Customer.class.getName() + ".addresses");
            createCache(cm, com.mycompany.myapp.domain.Customer.class.getName() + ".paymentAccounts");
            createCache(cm, com.mycompany.myapp.domain.Customer.class.getName() + ".applications");
            createCache(cm, com.mycompany.myapp.domain.Customer.class.getName() + ".documents");
            createCache(cm, com.mycompany.myapp.domain.DocumentCategory.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Employee.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Employee.class.getName() + ".employeesAssets");
            createCache(cm, com.mycompany.myapp.domain.EmployeesAssets.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Exchange.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Office.class.getName());
            createCache(cm, com.mycompany.myapp.domain.Office.class.getName() + ".contactDetails");
            createCache(cm, com.mycompany.myapp.domain.Office.class.getName() + ".employees");
            createCache(cm, com.mycompany.myapp.domain.JobPosition.class.getName());
            createCache(cm, com.mycompany.myapp.domain.PaymentAccount.class.getName());
            createCache(cm, com.mycompany.myapp.domain.AccountTransaction.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
