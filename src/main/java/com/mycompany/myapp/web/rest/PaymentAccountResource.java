package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.PaymentAccountRepository;
import com.mycompany.myapp.service.PaymentAccountService;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.PaymentAccount}.
 */
@RestController
@RequestMapping("/api/payment-accounts")
public class PaymentAccountResource {

    private static final Logger log = LoggerFactory.getLogger(PaymentAccountResource.class);

    private static final String ENTITY_NAME = "paymentAccount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentAccountService paymentAccountService;

    private final PaymentAccountRepository paymentAccountRepository;

    public PaymentAccountResource(PaymentAccountService paymentAccountService, PaymentAccountRepository paymentAccountRepository) {
        this.paymentAccountService = paymentAccountService;
        this.paymentAccountRepository = paymentAccountRepository;
    }

    /**
     * {@code POST  /payment-accounts} : Create a new paymentAccount.
     *
     * @param paymentAccountDTO the paymentAccountDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentAccountDTO, or with status {@code 400 (Bad Request)} if the paymentAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<PaymentAccountDTO> createPaymentAccount(@Valid @RequestBody PaymentAccountDTO paymentAccountDTO)
        throws URISyntaxException {
        log.debug("REST request to save PaymentAccount : {}", paymentAccountDTO);
        if (paymentAccountDTO.getId() != null) {
            throw new BadRequestAlertException("A new paymentAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        paymentAccountDTO = paymentAccountService.save(paymentAccountDTO);
        return ResponseEntity.created(new URI("/api/payment-accounts/" + paymentAccountDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, paymentAccountDTO.getId().toString()))
            .body(paymentAccountDTO);
    }

    /**
     * {@code PUT  /payment-accounts/:id} : Updates an existing paymentAccount.
     *
     * @param id the id of the paymentAccountDTO to save.
     * @param paymentAccountDTO the paymentAccountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentAccountDTO,
     * or with status {@code 400 (Bad Request)} if the paymentAccountDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentAccountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<PaymentAccountDTO> updatePaymentAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PaymentAccountDTO paymentAccountDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PaymentAccount : {}, {}", id, paymentAccountDTO);
        if (paymentAccountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentAccountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        paymentAccountDTO = paymentAccountService.update(paymentAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentAccountDTO.getId().toString()))
            .body(paymentAccountDTO);
    }

    /**
     * {@code PATCH  /payment-accounts/:id} : Partial updates given fields of an existing paymentAccount, field will ignore if it is null
     *
     * @param id the id of the paymentAccountDTO to save.
     * @param paymentAccountDTO the paymentAccountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentAccountDTO,
     * or with status {@code 400 (Bad Request)} if the paymentAccountDTO is not valid,
     * or with status {@code 404 (Not Found)} if the paymentAccountDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the paymentAccountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PaymentAccountDTO> partialUpdatePaymentAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PaymentAccountDTO paymentAccountDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PaymentAccount partially : {}, {}", id, paymentAccountDTO);
        if (paymentAccountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentAccountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PaymentAccountDTO> result = paymentAccountService.partialUpdate(paymentAccountDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentAccountDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /payment-accounts} : get all the paymentAccounts.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentAccounts in body.
     */
    @GetMapping("")
    public ResponseEntity<List<PaymentAccountDTO>> getAllPaymentAccounts(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of PaymentAccounts");
        Page<PaymentAccountDTO> page = paymentAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /payment-accounts/:id} : get the "id" paymentAccount.
     *
     * @param id the id of the paymentAccountDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentAccountDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<PaymentAccountDTO> getPaymentAccount(@PathVariable("id") Long id) {
        log.debug("REST request to get PaymentAccount : {}", id);
        Optional<PaymentAccountDTO> paymentAccountDTO = paymentAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentAccountDTO);
    }

    /**
     * {@code DELETE  /payment-accounts/:id} : delete the "id" paymentAccount.
     *
     * @param id the id of the paymentAccountDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePaymentAccount(@PathVariable("id") Long id) {
        log.debug("REST request to delete PaymentAccount : {}", id);
        paymentAccountService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
