package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.EmployeesAssetsRepository;
import com.mycompany.myapp.service.EmployeesAssetsService;
import com.mycompany.myapp.service.dto.EmployeesAssetsDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.EmployeesAssets}.
 */
@RestController
@RequestMapping("/api/employees-assets")
public class EmployeesAssetsResource {

    private static final Logger log = LoggerFactory.getLogger(EmployeesAssetsResource.class);

    private static final String ENTITY_NAME = "employeesAssets";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmployeesAssetsService employeesAssetsService;

    private final EmployeesAssetsRepository employeesAssetsRepository;

    public EmployeesAssetsResource(EmployeesAssetsService employeesAssetsService, EmployeesAssetsRepository employeesAssetsRepository) {
        this.employeesAssetsService = employeesAssetsService;
        this.employeesAssetsRepository = employeesAssetsRepository;
    }

    /**
     * {@code POST  /employees-assets} : Create a new employeesAssets.
     *
     * @param employeesAssetsDTO the employeesAssetsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new employeesAssetsDTO, or with status {@code 400 (Bad Request)} if the employeesAssets has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<EmployeesAssetsDTO> createEmployeesAssets(@Valid @RequestBody EmployeesAssetsDTO employeesAssetsDTO)
        throws URISyntaxException {
        log.debug("REST request to save EmployeesAssets : {}", employeesAssetsDTO);
        if (employeesAssetsDTO.getId() != null) {
            throw new BadRequestAlertException("A new employeesAssets cannot already have an ID", ENTITY_NAME, "idexists");
        }
        employeesAssetsDTO = employeesAssetsService.save(employeesAssetsDTO);
        return ResponseEntity.created(new URI("/api/employees-assets/" + employeesAssetsDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, employeesAssetsDTO.getId().toString()))
            .body(employeesAssetsDTO);
    }

    /**
     * {@code PUT  /employees-assets/:id} : Updates an existing employeesAssets.
     *
     * @param id the id of the employeesAssetsDTO to save.
     * @param employeesAssetsDTO the employeesAssetsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employeesAssetsDTO,
     * or with status {@code 400 (Bad Request)} if the employeesAssetsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the employeesAssetsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<EmployeesAssetsDTO> updateEmployeesAssets(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody EmployeesAssetsDTO employeesAssetsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update EmployeesAssets : {}, {}", id, employeesAssetsDTO);
        if (employeesAssetsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, employeesAssetsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!employeesAssetsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        employeesAssetsDTO = employeesAssetsService.update(employeesAssetsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, employeesAssetsDTO.getId().toString()))
            .body(employeesAssetsDTO);
    }

    /**
     * {@code PATCH  /employees-assets/:id} : Partial updates given fields of an existing employeesAssets, field will ignore if it is null
     *
     * @param id the id of the employeesAssetsDTO to save.
     * @param employeesAssetsDTO the employeesAssetsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated employeesAssetsDTO,
     * or with status {@code 400 (Bad Request)} if the employeesAssetsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the employeesAssetsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the employeesAssetsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EmployeesAssetsDTO> partialUpdateEmployeesAssets(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody EmployeesAssetsDTO employeesAssetsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update EmployeesAssets partially : {}, {}", id, employeesAssetsDTO);
        if (employeesAssetsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, employeesAssetsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!employeesAssetsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmployeesAssetsDTO> result = employeesAssetsService.partialUpdate(employeesAssetsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, employeesAssetsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /employees-assets} : get all the employeesAssets.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of employeesAssets in body.
     */
    @GetMapping("")
    public ResponseEntity<List<EmployeesAssetsDTO>> getAllEmployeesAssets(
        @org.springdoc.core.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of EmployeesAssets");
        Page<EmployeesAssetsDTO> page = employeesAssetsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /employees-assets/:id} : get the "id" employeesAssets.
     *
     * @param id the id of the employeesAssetsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the employeesAssetsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<EmployeesAssetsDTO> getEmployeesAssets(@PathVariable("id") Long id) {
        log.debug("REST request to get EmployeesAssets : {}", id);
        Optional<EmployeesAssetsDTO> employeesAssetsDTO = employeesAssetsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(employeesAssetsDTO);
    }

    /**
     * {@code DELETE  /employees-assets/:id} : delete the "id" employeesAssets.
     *
     * @param id the id of the employeesAssetsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployeesAssets(@PathVariable("id") Long id) {
        log.debug("REST request to delete EmployeesAssets : {}", id);
        employeesAssetsService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
