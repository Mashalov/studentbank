package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.ContactDetailsRepository;
import com.mycompany.myapp.service.ContactDetailsService;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.ContactDetails}.
 */
@RestController
@RequestMapping("/api/contact-details")
public class ContactDetailsResource {

    private static final Logger log = LoggerFactory.getLogger(ContactDetailsResource.class);

    private static final String ENTITY_NAME = "contactDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactDetailsService contactDetailsService;

    private final ContactDetailsRepository contactDetailsRepository;

    public ContactDetailsResource(ContactDetailsService contactDetailsService, ContactDetailsRepository contactDetailsRepository) {
        this.contactDetailsService = contactDetailsService;
        this.contactDetailsRepository = contactDetailsRepository;
    }

    /**
     * {@code POST  /contact-details} : Create a new contactDetails.
     *
     * @param contactDetailsDTO the contactDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactDetailsDTO, or with status {@code 400 (Bad Request)} if the contactDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<ContactDetailsDTO> createContactDetails(@Valid @RequestBody ContactDetailsDTO contactDetailsDTO)
        throws URISyntaxException {
        log.debug("REST request to save ContactDetails : {}", contactDetailsDTO);
        if (contactDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        contactDetailsDTO = contactDetailsService.save(contactDetailsDTO);
        return ResponseEntity.created(new URI("/api/contact-details/" + contactDetailsDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, contactDetailsDTO.getId().toString()))
            .body(contactDetailsDTO);
    }

    /**
     * {@code PUT  /contact-details/:id} : Updates an existing contactDetails.
     *
     * @param id the id of the contactDetailsDTO to save.
     * @param contactDetailsDTO the contactDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the contactDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<ContactDetailsDTO> updateContactDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ContactDetailsDTO contactDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ContactDetails : {}, {}", id, contactDetailsDTO);
        if (contactDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, contactDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!contactDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        contactDetailsDTO = contactDetailsService.update(contactDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactDetailsDTO.getId().toString()))
            .body(contactDetailsDTO);
    }

    /**
     * {@code PATCH  /contact-details/:id} : Partial updates given fields of an existing contactDetails, field will ignore if it is null
     *
     * @param id the id of the contactDetailsDTO to save.
     * @param contactDetailsDTO the contactDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the contactDetailsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the contactDetailsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the contactDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ContactDetailsDTO> partialUpdateContactDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ContactDetailsDTO contactDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ContactDetails partially : {}, {}", id, contactDetailsDTO);
        if (contactDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, contactDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!contactDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ContactDetailsDTO> result = contactDetailsService.partialUpdate(contactDetailsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactDetailsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /contact-details} : get all the contactDetails.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactDetails in body.
     */
    @GetMapping("")
    public ResponseEntity<List<ContactDetailsDTO>> getAllContactDetails(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ContactDetails");
        Page<ContactDetailsDTO> page = contactDetailsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-details/:id} : get the "id" contactDetails.
     *
     * @param id the id of the contactDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<ContactDetailsDTO> getContactDetails(@PathVariable("id") Long id) {
        log.debug("REST request to get ContactDetails : {}", id);
        Optional<ContactDetailsDTO> contactDetailsDTO = contactDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactDetailsDTO);
    }

    /**
     * {@code DELETE  /contact-details/:id} : delete the "id" contactDetails.
     *
     * @param id the id of the contactDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteContactDetails(@PathVariable("id") Long id) {
        log.debug("REST request to delete ContactDetails : {}", id);
        contactDetailsService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
