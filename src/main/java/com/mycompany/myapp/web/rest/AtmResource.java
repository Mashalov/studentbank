package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.AtmRepository;
import com.mycompany.myapp.service.AtmService;
import com.mycompany.myapp.service.dto.AtmDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Atm}.
 */
@RestController
@RequestMapping("/api/atms")
public class AtmResource {

    private static final Logger log = LoggerFactory.getLogger(AtmResource.class);

    private static final String ENTITY_NAME = "atm";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtmService atmService;

    private final AtmRepository atmRepository;

    public AtmResource(AtmService atmService, AtmRepository atmRepository) {
        this.atmService = atmService;
        this.atmRepository = atmRepository;
    }

    /**
     * {@code POST  /atms} : Create a new atm.
     *
     * @param atmDTO the atmDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atmDTO, or with status {@code 400 (Bad Request)} if the atm has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<AtmDTO> createAtm(@Valid @RequestBody AtmDTO atmDTO) throws URISyntaxException {
        log.debug("REST request to save Atm : {}", atmDTO);
        if (atmDTO.getId() != null) {
            throw new BadRequestAlertException("A new atm cannot already have an ID", ENTITY_NAME, "idexists");
        }
        atmDTO = atmService.save(atmDTO);
        return ResponseEntity.created(new URI("/api/atms/" + atmDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, atmDTO.getId().toString()))
            .body(atmDTO);
    }

    /**
     * {@code PUT  /atms/:id} : Updates an existing atm.
     *
     * @param id the id of the atmDTO to save.
     * @param atmDTO the atmDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atmDTO,
     * or with status {@code 400 (Bad Request)} if the atmDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atmDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<AtmDTO> updateAtm(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody AtmDTO atmDTO)
        throws URISyntaxException {
        log.debug("REST request to update Atm : {}, {}", id, atmDTO);
        if (atmDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, atmDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!atmRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        atmDTO = atmService.update(atmDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, atmDTO.getId().toString()))
            .body(atmDTO);
    }

    /**
     * {@code PATCH  /atms/:id} : Partial updates given fields of an existing atm, field will ignore if it is null
     *
     * @param id the id of the atmDTO to save.
     * @param atmDTO the atmDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atmDTO,
     * or with status {@code 400 (Bad Request)} if the atmDTO is not valid,
     * or with status {@code 404 (Not Found)} if the atmDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the atmDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AtmDTO> partialUpdateAtm(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AtmDTO atmDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Atm partially : {}, {}", id, atmDTO);
        if (atmDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, atmDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!atmRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AtmDTO> result = atmService.partialUpdate(atmDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, atmDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /atms} : get all the atms.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atms in body.
     */
    @GetMapping("")
    public ResponseEntity<List<AtmDTO>> getAllAtms(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Atms");
        Page<AtmDTO> page = atmService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /atms/:id} : get the "id" atm.
     *
     * @param id the id of the atmDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atmDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<AtmDTO> getAtm(@PathVariable("id") Long id) {
        log.debug("REST request to get Atm : {}", id);
        Optional<AtmDTO> atmDTO = atmService.findOne(id);
        return ResponseUtil.wrapOrNotFound(atmDTO);
    }

    /**
     * {@code DELETE  /atms/:id} : delete the "id" atm.
     *
     * @param id the id of the atmDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAtm(@PathVariable("id") Long id) {
        log.debug("REST request to delete Atm : {}", id);
        atmService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
