package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.CardDetailsRepository;
import com.mycompany.myapp.service.CardDetailsService;
import com.mycompany.myapp.service.dto.CardDetailsDTO;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.CardDetails}.
 */
@RestController
@RequestMapping("/api/card-details")
public class CardDetailsResource {

    private static final Logger log = LoggerFactory.getLogger(CardDetailsResource.class);

    private static final String ENTITY_NAME = "cardDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CardDetailsService cardDetailsService;

    private final CardDetailsRepository cardDetailsRepository;

    public CardDetailsResource(CardDetailsService cardDetailsService, CardDetailsRepository cardDetailsRepository) {
        this.cardDetailsService = cardDetailsService;
        this.cardDetailsRepository = cardDetailsRepository;
    }

    /**
     * {@code POST  /card-details} : Create a new cardDetails.
     *
     * @param cardDetailsDTO the cardDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cardDetailsDTO, or with status {@code 400 (Bad Request)} if the cardDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<CardDetailsDTO> createCardDetails(@Valid @RequestBody CardDetailsDTO cardDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save CardDetails : {}", cardDetailsDTO);
        if (cardDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new cardDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        cardDetailsDTO = cardDetailsService.save(cardDetailsDTO);
        return ResponseEntity.created(new URI("/api/card-details/" + cardDetailsDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, cardDetailsDTO.getId().toString()))
            .body(cardDetailsDTO);
    }

    /**
     * {@code PUT  /card-details/:id} : Updates an existing cardDetails.
     *
     * @param id the id of the cardDetailsDTO to save.
     * @param cardDetailsDTO the cardDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cardDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the cardDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cardDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<CardDetailsDTO> updateCardDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CardDetailsDTO cardDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CardDetails : {}, {}", id, cardDetailsDTO);
        if (cardDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cardDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cardDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        cardDetailsDTO = cardDetailsService.update(cardDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cardDetailsDTO.getId().toString()))
            .body(cardDetailsDTO);
    }

    /**
     * {@code PATCH  /card-details/:id} : Partial updates given fields of an existing cardDetails, field will ignore if it is null
     *
     * @param id the id of the cardDetailsDTO to save.
     * @param cardDetailsDTO the cardDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cardDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the cardDetailsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the cardDetailsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the cardDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CardDetailsDTO> partialUpdateCardDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CardDetailsDTO cardDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CardDetails partially : {}, {}", id, cardDetailsDTO);
        if (cardDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cardDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cardDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CardDetailsDTO> result = cardDetailsService.partialUpdate(cardDetailsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cardDetailsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /card-details} : get all the cardDetails.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cardDetails in body.
     */
    @GetMapping("")
    public ResponseEntity<List<CardDetailsDTO>> getAllCardDetails(@org.springdoc.core.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of CardDetails");
        Page<CardDetailsDTO> page = cardDetailsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /card-details/:id} : get the "id" cardDetails.
     *
     * @param id the id of the cardDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cardDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<CardDetailsDTO> getCardDetails(@PathVariable("id") Long id) {
        log.debug("REST request to get CardDetails : {}", id);
        Optional<CardDetailsDTO> cardDetailsDTO = cardDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cardDetailsDTO);
    }

    /**
     * {@code DELETE  /card-details/:id} : delete the "id" cardDetails.
     *
     * @param id the id of the cardDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCardDetails(@PathVariable("id") Long id) {
        log.debug("REST request to delete CardDetails : {}", id);
        cardDetailsService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
