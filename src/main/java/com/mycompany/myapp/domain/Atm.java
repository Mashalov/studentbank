package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.DocumentType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Atm.
 */
@Entity
@Table(name = "atm")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Atm implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "document_type")
    private DocumentType documentType;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "atm")
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Set<Employee> supportPersons = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customer" }, allowSetters = true)
    private Address address;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Atm id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Atm name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentType getDocumentType() {
        return this.documentType;
    }

    public Atm documentType(DocumentType documentType) {
        this.setDocumentType(documentType);
        return this;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public Set<Employee> getSupportPersons() {
        return this.supportPersons;
    }

    public void setSupportPersons(Set<Employee> employees) {
        if (this.supportPersons != null) {
            this.supportPersons.forEach(i -> i.setAtm(null));
        }
        if (employees != null) {
            employees.forEach(i -> i.setAtm(this));
        }
        this.supportPersons = employees;
    }

    public Atm supportPersons(Set<Employee> employees) {
        this.setSupportPersons(employees);
        return this;
    }

    public Atm addSupportPersons(Employee employee) {
        this.supportPersons.add(employee);
        employee.setAtm(this);
        return this;
    }

    public Atm removeSupportPersons(Employee employee) {
        this.supportPersons.remove(employee);
        employee.setAtm(null);
        return this;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Atm address(Address address) {
        this.setAddress(address);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Atm)) {
            return false;
        }
        return getId() != null && getId().equals(((Atm) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Atm{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", documentType='" + getDocumentType() + "'" +
            "}";
    }
}
