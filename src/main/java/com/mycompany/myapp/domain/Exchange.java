package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.Currency;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Exchange.
 */
@Entity
@Table(name = "exchange")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Exchange implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "amount", precision = 21, scale = 2, nullable = false)
    private BigDecimal amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency_in", nullable = false)
    private Currency currencyIn;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency_out", nullable = false)
    private Currency currencyOut;

    @Column(name = "exchange_rate")
    private Long exchangeRate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contactDetails", "employees", "address", "bankDetails" }, allowSetters = true)
    private Office office;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documentCategory", "application", "customer" }, allowSetters = true)
    private Document document;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Exchange id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public Exchange amount(BigDecimal amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrencyIn() {
        return this.currencyIn;
    }

    public Exchange currencyIn(Currency currencyIn) {
        this.setCurrencyIn(currencyIn);
        return this;
    }

    public void setCurrencyIn(Currency currencyIn) {
        this.currencyIn = currencyIn;
    }

    public Currency getCurrencyOut() {
        return this.currencyOut;
    }

    public Exchange currencyOut(Currency currencyOut) {
        this.setCurrencyOut(currencyOut);
        return this;
    }

    public void setCurrencyOut(Currency currencyOut) {
        this.currencyOut = currencyOut;
    }

    public Long getExchangeRate() {
        return this.exchangeRate;
    }

    public Exchange exchangeRate(Long exchangeRate) {
        this.setExchangeRate(exchangeRate);
        return this;
    }

    public void setExchangeRate(Long exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Exchange employee(Employee employee) {
        this.setEmployee(employee);
        return this;
    }

    public Office getOffice() {
        return this.office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Exchange office(Office office) {
        this.setOffice(office);
        return this;
    }

    public Document getDocument() {
        return this.document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Exchange document(Document document) {
        this.setDocument(document);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Exchange)) {
            return false;
        }
        return getId() != null && getId().equals(((Exchange) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Exchange{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", currencyIn='" + getCurrencyIn() + "'" +
            ", currencyOut='" + getCurrencyOut() + "'" +
            ", exchangeRate=" + getExchangeRate() +
            "}";
    }
}
