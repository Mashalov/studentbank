package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Employee.
 */
@Entity
@Table(name = "employee")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "is_on_holiday")
    private Boolean isOnHoliday;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @JsonIgnoreProperties(value = { "assignedBy", "recieversProtocol", "employee" }, allowSetters = true)
    private Set<EmployeesAssets> employeesAssets = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "office" }, allowSetters = true)
    private ContactDetails contactDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    private JobPosition jobPosition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documentCategory", "application", "customer" }, allowSetters = true)
    private Document employeeContract;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Employee manager;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "supportPersons", "address" }, allowSetters = true)
    private Atm atm;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contactDetails", "employees", "address", "bankDetails" }, allowSetters = true)
    private Office office;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Employee id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsOnHoliday() {
        return this.isOnHoliday;
    }

    public Employee isOnHoliday(Boolean isOnHoliday) {
        this.setIsOnHoliday(isOnHoliday);
        return this;
    }

    public void setIsOnHoliday(Boolean isOnHoliday) {
        this.isOnHoliday = isOnHoliday;
    }

    public Set<EmployeesAssets> getEmployeesAssets() {
        return this.employeesAssets;
    }

    public void setEmployeesAssets(Set<EmployeesAssets> employeesAssets) {
        if (this.employeesAssets != null) {
            this.employeesAssets.forEach(i -> i.setEmployee(null));
        }
        if (employeesAssets != null) {
            employeesAssets.forEach(i -> i.setEmployee(this));
        }
        this.employeesAssets = employeesAssets;
    }

    public Employee employeesAssets(Set<EmployeesAssets> employeesAssets) {
        this.setEmployeesAssets(employeesAssets);
        return this;
    }

    public Employee addEmployeesAssets(EmployeesAssets employeesAssets) {
        this.employeesAssets.add(employeesAssets);
        employeesAssets.setEmployee(this);
        return this;
    }

    public Employee removeEmployeesAssets(EmployeesAssets employeesAssets) {
        this.employeesAssets.remove(employeesAssets);
        employeesAssets.setEmployee(null);
        return this;
    }

    public ContactDetails getContactDetails() {
        return this.contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Employee contactDetails(ContactDetails contactDetails) {
        this.setContactDetails(contactDetails);
        return this;
    }

    public JobPosition getJobPosition() {
        return this.jobPosition;
    }

    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }

    public Employee jobPosition(JobPosition jobPosition) {
        this.setJobPosition(jobPosition);
        return this;
    }

    public Document getEmployeeContract() {
        return this.employeeContract;
    }

    public void setEmployeeContract(Document document) {
        this.employeeContract = document;
    }

    public Employee employeeContract(Document document) {
        this.setEmployeeContract(document);
        return this;
    }

    public Employee getManager() {
        return this.manager;
    }

    public void setManager(Employee employee) {
        this.manager = employee;
    }

    public Employee manager(Employee employee) {
        this.setManager(employee);
        return this;
    }

    public Atm getAtm() {
        return this.atm;
    }

    public void setAtm(Atm atm) {
        this.atm = atm;
    }

    public Employee atm(Atm atm) {
        this.setAtm(atm);
        return this;
    }

    public Office getOffice() {
        return this.office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Employee office(Office office) {
        this.setOffice(office);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        return getId() != null && getId().equals(((Employee) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", isOnHoliday='" + getIsOnHoliday() + "'" +
            "}";
    }
}
