package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;

/**
 * A EmployeesAssets.
 */
@Entity
@Table(name = "employees_assets")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EmployeesAssets implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Employee assignedBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documentCategory", "application", "customer" }, allowSetters = true)
    private Document recieversProtocol;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public EmployeesAssets id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public EmployeesAssets name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getAssignedBy() {
        return this.assignedBy;
    }

    public void setAssignedBy(Employee employee) {
        this.assignedBy = employee;
    }

    public EmployeesAssets assignedBy(Employee employee) {
        this.setAssignedBy(employee);
        return this;
    }

    public Document getRecieversProtocol() {
        return this.recieversProtocol;
    }

    public void setRecieversProtocol(Document document) {
        this.recieversProtocol = document;
    }

    public EmployeesAssets recieversProtocol(Document document) {
        this.setRecieversProtocol(document);
        return this;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public EmployeesAssets employee(Employee employee) {
        this.setEmployee(employee);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeesAssets)) {
            return false;
        }
        return getId() != null && getId().equals(((EmployeesAssets) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeesAssets{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
