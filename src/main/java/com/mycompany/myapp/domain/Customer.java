package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.CustomerType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "customer_type", nullable = false)
    private CustomerType customerType;

    @Column(name = "credit_score")
    private Long creditScore;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @JsonIgnoreProperties(value = { "customer" }, allowSetters = true)
    private Set<Address> addresses = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @JsonIgnoreProperties(value = { "contract", "customer" }, allowSetters = true)
    private Set<PaymentAccount> paymentAccounts = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @JsonIgnoreProperties(value = { "documents", "customer" }, allowSetters = true)
    private Set<Application> applications = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @JsonIgnoreProperties(value = { "documentCategory", "application", "customer" }, allowSetters = true)
    private Set<Document> documents = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "office" }, allowSetters = true)
    private ContactDetails contactDetails;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Customer id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerType getCustomerType() {
        return this.customerType;
    }

    public Customer customerType(CustomerType customerType) {
        this.setCustomerType(customerType);
        return this;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    public Long getCreditScore() {
        return this.creditScore;
    }

    public Customer creditScore(Long creditScore) {
        this.setCreditScore(creditScore);
        return this;
    }

    public void setCreditScore(Long creditScore) {
        this.creditScore = creditScore;
    }

    public Set<Address> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        if (this.addresses != null) {
            this.addresses.forEach(i -> i.setCustomer(null));
        }
        if (addresses != null) {
            addresses.forEach(i -> i.setCustomer(this));
        }
        this.addresses = addresses;
    }

    public Customer addresses(Set<Address> addresses) {
        this.setAddresses(addresses);
        return this;
    }

    public Customer addAddresses(Address address) {
        this.addresses.add(address);
        address.setCustomer(this);
        return this;
    }

    public Customer removeAddresses(Address address) {
        this.addresses.remove(address);
        address.setCustomer(null);
        return this;
    }

    public Set<PaymentAccount> getPaymentAccounts() {
        return this.paymentAccounts;
    }

    public void setPaymentAccounts(Set<PaymentAccount> paymentAccounts) {
        if (this.paymentAccounts != null) {
            this.paymentAccounts.forEach(i -> i.setCustomer(null));
        }
        if (paymentAccounts != null) {
            paymentAccounts.forEach(i -> i.setCustomer(this));
        }
        this.paymentAccounts = paymentAccounts;
    }

    public Customer paymentAccounts(Set<PaymentAccount> paymentAccounts) {
        this.setPaymentAccounts(paymentAccounts);
        return this;
    }

    public Customer addPaymentAccounts(PaymentAccount paymentAccount) {
        this.paymentAccounts.add(paymentAccount);
        paymentAccount.setCustomer(this);
        return this;
    }

    public Customer removePaymentAccounts(PaymentAccount paymentAccount) {
        this.paymentAccounts.remove(paymentAccount);
        paymentAccount.setCustomer(null);
        return this;
    }

    public Set<Application> getApplications() {
        return this.applications;
    }

    public void setApplications(Set<Application> applications) {
        if (this.applications != null) {
            this.applications.forEach(i -> i.setCustomer(null));
        }
        if (applications != null) {
            applications.forEach(i -> i.setCustomer(this));
        }
        this.applications = applications;
    }

    public Customer applications(Set<Application> applications) {
        this.setApplications(applications);
        return this;
    }

    public Customer addApplications(Application application) {
        this.applications.add(application);
        application.setCustomer(this);
        return this;
    }

    public Customer removeApplications(Application application) {
        this.applications.remove(application);
        application.setCustomer(null);
        return this;
    }

    public Set<Document> getDocuments() {
        return this.documents;
    }

    public void setDocuments(Set<Document> documents) {
        if (this.documents != null) {
            this.documents.forEach(i -> i.setCustomer(null));
        }
        if (documents != null) {
            documents.forEach(i -> i.setCustomer(this));
        }
        this.documents = documents;
    }

    public Customer documents(Set<Document> documents) {
        this.setDocuments(documents);
        return this;
    }

    public Customer addDocuments(Document document) {
        this.documents.add(document);
        document.setCustomer(this);
        return this;
    }

    public Customer removeDocuments(Document document) {
        this.documents.remove(document);
        document.setCustomer(null);
        return this;
    }

    public ContactDetails getContactDetails() {
        return this.contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Customer contactDetails(ContactDetails contactDetails) {
        this.setContactDetails(contactDetails);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Customer)) {
            return false;
        }
        return getId() != null && getId().equals(((Customer) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", customerType='" + getCustomerType() + "'" +
            ", creditScore=" + getCreditScore() +
            "}";
    }
}
