package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A BankDetails.
 */
@Entity
@Table(name = "bank_details")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BankDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "foundation_year")
    private ZonedDateTime foundationYear;

    @Column(name = "ceo")
    private String ceo;

    @Column(name = "main_website")
    private String mainWebsite;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bankDetails")
    @JsonIgnoreProperties(value = { "contactDetails", "employees", "address", "bankDetails" }, allowSetters = true)
    private Set<Office> offices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public BankDetails id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public BankDetails name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getFoundationYear() {
        return this.foundationYear;
    }

    public BankDetails foundationYear(ZonedDateTime foundationYear) {
        this.setFoundationYear(foundationYear);
        return this;
    }

    public void setFoundationYear(ZonedDateTime foundationYear) {
        this.foundationYear = foundationYear;
    }

    public String getCeo() {
        return this.ceo;
    }

    public BankDetails ceo(String ceo) {
        this.setCeo(ceo);
        return this;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }

    public String getMainWebsite() {
        return this.mainWebsite;
    }

    public BankDetails mainWebsite(String mainWebsite) {
        this.setMainWebsite(mainWebsite);
        return this;
    }

    public void setMainWebsite(String mainWebsite) {
        this.mainWebsite = mainWebsite;
    }

    public Set<Office> getOffices() {
        return this.offices;
    }

    public void setOffices(Set<Office> offices) {
        if (this.offices != null) {
            this.offices.forEach(i -> i.setBankDetails(null));
        }
        if (offices != null) {
            offices.forEach(i -> i.setBankDetails(this));
        }
        this.offices = offices;
    }

    public BankDetails offices(Set<Office> offices) {
        this.setOffices(offices);
        return this;
    }

    public BankDetails addOffices(Office office) {
        this.offices.add(office);
        office.setBankDetails(this);
        return this;
    }

    public BankDetails removeOffices(Office office) {
        this.offices.remove(office);
        office.setBankDetails(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankDetails)) {
            return false;
        }
        return getId() != null && getId().equals(((BankDetails) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankDetails{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", foundationYear='" + getFoundationYear() + "'" +
            ", ceo='" + getCeo() + "'" +
            ", mainWebsite='" + getMainWebsite() + "'" +
            "}";
    }
}
