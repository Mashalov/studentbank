package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Office.
 */
@Entity
@Table(name = "office")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Office implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "is_main")
    private Boolean isMain;

    @NotNull
    @Column(name = "openning_time", nullable = false)
    private String openningTime;

    @NotNull
    @Column(name = "closing_time", nullable = false)
    private String closingTime;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "office")
    @JsonIgnoreProperties(value = { "office" }, allowSetters = true)
    private Set<ContactDetails> contactDetails = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "office")
    @JsonIgnoreProperties(
        value = { "employeesAssets", "contactDetails", "jobPosition", "employeeContract", "manager", "atm", "office" },
        allowSetters = true
    )
    private Set<Employee> employees = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "customer" }, allowSetters = true)
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "offices" }, allowSetters = true)
    private BankDetails bankDetails;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Office id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Office name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsMain() {
        return this.isMain;
    }

    public Office isMain(Boolean isMain) {
        this.setIsMain(isMain);
        return this;
    }

    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }

    public String getOpenningTime() {
        return this.openningTime;
    }

    public Office openningTime(String openningTime) {
        this.setOpenningTime(openningTime);
        return this;
    }

    public void setOpenningTime(String openningTime) {
        this.openningTime = openningTime;
    }

    public String getClosingTime() {
        return this.closingTime;
    }

    public Office closingTime(String closingTime) {
        this.setClosingTime(closingTime);
        return this;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public Set<ContactDetails> getContactDetails() {
        return this.contactDetails;
    }

    public void setContactDetails(Set<ContactDetails> contactDetails) {
        if (this.contactDetails != null) {
            this.contactDetails.forEach(i -> i.setOffice(null));
        }
        if (contactDetails != null) {
            contactDetails.forEach(i -> i.setOffice(this));
        }
        this.contactDetails = contactDetails;
    }

    public Office contactDetails(Set<ContactDetails> contactDetails) {
        this.setContactDetails(contactDetails);
        return this;
    }

    public Office addContactDetails(ContactDetails contactDetails) {
        this.contactDetails.add(contactDetails);
        contactDetails.setOffice(this);
        return this;
    }

    public Office removeContactDetails(ContactDetails contactDetails) {
        this.contactDetails.remove(contactDetails);
        contactDetails.setOffice(null);
        return this;
    }

    public Set<Employee> getEmployees() {
        return this.employees;
    }

    public void setEmployees(Set<Employee> employees) {
        if (this.employees != null) {
            this.employees.forEach(i -> i.setOffice(null));
        }
        if (employees != null) {
            employees.forEach(i -> i.setOffice(this));
        }
        this.employees = employees;
    }

    public Office employees(Set<Employee> employees) {
        this.setEmployees(employees);
        return this;
    }

    public Office addEmployees(Employee employee) {
        this.employees.add(employee);
        employee.setOffice(this);
        return this;
    }

    public Office removeEmployees(Employee employee) {
        this.employees.remove(employee);
        employee.setOffice(null);
        return this;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Office address(Address address) {
        this.setAddress(address);
        return this;
    }

    public BankDetails getBankDetails() {
        return this.bankDetails;
    }

    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }

    public Office bankDetails(BankDetails bankDetails) {
        this.setBankDetails(bankDetails);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Office)) {
            return false;
        }
        return getId() != null && getId().equals(((Office) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Office{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", isMain='" + getIsMain() + "'" +
            ", openningTime='" + getOpenningTime() + "'" +
            ", closingTime='" + getClosingTime() + "'" +
            "}";
    }
}
