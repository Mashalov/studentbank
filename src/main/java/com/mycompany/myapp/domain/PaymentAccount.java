package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.PaymentAccountType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A PaymentAccount.
 */
@Entity
@Table(name = "payment_account")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PaymentAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "payment_account_type", nullable = false)
    private PaymentAccountType paymentAccountType;

    @NotNull
    @Column(name = "iban", nullable = false)
    private String iban;

    @NotNull
    @Column(name = "amount", precision = 21, scale = 2, nullable = false)
    private BigDecimal amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "deleted")
    private Boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documentCategory", "application", "customer" }, allowSetters = true)
    private Document contract;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "addresses", "paymentAccounts", "applications", "documents", "contactDetails" }, allowSetters = true)
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PaymentAccount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PaymentAccountType getPaymentAccountType() {
        return this.paymentAccountType;
    }

    public PaymentAccount paymentAccountType(PaymentAccountType paymentAccountType) {
        this.setPaymentAccountType(paymentAccountType);
        return this;
    }

    public void setPaymentAccountType(PaymentAccountType paymentAccountType) {
        this.paymentAccountType = paymentAccountType;
    }

    public String getIban() {
        return this.iban;
    }

    public PaymentAccount iban(String iban) {
        this.setIban(iban);
        return this;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public PaymentAccount amount(BigDecimal amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public PaymentAccount currency(Currency currency) {
        this.setCurrency(currency);
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Boolean getActive() {
        return this.active;
    }

    public PaymentAccount active(Boolean active) {
        this.setActive(active);
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public PaymentAccount deleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Document getContract() {
        return this.contract;
    }

    public void setContract(Document document) {
        this.contract = document;
    }

    public PaymentAccount contract(Document document) {
        this.setContract(document);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public PaymentAccount customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentAccount)) {
            return false;
        }
        return getId() != null && getId().equals(((PaymentAccount) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentAccount{" +
            "id=" + getId() +
            ", paymentAccountType='" + getPaymentAccountType() + "'" +
            ", iban='" + getIban() + "'" +
            ", amount=" + getAmount() +
            ", currency='" + getCurrency() + "'" +
            ", active='" + getActive() + "'" +
            ", deleted='" + getDeleted() + "'" +
            "}";
    }
}
