package com.mycompany.myapp.domain.enumeration;

/**
 * The CustomerType enumeration.
 */
public enum CustomerType {
    LEGAL_PERSON,
    NATURAL_PERSON,
    NOT_SET,
}
