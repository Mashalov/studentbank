package com.mycompany.myapp.domain.enumeration;

/**
 * The TransactionLocation enumeration.
 */
public enum TransactionLocation {
    ATM,
    OFFICE,
    ONLINE,
}
