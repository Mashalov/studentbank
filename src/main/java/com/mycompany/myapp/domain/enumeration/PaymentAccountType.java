package com.mycompany.myapp.domain.enumeration;

/**
 * The PaymentAccountType enumeration.
 */
public enum PaymentAccountType {
    SAVING_ACCOUNT,
    CREDIT_ACCOUNT,
    OVERDRAFT,
    DEPOSIT,
}
