package com.mycompany.myapp.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    BGN,
    EUR,
    USD,
}
