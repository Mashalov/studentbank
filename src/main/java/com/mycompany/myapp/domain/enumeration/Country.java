package com.mycompany.myapp.domain.enumeration;

/**
 * The Country enumeration.
 */
public enum Country {
    Bulgaria,
    Germany,
    UK,
}
