package com.mycompany.myapp.domain.enumeration;

/**
 * The TransactionOperationType enumeration.
 */
public enum TransactionOperationType {
    TRANSFER,
    CREDIT,
    DEPOSIT,
}
