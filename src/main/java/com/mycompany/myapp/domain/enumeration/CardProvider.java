package com.mycompany.myapp.domain.enumeration;

/**
 * The CardProvider enumeration.
 */
public enum CardProvider {
    MASTERCARD,
    VISA,
    MAESTRO,
    AMERICAN_EXPRESS,
}
