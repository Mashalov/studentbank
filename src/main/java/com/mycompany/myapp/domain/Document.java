package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.DocumentType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;

/**
 * A Document.
 */
@Entity
@Table(name = "document")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "document_extension", nullable = false)
    private String documentExtension;

    @Lob
    @Column(name = "document_data", nullable = false)
    private byte[] documentData;

    @NotNull
    @Column(name = "document_data_content_type", nullable = false)
    private String documentDataContentType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "document_type", nullable = false)
    private DocumentType documentType;

    @Column(name = "is_orinal_document")
    private Boolean isOrinalDocument;

    @Column(name = "document_compression")
    private String documentCompression;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documentCategory" }, allowSetters = true)
    private DocumentCategory documentCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "documents", "customer" }, allowSetters = true)
    private Application application;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "addresses", "paymentAccounts", "applications", "documents", "contactDetails" }, allowSetters = true)
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Document id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Document name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentExtension() {
        return this.documentExtension;
    }

    public Document documentExtension(String documentExtension) {
        this.setDocumentExtension(documentExtension);
        return this;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }

    public byte[] getDocumentData() {
        return this.documentData;
    }

    public Document documentData(byte[] documentData) {
        this.setDocumentData(documentData);
        return this;
    }

    public void setDocumentData(byte[] documentData) {
        this.documentData = documentData;
    }

    public String getDocumentDataContentType() {
        return this.documentDataContentType;
    }

    public Document documentDataContentType(String documentDataContentType) {
        this.documentDataContentType = documentDataContentType;
        return this;
    }

    public void setDocumentDataContentType(String documentDataContentType) {
        this.documentDataContentType = documentDataContentType;
    }

    public DocumentType getDocumentType() {
        return this.documentType;
    }

    public Document documentType(DocumentType documentType) {
        this.setDocumentType(documentType);
        return this;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public Boolean getIsOrinalDocument() {
        return this.isOrinalDocument;
    }

    public Document isOrinalDocument(Boolean isOrinalDocument) {
        this.setIsOrinalDocument(isOrinalDocument);
        return this;
    }

    public void setIsOrinalDocument(Boolean isOrinalDocument) {
        this.isOrinalDocument = isOrinalDocument;
    }

    public String getDocumentCompression() {
        return this.documentCompression;
    }

    public Document documentCompression(String documentCompression) {
        this.setDocumentCompression(documentCompression);
        return this;
    }

    public void setDocumentCompression(String documentCompression) {
        this.documentCompression = documentCompression;
    }

    public DocumentCategory getDocumentCategory() {
        return this.documentCategory;
    }

    public void setDocumentCategory(DocumentCategory documentCategory) {
        this.documentCategory = documentCategory;
    }

    public Document documentCategory(DocumentCategory documentCategory) {
        this.setDocumentCategory(documentCategory);
        return this;
    }

    public Application getApplication() {
        return this.application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Document application(Application application) {
        this.setApplication(application);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Document customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Document)) {
            return false;
        }
        return getId() != null && getId().equals(((Document) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Document{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", documentExtension='" + getDocumentExtension() + "'" +
            ", documentData='" + getDocumentData() + "'" +
            ", documentDataContentType='" + getDocumentDataContentType() + "'" +
            ", documentType='" + getDocumentType() + "'" +
            ", isOrinalDocument='" + getIsOrinalDocument() + "'" +
            ", documentCompression='" + getDocumentCompression() + "'" +
            "}";
    }
}
