package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.CardProvider;
import com.mycompany.myapp.domain.enumeration.CardType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A CardDetails.
 */
@Entity
@Table(name = "card_details")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CardDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "card_number", nullable = false)
    private Long cardNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "card_type", nullable = false)
    private CardType cardType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "card_provider", nullable = false)
    private CardProvider cardProvider;

    @NotNull
    @Column(name = "issue_date", nullable = false)
    private ZonedDateTime issueDate;

    @NotNull
    @Column(name = "expiry_date", nullable = false)
    private ZonedDateTime expiryDate;

    @NotNull
    @Column(name = "cvv", nullable = false)
    private Long cvv;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_blocked")
    private Boolean isBlocked;

    @Column(name = "pin")
    private Long pin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "addresses", "paymentAccounts", "applications", "documents", "contactDetails" }, allowSetters = true)
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contract", "customer" }, allowSetters = true)
    private PaymentAccount paymentAccount;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CardDetails id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCardNumber() {
        return this.cardNumber;
    }

    public CardDetails cardNumber(Long cardNumber) {
        this.setCardNumber(cardNumber);
        return this;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CardType getCardType() {
        return this.cardType;
    }

    public CardDetails cardType(CardType cardType) {
        this.setCardType(cardType);
        return this;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public CardProvider getCardProvider() {
        return this.cardProvider;
    }

    public CardDetails cardProvider(CardProvider cardProvider) {
        this.setCardProvider(cardProvider);
        return this;
    }

    public void setCardProvider(CardProvider cardProvider) {
        this.cardProvider = cardProvider;
    }

    public ZonedDateTime getIssueDate() {
        return this.issueDate;
    }

    public CardDetails issueDate(ZonedDateTime issueDate) {
        this.setIssueDate(issueDate);
        return this;
    }

    public void setIssueDate(ZonedDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public ZonedDateTime getExpiryDate() {
        return this.expiryDate;
    }

    public CardDetails expiryDate(ZonedDateTime expiryDate) {
        this.setExpiryDate(expiryDate);
        return this;
    }

    public void setExpiryDate(ZonedDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getCvv() {
        return this.cvv;
    }

    public CardDetails cvv(Long cvv) {
        this.setCvv(cvv);
        return this;
    }

    public void setCvv(Long cvv) {
        this.cvv = cvv;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public CardDetails isActive(Boolean isActive) {
        this.setIsActive(isActive);
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsBlocked() {
        return this.isBlocked;
    }

    public CardDetails isBlocked(Boolean isBlocked) {
        this.setIsBlocked(isBlocked);
        return this;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Long getPin() {
        return this.pin;
    }

    public CardDetails pin(Long pin) {
        this.setPin(pin);
        return this;
    }

    public void setPin(Long pin) {
        this.pin = pin;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CardDetails customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public PaymentAccount getPaymentAccount() {
        return this.paymentAccount;
    }

    public void setPaymentAccount(PaymentAccount paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public CardDetails paymentAccount(PaymentAccount paymentAccount) {
        this.setPaymentAccount(paymentAccount);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CardDetails)) {
            return false;
        }
        return getId() != null && getId().equals(((CardDetails) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CardDetails{" +
            "id=" + getId() +
            ", cardNumber=" + getCardNumber() +
            ", cardType='" + getCardType() + "'" +
            ", cardProvider='" + getCardProvider() + "'" +
            ", issueDate='" + getIssueDate() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", cvv=" + getCvv() +
            ", isActive='" + getIsActive() + "'" +
            ", isBlocked='" + getIsBlocked() + "'" +
            ", pin=" + getPin() +
            "}";
    }
}
