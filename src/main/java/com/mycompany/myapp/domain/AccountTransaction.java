package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.TransactionLocation;
import com.mycompany.myapp.domain.enumeration.TransactionOperationType;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A AccountTransaction.
 */
@Entity
@Table(name = "account_transaction")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "external_id")
    private String externalId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_operation_type", nullable = false)
    private TransactionOperationType transactionOperationType;

    @NotNull
    @Column(name = "amount", precision = 21, scale = 2, nullable = false)
    private BigDecimal amount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "location", nullable = false)
    private TransactionLocation location;

    @Column(name = "send_time")
    private ZonedDateTime sendTime;

    @Column(name = "receive_time")
    private ZonedDateTime receiveTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contract", "customer" }, allowSetters = true)
    private PaymentAccount sender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contract", "customer" }, allowSetters = true)
    private PaymentAccount receiver;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AccountTransaction id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return this.externalId;
    }

    public AccountTransaction externalId(String externalId) {
        this.setExternalId(externalId);
        return this;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public TransactionOperationType getTransactionOperationType() {
        return this.transactionOperationType;
    }

    public AccountTransaction transactionOperationType(TransactionOperationType transactionOperationType) {
        this.setTransactionOperationType(transactionOperationType);
        return this;
    }

    public void setTransactionOperationType(TransactionOperationType transactionOperationType) {
        this.transactionOperationType = transactionOperationType;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public AccountTransaction amount(BigDecimal amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public AccountTransaction currency(Currency currency) {
        this.setCurrency(currency);
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public TransactionLocation getLocation() {
        return this.location;
    }

    public AccountTransaction location(TransactionLocation location) {
        this.setLocation(location);
        return this;
    }

    public void setLocation(TransactionLocation location) {
        this.location = location;
    }

    public ZonedDateTime getSendTime() {
        return this.sendTime;
    }

    public AccountTransaction sendTime(ZonedDateTime sendTime) {
        this.setSendTime(sendTime);
        return this;
    }

    public void setSendTime(ZonedDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public ZonedDateTime getReceiveTime() {
        return this.receiveTime;
    }

    public AccountTransaction receiveTime(ZonedDateTime receiveTime) {
        this.setReceiveTime(receiveTime);
        return this;
    }

    public void setReceiveTime(ZonedDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public PaymentAccount getSender() {
        return this.sender;
    }

    public void setSender(PaymentAccount paymentAccount) {
        this.sender = paymentAccount;
    }

    public AccountTransaction sender(PaymentAccount paymentAccount) {
        this.setSender(paymentAccount);
        return this;
    }

    public PaymentAccount getReceiver() {
        return this.receiver;
    }

    public void setReceiver(PaymentAccount paymentAccount) {
        this.receiver = paymentAccount;
    }

    public AccountTransaction receiver(PaymentAccount paymentAccount) {
        this.setReceiver(paymentAccount);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountTransaction)) {
            return false;
        }
        return getId() != null && getId().equals(((AccountTransaction) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountTransaction{" +
            "id=" + getId() +
            ", externalId='" + getExternalId() + "'" +
            ", transactionOperationType='" + getTransactionOperationType() + "'" +
            ", amount=" + getAmount() +
            ", currency='" + getCurrency() + "'" +
            ", location='" + getLocation() + "'" +
            ", sendTime='" + getSendTime() + "'" +
            ", receiveTime='" + getReceiveTime() + "'" +
            "}";
    }
}
