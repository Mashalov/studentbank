package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.CardDetailsAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.CardDetails;
import com.mycompany.myapp.domain.enumeration.CardProvider;
import com.mycompany.myapp.domain.enumeration.CardType;
import com.mycompany.myapp.repository.CardDetailsRepository;
import com.mycompany.myapp.service.dto.CardDetailsDTO;
import com.mycompany.myapp.service.mapper.CardDetailsMapper;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CardDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CardDetailsResourceIT {

    private static final Long DEFAULT_CARD_NUMBER = 1L;
    private static final Long UPDATED_CARD_NUMBER = 2L;

    private static final CardType DEFAULT_CARD_TYPE = CardType.CREDIT;
    private static final CardType UPDATED_CARD_TYPE = CardType.DEBIT;

    private static final CardProvider DEFAULT_CARD_PROVIDER = CardProvider.MASTERCARD;
    private static final CardProvider UPDATED_CARD_PROVIDER = CardProvider.VISA;

    private static final ZonedDateTime DEFAULT_ISSUE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_ISSUE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_EXPIRY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRY_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_CVV = 1L;
    private static final Long UPDATED_CVV = 2L;

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final Boolean DEFAULT_IS_BLOCKED = false;
    private static final Boolean UPDATED_IS_BLOCKED = true;

    private static final Long DEFAULT_PIN = 1L;
    private static final Long UPDATED_PIN = 2L;

    private static final String ENTITY_API_URL = "/api/card-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private CardDetailsRepository cardDetailsRepository;

    @Autowired
    private CardDetailsMapper cardDetailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCardDetailsMockMvc;

    private CardDetails cardDetails;

    private CardDetails insertedCardDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardDetails createEntity(EntityManager em) {
        CardDetails cardDetails = new CardDetails()
            .cardNumber(DEFAULT_CARD_NUMBER)
            .cardType(DEFAULT_CARD_TYPE)
            .cardProvider(DEFAULT_CARD_PROVIDER)
            .issueDate(DEFAULT_ISSUE_DATE)
            .expiryDate(DEFAULT_EXPIRY_DATE)
            .cvv(DEFAULT_CVV)
            .isActive(DEFAULT_IS_ACTIVE)
            .isBlocked(DEFAULT_IS_BLOCKED)
            .pin(DEFAULT_PIN);
        return cardDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CardDetails createUpdatedEntity(EntityManager em) {
        CardDetails cardDetails = new CardDetails()
            .cardNumber(UPDATED_CARD_NUMBER)
            .cardType(UPDATED_CARD_TYPE)
            .cardProvider(UPDATED_CARD_PROVIDER)
            .issueDate(UPDATED_ISSUE_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .cvv(UPDATED_CVV)
            .isActive(UPDATED_IS_ACTIVE)
            .isBlocked(UPDATED_IS_BLOCKED)
            .pin(UPDATED_PIN);
        return cardDetails;
    }

    @BeforeEach
    public void initTest() {
        cardDetails = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedCardDetails != null) {
            cardDetailsRepository.delete(insertedCardDetails);
            insertedCardDetails = null;
        }
    }

    @Test
    @Transactional
    void createCardDetails() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);
        var returnedCardDetailsDTO = om.readValue(
            restCardDetailsMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            CardDetailsDTO.class
        );

        // Validate the CardDetails in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedCardDetails = cardDetailsMapper.toEntity(returnedCardDetailsDTO);
        assertCardDetailsUpdatableFieldsEquals(returnedCardDetails, getPersistedCardDetails(returnedCardDetails));

        insertedCardDetails = returnedCardDetails;
    }

    @Test
    @Transactional
    void createCardDetailsWithExistingId() throws Exception {
        // Create the CardDetails with an existing ID
        cardDetails.setId(1L);
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCardNumberIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setCardNumber(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCardTypeIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setCardType(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCardProviderIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setCardProvider(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIssueDateIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setIssueDate(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkExpiryDateIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setExpiryDate(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCvvIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        cardDetails.setCvv(null);

        // Create the CardDetails, which fails.
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        restCardDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCardDetails() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        // Get all the cardDetailsList
        restCardDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cardDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].cardType").value(hasItem(DEFAULT_CARD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].cardProvider").value(hasItem(DEFAULT_CARD_PROVIDER.toString())))
            .andExpect(jsonPath("$.[*].issueDate").value(hasItem(sameInstant(DEFAULT_ISSUE_DATE))))
            .andExpect(jsonPath("$.[*].expiryDate").value(hasItem(sameInstant(DEFAULT_EXPIRY_DATE))))
            .andExpect(jsonPath("$.[*].cvv").value(hasItem(DEFAULT_CVV.intValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].isBlocked").value(hasItem(DEFAULT_IS_BLOCKED.booleanValue())))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN.intValue())));
    }

    @Test
    @Transactional
    void getCardDetails() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        // Get the cardDetails
        restCardDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, cardDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cardDetails.getId().intValue()))
            .andExpect(jsonPath("$.cardNumber").value(DEFAULT_CARD_NUMBER.intValue()))
            .andExpect(jsonPath("$.cardType").value(DEFAULT_CARD_TYPE.toString()))
            .andExpect(jsonPath("$.cardProvider").value(DEFAULT_CARD_PROVIDER.toString()))
            .andExpect(jsonPath("$.issueDate").value(sameInstant(DEFAULT_ISSUE_DATE)))
            .andExpect(jsonPath("$.expiryDate").value(sameInstant(DEFAULT_EXPIRY_DATE)))
            .andExpect(jsonPath("$.cvv").value(DEFAULT_CVV.intValue()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.isBlocked").value(DEFAULT_IS_BLOCKED.booleanValue()))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingCardDetails() throws Exception {
        // Get the cardDetails
        restCardDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCardDetails() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the cardDetails
        CardDetails updatedCardDetails = cardDetailsRepository.findById(cardDetails.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedCardDetails are not directly saved in db
        em.detach(updatedCardDetails);
        updatedCardDetails
            .cardNumber(UPDATED_CARD_NUMBER)
            .cardType(UPDATED_CARD_TYPE)
            .cardProvider(UPDATED_CARD_PROVIDER)
            .issueDate(UPDATED_ISSUE_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .cvv(UPDATED_CVV)
            .isActive(UPDATED_IS_ACTIVE)
            .isBlocked(UPDATED_IS_BLOCKED)
            .pin(UPDATED_PIN);
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(updatedCardDetails);

        restCardDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cardDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(cardDetailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedCardDetailsToMatchAllProperties(updatedCardDetails);
    }

    @Test
    @Transactional
    void putNonExistingCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cardDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(cardDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(cardDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCardDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the cardDetails using partial update
        CardDetails partialUpdatedCardDetails = new CardDetails();
        partialUpdatedCardDetails.setId(cardDetails.getId());

        partialUpdatedCardDetails
            .cardNumber(UPDATED_CARD_NUMBER)
            .cardProvider(UPDATED_CARD_PROVIDER)
            .issueDate(UPDATED_ISSUE_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .isBlocked(UPDATED_IS_BLOCKED)
            .pin(UPDATED_PIN);

        restCardDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCardDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedCardDetails))
            )
            .andExpect(status().isOk());

        // Validate the CardDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertCardDetailsUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedCardDetails, cardDetails),
            getPersistedCardDetails(cardDetails)
        );
    }

    @Test
    @Transactional
    void fullUpdateCardDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the cardDetails using partial update
        CardDetails partialUpdatedCardDetails = new CardDetails();
        partialUpdatedCardDetails.setId(cardDetails.getId());

        partialUpdatedCardDetails
            .cardNumber(UPDATED_CARD_NUMBER)
            .cardType(UPDATED_CARD_TYPE)
            .cardProvider(UPDATED_CARD_PROVIDER)
            .issueDate(UPDATED_ISSUE_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .cvv(UPDATED_CVV)
            .isActive(UPDATED_IS_ACTIVE)
            .isBlocked(UPDATED_IS_BLOCKED)
            .pin(UPDATED_PIN);

        restCardDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCardDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedCardDetails))
            )
            .andExpect(status().isOk());

        // Validate the CardDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertCardDetailsUpdatableFieldsEquals(partialUpdatedCardDetails, getPersistedCardDetails(partialUpdatedCardDetails));
    }

    @Test
    @Transactional
    void patchNonExistingCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, cardDetailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(cardDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(cardDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCardDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        cardDetails.setId(longCount.incrementAndGet());

        // Create the CardDetails
        CardDetailsDTO cardDetailsDTO = cardDetailsMapper.toDto(cardDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCardDetailsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(cardDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CardDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCardDetails() throws Exception {
        // Initialize the database
        insertedCardDetails = cardDetailsRepository.saveAndFlush(cardDetails);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the cardDetails
        restCardDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, cardDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return cardDetailsRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected CardDetails getPersistedCardDetails(CardDetails cardDetails) {
        return cardDetailsRepository.findById(cardDetails.getId()).orElseThrow();
    }

    protected void assertPersistedCardDetailsToMatchAllProperties(CardDetails expectedCardDetails) {
        assertCardDetailsAllPropertiesEquals(expectedCardDetails, getPersistedCardDetails(expectedCardDetails));
    }

    protected void assertPersistedCardDetailsToMatchUpdatableProperties(CardDetails expectedCardDetails) {
        assertCardDetailsAllUpdatablePropertiesEquals(expectedCardDetails, getPersistedCardDetails(expectedCardDetails));
    }
}
