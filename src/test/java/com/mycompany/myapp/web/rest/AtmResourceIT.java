package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.AtmAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Atm;
import com.mycompany.myapp.domain.enumeration.DocumentType;
import com.mycompany.myapp.repository.AtmRepository;
import com.mycompany.myapp.service.dto.AtmDTO;
import com.mycompany.myapp.service.mapper.AtmMapper;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AtmResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AtmResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final DocumentType DEFAULT_DOCUMENT_TYPE = DocumentType.PDF;
    private static final DocumentType UPDATED_DOCUMENT_TYPE = DocumentType.DOCX;

    private static final String ENTITY_API_URL = "/api/atms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private AtmRepository atmRepository;

    @Autowired
    private AtmMapper atmMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAtmMockMvc;

    private Atm atm;

    private Atm insertedAtm;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atm createEntity(EntityManager em) {
        Atm atm = new Atm().name(DEFAULT_NAME).documentType(DEFAULT_DOCUMENT_TYPE);
        return atm;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Atm createUpdatedEntity(EntityManager em) {
        Atm atm = new Atm().name(UPDATED_NAME).documentType(UPDATED_DOCUMENT_TYPE);
        return atm;
    }

    @BeforeEach
    public void initTest() {
        atm = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedAtm != null) {
            atmRepository.delete(insertedAtm);
            insertedAtm = null;
        }
    }

    @Test
    @Transactional
    void createAtm() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);
        var returnedAtmDTO = om.readValue(
            restAtmMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            AtmDTO.class
        );

        // Validate the Atm in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedAtm = atmMapper.toEntity(returnedAtmDTO);
        assertAtmUpdatableFieldsEquals(returnedAtm, getPersistedAtm(returnedAtm));

        insertedAtm = returnedAtm;
    }

    @Test
    @Transactional
    void createAtmWithExistingId() throws Exception {
        // Create the Atm with an existing ID
        atm.setId(1L);
        AtmDTO atmDTO = atmMapper.toDto(atm);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtmMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        atm.setName(null);

        // Create the Atm, which fails.
        AtmDTO atmDTO = atmMapper.toDto(atm);

        restAtmMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllAtms() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        // Get all the atmList
        restAtmMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atm.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].documentType").value(hasItem(DEFAULT_DOCUMENT_TYPE.toString())));
    }

    @Test
    @Transactional
    void getAtm() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        // Get the atm
        restAtmMockMvc
            .perform(get(ENTITY_API_URL_ID, atm.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(atm.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.documentType").value(DEFAULT_DOCUMENT_TYPE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingAtm() throws Exception {
        // Get the atm
        restAtmMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAtm() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the atm
        Atm updatedAtm = atmRepository.findById(atm.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedAtm are not directly saved in db
        em.detach(updatedAtm);
        updatedAtm.name(UPDATED_NAME).documentType(UPDATED_DOCUMENT_TYPE);
        AtmDTO atmDTO = atmMapper.toDto(updatedAtm);

        restAtmMockMvc
            .perform(put(ENTITY_API_URL_ID, atmDTO.getId()).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isOk());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedAtmToMatchAllProperties(updatedAtm);
    }

    @Test
    @Transactional
    void putNonExistingAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(put(ENTITY_API_URL_ID, atmDTO.getId()).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(atmDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAtmWithPatch() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the atm using partial update
        Atm partialUpdatedAtm = new Atm();
        partialUpdatedAtm.setId(atm.getId());

        partialUpdatedAtm.name(UPDATED_NAME).documentType(UPDATED_DOCUMENT_TYPE);

        restAtmMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtm.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedAtm))
            )
            .andExpect(status().isOk());

        // Validate the Atm in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAtmUpdatableFieldsEquals(createUpdateProxyForBean(partialUpdatedAtm, atm), getPersistedAtm(atm));
    }

    @Test
    @Transactional
    void fullUpdateAtmWithPatch() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the atm using partial update
        Atm partialUpdatedAtm = new Atm();
        partialUpdatedAtm.setId(atm.getId());

        partialUpdatedAtm.name(UPDATED_NAME).documentType(UPDATED_DOCUMENT_TYPE);

        restAtmMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAtm.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedAtm))
            )
            .andExpect(status().isOk());

        // Validate the Atm in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAtmUpdatableFieldsEquals(partialUpdatedAtm, getPersistedAtm(partialUpdatedAtm));
    }

    @Test
    @Transactional
    void patchNonExistingAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, atmDTO.getId()).contentType("application/merge-patch+json").content(om.writeValueAsBytes(atmDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(atmDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAtm() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        atm.setId(longCount.incrementAndGet());

        // Create the Atm
        AtmDTO atmDTO = atmMapper.toDto(atm);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAtmMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(atmDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Atm in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAtm() throws Exception {
        // Initialize the database
        insertedAtm = atmRepository.saveAndFlush(atm);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the atm
        restAtmMockMvc.perform(delete(ENTITY_API_URL_ID, atm.getId()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return atmRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected Atm getPersistedAtm(Atm atm) {
        return atmRepository.findById(atm.getId()).orElseThrow();
    }

    protected void assertPersistedAtmToMatchAllProperties(Atm expectedAtm) {
        assertAtmAllPropertiesEquals(expectedAtm, getPersistedAtm(expectedAtm));
    }

    protected void assertPersistedAtmToMatchUpdatableProperties(Atm expectedAtm) {
        assertAtmAllUpdatablePropertiesEquals(expectedAtm, getPersistedAtm(expectedAtm));
    }
}
