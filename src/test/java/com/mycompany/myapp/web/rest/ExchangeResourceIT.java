package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.ExchangeAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static com.mycompany.myapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Exchange;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.repository.ExchangeRepository;
import com.mycompany.myapp.service.dto.ExchangeDTO;
import com.mycompany.myapp.service.mapper.ExchangeMapper;
import jakarta.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ExchangeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ExchangeResourceIT {

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

    private static final Currency DEFAULT_CURRENCY_IN = Currency.BGN;
    private static final Currency UPDATED_CURRENCY_IN = Currency.EUR;

    private static final Currency DEFAULT_CURRENCY_OUT = Currency.BGN;
    private static final Currency UPDATED_CURRENCY_OUT = Currency.EUR;

    private static final Long DEFAULT_EXCHANGE_RATE = 1L;
    private static final Long UPDATED_EXCHANGE_RATE = 2L;

    private static final String ENTITY_API_URL = "/api/exchanges";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ExchangeRepository exchangeRepository;

    @Autowired
    private ExchangeMapper exchangeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExchangeMockMvc;

    private Exchange exchange;

    private Exchange insertedExchange;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Exchange createEntity(EntityManager em) {
        Exchange exchange = new Exchange()
            .amount(DEFAULT_AMOUNT)
            .currencyIn(DEFAULT_CURRENCY_IN)
            .currencyOut(DEFAULT_CURRENCY_OUT)
            .exchangeRate(DEFAULT_EXCHANGE_RATE);
        return exchange;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Exchange createUpdatedEntity(EntityManager em) {
        Exchange exchange = new Exchange()
            .amount(UPDATED_AMOUNT)
            .currencyIn(UPDATED_CURRENCY_IN)
            .currencyOut(UPDATED_CURRENCY_OUT)
            .exchangeRate(UPDATED_EXCHANGE_RATE);
        return exchange;
    }

    @BeforeEach
    public void initTest() {
        exchange = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedExchange != null) {
            exchangeRepository.delete(insertedExchange);
            insertedExchange = null;
        }
    }

    @Test
    @Transactional
    void createExchange() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);
        var returnedExchangeDTO = om.readValue(
            restExchangeMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            ExchangeDTO.class
        );

        // Validate the Exchange in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedExchange = exchangeMapper.toEntity(returnedExchangeDTO);
        assertExchangeUpdatableFieldsEquals(returnedExchange, getPersistedExchange(returnedExchange));

        insertedExchange = returnedExchange;
    }

    @Test
    @Transactional
    void createExchangeWithExistingId() throws Exception {
        // Create the Exchange with an existing ID
        exchange.setId(1L);
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restExchangeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        exchange.setAmount(null);

        // Create the Exchange, which fails.
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        restExchangeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCurrencyInIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        exchange.setCurrencyIn(null);

        // Create the Exchange, which fails.
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        restExchangeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCurrencyOutIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        exchange.setCurrencyOut(null);

        // Create the Exchange, which fails.
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        restExchangeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllExchanges() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        // Get all the exchangeList
        restExchangeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(exchange.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(sameNumber(DEFAULT_AMOUNT))))
            .andExpect(jsonPath("$.[*].currencyIn").value(hasItem(DEFAULT_CURRENCY_IN.toString())))
            .andExpect(jsonPath("$.[*].currencyOut").value(hasItem(DEFAULT_CURRENCY_OUT.toString())))
            .andExpect(jsonPath("$.[*].exchangeRate").value(hasItem(DEFAULT_EXCHANGE_RATE.intValue())));
    }

    @Test
    @Transactional
    void getExchange() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        // Get the exchange
        restExchangeMockMvc
            .perform(get(ENTITY_API_URL_ID, exchange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(exchange.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(sameNumber(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.currencyIn").value(DEFAULT_CURRENCY_IN.toString()))
            .andExpect(jsonPath("$.currencyOut").value(DEFAULT_CURRENCY_OUT.toString()))
            .andExpect(jsonPath("$.exchangeRate").value(DEFAULT_EXCHANGE_RATE.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingExchange() throws Exception {
        // Get the exchange
        restExchangeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingExchange() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the exchange
        Exchange updatedExchange = exchangeRepository.findById(exchange.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedExchange are not directly saved in db
        em.detach(updatedExchange);
        updatedExchange
            .amount(UPDATED_AMOUNT)
            .currencyIn(UPDATED_CURRENCY_IN)
            .currencyOut(UPDATED_CURRENCY_OUT)
            .exchangeRate(UPDATED_EXCHANGE_RATE);
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(updatedExchange);

        restExchangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(exchangeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedExchangeToMatchAllProperties(updatedExchange);
    }

    @Test
    @Transactional
    void putNonExistingExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, exchangeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(exchangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(exchangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateExchangeWithPatch() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the exchange using partial update
        Exchange partialUpdatedExchange = new Exchange();
        partialUpdatedExchange.setId(exchange.getId());

        restExchangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchange.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedExchange))
            )
            .andExpect(status().isOk());

        // Validate the Exchange in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertExchangeUpdatableFieldsEquals(createUpdateProxyForBean(partialUpdatedExchange, exchange), getPersistedExchange(exchange));
    }

    @Test
    @Transactional
    void fullUpdateExchangeWithPatch() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the exchange using partial update
        Exchange partialUpdatedExchange = new Exchange();
        partialUpdatedExchange.setId(exchange.getId());

        partialUpdatedExchange
            .amount(UPDATED_AMOUNT)
            .currencyIn(UPDATED_CURRENCY_IN)
            .currencyOut(UPDATED_CURRENCY_OUT)
            .exchangeRate(UPDATED_EXCHANGE_RATE);

        restExchangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedExchange.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedExchange))
            )
            .andExpect(status().isOk());

        // Validate the Exchange in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertExchangeUpdatableFieldsEquals(partialUpdatedExchange, getPersistedExchange(partialUpdatedExchange));
    }

    @Test
    @Transactional
    void patchNonExistingExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, exchangeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(exchangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(exchangeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamExchange() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        exchange.setId(longCount.incrementAndGet());

        // Create the Exchange
        ExchangeDTO exchangeDTO = exchangeMapper.toDto(exchange);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restExchangeMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(exchangeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Exchange in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteExchange() throws Exception {
        // Initialize the database
        insertedExchange = exchangeRepository.saveAndFlush(exchange);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the exchange
        restExchangeMockMvc
            .perform(delete(ENTITY_API_URL_ID, exchange.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return exchangeRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected Exchange getPersistedExchange(Exchange exchange) {
        return exchangeRepository.findById(exchange.getId()).orElseThrow();
    }

    protected void assertPersistedExchangeToMatchAllProperties(Exchange expectedExchange) {
        assertExchangeAllPropertiesEquals(expectedExchange, getPersistedExchange(expectedExchange));
    }

    protected void assertPersistedExchangeToMatchUpdatableProperties(Exchange expectedExchange) {
        assertExchangeAllUpdatablePropertiesEquals(expectedExchange, getPersistedExchange(expectedExchange));
    }
}
