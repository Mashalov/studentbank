package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.ContactDetailsAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.ContactDetails;
import com.mycompany.myapp.repository.ContactDetailsRepository;
import com.mycompany.myapp.service.dto.ContactDetailsDTO;
import com.mycompany.myapp.service.mapper.ContactDetailsMapper;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ContactDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ContactDetailsResourceIT {

    private static final Long DEFAULT_COUNTRY_CODE = 1L;
    private static final Long UPDATED_COUNTRY_CODE = 2L;

    private static final Long DEFAULT_PHONE_NUMBER = 1L;
    private static final Long UPDATED_PHONE_NUMBER = 2L;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/contact-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ContactDetailsRepository contactDetailsRepository;

    @Autowired
    private ContactDetailsMapper contactDetailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactDetailsMockMvc;

    private ContactDetails contactDetails;

    private ContactDetails insertedContactDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactDetails createEntity(EntityManager em) {
        ContactDetails contactDetails = new ContactDetails()
            .countryCode(DEFAULT_COUNTRY_CODE)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .email(DEFAULT_EMAIL)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .name(DEFAULT_NAME);
        return contactDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactDetails createUpdatedEntity(EntityManager em) {
        ContactDetails contactDetails = new ContactDetails()
            .countryCode(UPDATED_COUNTRY_CODE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .name(UPDATED_NAME);
        return contactDetails;
    }

    @BeforeEach
    public void initTest() {
        contactDetails = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedContactDetails != null) {
            contactDetailsRepository.delete(insertedContactDetails);
            insertedContactDetails = null;
        }
    }

    @Test
    @Transactional
    void createContactDetails() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);
        var returnedContactDetailsDTO = om.readValue(
            restContactDetailsMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            ContactDetailsDTO.class
        );

        // Validate the ContactDetails in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedContactDetails = contactDetailsMapper.toEntity(returnedContactDetailsDTO);
        assertContactDetailsUpdatableFieldsEquals(returnedContactDetails, getPersistedContactDetails(returnedContactDetails));

        insertedContactDetails = returnedContactDetails;
    }

    @Test
    @Transactional
    void createContactDetailsWithExistingId() throws Exception {
        // Create the ContactDetails with an existing ID
        contactDetails.setId(1L);
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCountryCodeIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        contactDetails.setCountryCode(null);

        // Create the ContactDetails, which fails.
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPhoneNumberIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        contactDetails.setPhoneNumber(null);

        // Create the ContactDetails, which fails.
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        contactDetails.setEmail(null);

        // Create the ContactDetails, which fails.
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        contactDetails.setFirstName(null);

        // Create the ContactDetails, which fails.
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        contactDetails.setLastName(null);

        // Create the ContactDetails, which fails.
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        restContactDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllContactDetails() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        // Get all the contactDetailsList
        restContactDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE.intValue())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getContactDetails() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        // Get the contactDetails
        restContactDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, contactDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactDetails.getId().intValue()))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE.intValue()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingContactDetails() throws Exception {
        // Get the contactDetails
        restContactDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingContactDetails() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the contactDetails
        ContactDetails updatedContactDetails = contactDetailsRepository.findById(contactDetails.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedContactDetails are not directly saved in db
        em.detach(updatedContactDetails);
        updatedContactDetails
            .countryCode(UPDATED_COUNTRY_CODE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .name(UPDATED_NAME);
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(updatedContactDetails);

        restContactDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, contactDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(contactDetailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedContactDetailsToMatchAllProperties(updatedContactDetails);
    }

    @Test
    @Transactional
    void putNonExistingContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, contactDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(contactDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(contactDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateContactDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the contactDetails using partial update
        ContactDetails partialUpdatedContactDetails = new ContactDetails();
        partialUpdatedContactDetails.setId(contactDetails.getId());

        partialUpdatedContactDetails.countryCode(UPDATED_COUNTRY_CODE).email(UPDATED_EMAIL).middleName(UPDATED_MIDDLE_NAME);

        restContactDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedContactDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedContactDetails))
            )
            .andExpect(status().isOk());

        // Validate the ContactDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertContactDetailsUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedContactDetails, contactDetails),
            getPersistedContactDetails(contactDetails)
        );
    }

    @Test
    @Transactional
    void fullUpdateContactDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the contactDetails using partial update
        ContactDetails partialUpdatedContactDetails = new ContactDetails();
        partialUpdatedContactDetails.setId(contactDetails.getId());

        partialUpdatedContactDetails
            .countryCode(UPDATED_COUNTRY_CODE)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .name(UPDATED_NAME);

        restContactDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedContactDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedContactDetails))
            )
            .andExpect(status().isOk());

        // Validate the ContactDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertContactDetailsUpdatableFieldsEquals(partialUpdatedContactDetails, getPersistedContactDetails(partialUpdatedContactDetails));
    }

    @Test
    @Transactional
    void patchNonExistingContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, contactDetailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(contactDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(contactDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamContactDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        contactDetails.setId(longCount.incrementAndGet());

        // Create the ContactDetails
        ContactDetailsDTO contactDetailsDTO = contactDetailsMapper.toDto(contactDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restContactDetailsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(contactDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ContactDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteContactDetails() throws Exception {
        // Initialize the database
        insertedContactDetails = contactDetailsRepository.saveAndFlush(contactDetails);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the contactDetails
        restContactDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, contactDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return contactDetailsRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected ContactDetails getPersistedContactDetails(ContactDetails contactDetails) {
        return contactDetailsRepository.findById(contactDetails.getId()).orElseThrow();
    }

    protected void assertPersistedContactDetailsToMatchAllProperties(ContactDetails expectedContactDetails) {
        assertContactDetailsAllPropertiesEquals(expectedContactDetails, getPersistedContactDetails(expectedContactDetails));
    }

    protected void assertPersistedContactDetailsToMatchUpdatableProperties(ContactDetails expectedContactDetails) {
        assertContactDetailsAllUpdatablePropertiesEquals(expectedContactDetails, getPersistedContactDetails(expectedContactDetails));
    }
}
