package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.EmployeesAssetsAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.EmployeesAssets;
import com.mycompany.myapp.repository.EmployeesAssetsRepository;
import com.mycompany.myapp.service.dto.EmployeesAssetsDTO;
import com.mycompany.myapp.service.mapper.EmployeesAssetsMapper;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EmployeesAssetsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EmployeesAssetsResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/employees-assets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private EmployeesAssetsRepository employeesAssetsRepository;

    @Autowired
    private EmployeesAssetsMapper employeesAssetsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEmployeesAssetsMockMvc;

    private EmployeesAssets employeesAssets;

    private EmployeesAssets insertedEmployeesAssets;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmployeesAssets createEntity(EntityManager em) {
        EmployeesAssets employeesAssets = new EmployeesAssets().name(DEFAULT_NAME);
        return employeesAssets;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmployeesAssets createUpdatedEntity(EntityManager em) {
        EmployeesAssets employeesAssets = new EmployeesAssets().name(UPDATED_NAME);
        return employeesAssets;
    }

    @BeforeEach
    public void initTest() {
        employeesAssets = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedEmployeesAssets != null) {
            employeesAssetsRepository.delete(insertedEmployeesAssets);
            insertedEmployeesAssets = null;
        }
    }

    @Test
    @Transactional
    void createEmployeesAssets() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);
        var returnedEmployeesAssetsDTO = om.readValue(
            restEmployeesAssetsMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(employeesAssetsDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            EmployeesAssetsDTO.class
        );

        // Validate the EmployeesAssets in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedEmployeesAssets = employeesAssetsMapper.toEntity(returnedEmployeesAssetsDTO);
        assertEmployeesAssetsUpdatableFieldsEquals(returnedEmployeesAssets, getPersistedEmployeesAssets(returnedEmployeesAssets));

        insertedEmployeesAssets = returnedEmployeesAssets;
    }

    @Test
    @Transactional
    void createEmployeesAssetsWithExistingId() throws Exception {
        // Create the EmployeesAssets with an existing ID
        employeesAssets.setId(1L);
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmployeesAssetsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(employeesAssetsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        employeesAssets.setName(null);

        // Create the EmployeesAssets, which fails.
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        restEmployeesAssetsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(employeesAssetsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEmployeesAssets() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        // Get all the employeesAssetsList
        restEmployeesAssetsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employeesAssets.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getEmployeesAssets() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        // Get the employeesAssets
        restEmployeesAssetsMockMvc
            .perform(get(ENTITY_API_URL_ID, employeesAssets.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(employeesAssets.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingEmployeesAssets() throws Exception {
        // Get the employeesAssets
        restEmployeesAssetsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingEmployeesAssets() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the employeesAssets
        EmployeesAssets updatedEmployeesAssets = employeesAssetsRepository.findById(employeesAssets.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedEmployeesAssets are not directly saved in db
        em.detach(updatedEmployeesAssets);
        updatedEmployeesAssets.name(UPDATED_NAME);
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(updatedEmployeesAssets);

        restEmployeesAssetsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, employeesAssetsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(employeesAssetsDTO))
            )
            .andExpect(status().isOk());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedEmployeesAssetsToMatchAllProperties(updatedEmployeesAssets);
    }

    @Test
    @Transactional
    void putNonExistingEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, employeesAssetsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(employeesAssetsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(employeesAssetsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(employeesAssetsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEmployeesAssetsWithPatch() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the employeesAssets using partial update
        EmployeesAssets partialUpdatedEmployeesAssets = new EmployeesAssets();
        partialUpdatedEmployeesAssets.setId(employeesAssets.getId());

        restEmployeesAssetsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmployeesAssets.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedEmployeesAssets))
            )
            .andExpect(status().isOk());

        // Validate the EmployeesAssets in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertEmployeesAssetsUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedEmployeesAssets, employeesAssets),
            getPersistedEmployeesAssets(employeesAssets)
        );
    }

    @Test
    @Transactional
    void fullUpdateEmployeesAssetsWithPatch() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the employeesAssets using partial update
        EmployeesAssets partialUpdatedEmployeesAssets = new EmployeesAssets();
        partialUpdatedEmployeesAssets.setId(employeesAssets.getId());

        partialUpdatedEmployeesAssets.name(UPDATED_NAME);

        restEmployeesAssetsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmployeesAssets.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedEmployeesAssets))
            )
            .andExpect(status().isOk());

        // Validate the EmployeesAssets in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertEmployeesAssetsUpdatableFieldsEquals(
            partialUpdatedEmployeesAssets,
            getPersistedEmployeesAssets(partialUpdatedEmployeesAssets)
        );
    }

    @Test
    @Transactional
    void patchNonExistingEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, employeesAssetsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(employeesAssetsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(employeesAssetsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEmployeesAssets() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        employeesAssets.setId(longCount.incrementAndGet());

        // Create the EmployeesAssets
        EmployeesAssetsDTO employeesAssetsDTO = employeesAssetsMapper.toDto(employeesAssets);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmployeesAssetsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(employeesAssetsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmployeesAssets in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEmployeesAssets() throws Exception {
        // Initialize the database
        insertedEmployeesAssets = employeesAssetsRepository.saveAndFlush(employeesAssets);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the employeesAssets
        restEmployeesAssetsMockMvc
            .perform(delete(ENTITY_API_URL_ID, employeesAssets.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return employeesAssetsRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected EmployeesAssets getPersistedEmployeesAssets(EmployeesAssets employeesAssets) {
        return employeesAssetsRepository.findById(employeesAssets.getId()).orElseThrow();
    }

    protected void assertPersistedEmployeesAssetsToMatchAllProperties(EmployeesAssets expectedEmployeesAssets) {
        assertEmployeesAssetsAllPropertiesEquals(expectedEmployeesAssets, getPersistedEmployeesAssets(expectedEmployeesAssets));
    }

    protected void assertPersistedEmployeesAssetsToMatchUpdatableProperties(EmployeesAssets expectedEmployeesAssets) {
        assertEmployeesAssetsAllUpdatablePropertiesEquals(expectedEmployeesAssets, getPersistedEmployeesAssets(expectedEmployeesAssets));
    }
}
