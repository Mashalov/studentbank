package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.DocumentCategoryAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.DocumentCategory;
import com.mycompany.myapp.repository.DocumentCategoryRepository;
import com.mycompany.myapp.service.dto.DocumentCategoryDTO;
import com.mycompany.myapp.service.mapper.DocumentCategoryMapper;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DocumentCategoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DocumentCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/document-categories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private DocumentCategoryRepository documentCategoryRepository;

    @Autowired
    private DocumentCategoryMapper documentCategoryMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDocumentCategoryMockMvc;

    private DocumentCategory documentCategory;

    private DocumentCategory insertedDocumentCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DocumentCategory createEntity(EntityManager em) {
        DocumentCategory documentCategory = new DocumentCategory().name(DEFAULT_NAME);
        return documentCategory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DocumentCategory createUpdatedEntity(EntityManager em) {
        DocumentCategory documentCategory = new DocumentCategory().name(UPDATED_NAME);
        return documentCategory;
    }

    @BeforeEach
    public void initTest() {
        documentCategory = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedDocumentCategory != null) {
            documentCategoryRepository.delete(insertedDocumentCategory);
            insertedDocumentCategory = null;
        }
    }

    @Test
    @Transactional
    void createDocumentCategory() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);
        var returnedDocumentCategoryDTO = om.readValue(
            restDocumentCategoryMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(documentCategoryDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            DocumentCategoryDTO.class
        );

        // Validate the DocumentCategory in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedDocumentCategory = documentCategoryMapper.toEntity(returnedDocumentCategoryDTO);
        assertDocumentCategoryUpdatableFieldsEquals(returnedDocumentCategory, getPersistedDocumentCategory(returnedDocumentCategory));

        insertedDocumentCategory = returnedDocumentCategory;
    }

    @Test
    @Transactional
    void createDocumentCategoryWithExistingId() throws Exception {
        // Create the DocumentCategory with an existing ID
        documentCategory.setId(1L);
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentCategoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(documentCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        documentCategory.setName(null);

        // Create the DocumentCategory, which fails.
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        restDocumentCategoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(documentCategoryDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDocumentCategories() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        // Get all the documentCategoryList
        restDocumentCategoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documentCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getDocumentCategory() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        // Get the documentCategory
        restDocumentCategoryMockMvc
            .perform(get(ENTITY_API_URL_ID, documentCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(documentCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingDocumentCategory() throws Exception {
        // Get the documentCategory
        restDocumentCategoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDocumentCategory() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the documentCategory
        DocumentCategory updatedDocumentCategory = documentCategoryRepository.findById(documentCategory.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedDocumentCategory are not directly saved in db
        em.detach(updatedDocumentCategory);
        updatedDocumentCategory.name(UPDATED_NAME);
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(updatedDocumentCategory);

        restDocumentCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, documentCategoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(documentCategoryDTO))
            )
            .andExpect(status().isOk());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedDocumentCategoryToMatchAllProperties(updatedDocumentCategory);
    }

    @Test
    @Transactional
    void putNonExistingDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, documentCategoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(documentCategoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(documentCategoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(documentCategoryDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDocumentCategoryWithPatch() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the documentCategory using partial update
        DocumentCategory partialUpdatedDocumentCategory = new DocumentCategory();
        partialUpdatedDocumentCategory.setId(documentCategory.getId());

        restDocumentCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDocumentCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedDocumentCategory))
            )
            .andExpect(status().isOk());

        // Validate the DocumentCategory in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDocumentCategoryUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedDocumentCategory, documentCategory),
            getPersistedDocumentCategory(documentCategory)
        );
    }

    @Test
    @Transactional
    void fullUpdateDocumentCategoryWithPatch() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the documentCategory using partial update
        DocumentCategory partialUpdatedDocumentCategory = new DocumentCategory();
        partialUpdatedDocumentCategory.setId(documentCategory.getId());

        partialUpdatedDocumentCategory.name(UPDATED_NAME);

        restDocumentCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDocumentCategory.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedDocumentCategory))
            )
            .andExpect(status().isOk());

        // Validate the DocumentCategory in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertDocumentCategoryUpdatableFieldsEquals(
            partialUpdatedDocumentCategory,
            getPersistedDocumentCategory(partialUpdatedDocumentCategory)
        );
    }

    @Test
    @Transactional
    void patchNonExistingDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, documentCategoryDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(documentCategoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(documentCategoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDocumentCategory() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        documentCategory.setId(longCount.incrementAndGet());

        // Create the DocumentCategory
        DocumentCategoryDTO documentCategoryDTO = documentCategoryMapper.toDto(documentCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDocumentCategoryMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(documentCategoryDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DocumentCategory in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDocumentCategory() throws Exception {
        // Initialize the database
        insertedDocumentCategory = documentCategoryRepository.saveAndFlush(documentCategory);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the documentCategory
        restDocumentCategoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, documentCategory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return documentCategoryRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected DocumentCategory getPersistedDocumentCategory(DocumentCategory documentCategory) {
        return documentCategoryRepository.findById(documentCategory.getId()).orElseThrow();
    }

    protected void assertPersistedDocumentCategoryToMatchAllProperties(DocumentCategory expectedDocumentCategory) {
        assertDocumentCategoryAllPropertiesEquals(expectedDocumentCategory, getPersistedDocumentCategory(expectedDocumentCategory));
    }

    protected void assertPersistedDocumentCategoryToMatchUpdatableProperties(DocumentCategory expectedDocumentCategory) {
        assertDocumentCategoryAllUpdatablePropertiesEquals(
            expectedDocumentCategory,
            getPersistedDocumentCategory(expectedDocumentCategory)
        );
    }
}
