package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.PaymentAccountAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static com.mycompany.myapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.PaymentAccount;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.PaymentAccountType;
import com.mycompany.myapp.repository.PaymentAccountRepository;
import com.mycompany.myapp.service.dto.PaymentAccountDTO;
import com.mycompany.myapp.service.mapper.PaymentAccountMapper;
import jakarta.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PaymentAccountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentAccountResourceIT {

    private static final PaymentAccountType DEFAULT_PAYMENT_ACCOUNT_TYPE = PaymentAccountType.SAVING_ACCOUNT;
    private static final PaymentAccountType UPDATED_PAYMENT_ACCOUNT_TYPE = PaymentAccountType.CREDIT_ACCOUNT;

    private static final String DEFAULT_IBAN = "AAAAAAAAAA";
    private static final String UPDATED_IBAN = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

    private static final Currency DEFAULT_CURRENCY = Currency.BGN;
    private static final Currency UPDATED_CURRENCY = Currency.EUR;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final String ENTITY_API_URL = "/api/payment-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private PaymentAccountRepository paymentAccountRepository;

    @Autowired
    private PaymentAccountMapper paymentAccountMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentAccountMockMvc;

    private PaymentAccount paymentAccount;

    private PaymentAccount insertedPaymentAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentAccount createEntity(EntityManager em) {
        PaymentAccount paymentAccount = new PaymentAccount()
            .paymentAccountType(DEFAULT_PAYMENT_ACCOUNT_TYPE)
            .iban(DEFAULT_IBAN)
            .amount(DEFAULT_AMOUNT)
            .currency(DEFAULT_CURRENCY)
            .active(DEFAULT_ACTIVE)
            .deleted(DEFAULT_DELETED);
        return paymentAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PaymentAccount createUpdatedEntity(EntityManager em) {
        PaymentAccount paymentAccount = new PaymentAccount()
            .paymentAccountType(UPDATED_PAYMENT_ACCOUNT_TYPE)
            .iban(UPDATED_IBAN)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .active(UPDATED_ACTIVE)
            .deleted(UPDATED_DELETED);
        return paymentAccount;
    }

    @BeforeEach
    public void initTest() {
        paymentAccount = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedPaymentAccount != null) {
            paymentAccountRepository.delete(insertedPaymentAccount);
            insertedPaymentAccount = null;
        }
    }

    @Test
    @Transactional
    void createPaymentAccount() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);
        var returnedPaymentAccountDTO = om.readValue(
            restPaymentAccountMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            PaymentAccountDTO.class
        );

        // Validate the PaymentAccount in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedPaymentAccount = paymentAccountMapper.toEntity(returnedPaymentAccountDTO);
        assertPaymentAccountUpdatableFieldsEquals(returnedPaymentAccount, getPersistedPaymentAccount(returnedPaymentAccount));

        insertedPaymentAccount = returnedPaymentAccount;
    }

    @Test
    @Transactional
    void createPaymentAccountWithExistingId() throws Exception {
        // Create the PaymentAccount with an existing ID
        paymentAccount.setId(1L);
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPaymentAccountTypeIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        paymentAccount.setPaymentAccountType(null);

        // Create the PaymentAccount, which fails.
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        restPaymentAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIbanIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        paymentAccount.setIban(null);

        // Create the PaymentAccount, which fails.
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        restPaymentAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        paymentAccount.setAmount(null);

        // Create the PaymentAccount, which fails.
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        restPaymentAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCurrencyIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        paymentAccount.setCurrency(null);

        // Create the PaymentAccount, which fails.
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        restPaymentAccountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPaymentAccounts() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        // Get all the paymentAccountList
        restPaymentAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].paymentAccountType").value(hasItem(DEFAULT_PAYMENT_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].iban").value(hasItem(DEFAULT_IBAN)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(sameNumber(DEFAULT_AMOUNT))))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    void getPaymentAccount() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        // Get the paymentAccount
        restPaymentAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, paymentAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(paymentAccount.getId().intValue()))
            .andExpect(jsonPath("$.paymentAccountType").value(DEFAULT_PAYMENT_ACCOUNT_TYPE.toString()))
            .andExpect(jsonPath("$.iban").value(DEFAULT_IBAN))
            .andExpect(jsonPath("$.amount").value(sameNumber(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingPaymentAccount() throws Exception {
        // Get the paymentAccount
        restPaymentAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingPaymentAccount() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the paymentAccount
        PaymentAccount updatedPaymentAccount = paymentAccountRepository.findById(paymentAccount.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedPaymentAccount are not directly saved in db
        em.detach(updatedPaymentAccount);
        updatedPaymentAccount
            .paymentAccountType(UPDATED_PAYMENT_ACCOUNT_TYPE)
            .iban(UPDATED_IBAN)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .active(UPDATED_ACTIVE)
            .deleted(UPDATED_DELETED);
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(updatedPaymentAccount);

        restPaymentAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(paymentAccountDTO))
            )
            .andExpect(status().isOk());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedPaymentAccountToMatchAllProperties(updatedPaymentAccount);
    }

    @Test
    @Transactional
    void putNonExistingPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(paymentAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(paymentAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentAccountWithPatch() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the paymentAccount using partial update
        PaymentAccount partialUpdatedPaymentAccount = new PaymentAccount();
        partialUpdatedPaymentAccount.setId(paymentAccount.getId());

        partialUpdatedPaymentAccount
            .paymentAccountType(UPDATED_PAYMENT_ACCOUNT_TYPE)
            .iban(UPDATED_IBAN)
            .currency(UPDATED_CURRENCY)
            .active(UPDATED_ACTIVE)
            .deleted(UPDATED_DELETED);

        restPaymentAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedPaymentAccount))
            )
            .andExpect(status().isOk());

        // Validate the PaymentAccount in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPaymentAccountUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedPaymentAccount, paymentAccount),
            getPersistedPaymentAccount(paymentAccount)
        );
    }

    @Test
    @Transactional
    void fullUpdatePaymentAccountWithPatch() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the paymentAccount using partial update
        PaymentAccount partialUpdatedPaymentAccount = new PaymentAccount();
        partialUpdatedPaymentAccount.setId(paymentAccount.getId());

        partialUpdatedPaymentAccount
            .paymentAccountType(UPDATED_PAYMENT_ACCOUNT_TYPE)
            .iban(UPDATED_IBAN)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .active(UPDATED_ACTIVE)
            .deleted(UPDATED_DELETED);

        restPaymentAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedPaymentAccount))
            )
            .andExpect(status().isOk());

        // Validate the PaymentAccount in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPaymentAccountUpdatableFieldsEquals(partialUpdatedPaymentAccount, getPersistedPaymentAccount(partialUpdatedPaymentAccount));
    }

    @Test
    @Transactional
    void patchNonExistingPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentAccountDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(paymentAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(paymentAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPaymentAccount() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        paymentAccount.setId(longCount.incrementAndGet());

        // Create the PaymentAccount
        PaymentAccountDTO paymentAccountDTO = paymentAccountMapper.toDto(paymentAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentAccountMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(paymentAccountDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentAccount in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePaymentAccount() throws Exception {
        // Initialize the database
        insertedPaymentAccount = paymentAccountRepository.saveAndFlush(paymentAccount);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the paymentAccount
        restPaymentAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, paymentAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return paymentAccountRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected PaymentAccount getPersistedPaymentAccount(PaymentAccount paymentAccount) {
        return paymentAccountRepository.findById(paymentAccount.getId()).orElseThrow();
    }

    protected void assertPersistedPaymentAccountToMatchAllProperties(PaymentAccount expectedPaymentAccount) {
        assertPaymentAccountAllPropertiesEquals(expectedPaymentAccount, getPersistedPaymentAccount(expectedPaymentAccount));
    }

    protected void assertPersistedPaymentAccountToMatchUpdatableProperties(PaymentAccount expectedPaymentAccount) {
        assertPaymentAccountAllUpdatablePropertiesEquals(expectedPaymentAccount, getPersistedPaymentAccount(expectedPaymentAccount));
    }
}
