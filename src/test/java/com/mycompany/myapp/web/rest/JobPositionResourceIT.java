package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.JobPositionAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.JobPosition;
import com.mycompany.myapp.repository.JobPositionRepository;
import com.mycompany.myapp.service.dto.JobPositionDTO;
import com.mycompany.myapp.service.mapper.JobPositionMapper;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link JobPositionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class JobPositionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/job-positions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private JobPositionRepository jobPositionRepository;

    @Autowired
    private JobPositionMapper jobPositionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJobPositionMockMvc;

    private JobPosition jobPosition;

    private JobPosition insertedJobPosition;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JobPosition createEntity(EntityManager em) {
        JobPosition jobPosition = new JobPosition().name(DEFAULT_NAME).company(DEFAULT_COMPANY);
        return jobPosition;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JobPosition createUpdatedEntity(EntityManager em) {
        JobPosition jobPosition = new JobPosition().name(UPDATED_NAME).company(UPDATED_COMPANY);
        return jobPosition;
    }

    @BeforeEach
    public void initTest() {
        jobPosition = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedJobPosition != null) {
            jobPositionRepository.delete(insertedJobPosition);
            insertedJobPosition = null;
        }
    }

    @Test
    @Transactional
    void createJobPosition() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);
        var returnedJobPositionDTO = om.readValue(
            restJobPositionMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(jobPositionDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            JobPositionDTO.class
        );

        // Validate the JobPosition in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedJobPosition = jobPositionMapper.toEntity(returnedJobPositionDTO);
        assertJobPositionUpdatableFieldsEquals(returnedJobPosition, getPersistedJobPosition(returnedJobPosition));

        insertedJobPosition = returnedJobPosition;
    }

    @Test
    @Transactional
    void createJobPositionWithExistingId() throws Exception {
        // Create the JobPosition with an existing ID
        jobPosition.setId(1L);
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobPositionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(jobPositionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        jobPosition.setName(null);

        // Create the JobPosition, which fails.
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        restJobPositionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(jobPositionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCompanyIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        jobPosition.setCompany(null);

        // Create the JobPosition, which fails.
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        restJobPositionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(jobPositionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllJobPositions() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        // Get all the jobPositionList
        restJobPositionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jobPosition.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)));
    }

    @Test
    @Transactional
    void getJobPosition() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        // Get the jobPosition
        restJobPositionMockMvc
            .perform(get(ENTITY_API_URL_ID, jobPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(jobPosition.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY));
    }

    @Test
    @Transactional
    void getNonExistingJobPosition() throws Exception {
        // Get the jobPosition
        restJobPositionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingJobPosition() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the jobPosition
        JobPosition updatedJobPosition = jobPositionRepository.findById(jobPosition.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedJobPosition are not directly saved in db
        em.detach(updatedJobPosition);
        updatedJobPosition.name(UPDATED_NAME).company(UPDATED_COMPANY);
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(updatedJobPosition);

        restJobPositionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, jobPositionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(jobPositionDTO))
            )
            .andExpect(status().isOk());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedJobPositionToMatchAllProperties(updatedJobPosition);
    }

    @Test
    @Transactional
    void putNonExistingJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, jobPositionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(jobPositionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(jobPositionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(jobPositionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateJobPositionWithPatch() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the jobPosition using partial update
        JobPosition partialUpdatedJobPosition = new JobPosition();
        partialUpdatedJobPosition.setId(jobPosition.getId());

        partialUpdatedJobPosition.name(UPDATED_NAME).company(UPDATED_COMPANY);

        restJobPositionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedJobPosition.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedJobPosition))
            )
            .andExpect(status().isOk());

        // Validate the JobPosition in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertJobPositionUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedJobPosition, jobPosition),
            getPersistedJobPosition(jobPosition)
        );
    }

    @Test
    @Transactional
    void fullUpdateJobPositionWithPatch() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the jobPosition using partial update
        JobPosition partialUpdatedJobPosition = new JobPosition();
        partialUpdatedJobPosition.setId(jobPosition.getId());

        partialUpdatedJobPosition.name(UPDATED_NAME).company(UPDATED_COMPANY);

        restJobPositionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedJobPosition.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedJobPosition))
            )
            .andExpect(status().isOk());

        // Validate the JobPosition in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertJobPositionUpdatableFieldsEquals(partialUpdatedJobPosition, getPersistedJobPosition(partialUpdatedJobPosition));
    }

    @Test
    @Transactional
    void patchNonExistingJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, jobPositionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(jobPositionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(jobPositionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamJobPosition() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        jobPosition.setId(longCount.incrementAndGet());

        // Create the JobPosition
        JobPositionDTO jobPositionDTO = jobPositionMapper.toDto(jobPosition);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJobPositionMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(jobPositionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the JobPosition in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteJobPosition() throws Exception {
        // Initialize the database
        insertedJobPosition = jobPositionRepository.saveAndFlush(jobPosition);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the jobPosition
        restJobPositionMockMvc
            .perform(delete(ENTITY_API_URL_ID, jobPosition.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return jobPositionRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected JobPosition getPersistedJobPosition(JobPosition jobPosition) {
        return jobPositionRepository.findById(jobPosition.getId()).orElseThrow();
    }

    protected void assertPersistedJobPositionToMatchAllProperties(JobPosition expectedJobPosition) {
        assertJobPositionAllPropertiesEquals(expectedJobPosition, getPersistedJobPosition(expectedJobPosition));
    }

    protected void assertPersistedJobPositionToMatchUpdatableProperties(JobPosition expectedJobPosition) {
        assertJobPositionAllUpdatablePropertiesEquals(expectedJobPosition, getPersistedJobPosition(expectedJobPosition));
    }
}
