package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.BankDetailsAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.BankDetails;
import com.mycompany.myapp.repository.BankDetailsRepository;
import com.mycompany.myapp.service.dto.BankDetailsDTO;
import com.mycompany.myapp.service.mapper.BankDetailsMapper;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BankDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BankDetailsResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FOUNDATION_YEAR = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FOUNDATION_YEAR = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CEO = "AAAAAAAAAA";
    private static final String UPDATED_CEO = "BBBBBBBBBB";

    private static final String DEFAULT_MAIN_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_WEBSITE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/bank-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private BankDetailsRepository bankDetailsRepository;

    @Autowired
    private BankDetailsMapper bankDetailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBankDetailsMockMvc;

    private BankDetails bankDetails;

    private BankDetails insertedBankDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankDetails createEntity(EntityManager em) {
        BankDetails bankDetails = new BankDetails()
            .name(DEFAULT_NAME)
            .foundationYear(DEFAULT_FOUNDATION_YEAR)
            .ceo(DEFAULT_CEO)
            .mainWebsite(DEFAULT_MAIN_WEBSITE);
        return bankDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankDetails createUpdatedEntity(EntityManager em) {
        BankDetails bankDetails = new BankDetails()
            .name(UPDATED_NAME)
            .foundationYear(UPDATED_FOUNDATION_YEAR)
            .ceo(UPDATED_CEO)
            .mainWebsite(UPDATED_MAIN_WEBSITE);
        return bankDetails;
    }

    @BeforeEach
    public void initTest() {
        bankDetails = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedBankDetails != null) {
            bankDetailsRepository.delete(insertedBankDetails);
            insertedBankDetails = null;
        }
    }

    @Test
    @Transactional
    void createBankDetails() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);
        var returnedBankDetailsDTO = om.readValue(
            restBankDetailsMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(bankDetailsDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            BankDetailsDTO.class
        );

        // Validate the BankDetails in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedBankDetails = bankDetailsMapper.toEntity(returnedBankDetailsDTO);
        assertBankDetailsUpdatableFieldsEquals(returnedBankDetails, getPersistedBankDetails(returnedBankDetails));

        insertedBankDetails = returnedBankDetails;
    }

    @Test
    @Transactional
    void createBankDetailsWithExistingId() throws Exception {
        // Create the BankDetails with an existing ID
        bankDetails.setId(1L);
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(bankDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        bankDetails.setName(null);

        // Create the BankDetails, which fails.
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        restBankDetailsMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(bankDetailsDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBankDetails() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        // Get all the bankDetailsList
        restBankDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].foundationYear").value(hasItem(sameInstant(DEFAULT_FOUNDATION_YEAR))))
            .andExpect(jsonPath("$.[*].ceo").value(hasItem(DEFAULT_CEO)))
            .andExpect(jsonPath("$.[*].mainWebsite").value(hasItem(DEFAULT_MAIN_WEBSITE)));
    }

    @Test
    @Transactional
    void getBankDetails() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        // Get the bankDetails
        restBankDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, bankDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bankDetails.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.foundationYear").value(sameInstant(DEFAULT_FOUNDATION_YEAR)))
            .andExpect(jsonPath("$.ceo").value(DEFAULT_CEO))
            .andExpect(jsonPath("$.mainWebsite").value(DEFAULT_MAIN_WEBSITE));
    }

    @Test
    @Transactional
    void getNonExistingBankDetails() throws Exception {
        // Get the bankDetails
        restBankDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBankDetails() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the bankDetails
        BankDetails updatedBankDetails = bankDetailsRepository.findById(bankDetails.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedBankDetails are not directly saved in db
        em.detach(updatedBankDetails);
        updatedBankDetails.name(UPDATED_NAME).foundationYear(UPDATED_FOUNDATION_YEAR).ceo(UPDATED_CEO).mainWebsite(UPDATED_MAIN_WEBSITE);
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(updatedBankDetails);

        restBankDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bankDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(bankDetailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedBankDetailsToMatchAllProperties(updatedBankDetails);
    }

    @Test
    @Transactional
    void putNonExistingBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bankDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(bankDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(bankDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(bankDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBankDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the bankDetails using partial update
        BankDetails partialUpdatedBankDetails = new BankDetails();
        partialUpdatedBankDetails.setId(bankDetails.getId());

        partialUpdatedBankDetails.foundationYear(UPDATED_FOUNDATION_YEAR).ceo(UPDATED_CEO);

        restBankDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBankDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedBankDetails))
            )
            .andExpect(status().isOk());

        // Validate the BankDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertBankDetailsUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedBankDetails, bankDetails),
            getPersistedBankDetails(bankDetails)
        );
    }

    @Test
    @Transactional
    void fullUpdateBankDetailsWithPatch() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the bankDetails using partial update
        BankDetails partialUpdatedBankDetails = new BankDetails();
        partialUpdatedBankDetails.setId(bankDetails.getId());

        partialUpdatedBankDetails
            .name(UPDATED_NAME)
            .foundationYear(UPDATED_FOUNDATION_YEAR)
            .ceo(UPDATED_CEO)
            .mainWebsite(UPDATED_MAIN_WEBSITE);

        restBankDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBankDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedBankDetails))
            )
            .andExpect(status().isOk());

        // Validate the BankDetails in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertBankDetailsUpdatableFieldsEquals(partialUpdatedBankDetails, getPersistedBankDetails(partialUpdatedBankDetails));
    }

    @Test
    @Transactional
    void patchNonExistingBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, bankDetailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(bankDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(bankDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBankDetails() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        bankDetails.setId(longCount.incrementAndGet());

        // Create the BankDetails
        BankDetailsDTO bankDetailsDTO = bankDetailsMapper.toDto(bankDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBankDetailsMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(bankDetailsDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BankDetails in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBankDetails() throws Exception {
        // Initialize the database
        insertedBankDetails = bankDetailsRepository.saveAndFlush(bankDetails);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the bankDetails
        restBankDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, bankDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return bankDetailsRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected BankDetails getPersistedBankDetails(BankDetails bankDetails) {
        return bankDetailsRepository.findById(bankDetails.getId()).orElseThrow();
    }

    protected void assertPersistedBankDetailsToMatchAllProperties(BankDetails expectedBankDetails) {
        assertBankDetailsAllPropertiesEquals(expectedBankDetails, getPersistedBankDetails(expectedBankDetails));
    }

    protected void assertPersistedBankDetailsToMatchUpdatableProperties(BankDetails expectedBankDetails) {
        assertBankDetailsAllUpdatablePropertiesEquals(expectedBankDetails, getPersistedBankDetails(expectedBankDetails));
    }
}
