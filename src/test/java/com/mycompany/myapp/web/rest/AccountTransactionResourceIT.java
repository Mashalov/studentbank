package com.mycompany.myapp.web.rest;

import static com.mycompany.myapp.domain.AccountTransactionAsserts.*;
import static com.mycompany.myapp.web.rest.TestUtil.createUpdateProxyForBean;
import static com.mycompany.myapp.web.rest.TestUtil.sameInstant;
import static com.mycompany.myapp.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.AccountTransaction;
import com.mycompany.myapp.domain.enumeration.Currency;
import com.mycompany.myapp.domain.enumeration.TransactionLocation;
import com.mycompany.myapp.domain.enumeration.TransactionOperationType;
import com.mycompany.myapp.repository.AccountTransactionRepository;
import com.mycompany.myapp.service.dto.AccountTransactionDTO;
import com.mycompany.myapp.service.mapper.AccountTransactionMapper;
import jakarta.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AccountTransactionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AccountTransactionResourceIT {

    private static final String DEFAULT_EXTERNAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_ID = "BBBBBBBBBB";

    private static final TransactionOperationType DEFAULT_TRANSACTION_OPERATION_TYPE = TransactionOperationType.TRANSFER;
    private static final TransactionOperationType UPDATED_TRANSACTION_OPERATION_TYPE = TransactionOperationType.CREDIT;

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

    private static final Currency DEFAULT_CURRENCY = Currency.BGN;
    private static final Currency UPDATED_CURRENCY = Currency.EUR;

    private static final TransactionLocation DEFAULT_LOCATION = TransactionLocation.ATM;
    private static final TransactionLocation UPDATED_LOCATION = TransactionLocation.OFFICE;

    private static final ZonedDateTime DEFAULT_SEND_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SEND_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_RECEIVE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_RECEIVE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/account-transactions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private AccountTransactionRepository accountTransactionRepository;

    @Autowired
    private AccountTransactionMapper accountTransactionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAccountTransactionMockMvc;

    private AccountTransaction accountTransaction;

    private AccountTransaction insertedAccountTransaction;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountTransaction createEntity(EntityManager em) {
        AccountTransaction accountTransaction = new AccountTransaction()
            .externalId(DEFAULT_EXTERNAL_ID)
            .transactionOperationType(DEFAULT_TRANSACTION_OPERATION_TYPE)
            .amount(DEFAULT_AMOUNT)
            .currency(DEFAULT_CURRENCY)
            .location(DEFAULT_LOCATION)
            .sendTime(DEFAULT_SEND_TIME)
            .receiveTime(DEFAULT_RECEIVE_TIME);
        return accountTransaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountTransaction createUpdatedEntity(EntityManager em) {
        AccountTransaction accountTransaction = new AccountTransaction()
            .externalId(UPDATED_EXTERNAL_ID)
            .transactionOperationType(UPDATED_TRANSACTION_OPERATION_TYPE)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .location(UPDATED_LOCATION)
            .sendTime(UPDATED_SEND_TIME)
            .receiveTime(UPDATED_RECEIVE_TIME);
        return accountTransaction;
    }

    @BeforeEach
    public void initTest() {
        accountTransaction = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedAccountTransaction != null) {
            accountTransactionRepository.delete(insertedAccountTransaction);
            insertedAccountTransaction = null;
        }
    }

    @Test
    @Transactional
    void createAccountTransaction() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);
        var returnedAccountTransactionDTO = om.readValue(
            restAccountTransactionMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            AccountTransactionDTO.class
        );

        // Validate the AccountTransaction in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        var returnedAccountTransaction = accountTransactionMapper.toEntity(returnedAccountTransactionDTO);
        assertAccountTransactionUpdatableFieldsEquals(
            returnedAccountTransaction,
            getPersistedAccountTransaction(returnedAccountTransaction)
        );

        insertedAccountTransaction = returnedAccountTransaction;
    }

    @Test
    @Transactional
    void createAccountTransactionWithExistingId() throws Exception {
        // Create the AccountTransaction with an existing ID
        accountTransaction.setId(1L);
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccountTransactionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTransactionOperationTypeIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        accountTransaction.setTransactionOperationType(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        accountTransaction.setAmount(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCurrencyIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        accountTransaction.setCurrency(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLocationIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        accountTransaction.setLocation(null);

        // Create the AccountTransaction, which fails.
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        restAccountTransactionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllAccountTransactions() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        // Get all the accountTransactionList
        restAccountTransactionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(DEFAULT_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].transactionOperationType").value(hasItem(DEFAULT_TRANSACTION_OPERATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(sameNumber(DEFAULT_AMOUNT))))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].sendTime").value(hasItem(sameInstant(DEFAULT_SEND_TIME))))
            .andExpect(jsonPath("$.[*].receiveTime").value(hasItem(sameInstant(DEFAULT_RECEIVE_TIME))));
    }

    @Test
    @Transactional
    void getAccountTransaction() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        // Get the accountTransaction
        restAccountTransactionMockMvc
            .perform(get(ENTITY_API_URL_ID, accountTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(accountTransaction.getId().intValue()))
            .andExpect(jsonPath("$.externalId").value(DEFAULT_EXTERNAL_ID))
            .andExpect(jsonPath("$.transactionOperationType").value(DEFAULT_TRANSACTION_OPERATION_TYPE.toString()))
            .andExpect(jsonPath("$.amount").value(sameNumber(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.sendTime").value(sameInstant(DEFAULT_SEND_TIME)))
            .andExpect(jsonPath("$.receiveTime").value(sameInstant(DEFAULT_RECEIVE_TIME)));
    }

    @Test
    @Transactional
    void getNonExistingAccountTransaction() throws Exception {
        // Get the accountTransaction
        restAccountTransactionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingAccountTransaction() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the accountTransaction
        AccountTransaction updatedAccountTransaction = accountTransactionRepository.findById(accountTransaction.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedAccountTransaction are not directly saved in db
        em.detach(updatedAccountTransaction);
        updatedAccountTransaction
            .externalId(UPDATED_EXTERNAL_ID)
            .transactionOperationType(UPDATED_TRANSACTION_OPERATION_TYPE)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .location(UPDATED_LOCATION)
            .sendTime(UPDATED_SEND_TIME)
            .receiveTime(UPDATED_RECEIVE_TIME);
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(updatedAccountTransaction);

        restAccountTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, accountTransactionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(accountTransactionDTO))
            )
            .andExpect(status().isOk());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedAccountTransactionToMatchAllProperties(updatedAccountTransaction);
    }

    @Test
    @Transactional
    void putNonExistingAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, accountTransactionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(accountTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(accountTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAccountTransactionWithPatch() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the accountTransaction using partial update
        AccountTransaction partialUpdatedAccountTransaction = new AccountTransaction();
        partialUpdatedAccountTransaction.setId(accountTransaction.getId());

        partialUpdatedAccountTransaction.externalId(UPDATED_EXTERNAL_ID).currency(UPDATED_CURRENCY);

        restAccountTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedAccountTransaction))
            )
            .andExpect(status().isOk());

        // Validate the AccountTransaction in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAccountTransactionUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedAccountTransaction, accountTransaction),
            getPersistedAccountTransaction(accountTransaction)
        );
    }

    @Test
    @Transactional
    void fullUpdateAccountTransactionWithPatch() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the accountTransaction using partial update
        AccountTransaction partialUpdatedAccountTransaction = new AccountTransaction();
        partialUpdatedAccountTransaction.setId(accountTransaction.getId());

        partialUpdatedAccountTransaction
            .externalId(UPDATED_EXTERNAL_ID)
            .transactionOperationType(UPDATED_TRANSACTION_OPERATION_TYPE)
            .amount(UPDATED_AMOUNT)
            .currency(UPDATED_CURRENCY)
            .location(UPDATED_LOCATION)
            .sendTime(UPDATED_SEND_TIME)
            .receiveTime(UPDATED_RECEIVE_TIME);

        restAccountTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAccountTransaction.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedAccountTransaction))
            )
            .andExpect(status().isOk());

        // Validate the AccountTransaction in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAccountTransactionUpdatableFieldsEquals(
            partialUpdatedAccountTransaction,
            getPersistedAccountTransaction(partialUpdatedAccountTransaction)
        );
    }

    @Test
    @Transactional
    void patchNonExistingAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, accountTransactionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(accountTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(accountTransactionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAccountTransaction() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        accountTransaction.setId(longCount.incrementAndGet());

        // Create the AccountTransaction
        AccountTransactionDTO accountTransactionDTO = accountTransactionMapper.toDto(accountTransaction);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAccountTransactionMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(accountTransactionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AccountTransaction in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAccountTransaction() throws Exception {
        // Initialize the database
        insertedAccountTransaction = accountTransactionRepository.saveAndFlush(accountTransaction);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the accountTransaction
        restAccountTransactionMockMvc
            .perform(delete(ENTITY_API_URL_ID, accountTransaction.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return accountTransactionRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected AccountTransaction getPersistedAccountTransaction(AccountTransaction accountTransaction) {
        return accountTransactionRepository.findById(accountTransaction.getId()).orElseThrow();
    }

    protected void assertPersistedAccountTransactionToMatchAllProperties(AccountTransaction expectedAccountTransaction) {
        assertAccountTransactionAllPropertiesEquals(expectedAccountTransaction, getPersistedAccountTransaction(expectedAccountTransaction));
    }

    protected void assertPersistedAccountTransactionToMatchUpdatableProperties(AccountTransaction expectedAccountTransaction) {
        assertAccountTransactionAllUpdatablePropertiesEquals(
            expectedAccountTransaction,
            getPersistedAccountTransaction(expectedAccountTransaction)
        );
    }
}
