package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.CardDetailsTestSamples.*;
import static com.mycompany.myapp.domain.CustomerTestSamples.*;
import static com.mycompany.myapp.domain.PaymentAccountTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CardDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardDetails.class);
        CardDetails cardDetails1 = getCardDetailsSample1();
        CardDetails cardDetails2 = new CardDetails();
        assertThat(cardDetails1).isNotEqualTo(cardDetails2);

        cardDetails2.setId(cardDetails1.getId());
        assertThat(cardDetails1).isEqualTo(cardDetails2);

        cardDetails2 = getCardDetailsSample2();
        assertThat(cardDetails1).isNotEqualTo(cardDetails2);
    }

    @Test
    void customerTest() {
        CardDetails cardDetails = getCardDetailsRandomSampleGenerator();
        Customer customerBack = getCustomerRandomSampleGenerator();

        cardDetails.setCustomer(customerBack);
        assertThat(cardDetails.getCustomer()).isEqualTo(customerBack);

        cardDetails.customer(null);
        assertThat(cardDetails.getCustomer()).isNull();
    }

    @Test
    void paymentAccountTest() {
        CardDetails cardDetails = getCardDetailsRandomSampleGenerator();
        PaymentAccount paymentAccountBack = getPaymentAccountRandomSampleGenerator();

        cardDetails.setPaymentAccount(paymentAccountBack);
        assertThat(cardDetails.getPaymentAccount()).isEqualTo(paymentAccountBack);

        cardDetails.paymentAccount(null);
        assertThat(cardDetails.getPaymentAccount()).isNull();
    }
}
