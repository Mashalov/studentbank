package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class DocumentTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Document getDocumentSample1() {
        return new Document().id(1L).name("name1").documentExtension("documentExtension1").documentCompression("documentCompression1");
    }

    public static Document getDocumentSample2() {
        return new Document().id(2L).name("name2").documentExtension("documentExtension2").documentCompression("documentCompression2");
    }

    public static Document getDocumentRandomSampleGenerator() {
        return new Document()
            .id(longCount.incrementAndGet())
            .name(UUID.randomUUID().toString())
            .documentExtension(UUID.randomUUID().toString())
            .documentCompression(UUID.randomUUID().toString());
    }
}
