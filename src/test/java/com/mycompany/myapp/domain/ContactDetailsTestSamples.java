package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class ContactDetailsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static ContactDetails getContactDetailsSample1() {
        return new ContactDetails()
            .id(1L)
            .countryCode(1L)
            .phoneNumber(1L)
            .email("email1")
            .firstName("firstName1")
            .lastName("lastName1")
            .middleName("middleName1")
            .name("name1");
    }

    public static ContactDetails getContactDetailsSample2() {
        return new ContactDetails()
            .id(2L)
            .countryCode(2L)
            .phoneNumber(2L)
            .email("email2")
            .firstName("firstName2")
            .lastName("lastName2")
            .middleName("middleName2")
            .name("name2");
    }

    public static ContactDetails getContactDetailsRandomSampleGenerator() {
        return new ContactDetails()
            .id(longCount.incrementAndGet())
            .countryCode(longCount.incrementAndGet())
            .phoneNumber(longCount.incrementAndGet())
            .email(UUID.randomUUID().toString())
            .firstName(UUID.randomUUID().toString())
            .lastName(UUID.randomUUID().toString())
            .middleName(UUID.randomUUID().toString())
            .name(UUID.randomUUID().toString());
    }
}
