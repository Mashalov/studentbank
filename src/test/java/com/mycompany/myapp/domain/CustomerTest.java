package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AddressTestSamples.*;
import static com.mycompany.myapp.domain.ApplicationTestSamples.*;
import static com.mycompany.myapp.domain.ContactDetailsTestSamples.*;
import static com.mycompany.myapp.domain.CustomerTestSamples.*;
import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static com.mycompany.myapp.domain.PaymentAccountTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Customer.class);
        Customer customer1 = getCustomerSample1();
        Customer customer2 = new Customer();
        assertThat(customer1).isNotEqualTo(customer2);

        customer2.setId(customer1.getId());
        assertThat(customer1).isEqualTo(customer2);

        customer2 = getCustomerSample2();
        assertThat(customer1).isNotEqualTo(customer2);
    }

    @Test
    void addressesTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        Address addressBack = getAddressRandomSampleGenerator();

        customer.addAddresses(addressBack);
        assertThat(customer.getAddresses()).containsOnly(addressBack);
        assertThat(addressBack.getCustomer()).isEqualTo(customer);

        customer.removeAddresses(addressBack);
        assertThat(customer.getAddresses()).doesNotContain(addressBack);
        assertThat(addressBack.getCustomer()).isNull();

        customer.addresses(new HashSet<>(Set.of(addressBack)));
        assertThat(customer.getAddresses()).containsOnly(addressBack);
        assertThat(addressBack.getCustomer()).isEqualTo(customer);

        customer.setAddresses(new HashSet<>());
        assertThat(customer.getAddresses()).doesNotContain(addressBack);
        assertThat(addressBack.getCustomer()).isNull();
    }

    @Test
    void paymentAccountsTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        PaymentAccount paymentAccountBack = getPaymentAccountRandomSampleGenerator();

        customer.addPaymentAccounts(paymentAccountBack);
        assertThat(customer.getPaymentAccounts()).containsOnly(paymentAccountBack);
        assertThat(paymentAccountBack.getCustomer()).isEqualTo(customer);

        customer.removePaymentAccounts(paymentAccountBack);
        assertThat(customer.getPaymentAccounts()).doesNotContain(paymentAccountBack);
        assertThat(paymentAccountBack.getCustomer()).isNull();

        customer.paymentAccounts(new HashSet<>(Set.of(paymentAccountBack)));
        assertThat(customer.getPaymentAccounts()).containsOnly(paymentAccountBack);
        assertThat(paymentAccountBack.getCustomer()).isEqualTo(customer);

        customer.setPaymentAccounts(new HashSet<>());
        assertThat(customer.getPaymentAccounts()).doesNotContain(paymentAccountBack);
        assertThat(paymentAccountBack.getCustomer()).isNull();
    }

    @Test
    void applicationsTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        Application applicationBack = getApplicationRandomSampleGenerator();

        customer.addApplications(applicationBack);
        assertThat(customer.getApplications()).containsOnly(applicationBack);
        assertThat(applicationBack.getCustomer()).isEqualTo(customer);

        customer.removeApplications(applicationBack);
        assertThat(customer.getApplications()).doesNotContain(applicationBack);
        assertThat(applicationBack.getCustomer()).isNull();

        customer.applications(new HashSet<>(Set.of(applicationBack)));
        assertThat(customer.getApplications()).containsOnly(applicationBack);
        assertThat(applicationBack.getCustomer()).isEqualTo(customer);

        customer.setApplications(new HashSet<>());
        assertThat(customer.getApplications()).doesNotContain(applicationBack);
        assertThat(applicationBack.getCustomer()).isNull();
    }

    @Test
    void documentsTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        customer.addDocuments(documentBack);
        assertThat(customer.getDocuments()).containsOnly(documentBack);
        assertThat(documentBack.getCustomer()).isEqualTo(customer);

        customer.removeDocuments(documentBack);
        assertThat(customer.getDocuments()).doesNotContain(documentBack);
        assertThat(documentBack.getCustomer()).isNull();

        customer.documents(new HashSet<>(Set.of(documentBack)));
        assertThat(customer.getDocuments()).containsOnly(documentBack);
        assertThat(documentBack.getCustomer()).isEqualTo(customer);

        customer.setDocuments(new HashSet<>());
        assertThat(customer.getDocuments()).doesNotContain(documentBack);
        assertThat(documentBack.getCustomer()).isNull();
    }

    @Test
    void contactDetailsTest() {
        Customer customer = getCustomerRandomSampleGenerator();
        ContactDetails contactDetailsBack = getContactDetailsRandomSampleGenerator();

        customer.setContactDetails(contactDetailsBack);
        assertThat(customer.getContactDetails()).isEqualTo(contactDetailsBack);

        customer.contactDetails(null);
        assertThat(customer.getContactDetails()).isNull();
    }
}
