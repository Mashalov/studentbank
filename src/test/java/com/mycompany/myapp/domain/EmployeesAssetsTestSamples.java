package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class EmployeesAssetsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static EmployeesAssets getEmployeesAssetsSample1() {
        return new EmployeesAssets().id(1L).name("name1");
    }

    public static EmployeesAssets getEmployeesAssetsSample2() {
        return new EmployeesAssets().id(2L).name("name2");
    }

    public static EmployeesAssets getEmployeesAssetsRandomSampleGenerator() {
        return new EmployeesAssets().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString());
    }
}
