package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AccountTransactionTestSamples.*;
import static com.mycompany.myapp.domain.PaymentAccountTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AccountTransactionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountTransaction.class);
        AccountTransaction accountTransaction1 = getAccountTransactionSample1();
        AccountTransaction accountTransaction2 = new AccountTransaction();
        assertThat(accountTransaction1).isNotEqualTo(accountTransaction2);

        accountTransaction2.setId(accountTransaction1.getId());
        assertThat(accountTransaction1).isEqualTo(accountTransaction2);

        accountTransaction2 = getAccountTransactionSample2();
        assertThat(accountTransaction1).isNotEqualTo(accountTransaction2);
    }

    @Test
    void senderTest() {
        AccountTransaction accountTransaction = getAccountTransactionRandomSampleGenerator();
        PaymentAccount paymentAccountBack = getPaymentAccountRandomSampleGenerator();

        accountTransaction.setSender(paymentAccountBack);
        assertThat(accountTransaction.getSender()).isEqualTo(paymentAccountBack);

        accountTransaction.sender(null);
        assertThat(accountTransaction.getSender()).isNull();
    }

    @Test
    void receiverTest() {
        AccountTransaction accountTransaction = getAccountTransactionRandomSampleGenerator();
        PaymentAccount paymentAccountBack = getPaymentAccountRandomSampleGenerator();

        accountTransaction.setReceiver(paymentAccountBack);
        assertThat(accountTransaction.getReceiver()).isEqualTo(paymentAccountBack);

        accountTransaction.receiver(null);
        assertThat(accountTransaction.getReceiver()).isNull();
    }
}
