package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.CustomerTestSamples.*;
import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static com.mycompany.myapp.domain.PaymentAccountTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PaymentAccountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentAccount.class);
        PaymentAccount paymentAccount1 = getPaymentAccountSample1();
        PaymentAccount paymentAccount2 = new PaymentAccount();
        assertThat(paymentAccount1).isNotEqualTo(paymentAccount2);

        paymentAccount2.setId(paymentAccount1.getId());
        assertThat(paymentAccount1).isEqualTo(paymentAccount2);

        paymentAccount2 = getPaymentAccountSample2();
        assertThat(paymentAccount1).isNotEqualTo(paymentAccount2);
    }

    @Test
    void contractTest() {
        PaymentAccount paymentAccount = getPaymentAccountRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        paymentAccount.setContract(documentBack);
        assertThat(paymentAccount.getContract()).isEqualTo(documentBack);

        paymentAccount.contract(null);
        assertThat(paymentAccount.getContract()).isNull();
    }

    @Test
    void customerTest() {
        PaymentAccount paymentAccount = getPaymentAccountRandomSampleGenerator();
        Customer customerBack = getCustomerRandomSampleGenerator();

        paymentAccount.setCustomer(customerBack);
        assertThat(paymentAccount.getCustomer()).isEqualTo(customerBack);

        paymentAccount.customer(null);
        assertThat(paymentAccount.getCustomer()).isNull();
    }
}
