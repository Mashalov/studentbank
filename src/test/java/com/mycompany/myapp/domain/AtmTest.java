package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AddressTestSamples.*;
import static com.mycompany.myapp.domain.AtmTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class AtmTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Atm.class);
        Atm atm1 = getAtmSample1();
        Atm atm2 = new Atm();
        assertThat(atm1).isNotEqualTo(atm2);

        atm2.setId(atm1.getId());
        assertThat(atm1).isEqualTo(atm2);

        atm2 = getAtmSample2();
        assertThat(atm1).isNotEqualTo(atm2);
    }

    @Test
    void supportPersonsTest() {
        Atm atm = getAtmRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        atm.addSupportPersons(employeeBack);
        assertThat(atm.getSupportPersons()).containsOnly(employeeBack);
        assertThat(employeeBack.getAtm()).isEqualTo(atm);

        atm.removeSupportPersons(employeeBack);
        assertThat(atm.getSupportPersons()).doesNotContain(employeeBack);
        assertThat(employeeBack.getAtm()).isNull();

        atm.supportPersons(new HashSet<>(Set.of(employeeBack)));
        assertThat(atm.getSupportPersons()).containsOnly(employeeBack);
        assertThat(employeeBack.getAtm()).isEqualTo(atm);

        atm.setSupportPersons(new HashSet<>());
        assertThat(atm.getSupportPersons()).doesNotContain(employeeBack);
        assertThat(employeeBack.getAtm()).isNull();
    }

    @Test
    void addressTest() {
        Atm atm = getAtmRandomSampleGenerator();
        Address addressBack = getAddressRandomSampleGenerator();

        atm.setAddress(addressBack);
        assertThat(atm.getAddress()).isEqualTo(addressBack);

        atm.address(null);
        assertThat(atm.getAddress()).isNull();
    }
}
