package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.ContactDetailsTestSamples.*;
import static com.mycompany.myapp.domain.OfficeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ContactDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactDetails.class);
        ContactDetails contactDetails1 = getContactDetailsSample1();
        ContactDetails contactDetails2 = new ContactDetails();
        assertThat(contactDetails1).isNotEqualTo(contactDetails2);

        contactDetails2.setId(contactDetails1.getId());
        assertThat(contactDetails1).isEqualTo(contactDetails2);

        contactDetails2 = getContactDetailsSample2();
        assertThat(contactDetails1).isNotEqualTo(contactDetails2);
    }

    @Test
    void officeTest() {
        ContactDetails contactDetails = getContactDetailsRandomSampleGenerator();
        Office officeBack = getOfficeRandomSampleGenerator();

        contactDetails.setOffice(officeBack);
        assertThat(contactDetails.getOffice()).isEqualTo(officeBack);

        contactDetails.office(null);
        assertThat(contactDetails.getOffice()).isNull();
    }
}
