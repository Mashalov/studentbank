package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class CardDetailsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static CardDetails getCardDetailsSample1() {
        return new CardDetails().id(1L).cardNumber(1L).cvv(1L).pin(1L);
    }

    public static CardDetails getCardDetailsSample2() {
        return new CardDetails().id(2L).cardNumber(2L).cvv(2L).pin(2L);
    }

    public static CardDetails getCardDetailsRandomSampleGenerator() {
        return new CardDetails()
            .id(longCount.incrementAndGet())
            .cardNumber(longCount.incrementAndGet())
            .cvv(longCount.incrementAndGet())
            .pin(longCount.incrementAndGet());
    }
}
