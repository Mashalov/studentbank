package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static com.mycompany.myapp.domain.EmployeesAssetsTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmployeesAssetsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeesAssets.class);
        EmployeesAssets employeesAssets1 = getEmployeesAssetsSample1();
        EmployeesAssets employeesAssets2 = new EmployeesAssets();
        assertThat(employeesAssets1).isNotEqualTo(employeesAssets2);

        employeesAssets2.setId(employeesAssets1.getId());
        assertThat(employeesAssets1).isEqualTo(employeesAssets2);

        employeesAssets2 = getEmployeesAssetsSample2();
        assertThat(employeesAssets1).isNotEqualTo(employeesAssets2);
    }

    @Test
    void assignedByTest() {
        EmployeesAssets employeesAssets = getEmployeesAssetsRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        employeesAssets.setAssignedBy(employeeBack);
        assertThat(employeesAssets.getAssignedBy()).isEqualTo(employeeBack);

        employeesAssets.assignedBy(null);
        assertThat(employeesAssets.getAssignedBy()).isNull();
    }

    @Test
    void recieversProtocolTest() {
        EmployeesAssets employeesAssets = getEmployeesAssetsRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        employeesAssets.setRecieversProtocol(documentBack);
        assertThat(employeesAssets.getRecieversProtocol()).isEqualTo(documentBack);

        employeesAssets.recieversProtocol(null);
        assertThat(employeesAssets.getRecieversProtocol()).isNull();
    }

    @Test
    void employeeTest() {
        EmployeesAssets employeesAssets = getEmployeesAssetsRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        employeesAssets.setEmployee(employeeBack);
        assertThat(employeesAssets.getEmployee()).isEqualTo(employeeBack);

        employeesAssets.employee(null);
        assertThat(employeesAssets.getEmployee()).isNull();
    }
}
