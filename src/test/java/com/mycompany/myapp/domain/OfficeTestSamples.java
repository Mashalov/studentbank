package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class OfficeTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Office getOfficeSample1() {
        return new Office().id(1L).name("name1").openningTime("openningTime1").closingTime("closingTime1");
    }

    public static Office getOfficeSample2() {
        return new Office().id(2L).name("name2").openningTime("openningTime2").closingTime("closingTime2");
    }

    public static Office getOfficeRandomSampleGenerator() {
        return new Office()
            .id(longCount.incrementAndGet())
            .name(UUID.randomUUID().toString())
            .openningTime(UUID.randomUUID().toString())
            .closingTime(UUID.randomUUID().toString());
    }
}
