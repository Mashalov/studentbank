package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class ExchangeTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Exchange getExchangeSample1() {
        return new Exchange().id(1L).exchangeRate(1L);
    }

    public static Exchange getExchangeSample2() {
        return new Exchange().id(2L).exchangeRate(2L);
    }

    public static Exchange getExchangeRandomSampleGenerator() {
        return new Exchange().id(longCount.incrementAndGet()).exchangeRate(longCount.incrementAndGet());
    }
}
