package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.DocumentCategoryTestSamples.*;
import static com.mycompany.myapp.domain.DocumentCategoryTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DocumentCategoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DocumentCategory.class);
        DocumentCategory documentCategory1 = getDocumentCategorySample1();
        DocumentCategory documentCategory2 = new DocumentCategory();
        assertThat(documentCategory1).isNotEqualTo(documentCategory2);

        documentCategory2.setId(documentCategory1.getId());
        assertThat(documentCategory1).isEqualTo(documentCategory2);

        documentCategory2 = getDocumentCategorySample2();
        assertThat(documentCategory1).isNotEqualTo(documentCategory2);
    }

    @Test
    void documentCategoryTest() {
        DocumentCategory documentCategory = getDocumentCategoryRandomSampleGenerator();
        DocumentCategory documentCategoryBack = getDocumentCategoryRandomSampleGenerator();

        documentCategory.setDocumentCategory(documentCategoryBack);
        assertThat(documentCategory.getDocumentCategory()).isEqualTo(documentCategoryBack);

        documentCategory.documentCategory(null);
        assertThat(documentCategory.getDocumentCategory()).isNull();
    }
}
