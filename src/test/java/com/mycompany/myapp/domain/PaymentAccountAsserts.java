package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AssertUtils.bigDecimalCompareTo;
import static org.assertj.core.api.Assertions.assertThat;

public class PaymentAccountAsserts {

    /**
     * Asserts that the entity has all properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertPaymentAccountAllPropertiesEquals(PaymentAccount expected, PaymentAccount actual) {
        assertPaymentAccountAutoGeneratedPropertiesEquals(expected, actual);
        assertPaymentAccountAllUpdatablePropertiesEquals(expected, actual);
    }

    /**
     * Asserts that the entity has all updatable properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertPaymentAccountAllUpdatablePropertiesEquals(PaymentAccount expected, PaymentAccount actual) {
        assertPaymentAccountUpdatableFieldsEquals(expected, actual);
        assertPaymentAccountUpdatableRelationshipsEquals(expected, actual);
    }

    /**
     * Asserts that the entity has all the auto generated properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertPaymentAccountAutoGeneratedPropertiesEquals(PaymentAccount expected, PaymentAccount actual) {
        assertThat(expected)
            .as("Verify PaymentAccount auto generated properties")
            .satisfies(e -> assertThat(e.getId()).as("check id").isEqualTo(actual.getId()));
    }

    /**
     * Asserts that the entity has all the updatable fields set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertPaymentAccountUpdatableFieldsEquals(PaymentAccount expected, PaymentAccount actual) {
        assertThat(expected)
            .as("Verify PaymentAccount relevant properties")
            .satisfies(e -> assertThat(e.getPaymentAccountType()).as("check paymentAccountType").isEqualTo(actual.getPaymentAccountType()))
            .satisfies(e -> assertThat(e.getIban()).as("check iban").isEqualTo(actual.getIban()))
            .satisfies(e -> assertThat(e.getAmount()).as("check amount").usingComparator(bigDecimalCompareTo).isEqualTo(actual.getAmount()))
            .satisfies(e -> assertThat(e.getCurrency()).as("check currency").isEqualTo(actual.getCurrency()))
            .satisfies(e -> assertThat(e.getActive()).as("check active").isEqualTo(actual.getActive()))
            .satisfies(e -> assertThat(e.getDeleted()).as("check deleted").isEqualTo(actual.getDeleted()));
    }

    /**
     * Asserts that the entity has all the updatable relationships set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertPaymentAccountUpdatableRelationshipsEquals(PaymentAccount expected, PaymentAccount actual) {
        assertThat(expected)
            .as("Verify PaymentAccount relationships")
            .satisfies(e -> assertThat(e.getContract()).as("check contract").isEqualTo(actual.getContract()))
            .satisfies(e -> assertThat(e.getCustomer()).as("check customer").isEqualTo(actual.getCustomer()));
    }
}
