package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AddressTestSamples.*;
import static com.mycompany.myapp.domain.BankDetailsTestSamples.*;
import static com.mycompany.myapp.domain.ContactDetailsTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static com.mycompany.myapp.domain.OfficeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class OfficeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Office.class);
        Office office1 = getOfficeSample1();
        Office office2 = new Office();
        assertThat(office1).isNotEqualTo(office2);

        office2.setId(office1.getId());
        assertThat(office1).isEqualTo(office2);

        office2 = getOfficeSample2();
        assertThat(office1).isNotEqualTo(office2);
    }

    @Test
    void contactDetailsTest() {
        Office office = getOfficeRandomSampleGenerator();
        ContactDetails contactDetailsBack = getContactDetailsRandomSampleGenerator();

        office.addContactDetails(contactDetailsBack);
        assertThat(office.getContactDetails()).containsOnly(contactDetailsBack);
        assertThat(contactDetailsBack.getOffice()).isEqualTo(office);

        office.removeContactDetails(contactDetailsBack);
        assertThat(office.getContactDetails()).doesNotContain(contactDetailsBack);
        assertThat(contactDetailsBack.getOffice()).isNull();

        office.contactDetails(new HashSet<>(Set.of(contactDetailsBack)));
        assertThat(office.getContactDetails()).containsOnly(contactDetailsBack);
        assertThat(contactDetailsBack.getOffice()).isEqualTo(office);

        office.setContactDetails(new HashSet<>());
        assertThat(office.getContactDetails()).doesNotContain(contactDetailsBack);
        assertThat(contactDetailsBack.getOffice()).isNull();
    }

    @Test
    void employeesTest() {
        Office office = getOfficeRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        office.addEmployees(employeeBack);
        assertThat(office.getEmployees()).containsOnly(employeeBack);
        assertThat(employeeBack.getOffice()).isEqualTo(office);

        office.removeEmployees(employeeBack);
        assertThat(office.getEmployees()).doesNotContain(employeeBack);
        assertThat(employeeBack.getOffice()).isNull();

        office.employees(new HashSet<>(Set.of(employeeBack)));
        assertThat(office.getEmployees()).containsOnly(employeeBack);
        assertThat(employeeBack.getOffice()).isEqualTo(office);

        office.setEmployees(new HashSet<>());
        assertThat(office.getEmployees()).doesNotContain(employeeBack);
        assertThat(employeeBack.getOffice()).isNull();
    }

    @Test
    void addressTest() {
        Office office = getOfficeRandomSampleGenerator();
        Address addressBack = getAddressRandomSampleGenerator();

        office.setAddress(addressBack);
        assertThat(office.getAddress()).isEqualTo(addressBack);

        office.address(null);
        assertThat(office.getAddress()).isNull();
    }

    @Test
    void bankDetailsTest() {
        Office office = getOfficeRandomSampleGenerator();
        BankDetails bankDetailsBack = getBankDetailsRandomSampleGenerator();

        office.setBankDetails(bankDetailsBack);
        assertThat(office.getBankDetails()).isEqualTo(bankDetailsBack);

        office.bankDetails(null);
        assertThat(office.getBankDetails()).isNull();
    }
}
