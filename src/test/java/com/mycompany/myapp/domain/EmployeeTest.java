package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AtmTestSamples.*;
import static com.mycompany.myapp.domain.ContactDetailsTestSamples.*;
import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static com.mycompany.myapp.domain.EmployeesAssetsTestSamples.*;
import static com.mycompany.myapp.domain.JobPositionTestSamples.*;
import static com.mycompany.myapp.domain.OfficeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class EmployeeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Employee.class);
        Employee employee1 = getEmployeeSample1();
        Employee employee2 = new Employee();
        assertThat(employee1).isNotEqualTo(employee2);

        employee2.setId(employee1.getId());
        assertThat(employee1).isEqualTo(employee2);

        employee2 = getEmployeeSample2();
        assertThat(employee1).isNotEqualTo(employee2);
    }

    @Test
    void employeesAssetsTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        EmployeesAssets employeesAssetsBack = getEmployeesAssetsRandomSampleGenerator();

        employee.addEmployeesAssets(employeesAssetsBack);
        assertThat(employee.getEmployeesAssets()).containsOnly(employeesAssetsBack);
        assertThat(employeesAssetsBack.getEmployee()).isEqualTo(employee);

        employee.removeEmployeesAssets(employeesAssetsBack);
        assertThat(employee.getEmployeesAssets()).doesNotContain(employeesAssetsBack);
        assertThat(employeesAssetsBack.getEmployee()).isNull();

        employee.employeesAssets(new HashSet<>(Set.of(employeesAssetsBack)));
        assertThat(employee.getEmployeesAssets()).containsOnly(employeesAssetsBack);
        assertThat(employeesAssetsBack.getEmployee()).isEqualTo(employee);

        employee.setEmployeesAssets(new HashSet<>());
        assertThat(employee.getEmployeesAssets()).doesNotContain(employeesAssetsBack);
        assertThat(employeesAssetsBack.getEmployee()).isNull();
    }

    @Test
    void contactDetailsTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        ContactDetails contactDetailsBack = getContactDetailsRandomSampleGenerator();

        employee.setContactDetails(contactDetailsBack);
        assertThat(employee.getContactDetails()).isEqualTo(contactDetailsBack);

        employee.contactDetails(null);
        assertThat(employee.getContactDetails()).isNull();
    }

    @Test
    void jobPositionTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        JobPosition jobPositionBack = getJobPositionRandomSampleGenerator();

        employee.setJobPosition(jobPositionBack);
        assertThat(employee.getJobPosition()).isEqualTo(jobPositionBack);

        employee.jobPosition(null);
        assertThat(employee.getJobPosition()).isNull();
    }

    @Test
    void employeeContractTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        employee.setEmployeeContract(documentBack);
        assertThat(employee.getEmployeeContract()).isEqualTo(documentBack);

        employee.employeeContract(null);
        assertThat(employee.getEmployeeContract()).isNull();
    }

    @Test
    void managerTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        employee.setManager(employeeBack);
        assertThat(employee.getManager()).isEqualTo(employeeBack);

        employee.manager(null);
        assertThat(employee.getManager()).isNull();
    }

    @Test
    void atmTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        Atm atmBack = getAtmRandomSampleGenerator();

        employee.setAtm(atmBack);
        assertThat(employee.getAtm()).isEqualTo(atmBack);

        employee.atm(null);
        assertThat(employee.getAtm()).isNull();
    }

    @Test
    void officeTest() {
        Employee employee = getEmployeeRandomSampleGenerator();
        Office officeBack = getOfficeRandomSampleGenerator();

        employee.setOffice(officeBack);
        assertThat(employee.getOffice()).isEqualTo(officeBack);

        employee.office(null);
        assertThat(employee.getOffice()).isNull();
    }
}
