package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.BankDetailsTestSamples.*;
import static com.mycompany.myapp.domain.OfficeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class BankDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankDetails.class);
        BankDetails bankDetails1 = getBankDetailsSample1();
        BankDetails bankDetails2 = new BankDetails();
        assertThat(bankDetails1).isNotEqualTo(bankDetails2);

        bankDetails2.setId(bankDetails1.getId());
        assertThat(bankDetails1).isEqualTo(bankDetails2);

        bankDetails2 = getBankDetailsSample2();
        assertThat(bankDetails1).isNotEqualTo(bankDetails2);
    }

    @Test
    void officesTest() {
        BankDetails bankDetails = getBankDetailsRandomSampleGenerator();
        Office officeBack = getOfficeRandomSampleGenerator();

        bankDetails.addOffices(officeBack);
        assertThat(bankDetails.getOffices()).containsOnly(officeBack);
        assertThat(officeBack.getBankDetails()).isEqualTo(bankDetails);

        bankDetails.removeOffices(officeBack);
        assertThat(bankDetails.getOffices()).doesNotContain(officeBack);
        assertThat(officeBack.getBankDetails()).isNull();

        bankDetails.offices(new HashSet<>(Set.of(officeBack)));
        assertThat(bankDetails.getOffices()).containsOnly(officeBack);
        assertThat(officeBack.getBankDetails()).isEqualTo(bankDetails);

        bankDetails.setOffices(new HashSet<>());
        assertThat(bankDetails.getOffices()).doesNotContain(officeBack);
        assertThat(officeBack.getBankDetails()).isNull();
    }
}
