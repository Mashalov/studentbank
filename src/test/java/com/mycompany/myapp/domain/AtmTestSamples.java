package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AtmTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Atm getAtmSample1() {
        return new Atm().id(1L).name("name1");
    }

    public static Atm getAtmSample2() {
        return new Atm().id(2L).name("name2");
    }

    public static Atm getAtmRandomSampleGenerator() {
        return new Atm().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString());
    }
}
