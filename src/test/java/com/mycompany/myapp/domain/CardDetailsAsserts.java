package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.AssertUtils.zonedDataTimeSameInstant;
import static org.assertj.core.api.Assertions.assertThat;

public class CardDetailsAsserts {

    /**
     * Asserts that the entity has all properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertCardDetailsAllPropertiesEquals(CardDetails expected, CardDetails actual) {
        assertCardDetailsAutoGeneratedPropertiesEquals(expected, actual);
        assertCardDetailsAllUpdatablePropertiesEquals(expected, actual);
    }

    /**
     * Asserts that the entity has all updatable properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertCardDetailsAllUpdatablePropertiesEquals(CardDetails expected, CardDetails actual) {
        assertCardDetailsUpdatableFieldsEquals(expected, actual);
        assertCardDetailsUpdatableRelationshipsEquals(expected, actual);
    }

    /**
     * Asserts that the entity has all the auto generated properties (fields/relationships) set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertCardDetailsAutoGeneratedPropertiesEquals(CardDetails expected, CardDetails actual) {
        assertThat(expected)
            .as("Verify CardDetails auto generated properties")
            .satisfies(e -> assertThat(e.getId()).as("check id").isEqualTo(actual.getId()));
    }

    /**
     * Asserts that the entity has all the updatable fields set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertCardDetailsUpdatableFieldsEquals(CardDetails expected, CardDetails actual) {
        assertThat(expected)
            .as("Verify CardDetails relevant properties")
            .satisfies(e -> assertThat(e.getCardNumber()).as("check cardNumber").isEqualTo(actual.getCardNumber()))
            .satisfies(e -> assertThat(e.getCardType()).as("check cardType").isEqualTo(actual.getCardType()))
            .satisfies(e -> assertThat(e.getCardProvider()).as("check cardProvider").isEqualTo(actual.getCardProvider()))
            .satisfies(
                e ->
                    assertThat(e.getIssueDate())
                        .as("check issueDate")
                        .usingComparator(zonedDataTimeSameInstant)
                        .isEqualTo(actual.getIssueDate())
            )
            .satisfies(
                e ->
                    assertThat(e.getExpiryDate())
                        .as("check expiryDate")
                        .usingComparator(zonedDataTimeSameInstant)
                        .isEqualTo(actual.getExpiryDate())
            )
            .satisfies(e -> assertThat(e.getCvv()).as("check cvv").isEqualTo(actual.getCvv()))
            .satisfies(e -> assertThat(e.getIsActive()).as("check isActive").isEqualTo(actual.getIsActive()))
            .satisfies(e -> assertThat(e.getIsBlocked()).as("check isBlocked").isEqualTo(actual.getIsBlocked()))
            .satisfies(e -> assertThat(e.getPin()).as("check pin").isEqualTo(actual.getPin()));
    }

    /**
     * Asserts that the entity has all the updatable relationships set.
     *
     * @param expected the expected entity
     * @param actual the actual entity
     */
    public static void assertCardDetailsUpdatableRelationshipsEquals(CardDetails expected, CardDetails actual) {
        assertThat(expected)
            .as("Verify CardDetails relationships")
            .satisfies(e -> assertThat(e.getCustomer()).as("check customer").isEqualTo(actual.getCustomer()))
            .satisfies(e -> assertThat(e.getPaymentAccount()).as("check paymentAccount").isEqualTo(actual.getPaymentAccount()));
    }
}
