package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.ApplicationTestSamples.*;
import static com.mycompany.myapp.domain.CustomerTestSamples.*;
import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ApplicationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Application.class);
        Application application1 = getApplicationSample1();
        Application application2 = new Application();
        assertThat(application1).isNotEqualTo(application2);

        application2.setId(application1.getId());
        assertThat(application1).isEqualTo(application2);

        application2 = getApplicationSample2();
        assertThat(application1).isNotEqualTo(application2);
    }

    @Test
    void documentTest() {
        Application application = getApplicationRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        application.addDocument(documentBack);
        assertThat(application.getDocuments()).containsOnly(documentBack);
        assertThat(documentBack.getApplication()).isEqualTo(application);

        application.removeDocument(documentBack);
        assertThat(application.getDocuments()).doesNotContain(documentBack);
        assertThat(documentBack.getApplication()).isNull();

        application.documents(new HashSet<>(Set.of(documentBack)));
        assertThat(application.getDocuments()).containsOnly(documentBack);
        assertThat(documentBack.getApplication()).isEqualTo(application);

        application.setDocuments(new HashSet<>());
        assertThat(application.getDocuments()).doesNotContain(documentBack);
        assertThat(documentBack.getApplication()).isNull();
    }

    @Test
    void customerTest() {
        Application application = getApplicationRandomSampleGenerator();
        Customer customerBack = getCustomerRandomSampleGenerator();

        application.setCustomer(customerBack);
        assertThat(application.getCustomer()).isEqualTo(customerBack);

        application.customer(null);
        assertThat(application.getCustomer()).isNull();
    }
}
