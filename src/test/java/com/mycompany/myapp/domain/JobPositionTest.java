package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.JobPositionTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class JobPositionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobPosition.class);
        JobPosition jobPosition1 = getJobPositionSample1();
        JobPosition jobPosition2 = new JobPosition();
        assertThat(jobPosition1).isNotEqualTo(jobPosition2);

        jobPosition2.setId(jobPosition1.getId());
        assertThat(jobPosition1).isEqualTo(jobPosition2);

        jobPosition2 = getJobPositionSample2();
        assertThat(jobPosition1).isNotEqualTo(jobPosition2);
    }
}
