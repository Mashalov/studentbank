package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class BankDetailsTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static BankDetails getBankDetailsSample1() {
        return new BankDetails().id(1L).name("name1").ceo("ceo1").mainWebsite("mainWebsite1");
    }

    public static BankDetails getBankDetailsSample2() {
        return new BankDetails().id(2L).name("name2").ceo("ceo2").mainWebsite("mainWebsite2");
    }

    public static BankDetails getBankDetailsRandomSampleGenerator() {
        return new BankDetails()
            .id(longCount.incrementAndGet())
            .name(UUID.randomUUID().toString())
            .ceo(UUID.randomUUID().toString())
            .mainWebsite(UUID.randomUUID().toString());
    }
}
