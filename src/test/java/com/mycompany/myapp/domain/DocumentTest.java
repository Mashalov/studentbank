package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.ApplicationTestSamples.*;
import static com.mycompany.myapp.domain.CustomerTestSamples.*;
import static com.mycompany.myapp.domain.DocumentCategoryTestSamples.*;
import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DocumentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Document.class);
        Document document1 = getDocumentSample1();
        Document document2 = new Document();
        assertThat(document1).isNotEqualTo(document2);

        document2.setId(document1.getId());
        assertThat(document1).isEqualTo(document2);

        document2 = getDocumentSample2();
        assertThat(document1).isNotEqualTo(document2);
    }

    @Test
    void documentCategoryTest() {
        Document document = getDocumentRandomSampleGenerator();
        DocumentCategory documentCategoryBack = getDocumentCategoryRandomSampleGenerator();

        document.setDocumentCategory(documentCategoryBack);
        assertThat(document.getDocumentCategory()).isEqualTo(documentCategoryBack);

        document.documentCategory(null);
        assertThat(document.getDocumentCategory()).isNull();
    }

    @Test
    void applicationTest() {
        Document document = getDocumentRandomSampleGenerator();
        Application applicationBack = getApplicationRandomSampleGenerator();

        document.setApplication(applicationBack);
        assertThat(document.getApplication()).isEqualTo(applicationBack);

        document.application(null);
        assertThat(document.getApplication()).isNull();
    }

    @Test
    void customerTest() {
        Document document = getDocumentRandomSampleGenerator();
        Customer customerBack = getCustomerRandomSampleGenerator();

        document.setCustomer(customerBack);
        assertThat(document.getCustomer()).isEqualTo(customerBack);

        document.customer(null);
        assertThat(document.getCustomer()).isNull();
    }
}
