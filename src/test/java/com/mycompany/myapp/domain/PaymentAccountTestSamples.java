package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class PaymentAccountTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static PaymentAccount getPaymentAccountSample1() {
        return new PaymentAccount().id(1L).iban("iban1");
    }

    public static PaymentAccount getPaymentAccountSample2() {
        return new PaymentAccount().id(2L).iban("iban2");
    }

    public static PaymentAccount getPaymentAccountRandomSampleGenerator() {
        return new PaymentAccount().id(longCount.incrementAndGet()).iban(UUID.randomUUID().toString());
    }
}
