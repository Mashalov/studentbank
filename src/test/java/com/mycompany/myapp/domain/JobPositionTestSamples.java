package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class JobPositionTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static JobPosition getJobPositionSample1() {
        return new JobPosition().id(1L).name("name1").company("company1");
    }

    public static JobPosition getJobPositionSample2() {
        return new JobPosition().id(2L).name("name2").company("company2");
    }

    public static JobPosition getJobPositionRandomSampleGenerator() {
        return new JobPosition().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString()).company(UUID.randomUUID().toString());
    }
}
