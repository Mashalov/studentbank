package com.mycompany.myapp.domain;

import static com.mycompany.myapp.domain.DocumentTestSamples.*;
import static com.mycompany.myapp.domain.EmployeeTestSamples.*;
import static com.mycompany.myapp.domain.ExchangeTestSamples.*;
import static com.mycompany.myapp.domain.OfficeTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ExchangeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Exchange.class);
        Exchange exchange1 = getExchangeSample1();
        Exchange exchange2 = new Exchange();
        assertThat(exchange1).isNotEqualTo(exchange2);

        exchange2.setId(exchange1.getId());
        assertThat(exchange1).isEqualTo(exchange2);

        exchange2 = getExchangeSample2();
        assertThat(exchange1).isNotEqualTo(exchange2);
    }

    @Test
    void employeeTest() {
        Exchange exchange = getExchangeRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        exchange.setEmployee(employeeBack);
        assertThat(exchange.getEmployee()).isEqualTo(employeeBack);

        exchange.employee(null);
        assertThat(exchange.getEmployee()).isNull();
    }

    @Test
    void officeTest() {
        Exchange exchange = getExchangeRandomSampleGenerator();
        Office officeBack = getOfficeRandomSampleGenerator();

        exchange.setOffice(officeBack);
        assertThat(exchange.getOffice()).isEqualTo(officeBack);

        exchange.office(null);
        assertThat(exchange.getOffice()).isNull();
    }

    @Test
    void documentTest() {
        Exchange exchange = getExchangeRandomSampleGenerator();
        Document documentBack = getDocumentRandomSampleGenerator();

        exchange.setDocument(documentBack);
        assertThat(exchange.getDocument()).isEqualTo(documentBack);

        exchange.document(null);
        assertThat(exchange.getDocument()).isNull();
    }
}
