package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AccountTransactionTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static AccountTransaction getAccountTransactionSample1() {
        return new AccountTransaction().id(1L).externalId("externalId1");
    }

    public static AccountTransaction getAccountTransactionSample2() {
        return new AccountTransaction().id(2L).externalId("externalId2");
    }

    public static AccountTransaction getAccountTransactionRandomSampleGenerator() {
        return new AccountTransaction().id(longCount.incrementAndGet()).externalId(UUID.randomUUID().toString());
    }
}
