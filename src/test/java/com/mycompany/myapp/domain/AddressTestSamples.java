package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class AddressTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Address getAddressSample1() {
        return new Address().id(1L).region("region1").city("city1").street("street1").zipCode(1L);
    }

    public static Address getAddressSample2() {
        return new Address().id(2L).region("region2").city("city2").street("street2").zipCode(2L);
    }

    public static Address getAddressRandomSampleGenerator() {
        return new Address()
            .id(longCount.incrementAndGet())
            .region(UUID.randomUUID().toString())
            .city(UUID.randomUUID().toString())
            .street(UUID.randomUUID().toString())
            .zipCode(longCount.incrementAndGet());
    }
}
