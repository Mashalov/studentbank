package com.mycompany.myapp.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class DocumentCategoryTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static DocumentCategory getDocumentCategorySample1() {
        return new DocumentCategory().id(1L).name("name1");
    }

    public static DocumentCategory getDocumentCategorySample2() {
        return new DocumentCategory().id(2L).name("name2");
    }

    public static DocumentCategory getDocumentCategoryRandomSampleGenerator() {
        return new DocumentCategory().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString());
    }
}
