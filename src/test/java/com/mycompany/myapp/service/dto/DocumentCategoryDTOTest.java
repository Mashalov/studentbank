package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DocumentCategoryDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DocumentCategoryDTO.class);
        DocumentCategoryDTO documentCategoryDTO1 = new DocumentCategoryDTO();
        documentCategoryDTO1.setId(1L);
        DocumentCategoryDTO documentCategoryDTO2 = new DocumentCategoryDTO();
        assertThat(documentCategoryDTO1).isNotEqualTo(documentCategoryDTO2);
        documentCategoryDTO2.setId(documentCategoryDTO1.getId());
        assertThat(documentCategoryDTO1).isEqualTo(documentCategoryDTO2);
        documentCategoryDTO2.setId(2L);
        assertThat(documentCategoryDTO1).isNotEqualTo(documentCategoryDTO2);
        documentCategoryDTO1.setId(null);
        assertThat(documentCategoryDTO1).isNotEqualTo(documentCategoryDTO2);
    }
}
