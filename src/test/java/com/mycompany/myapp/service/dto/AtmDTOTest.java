package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AtmDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtmDTO.class);
        AtmDTO atmDTO1 = new AtmDTO();
        atmDTO1.setId(1L);
        AtmDTO atmDTO2 = new AtmDTO();
        assertThat(atmDTO1).isNotEqualTo(atmDTO2);
        atmDTO2.setId(atmDTO1.getId());
        assertThat(atmDTO1).isEqualTo(atmDTO2);
        atmDTO2.setId(2L);
        assertThat(atmDTO1).isNotEqualTo(atmDTO2);
        atmDTO1.setId(null);
        assertThat(atmDTO1).isNotEqualTo(atmDTO2);
    }
}
