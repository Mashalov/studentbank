package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CardDetailsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CardDetailsDTO.class);
        CardDetailsDTO cardDetailsDTO1 = new CardDetailsDTO();
        cardDetailsDTO1.setId(1L);
        CardDetailsDTO cardDetailsDTO2 = new CardDetailsDTO();
        assertThat(cardDetailsDTO1).isNotEqualTo(cardDetailsDTO2);
        cardDetailsDTO2.setId(cardDetailsDTO1.getId());
        assertThat(cardDetailsDTO1).isEqualTo(cardDetailsDTO2);
        cardDetailsDTO2.setId(2L);
        assertThat(cardDetailsDTO1).isNotEqualTo(cardDetailsDTO2);
        cardDetailsDTO1.setId(null);
        assertThat(cardDetailsDTO1).isNotEqualTo(cardDetailsDTO2);
    }
}
