package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PaymentAccountDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PaymentAccountDTO.class);
        PaymentAccountDTO paymentAccountDTO1 = new PaymentAccountDTO();
        paymentAccountDTO1.setId(1L);
        PaymentAccountDTO paymentAccountDTO2 = new PaymentAccountDTO();
        assertThat(paymentAccountDTO1).isNotEqualTo(paymentAccountDTO2);
        paymentAccountDTO2.setId(paymentAccountDTO1.getId());
        assertThat(paymentAccountDTO1).isEqualTo(paymentAccountDTO2);
        paymentAccountDTO2.setId(2L);
        assertThat(paymentAccountDTO1).isNotEqualTo(paymentAccountDTO2);
        paymentAccountDTO1.setId(null);
        assertThat(paymentAccountDTO1).isNotEqualTo(paymentAccountDTO2);
    }
}
