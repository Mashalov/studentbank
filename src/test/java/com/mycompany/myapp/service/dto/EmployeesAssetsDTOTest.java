package com.mycompany.myapp.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmployeesAssetsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeesAssetsDTO.class);
        EmployeesAssetsDTO employeesAssetsDTO1 = new EmployeesAssetsDTO();
        employeesAssetsDTO1.setId(1L);
        EmployeesAssetsDTO employeesAssetsDTO2 = new EmployeesAssetsDTO();
        assertThat(employeesAssetsDTO1).isNotEqualTo(employeesAssetsDTO2);
        employeesAssetsDTO2.setId(employeesAssetsDTO1.getId());
        assertThat(employeesAssetsDTO1).isEqualTo(employeesAssetsDTO2);
        employeesAssetsDTO2.setId(2L);
        assertThat(employeesAssetsDTO1).isNotEqualTo(employeesAssetsDTO2);
        employeesAssetsDTO1.setId(null);
        assertThat(employeesAssetsDTO1).isNotEqualTo(employeesAssetsDTO2);
    }
}
