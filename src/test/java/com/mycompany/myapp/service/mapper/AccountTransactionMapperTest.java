package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.AccountTransactionAsserts.*;
import static com.mycompany.myapp.domain.AccountTransactionTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountTransactionMapperTest {

    private AccountTransactionMapper accountTransactionMapper;

    @BeforeEach
    void setUp() {
        accountTransactionMapper = new AccountTransactionMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getAccountTransactionSample1();
        var actual = accountTransactionMapper.toEntity(accountTransactionMapper.toDto(expected));
        assertAccountTransactionAllPropertiesEquals(expected, actual);
    }
}
