package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.JobPositionAsserts.*;
import static com.mycompany.myapp.domain.JobPositionTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JobPositionMapperTest {

    private JobPositionMapper jobPositionMapper;

    @BeforeEach
    void setUp() {
        jobPositionMapper = new JobPositionMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getJobPositionSample1();
        var actual = jobPositionMapper.toEntity(jobPositionMapper.toDto(expected));
        assertJobPositionAllPropertiesEquals(expected, actual);
    }
}
