package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.PaymentAccountAsserts.*;
import static com.mycompany.myapp.domain.PaymentAccountTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PaymentAccountMapperTest {

    private PaymentAccountMapper paymentAccountMapper;

    @BeforeEach
    void setUp() {
        paymentAccountMapper = new PaymentAccountMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getPaymentAccountSample1();
        var actual = paymentAccountMapper.toEntity(paymentAccountMapper.toDto(expected));
        assertPaymentAccountAllPropertiesEquals(expected, actual);
    }
}
