package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.CardDetailsAsserts.*;
import static com.mycompany.myapp.domain.CardDetailsTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CardDetailsMapperTest {

    private CardDetailsMapper cardDetailsMapper;

    @BeforeEach
    void setUp() {
        cardDetailsMapper = new CardDetailsMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getCardDetailsSample1();
        var actual = cardDetailsMapper.toEntity(cardDetailsMapper.toDto(expected));
        assertCardDetailsAllPropertiesEquals(expected, actual);
    }
}
