package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.AtmAsserts.*;
import static com.mycompany.myapp.domain.AtmTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AtmMapperTest {

    private AtmMapper atmMapper;

    @BeforeEach
    void setUp() {
        atmMapper = new AtmMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getAtmSample1();
        var actual = atmMapper.toEntity(atmMapper.toDto(expected));
        assertAtmAllPropertiesEquals(expected, actual);
    }
}
