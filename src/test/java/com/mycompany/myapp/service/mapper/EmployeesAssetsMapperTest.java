package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.EmployeesAssetsAsserts.*;
import static com.mycompany.myapp.domain.EmployeesAssetsTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmployeesAssetsMapperTest {

    private EmployeesAssetsMapper employeesAssetsMapper;

    @BeforeEach
    void setUp() {
        employeesAssetsMapper = new EmployeesAssetsMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getEmployeesAssetsSample1();
        var actual = employeesAssetsMapper.toEntity(employeesAssetsMapper.toDto(expected));
        assertEmployeesAssetsAllPropertiesEquals(expected, actual);
    }
}
