package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.DocumentCategoryAsserts.*;
import static com.mycompany.myapp.domain.DocumentCategoryTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DocumentCategoryMapperTest {

    private DocumentCategoryMapper documentCategoryMapper;

    @BeforeEach
    void setUp() {
        documentCategoryMapper = new DocumentCategoryMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getDocumentCategorySample1();
        var actual = documentCategoryMapper.toEntity(documentCategoryMapper.toDto(expected));
        assertDocumentCategoryAllPropertiesEquals(expected, actual);
    }
}
