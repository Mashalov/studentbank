package com.mycompany.myapp.service.mapper;

import static com.mycompany.myapp.domain.ContactDetailsAsserts.*;
import static com.mycompany.myapp.domain.ContactDetailsTestSamples.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ContactDetailsMapperTest {

    private ContactDetailsMapper contactDetailsMapper;

    @BeforeEach
    void setUp() {
        contactDetailsMapper = new ContactDetailsMapperImpl();
    }

    @Test
    void shouldConvertToDtoAndBack() {
        var expected = getContactDetailsSample1();
        var actual = contactDetailsMapper.toEntity(contactDetailsMapper.toDto(expected));
        assertContactDetailsAllPropertiesEquals(expected, actual);
    }
}
